/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.dsregistry;

import org.ws4d.java.communication.ResponseCallback;
import org.ws4d.java.dispatch.ServiceReferenceFactory;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.service.reference.ServiceReferenceInternal;

public class MDPWSServiceReferenceFactory extends ServiceReferenceFactory {

	@Override
	public ResponseCallback newResponseCallbackForServiceReference(
			ServiceReference servRefHandler) {
		if (servRefHandler instanceof ServiceReferenceInternal)
			return super.newResponseCallbackForServiceReference(servRefHandler);
		else
			return null;
	}

}
