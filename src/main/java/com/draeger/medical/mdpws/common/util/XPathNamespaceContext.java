/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.XMLConstants;

public class XPathNamespaceContext implements javax.xml.namespace.NamespaceContext
{
	
	protected HashMap<String, String> map=new HashMap<String, String>();

	public void addNamespace(String prefix, String uri)
	{
		map.put(prefix, uri);
	}
	
	@Override
	public String getNamespaceURI(String prefix) {
		
		String namespace=XMLConstants.NULL_NS_URI;
		if (prefix==null) return namespace;
		if (map.containsKey(prefix))
		{
			namespace=map.get(prefix);
		}
		
		return namespace;
	}

	@Override
	public String getPrefix(String namespaceURI) {
		if (namespaceURI==null) return null;

		Iterator<String> it=getPrefixes(namespaceURI);
		if (it!=null && it.hasNext())
			return it.next().toString();
		return null;
	}

	@Override
	public java.util.Iterator<String> getPrefixes(String namespaceURI) {
		if (namespaceURI==null) return null;
		ArrayList<String> list=new ArrayList<String>();
		Set<Entry<String,String>> entries= map.entrySet();
		for (Entry<String, String> entry : entries) {
			if (entry.getValue().equals(namespaceURI))
				list.add(entry.getKey());
		}
		return list.iterator();
	}
	
	public java.util.Iterator<String> getAllPrefixes()
	{
		return map.keySet().iterator();
	}
	
	public java.util.Iterator<String> getAllNamespaces()
	{
		return map.values().iterator();
	}
	
	public java.util.Iterator<Entry<String, String>> getAllEntries()
	{
		return map.entrySet().iterator();
	}

	/**
	 * @param context
	 */
	public void addNamespaceContext(XPathNamespaceContext context) 
	{
		Iterator<Entry<String,String>> entries=  context.getAllEntries();
		while(entries.hasNext())
		{
			Entry<String,String> entry=entries.next();
			addNamespace(entry.getKey(), entry.getValue());
		}
	}
	
}
