/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.common.util;

import org.ws4d.java.communication.CommunicationManager;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.communication.Discovery;
import org.ws4d.java.communication.ProtocolDomain;
import org.ws4d.java.communication.connection.ip.IPDetection;
import org.ws4d.java.communication.connection.ip.NetworkDetection;
import org.ws4d.java.communication.connection.ip.NetworkInterface;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;

public class DomainUtil {
	private static DomainUtil util=new DomainUtil();

	private DomainUtil()
	{
		//void
	}

	public static DomainUtil getInstance()
	{
		return util;
	}

	public String restrictDomains() {
		return restrictDomains(null);
	}

	public String restrictDomains(String defaultDomain) {
		if (defaultDomain==null)
			defaultDomain=System.getProperty("MDPWS.DefaultDomain");

		DataStructure domains = new ArrayList();
		for (Iterator it = CommunicationManagerRegistry.getLoadedManagers(); it.hasNext();) {
			CommunicationManager manager = (CommunicationManager) it.next();
			domains.addAll(manager.getAvailableDomains());
		}

		if (defaultDomain!=null)
		{
			for(Iterator it=domains.iterator();it.hasNext();)
			{	
				ProtocolDomain domain=(ProtocolDomain) it.next();

				String[] t=domain.getDomainIds();
				if (t!=null && t.length>0)
				{
					for (String dId : t) 
					{
						Log.debug("Checking Domain ID: "+dId);
						if (defaultDomain.equals(dId))
						{
							Log.debug("Added Domain" +domain);
							Discovery.addDefaultOutputDomain(domain);
						}
					}
				}


			}
		}
		return defaultDomain;
	}

	public String getAddress(String networkInterfaceName) 
	{
		NetworkDetection networkDetection= new IPDetection();

		for (Iterator it = networkDetection.getNetworkInterfaces(); it.hasNext();) {
			NetworkInterface ni = (NetworkInterface) it.next();
			String niName = ni.getName();
			if (networkInterfaceName==null || niName.equals(networkInterfaceName) )
			{
				Iterator it2=ni.getAddresses();
				if (it2.hasNext()) {
					return (String) it2.next();
				}
			}
		}

		return networkInterfaceName;
	}

}
