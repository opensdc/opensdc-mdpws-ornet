/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.common.util;

import java.io.IOException;

import org.ws4d.java.io.xml.AbstractElementParser;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.util.Log;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class XMLParserUtil {

	public static void ignoreEmbeddedTags(ElementParser parser) throws XmlPullParserException, IOException {
		int d = parser.getDepth();
		ignoreEmbeddedTags(parser, d);
	}

	public static void ignoreEmbeddedTags(AbstractElementParser parser, int d) throws XmlPullParserException, IOException {

		while (parser.getDepth() >= d) {
			//Are we at the level where we started? Then we are done.
			if (parser.getEventType() == XmlPullParser.END_TAG && parser.getDepth() == d) {
				break;
			}
			//Ignore the rest
			parser.next();
		}
	}

	public static void nextAndSkipRemainingOnLevel(AbstractElementParser parser,
			int depth) throws XmlPullParserException, IOException {
		parser.next();
		skipRemainingOnLevel(parser,depth);
	}

	public static boolean checkElementStart(AbstractElementParser parser, String namespace, String name) throws XmlPullParserException
	{
		return checkElementStart(parser, namespace, name, true);
	}

	public static boolean checkElementStart(AbstractElementParser parser, String namespace, String name, boolean ignoreLeadingText) throws XmlPullParserException
	{
		//Optimized SSch 2011-11-25 Allow to ignore leading whitespace text
		if (ignoreLeadingText)
		{
			try{
				while (parser.getEventType()==XmlPullParser.TEXT)
				{
					if (parser.isWhitespace())
						parser.next();
					else 
						return false;
				}
			}catch(IOException e){
				throw new XmlPullParserException(e.getMessage());
			}
		}
		return (parser.getEventType()==XmlPullParser.START_TAG && namespace.equals(parser.getNamespace()) && name.equals(parser.getName()));
	}

	public static void skipRemainingOnLevel(AbstractElementParser parser, int level) throws XmlPullParserException, IOException 
	{

		do 
		{
			if (parser.getEventType()==XmlPullParser.START_TAG) parser.next();
			
			if (parser.getDepth()-level==0 && parser.getEventType()==XmlPullParser.TEXT)
				parser.next();

			while(parser.getDepth()-level>0)
			{
				ignoreEmbeddedTags(parser, level-1);
			}
		}while((parser.getDepth()-level==0 && parser.getEventType()==XmlPullParser.TEXT));

		if (parser.getDepth()-level<=0 && parser.getEventType()==XmlPullParser.END_TAG)
		{
			if (parser.getDepth()-level==0)
				parser.next();
			else if (Log.isDebug())
				Log.debug("XMLParserUtil SkipRemainingOnLevel Failed: "+(parser.getDepth()-level)+" -> "+parser.getName());
		} else
		{
			Log.warn("Confusion while parsing "+level+" "+parser.getName()+" "+parser.getDepth()+" "+parser.getEventType());
			parser.nextTag();
			Log.warn("After Next Tag: Confusion while parsing "+level+" "+parser.getName()+" "+parser.getDepth()+" "+parser.getEventType());
		}

	}

	public static String getAttributeValue(AbstractElementParser parser, String attrName, String attrNamespace)
	{				
		int attributeCount = parser.getAttributeCount();
		for (int i = 0; i < attributeCount; i++) {
			String attributeNamespace = parser.getAttributeNamespace(i);
			String attributeName = parser.getAttributeName(i);
			if ("".equals(attributeNamespace)) {
				attributeNamespace = parser.getNamespace();
			}
			if (attrNamespace.equals(attributeNamespace)) {
				if (attrName.equals(attributeName)) {
					return parser.getAttributeValue(i);
				}
			}
		}
		return null;
	}
}
