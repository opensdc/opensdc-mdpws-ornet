/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package com.draeger.medical.mdpws.common.util;

import org.ws4d.java.constants.SOAPConstants;
import org.ws4d.java.constants.WSAConstants;
import org.ws4d.java.constants.WSAConstants2006;

/**
 *
 *
 */
public class SOAPNameSpaceContext extends XPathNamespaceContext {

	public SOAPNameSpaceContext() {
		super();
		addNamespace("s12", SOAPConstants.SOAP12_NAMESPACE_NAME);
		addNamespace("wsa",WSAConstants.WSA_NAMESPACE_NAME);
		addNamespace("wsa2004",WSAConstants2006.WSA_NAMESPACE_NAME);
	}

}
