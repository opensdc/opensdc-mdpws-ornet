/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.common.util;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.ws4d.java.constants.SOAPConstants;
import org.ws4d.java.constants.XMLConstants;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.HashMap.Entry;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.Set;
import org.ws4d.java.util.Log;

public class XPathInfo {
	private final String expression;
	private final XPathNamespaceContext context;
	private final XPath xPath;
	private QName returnType=XPathConstants.NODESET;
	private XPathExpression xPathExpression;
	
	public XPathInfo(String expression, XPathNamespaceContext context) {
		super();
		
		if (context==null)
		{
			context=new SOAPNameSpaceContext();
		}
		
		this.context = context;
		if (expression==null || expression.trim().length()==0)
		{
			String soapNS=SOAPConstants.SOAP12_NAMESPACE_NAME;
			String prefix= context.getPrefix(soapNS);
			if (prefix==null){
				prefix="s12"+System.currentTimeMillis();
				this.context.addNamespace(prefix, soapNS);
			}
			expression="//"+prefix+":Body[1]/*[1]";
		}
		this.expression = expression;
		xPath = XPathFactory.newInstance().newXPath();
		xPath.setNamespaceContext(getContext());
	}

	public String getExpression() {
		return expression;
	}

	public XPathNamespaceContext getContext() {
		return context;
	}

	@Override
	public String toString() {
		return "XPathInfo [expression=" + expression + ", context=" + context
				+ "]";
	}
	
	public synchronized Object evaluate(Document xmlDocument) throws XPathExpressionException
	{
		return evaluate(xmlDocument, returnType);
		
	}
	

	public QName getReturnType() {
		return returnType;
	}

	public void setReturnType(QName returnType) {
		this.returnType = returnType;
	}

	public synchronized Object evaluate(Document xmlDocument, QName returnType) throws XPathExpressionException
	{
		if (xPathExpression==null)
			xPathExpression = xPath.compile(expression);
		
		return xPathExpression.evaluate(xmlDocument, returnType);
		
	}
	
	public synchronized Object evaluate(Node node, QName returnType) throws XPathExpressionException
	{
		if (xPathExpression==null)
			xPathExpression = xPath.compile(expression);
		
		return xPathExpression.evaluate(node, returnType);
		
	}
	
	public XPath getXPath() {
		return xPath;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((expression == null) ? 0 : expression.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		XPathInfo other = (XPathInfo) obj;
		if (expression == null) {
			if (other.expression != null)
				return false;
		} else if (!expression.equals(other.expression))
			return false;
		return true;
	}







	private static XPath nSpaceXPath= XPathFactory.newInstance().newXPath();
	private static final XPathExpression nSpaceXPathExpression;
	
	static{
		XPathExpression temp=null;
		try {
			temp=nSpaceXPath.compile("//namespace::*");
		} catch (XPathExpressionException e) {
			Log.error(e);
		}finally{
			nSpaceXPathExpression=temp;
		}
	}
	
	public static HashMap getDeclaredNamesSpaces(Document xmlDocument) throws XPathExpressionException
	{
		HashMap retVal=new HashMap();

		
		NodeList nSet=(NodeList) nSpaceXPathExpression.evaluate(xmlDocument, XPathConstants.NODESET);
		for (int i=0;i<nSet.getLength();i++)
		{
			Node n=nSet.item(i);
			if (n instanceof Attr)
			{
				Attr attr=(Attr)n;
				retVal.put(attr.getValue(), attr.getName().replace(XMLConstants.XMLNS_NAMESPACE_PREFIX+":",""));
			}
		}
		
		return retVal;
	}
	
	public static XPathNamespaceContext createXPathSpaceContext(HashMap declaredNameSpaces) 
	{
		XPathNamespaceContext nsContext=new SOAPNameSpaceContext();
		if (declaredNameSpaces!=null && !declaredNameSpaces.isEmpty())
		{
			Set entries= declaredNameSpaces.entrySet();
			for (Iterator entIt=entries.iterator();entIt.hasNext();)
			{
				Entry entry= (Entry) entIt.next();
				nsContext.addNamespace((String)entry.getValue(), (String)entry.getKey());
			}
		}
		return nsContext;
	}
}
