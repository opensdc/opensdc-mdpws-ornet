/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.protocol.constants;

public interface WSSecurityPolicyConstants 
{
	public static final String WS_SECURITY_POLICY_NAMESPACE = "http://docs.oasis-open.org/ws-sx/ws-securitypolicy/200702";

	public static final String WSSP_SIGNEDPARTS = "SignedParts";	
	public static final String WSSP_SIGNEDELEMENTS = "SignedElements";
	//XXX Maybe add Attachment
	public static final String WSSP_REQPARTS="RequiredParts ";
	public static final String WSSP_REQELEMENTS="RequiredElements";
	public static final String WSSP_BODY = "Body";
	public static final String WSSP_HEADER = "Header";
	public static final String WSSP_XPATH="XPath";
	
}
