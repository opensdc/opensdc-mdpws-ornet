/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication;

import org.ws4d.java.communication.CommunicationManagerID;



public class MDPWSCommunicationManagerID implements CommunicationManagerID {

	public static final CommunicationManagerID	INSTANCE	= new MDPWSCommunicationManagerID();

	/*
	 * Disallow any instances from outside this class.
	 */
	private MDPWSCommunicationManagerID() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.CommunicationID#getId()
	 */
	@Override
	public String getId() {
		return "MDPWS";
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getId();
	}

	@Override
	public String getImplementationClassname() {
		return System.getProperty("MDPWSCommunicationManagerID.classname",MDPWSCommunicationManager.class.getName());
	}
}
