/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.util;

import org.ws4d.java.structures.HashMap;
import org.ws4d.java.util.Log;

public class MDPWSOutStreamfactory {
	private static final int maxByteArrays = 10;
	private final HashMap	BYTE_ARRAY_POOL	= new HashMap(maxByteArrays);
	private static MDPWSOutStreamfactory factory=new MDPWSOutStreamfactory();

	private MDPWSOutStreamfactory()
	{
		//void
	}

	public static MDPWSOutStreamfactory getInstance() {
		return factory;
	}

	public MDPWSOutputStream getOutputByteStream() {
		synchronized (BYTE_ARRAY_POOL) {
			Thread t = Thread.currentThread();
			MDPWSOutputStream bout = (MDPWSOutputStream) BYTE_ARRAY_POOL.get(t);
			if (bout == null) {
				bout = new MDPWSOutputStream();
				BYTE_ARRAY_POOL.put(t, bout);
			}
			return bout;
		}
	}

	public void releaseOutputStream(MDPWSOutputStream tmpOutput) 
	{

		synchronized (BYTE_ARRAY_POOL) {
			if (BYTE_ARRAY_POOL.size()>=maxByteArrays){
				if (tmpOutput!=null)
				{
					if (Log.isDebug())
						Log.debug("Clearing Byte Array pool"+Thread.currentThread().getName());
					
					BYTE_ARRAY_POOL.clear();
				}
			}
		}
	}

}
