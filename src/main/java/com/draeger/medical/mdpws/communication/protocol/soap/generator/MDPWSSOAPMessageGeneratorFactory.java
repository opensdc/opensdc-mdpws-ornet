/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.protocol.soap.generator;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.ws4d.java.communication.protocol.soap.generator.Message2SOAPGenerator;
import org.ws4d.java.communication.protocol.soap.generator.SOAP2MessageGenerator;
import org.ws4d.java.communication.protocol.soap.generator.SOAPMessageGeneratorFactory;
import org.ws4d.java.util.Log;

public class MDPWSSOAPMessageGeneratorFactory extends
		SOAPMessageGeneratorFactory {

	private static final int 			maxProvider=Integer.parseInt(System.getProperty("MPDWS.MDPWSSOAPMessageGeneratorFactory.PoolSize", "15"));
	private static final int 			maxBlockTime=Integer.parseInt(System.getProperty("MPDWS.MDPWSSOAPMessageGeneratorFactory.maxBlockTime", "5000"));
	private static final byte 			whenExhaustedAction=(byte)Integer.parseInt(System.getProperty("MPDWS.MDPWSSOAPMessageGeneratorFactory.whenExhaustedAction", String.valueOf(GenericObjectPool.WHEN_EXHAUSTED_GROW)));

	
	private static final ObjectPool m2sGeneratorPool=new GenericObjectPool(new Message2SOAPGeneratorFactory(),maxProvider,whenExhaustedAction,maxBlockTime);
	private static final ObjectPool s2mGeneratorPool=new GenericObjectPool(new SOAP2MessageGeneratorFactory(),maxProvider,whenExhaustedAction,maxBlockTime);
	
	
	
	@Override
	public synchronized SOAP2MessageGenerator getSOAP2MessageGeneratorForCurrentThread() 
	{
		return newSOAP2MessageGenerator();
	}

	@Override
	public synchronized Message2SOAPGenerator getMessage2SOAPGeneratorForCurrentThread() 
	{
		return newMessage2SOAPGenerator();
	}

	@Override
	protected Message2SOAPGenerator newMessage2SOAPGenerator() {
		MDPWSMessage2SOAPGenerator retVal=null;
		try{
			retVal=(MDPWSMessage2SOAPGenerator) m2sGeneratorPool.borrowObject();
		}catch(Exception e){
			Log.error(e);
		}
		return retVal;
	}

	@Override
	protected SOAP2MessageGenerator newSOAP2MessageGenerator() {
//		return new MDPWSSOAP2MessageGenerator();
		
		SOAP2MessageGenerator retVal=null;
		try{
			retVal=(SOAP2MessageGenerator) s2mGeneratorPool.borrowObject();
		}catch(Exception e){
			Log.error(e);
		}
		return retVal;
	}
		
	
	@Override
	public synchronized void releaseMessage2SOAPGenerator(
			Message2SOAPGenerator generator) {
		super.releaseMessage2SOAPGenerator(generator);
		try {
			m2sGeneratorPool.returnObject(generator);
		} catch (Exception e) {
			Log.error(e);
		}
	}

	@Override
	public synchronized void releaseSOAP2MessageGenerator(
			SOAP2MessageGenerator generator) {
		super.releaseSOAP2MessageGenerator(generator);
		try {
			s2mGeneratorPool.returnObject(generator);
		} catch (Exception e) {
			Log.error(e);
		}
	}


	private static class Message2SOAPGeneratorFactory extends BasePoolableObjectFactory{

		/* (non-Javadoc)
		 * @see org.apache.commons.pool.BasePoolableObjectFactory#makeObject()
		 */
		@Override
		public Object makeObject() throws Exception {
			return new MDPWSMessage2SOAPGenerator();
		}

		@Override
		public void passivateObject(Object obj) throws Exception 
		{
			if (obj instanceof MDPWSMessage2SOAPGenerator)
				((MDPWSMessage2SOAPGenerator)obj).reset();
		}
	}
	
	private static class SOAP2MessageGeneratorFactory extends BasePoolableObjectFactory{

		/* (non-Javadoc)
		 * @see org.apache.commons.pool.BasePoolableObjectFactory#makeObject()
		 */
		@Override
		public Object makeObject() throws Exception {
			return new MDPWSSOAP2MessageGenerator();
		}

		@Override
		public void passivateObject(Object obj) throws Exception 
		{
			if (obj instanceof MDPWSSOAP2MessageGenerator)
				((MDPWSSOAP2MessageGenerator)obj).reset();
		}
	}

}
