/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package com.draeger.medical.mdpws.communication.protocol.soap.wsdl;

import java.io.IOException;

import org.ws4d.java.constants.WSDLConstants;
import org.ws4d.java.constants.WSPConstants;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;
import org.ws4d.java.wsdl.UnsupportedBindingException;
import org.ws4d.java.wsdl.WSDL;
import org.ws4d.java.wsdl.WSDLBinding;
import org.ws4d.java.wsdl.WSDLOperation;
import org.ws4d.java.wsdl.WSDLPortType;
import org.ws4d.java.wsdl.soap12.SOAP12DocumentLiteralHTTPBinding;
import org.ws4d.java.wsdl.soap12.SOAP12DocumentLiteralHTTPBindingBuilder;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentSupportFactory;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyDirection;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSIOType;

/**
 *
 *
 */
public class MDPWSSOAP12DocumentLiteralHTTPBindingBuilder extends
SOAP12DocumentLiteralHTTPBindingBuilder {

	private WSDL wsdl;

	public MDPWSSOAP12DocumentLiteralHTTPBindingBuilder() {
		super();
	}

	@Override
	protected MDPWSSOAP12DocumentLiteralHTTPBinding buildBindingInternal(QName name,
			QName type) {
		return new MDPWSSOAP12DocumentLiteralHTTPBinding(name,type);
	}

	@Override
	public void parseBindingExtension(QName bindingName, QName portType,
			ElementParser parser) throws UnsupportedBindingException, IOException {

		int extensionDepth=parser.getDepth();
		//Let the super class parse the extensions
		super.parseBindingExtension(bindingName, portType, parser);

		//if it was successful we have a SOAPDocumentLiteralHTTPBinding and we just build a mdpws soap binding
		SOAP12DocumentLiteralHTTPBinding origSOAPBinding=getBindingInternal();
		if( origSOAPBinding!=null)
		{
			setBindingInternal(buildBindingInternal(bindingName, portType));
		}

		//check if we have a mdpws soap binding now
		origSOAPBinding=getBindingInternal();
		if (!(origSOAPBinding instanceof WSDLPolicyAttachmentPoint))
		{
			//nothing we can do here
			return;
		}

		//we have a mdpws soap binding so we can proceed
		WSDLPolicyAttachmentPoint attachmentPoint=(WSDLPolicyAttachmentPoint)origSOAPBinding;
		
		if (Log.isDebug()) Log.debug("Policy AttachmentPoint: "+attachmentPoint.toString());

		String namespace;
		String name;
		try{

			//Consume all per-binding extensibility elements (#1 http://www.w3.org/TR/wsdl.html#_bindings)
			while (parser.getDepth()>=extensionDepth && parser.getEventType()!=XmlPullParser.END_DOCUMENT) 
			{ 		
				if (parser.getEventType()!=XmlPullParser.END_TAG && parser.getEventType()!=XmlPullParser.END_DOCUMENT)
				{
					// run through possible operations
					namespace = parser.getNamespace();
					name = parser.getName();
					if (WSPConstants.WSP_NAMESPACE_NAME.equals(namespace)) {
						if (Log.isDebug()) Log.debug("Found per-binding extensibility element from policy ns: "+name+" "+namespace);
						ElementParser policyParser=new ElementParser(parser);
						WSDLPolicyAttachmentSupportFactory.getInstance().getParser().parsePolicyTag(attachmentPoint, policyParser);
						policyParser.consume();
					}else{
						if (WSDLConstants.WSDL_NAMESPACE_NAME.equals(namespace) &&
								WSDLConstants.WSDL_ELEM_OPERATION.equals(name)) {
							//full stop: we found an operation, no more per-binding extensibility elements after this point allowed
							return;
						}
					}

				}
				if (parser.getEventType()!=XmlPullParser.END_DOCUMENT)
					parser.nextTag();

			}
		}catch(XmlPullParserException e){
			Log.error(e);
			
			throw new IOException(e.getMessage());
		}
	}

	@Override
	public void parseOperationExtension(String operationName,
			ElementParser parser) throws UnsupportedBindingException {
		super.parseOperationExtension(operationName, parser);
	}

	@Override
	public void parseInputExtension(String inputName, ElementParser parser, String operationName)
	throws UnsupportedBindingException {
		if (WSPConstants.WSP_NAMESPACE_NAME.equals(parser.getNamespace()))
		{
			parseIOPolicyExtension(inputName, parser, operationName, WSDLPolicyDirection.INBOUND);
		}else{
			if (Log.isDebug())
				Log.debug("No WSP-Namespace "+parser.getName()+" "+parser.getNamespace());
			
			super.parseInputExtension(inputName, parser, operationName);
		}
	}

	/**
	 * @param ioTypeName
	 * @param parser
	 * @param operationName
	 * @param inbound 
	 * @throws UnsupportedBindingException
	 */
	protected void parseIOPolicyExtension(String ioTypeName,
			ElementParser parser, String operationName, WSDLPolicyDirection direction)
	throws UnsupportedBindingException {
		if (WSPConstants.WSP_NAMESPACE_NAME.equals(parser.getNamespace()))
		{
			//check if we have a mdpws soap binding now
			SOAP12DocumentLiteralHTTPBinding binding=getBindingInternal();
			if (binding instanceof MDPWSSOAP12DocumentLiteralHTTPBinding)
			{
				MDPWSSOAP12DocumentLiteralHTTPBinding mdpwsBinding=(MDPWSSOAP12DocumentLiteralHTTPBinding)binding;
				//we have a mdpws soap binding so we can proceed
				MDPWSIOType attachmentPoint=null;

				if (wsdl!=null)
				{
					WSDLPortType pt=wsdl.getPortType(mdpwsBinding.getTypeName());
					for(Iterator opIt=pt.getOperations().iterator();opIt.hasNext();)
					{
						WSDLOperation tWSDLOp=(WSDLOperation)opIt.next();
						if (tWSDLOp.getName().equals(operationName))
						{
							attachmentPoint=new MDPWSIOType(WSDLPolicyDirection.INBOUND.equals(direction)?tWSDLOp.getInput():tWSDLOp.getOutput());
							//SSch: input extension means that the policy has to be applied on outgoing messages
							attachmentPoint.setPolicyDirection(direction); 
							break;
						}
					}

					if (attachmentPoint!=null)
					{
						ElementParser policyParser=new ElementParser(parser);
						try {
							WSDLPolicyAttachmentSupportFactory.getInstance().getParser().parsePolicyTag(attachmentPoint, policyParser);
							policyParser.consume();
						} catch (Exception e) {
							Log.warn(e);
						}
						boolean addedBinding=mdpwsBinding.addChildIOTypenAttachmentPoint(attachmentPoint);
						if (Log.isDebug())
							Log.debug("Added attachmentPoint to mdpwsbinding: "+addedBinding+ " "+attachmentPoint);
					}
				}
			}
		}
	}



	@Override
	public void parseOutputExtension(String outputName, ElementParser parser, String operationName)
	throws UnsupportedBindingException {
		if (WSPConstants.WSP_NAMESPACE_NAME.equals(parser.getNamespace()))
		{
			parseIOPolicyExtension(outputName, parser, operationName, WSDLPolicyDirection.OUTBOUND);
		}else{
			if (Log.isDebug()) Log.debug("No WSP-Namespace "+parser.getName()+" "+parser.getNamespace());
			
			super.parseInputExtension(outputName, parser, operationName);
		}
	}

	@Override
	public void setWSDL(WSDL wsdl) {
		this.wsdl=wsdl;
	}

	@Override
	public WSDLBinding getBinding() {
		WSDLBinding binding= super.getBinding();
		if (binding instanceof MDPWSSOAP12DocumentLiteralHTTPBinding)
		{
			MDPWSSOAP12DocumentLiteralHTTPBinding mBinding=(MDPWSSOAP12DocumentLiteralHTTPBinding)binding;
			mBinding.setWsdl(wsdl);
		}
		return binding;
	}	


}
