/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.protocol.soap.wsdl;

import java.io.IOException;

import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.types.QName;
import org.ws4d.java.wsdl.IOType;
import org.ws4d.java.wsdl.WSDL;
import org.ws4d.java.wsdl.WSDLOperation;
import org.ws4d.java.wsdl.soap12.SOAP12DocumentLiteralHTTPBinding;
import org.xmlpull.v1.XmlSerializer;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachment;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentAbstractionState;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentSupportFactory;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyDirection;

/**
 * Used for QoS-Policy Framework ???
 * 
*
 *
 */
public class MDPWSSOAP12DocumentLiteralHTTPBinding extends SOAP12DocumentLiteralHTTPBinding implements WSDLPolicyAttachmentPoint
{

	private final ArrayList childOperationAttachmentPoints=new ArrayList();
	private final ArrayList childIOPTypeAttachmentPoints=new ArrayList();
	
	private final ArrayList policyAttachments=new ArrayList();

	public MDPWSSOAP12DocumentLiteralHTTPBinding(QName name, QName type) {
		super(name, type);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint#getWSDLPolicyAttachments()
	 */
	@Override
	public Iterator getWSDLPolicyAttachments() {
		return new ReadOnlyIterator(policyAttachments.iterator());
	}
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint#addWSDLPolicyAttachment(com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachment)
	 */
	@Override
	public boolean addWSDLPolicyAttachment(WSDLPolicyAttachment attachment) {
		if (attachment !=null && !policyAttachments.contains(attachment))
		{
			return policyAttachments.add(attachment);
		}
		return false;
	}
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint#removeWSDLPolicyAttachment(com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachment)
	 */
	@Override
	public boolean removeWSDLPolicyAttachment(WSDLPolicyAttachment attachment) {
		if (attachment !=null)
		{
			return policyAttachments.remove(attachment);
		}
		return false;
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint#getPolicyDirection()
	 */
	@Override
	public WSDLPolicyDirection getPolicyDirection() {
		return WSDLPolicyDirection.INOUTBOUND;
	}


	@Override
	public void serializeBindingExtensions(XmlSerializer serializer)
	throws IOException {
		//1. serialize all extensions from the super class  
		super.serializeBindingExtensions(serializer);

		//2. Category: policies -> serialize policies
		Iterator policyAttachments=getWSDLPolicyAttachments();
		WSDLPolicyAttachmentSupportFactory.getInstance().getSerializer().serializePolicyAttachments(policyAttachments, serializer, "BindingPolicy");

		//No more binding extensions
	}



	@Override
	public void serializeOperationExtension(WSDLOperation operation,
			XmlSerializer serializer) throws IOException {
		//1. serialize all extensions from the super class, e.g. wsoap12:operation
		super.serializeOperationExtension(operation, serializer);

		Iterator policyAttachments=getWSDLPolicyAttachmentsForAnyNotAbstractAttachmentPoint(operation);
		if (policyAttachments!=null && policyAttachments.hasNext())
		{
			WSDLPolicyAttachmentSupportFactory.getInstance().getSerializer().serializePolicyAttachments(policyAttachments, serializer, "BindingOperationPolicy");
		}
	}



	@Override
	public void serializeInputExtension(IOType input, XmlSerializer serializer)
	throws IOException {
		//1. serialize all extensions from the super class, e.g. wsoap12:body
		super.serializeInputExtension(input, serializer);

		Iterator policyAttachments=getWSDLPolicyAttachmentsForAnyNotAbstractAttachmentPoint(input);
		if (policyAttachments!=null && policyAttachments.hasNext())
		{
			WSDLPolicyAttachmentSupportFactory.getInstance().getSerializer().serializePolicyAttachments(policyAttachments, serializer, input.getMessageName().getLocalPart()+"BindingInputPolicy");
		}	
	}

	@Override
	public void serializeOutputExtension(IOType output, XmlSerializer serializer)
	throws IOException {
		//1. serialize all extensions from the super class, e.g. wsoap12:body
		super.serializeOutputExtension(output, serializer);

		Iterator policyAttachments=getWSDLPolicyAttachmentsForAnyNotAbstractAttachmentPoint(output);
		if (policyAttachments!=null && policyAttachments.hasNext())
		{
			WSDLPolicyAttachmentSupportFactory.getInstance().getSerializer().serializePolicyAttachments(policyAttachments, serializer, "BindingOutputPolicy");
		}
	}

	/**
	 * @param output
	 * @return
	 */
	private Iterator getWSDLPolicyAttachmentsForAnyNotAbstractAttachmentPoint(Object possibleAttachmentPoint) {
		Iterator retVal=null;
		Iterator unfilteredRetVal=null;
		if (possibleAttachmentPoint instanceof WSDLPolicyAttachmentPoint)
		{
			WSDLPolicyAttachmentPoint attachmentPoint= (WSDLPolicyAttachmentPoint)possibleAttachmentPoint;
			unfilteredRetVal= attachmentPoint.getWSDLPolicyAttachments();
		}

		if (unfilteredRetVal!=null)
		{
			ArrayList filteredPolicyAttachments=new ArrayList();
			while(unfilteredRetVal.hasNext())
			{
				WSDLPolicyAttachment attachment=(WSDLPolicyAttachment)unfilteredRetVal.next();
				if (attachment.getAbstractionState().equals(WSDLPolicyAttachmentAbstractionState.NOTABSTRACT))
					filteredPolicyAttachments.add(attachment);
			}
			retVal=filteredPolicyAttachments.iterator();
		}
		return retVal;
	}

	/**
	 * @return the childAttachmentPoints
	 */
	public ArrayList getChildOperationAttachmentPoints() {
		return childOperationAttachmentPoints;
	}

	
	public void addChildOperationAttachmentPoint(WSDLPolicyAttachmentPoint attachmentPoint)
	{
		if (!childOperationAttachmentPoints.contains(attachmentPoint))
		{
			childOperationAttachmentPoints.add(attachmentPoint);
		}
	}
	
	/**
	 * @return the childAttachmentPoints
	 */
	public ArrayList getChildIOTypeAttachmentPoints() {
		return childIOPTypeAttachmentPoints;
	}

	
	public boolean addChildIOTypenAttachmentPoint(WSDLPolicyAttachmentPoint attachmentPoint)
	{
		boolean added=false;
		if (!childIOPTypeAttachmentPoints.contains(attachmentPoint))
		{
			added=childIOPTypeAttachmentPoints.add(attachmentPoint);
		}
		return added;
	}
	
	@Override
	public void setWsdl(WSDL wsdl) {
		super.setWsdl(wsdl);
	}



	//	public void serializeInputExtension(IOType input,
	//			XmlSerializer serializer) throws IOException {
	//		
	//		super.serializeInputExtension(input, serializer);
	//		
	//		//TODO QoSPolicy FW implementation ???
	////		if (input instanceof MDPWSIOType)
	////		{
	////
	////			MDPWSIOType iotype=(MDPWSIOType)input;
	////			WSDLOperation wsdlOP=iotype.getOperation();
	////			if (wsdlOP instanceof MDPWSWSDLOperation)
	////			{
	////				MDPWSWSDLOperation mwsdlOP=(MDPWSWSDLOperation)wsdlOP;
	////				this.serializeInputExtensionMDPWS(iotype, mwsdlOP,serializer);
	////			}else{
	////				super.serializeInputExtension(input, serializer);
	////			}
	////		}else{
	////			super.serializeInputExtension(input, serializer);
	////		}
	//	}
	//
	//
	//
	//	public void serializeOutputExtension(IOType output,
	//			XmlSerializer serializer) throws IOException {
	//		
	//		super.serializeOutputExtension(output, serializer);
	//		
	//		//TODO QoSPolicy FW implementation ??? 
	////		if (output instanceof MDPWSIOType)
	////		{
	////
	////			MDPWSIOType iotype=(MDPWSIOType)output;
	////			WSDLOperation wsdlOP=iotype.getOperation();
	////			if (wsdlOP instanceof MDPWSWSDLOperation)
	////			{
	////				MDPWSWSDLOperation mwsdlOP=(MDPWSWSDLOperation)wsdlOP;
	////				this.serializeOutputExtensionMDPWS(iotype, mwsdlOP,serializer);
	////			}else{
	////				super.serializeOutputExtension(output, serializer);
	////			}
	////		}else{
	////			super.serializeOutputExtension(output, serializer);
	////		}
	//	}
	//
	//	protected void serializeInputExtensionMDPWS(MDPWSIOType iotype,
	//			MDPWSWSDLOperation mwsdlOP, XmlSerializer serializer) throws IllegalArgumentException, IllegalStateException, IOException {
	//		serializer.startTag(WSDLConstants.SOAP12_BINDING_NAMESPACE_NAME, WSDLConstants.SOAP12_ELEM_BODY);
	//		serializer.attribute(null, WSDLConstants.WSDL_ATTRIB_USE, LITERAL_USE);
	//		serializer.endTag(WSDLConstants.SOAP12_BINDING_NAMESPACE_NAME, WSDLConstants.SOAP12_ELEM_BODY);
	//
	//		//Check for additional headers
	//		if (mwsdlOP.isIncomingSecondChannelRequired())
	//		{
	//			//add the message description, used only for framework
	//			QName wsdcMessage=WSDualChannelConstants.WSDC_DUALCHANNEL_MESSAGE;
	//			String namespace = wsdcMessage.getNamespace();
	//			String prefix = serializer.getPrefix(namespace, false);
	//			String msgQName = (prefix == null) ? wsdcMessage.getLocalPart() : prefix + ":" + wsdcMessage.getLocalPart();
	//
	//			String partName=WSDualChannelConstants.WSDC_MESSAGEPART_NAME;
	//			serializer.startTag(WSDLConstants.SOAP12_BINDING_NAMESPACE_NAME, WSDLConstants.SOAP12_ELEM_HEADER);
	//			serializer.attribute(null, WSDLConstants.WSDL_ATTRIB_MESSAGE, msgQName);
	//			serializer.attribute(null, WSDLConstants.WSDL_ATTRIB_PART, partName);
	//			serializer.attribute(null, WSDLConstants.WSDL_ATTRIB_USE, LITERAL_USE);
	//			serializer.endTag(WSDLConstants.SOAP12_BINDING_NAMESPACE_NAME, WSDLConstants.SOAP12_ELEM_HEADER);
	//
	//			//add the policy
	//			serializer.startTag(WSPConstants.WSP_NAMESPACE_NAME, WSPConstants.WSP_ELEM_POLICY);
	//			serializer.startTag(WSDualChannelConstants.WSDC_NAMESPACE, WSDualChannelConstants.WSDC_SECOND_CHANNEL_POLICY);
	//			Iterator reqvalIter= mwsdlOP.getSecondChannelRequiredValues();
	//			//if no reqVal than the whole body is required
	//			while(reqvalIter.hasNext())
	//			{
	//				XPath xpath=(XPath) reqvalIter.next();
	//				String expression=xpath.getExpression();
	//				//use the xpath context to adapt the expression to the currently used namespaces
	//				if (expression!=null && expression.length()>0)
	//				{
	//					java.util.Iterator<Entry<String, String>> it=xpath.getContext().getAllEntries();
	//					while (it.hasNext())
	//					{
	//						Entry<String, String> entry=it.next();
	//						String xPathPreFix=serializer.getPrefix(entry.getValue(), true);
	//						expression=expression.replaceAll(entry.getKey()+":", xPathPreFix+":");					
	//					}
	//
	//				serializer.startTag(WSDualChannelConstants.WSDC_NAMESPACE, WSDualChannelConstants.WSDC_REQVALUE);
	//				serializer.attribute(null, WSDualChannelConstants.WSDC_ATTR_ALGORITHM, WSDualChannelConstants.WSDC_DEFAULT_ALGORITHM);
	//				serializer.attribute(null, WSDualChannelConstants.WSDC_ATTR_XPATH, expression);
	//				serializer.endTag(WSDualChannelConstants.WSDC_NAMESPACE, WSDualChannelConstants.WSDC_REQVALUE);
	//				}
	//			}
	//			serializer.endTag(WSDualChannelConstants.WSDC_NAMESPACE, WSDualChannelConstants.WSDC_SECOND_CHANNEL_POLICY);
	//			serializer.endTag(WSPConstants.WSP_NAMESPACE_NAME, WSPConstants.WSP_ELEM_POLICY);
	//		}
	//
	//	}
	//
	//	protected void serializeOutputExtensionMDPWS(MDPWSIOType iotype,
	//			MDPWSWSDLOperation mwsdlOP, XmlSerializer serializer) throws IllegalArgumentException, IllegalStateException, IOException {
	//		serializer.startTag(WSDLConstants.SOAP12_BINDING_NAMESPACE_NAME, WSDLConstants.SOAP12_ELEM_BODY);
	//		serializer.attribute(null, WSDLConstants.WSDL_ATTRIB_USE, LITERAL_USE);
	//		serializer.endTag(WSDLConstants.SOAP12_BINDING_NAMESPACE_NAME, WSDLConstants.SOAP12_ELEM_BODY);
	//	}


}
