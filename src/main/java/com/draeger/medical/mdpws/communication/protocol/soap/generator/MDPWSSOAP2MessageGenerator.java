/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.protocol.soap.generator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.protocol.soap.generator.DefaultSOAP2MessageGenerator;
import org.ws4d.java.communication.protocol.soap.generator.MessageDiscarder;
import org.ws4d.java.communication.protocol.soap.generator.MessageReceiver;
import org.ws4d.java.communication.protocol.soap.generator.SOAP2BasicTypesConverter;
import org.ws4d.java.communication.protocol.soap.generator.SOAP2ParameterValueConverter;
import org.ws4d.java.communication.protocol.soap.generator.VersionMismatchException;
import org.ws4d.java.constants.MEXConstants;
import org.ws4d.java.constants.SOAPConstants;
import org.ws4d.java.constants.WSAConstants;
import org.ws4d.java.constants.WSAConstants2006;
import org.ws4d.java.constants.WXFConstants;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.io.xml.XmlPullParserSupport;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.SOAPHeader;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.EmptyStructures;
import org.ws4d.java.structures.HashSet;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.draeger.medical.mdpws.message.MDPWSMessage;
import com.draeger.medical.mdpws.message.invoke.MDPWSInvokeMessage;
import com.draeger.medical.mdpws.message.metadata.MDPWSGetMetadataResponseMessage;
import com.draeger.medical.mdpws.qos.QoSMessageContext;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.QoSPolicyUtil;
import com.draeger.medical.mdpws.qos.interception.InboundSOAPUTF8TransformationInterceptor;
import com.draeger.medical.mdpws.qos.interception.InboundSOAPXMLDocumentInterceptor;
import com.draeger.medical.mdpws.qos.interception.InterceptionException;
import com.draeger.medical.mdpws.qos.interception.OutboundSOAPUTF8TransformationInterceptor;
import com.draeger.medical.mdpws.qos.interception.OutboundSOAPXMLInterceptor;
import com.draeger.medical.mdpws.qos.interception.QoSInterceptionAttributeMap;
import com.draeger.medical.mdpws.qos.interception.QoSInterceptionAttributesRegistry;
import com.draeger.medical.mdpws.qos.management.QoSPolicyManager;
import com.draeger.medical.mdpws.qos.util.InputStreamWrapper;
import com.draeger.medical.mdpws.utils.InputStreamUtil;

public class MDPWSSOAP2MessageGenerator extends DefaultSOAP2MessageGenerator {

	private static final DocumentBuilderFactory docBuilderFactory;
	//	private static final TransformerFactory tf;
	private DocumentBuilder builder;

	private static final int 			maxProvider=Integer.parseInt(System.getProperty("MPDWS.MDPWSSOAP2MessageGenerator.PoolSize", "5"));
	private static final int 			maxBlockTime=Integer.parseInt(System.getProperty("MPDWS.MDPWSSOAP2MessageGenerator.maxBlockTime", "1000"));
	private static final byte 			whenExhaustedAction=(byte)Integer.parseInt(System.getProperty("MPDWS.MDPWSSOAP2MessageGenerator.whenExhaustedAction", String.valueOf(GenericObjectPool.WHEN_EXHAUSTED_GROW)));

	private static final ObjectPool baosPool		=new GenericObjectPool(new ByteArrayOutputStreamFactory(),maxProvider,whenExhaustedAction,maxBlockTime);
	private static final ObjectPool transformerPool	=new GenericObjectPool(new TransformerPoolFactory(),maxProvider,whenExhaustedAction,maxBlockTime);
	private static final ObjectPool parserPool		=new GenericObjectPool(new ParserPoolFactory(), maxProvider,whenExhaustedAction,maxBlockTime);
//	private static final LockSupport transformerPoolLock=new LockSupport();
//	private static final LockSupport baosPoolLock=new LockSupport();


	static{
		//		TODO Ssch check
		//		System.setProperty("org.xml.sax.parser", "org.apache.xerces.parsers.SAXParser");
		//		System.setProperty("javax.xml.parsers.SAXParserFactory", "org.apache.xerces.jaxp.SAXParserFactoryImpl");
		//		System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
		//		System.setProperty("javax.xml.transform.TransformerFactory", "org.apache.xalan.processor.TransformerFactoryImpl");

		docBuilderFactory = DocumentBuilderFactory.newInstance();
		docBuilderFactory.setNamespaceAware(true);

		//		tf=TransformerFactory.newInstance();
	}



	public MDPWSSOAP2MessageGenerator() {
		super();
	}


	@Override
	protected SOAP2ParameterValueConverter createSOAP2MessageConverter(SOAP2BasicTypesConverter 		basicTypesConverter) {
		return new MDPWSSOAP2ParameterValueConverter(basicTypesConverter);
	}


	private final FilterHelper filterHelper=new FilterHelper();

	@Override
	public synchronized void  deliver(InputStream in, MessageReceiver to, DPWSProtocolData protocolData, MessageDiscarder discarder, Message request) 
	{
		String action=null;
		if (request!=null && request.getAction()!=null)
			action=request.getAction().toString();
		
		try{
			//Holder for possible exception during interception
			Exception exception=null;

			//Create Message Context
			QoSMessageContext qosMessageCtxt=null;
			
			Iterator policiesIterator=filterAndSortPoliciesForApplication(getUnfilteredPolicies(protocolData, to, in));

			//System.out.println("\t\t\t\tPolicy Handling.[id="+protocolData.getInstanceId()+"] "+this);
			if (policiesIterator.hasNext())
			{

				qosMessageCtxt=new QoSMessageContext();

				QoSInterceptionAttributeMap attributes = getAttributeMap(protocolData, true);
				attributes.put(QoSMessageContext.class, qosMessageCtxt);

				boolean inputStreamDecoded=false;

				InputStream transformedStream=null;

				Document xmlInputDocument = null;
				Document xmlOutputDocument= null;

				while(policiesIterator.hasNext() && exception==null)
				{
					Object qosPolicyO=policiesIterator.next();
					//System.out.println("\t\t\t\t\t Checking Policy "+qosPolicyO+" [id="+protocolData.getInstanceId()+"] "+this);
					if (!(qosPolicyO instanceof QoSPolicy)){continue;}

					//get interceptors from policy
					QoSPolicy policy=(QoSPolicy)qosPolicyO;

					//find out if the policy is applicable for this message. 
					//A policy is applicable if the message has been created from an operation 
					//that belongs to the service the policies subject is associated with
					//System.out.println("\t\t\t\t\t Checking is Policy applicable "+qosPolicyO+" [id="+protocolData.getInstanceId()+"] "+this);
					if (!isPolicyApplicableForThisMessage(protocolData, null, policy, request))
					{
						if (request!=null && request.getAction()!=null)
						{
							action=request.getAction().toString();
						}
						continue;
					}

					Iterator interceptors= policy.getInterceptors();

					//sort interceptors with respect to their type
					interceptors=filterAndSortInterceptorsForApplication(interceptors);

					//System.out.println("\t\t\t\t\t Checking Interceptors [id="+protocolData.getInstanceId()+"] "+this);
					while (interceptors.hasNext() && exception==null)
					{
						Object interceptorO=interceptors.next();
						//System.out.println("\t\t\t\t\t Checking Interceptor "+interceptorO+" [id="+protocolData.getInstanceId()+"] "+this);
						try{
							if (interceptorO instanceof InboundSOAPUTF8TransformationInterceptor)
							{
								if (inputStreamDecoded){
									if (Log.isWarn()) Log.warn("InputStream decoder could not be applied.");
									continue;
								}else{
									//TODO
									InboundSOAPUTF8TransformationInterceptor interceptor=(InboundSOAPUTF8TransformationInterceptor)interceptorO;

									transformedStream=in;
									//System.out.println("\t\t\t\t\t Intercepting UTF-8 "+interceptorO+" [id="+protocolData.getInstanceId()+"] "+this);
									InputStream tmpTransformedStream=interceptor.interceptInbound(in, protocolData, to, qosMessageCtxt, policy);
									//System.out.println("\t\t\t\t\t Intercepting UTF-8 done. "+interceptorO+" [id="+protocolData.getInstanceId()+"] "+this);

									if (tmpTransformedStream!=null)
									{
										transformedStream= tmpTransformedStream;
										closeOldInputStream(in);
									}

									in=transformedStream;
								}

							}else if (interceptorO instanceof InboundSOAPXMLDocumentInterceptor){

								//System.out.println("\t\t\t\t\t Intercepting XML "+interceptorO+" decoded"+inputStreamDecoded+" [id="+protocolData.getInstanceId()+"] "+this);
								if (!inputStreamDecoded)
								{
									//									String inputStreamString=InputStreamUtil.convertStreamToStringExtended(in, false);
									//									in= InputStreamUtil.convertStringToInputStream(inputStreamString);
									in=InputStreamUtil.convertToByteArrayInputStream(in, false);
								}

								inputStreamDecoded=true; //no more decoding allowed

								InboundSOAPXMLDocumentInterceptor interceptor=(InboundSOAPXMLDocumentInterceptor)interceptorO;

								//TODO SSch use request for replys
								//								if (request!=null && request.getAction()!=null)
								//								{
								//									action=request.getAction().toString();
								//								}
								//System.out.println("\t\t\t\t\t Intercepting XML "+interceptorO+" parsing. [id="+protocolData.getInstanceId()+"] "+this);
								if (xmlInputDocument==null){
									xmlInputDocument=getDocumentBuilder().parse (in);
								}
								xmlOutputDocument= xmlInputDocument;

								if (action==null){
									action=getAction(xmlInputDocument);
								}

								//System.out.println("\t\t\t\t\t Intercepting XML "+interceptorO+" parsing done. [id="+protocolData.getInstanceId()+"] "+this);
								if (isInterceptorApplicable(interceptor, action, protocolData, policy))
								{

									//System.out.println("\t\t\t\t\t Intercepting XML "+interceptorO+" intercepting... [id="+protocolData.getInstanceId()+"] "+this);
									Document tmpXMLOutputDocument= interceptor.interceptInbound(xmlInputDocument,protocolData,to, qosMessageCtxt, policy, action);
									if (tmpXMLOutputDocument!=null)
										xmlOutputDocument=tmpXMLOutputDocument;
									

								}
								//System.out.println("\t\t\t\t\t Intercepting XML "+interceptorO+" intercepting...done. [id="+protocolData.getInstanceId()+"] "+this);
								xmlInputDocument=xmlOutputDocument; 

							}else{
								continue;
							}
						}catch(InterceptionException e)
						{
							Log.error(e.getMessage());
							exception=e;
							xmlInputDocument=xmlOutputDocument; 
						}
					}
				}
				//System.out.println("\t\t\t\t\t Interception done. Handling input stream [id="+protocolData.getInstanceId()+"] "+this);

				if (xmlOutputDocument!=null)
				{
					//System.out.println("\t\t\t\t\t\t xmlOutputDocument!=null [id="+protocolData.getInstanceId()+"] "+this);
					closeOldInputStream(in);
					//Transforming the result back to an XML Inputstream
					
					in = convertDocumentToInputStream(xmlOutputDocument);
					//System.out.println("\t\t\t\t\t\t xmlOutputDocument!=null done. [id="+protocolData.getInstanceId()+"] "+this);

				}else if (xmlInputDocument!=null){
					//System.out.println("\t\t\t\t\t\t InputDocument!=null [id="+protocolData.getInstanceId()+"] "+this);
					closeOldInputStream(in);
					in = convertDocumentToInputStream(xmlInputDocument);
					//System.out.println("\t\t\t\t\t\t InputDocument!=null done. [id="+protocolData.getInstanceId()+"] "+this);
				}else if (transformedStream!=null)
				{
					//System.out.println("\t\t\t\t\t\t transformedStream!=null done. [id="+protocolData.getInstanceId()+"] "+this);
					//orig input stream should already be closed
					// and in= transformedStream;
					// --> void
				}
				else{
					//in=InputStreamUtil.convertStringToInputStream(inputStreamString);
				}
			}
			//System.out.println("\t\t\t\tPolicy Handling done.[id="+protocolData.getInstanceId()+"] "+this);

			if (exception!=null)
			{
				to.receiveFailed(exception,protocolData);
			}else{
				//Bugfix SSch 2013-02-18 if an exception is thrown the request should not be delivered
				super.deliver(in, to, protocolData, discarder, request);				
			}

			//System.out.println("\t\t\t\tDelivering message MDPWS done.[id="+protocolData.getInstanceId()+"] "+this);
		}catch(Exception e){
			Log.error(e);
		}
	}




	private String getAction(Document xmlInputDocument) throws XPathExpressionException
	{
		return filterHelper.getSOAPAction(xmlInputDocument);
	}

	/**
	 * @param interceptor
	 * @param xmlInputDocument
	 * @param protocolData
	 * @param policy
	 * @return
	 */
	private boolean isInterceptorApplicable(
			InboundSOAPXMLDocumentInterceptor interceptor,
			String action, DPWSProtocolData protocolData,
			QoSPolicy policy) {
		boolean retVal=false;


		if (action!=null)
		{
			retVal=QoSPolicyUtil.isPolicyApplicableForThisMessage(action, policy);			
		}else{
			//TODO SSch throw SOAP exception...
			Log.warn("No action specified in message");
		}

		Log.debug("Interceptor applicable? "+retVal+" "+action);


		return retVal;
	}


	/**
	 * @param in 
	 * @param to 
	 * @param protocolData 
	 * @return
	 */
	private static final String discoveryID="239.255.255.250@3702";
	private static final String streamingID="239.239.239.255@5555";
	protected Iterator getUnfilteredPolicies(DPWSProtocolData protocolData, MessageReceiver to, InputStream in) {
		//		Iterator policies=QoSPolicyManager.getInstance().getQoSPoliciesForSubjectType(ServicePolicySubject.class);
		Iterator retVal=EmptyStructures.EMPTY_ITERATOR;

		//TODO SSch Allow configuration of IP-address.
		if (protocolData!=null && !discoveryID.equals(protocolData.getDestinationAddress()) 
				&& !streamingID.equals(protocolData.getDestinationAddress()))
		{
			retVal=QoSPolicyManager.getInstance().getQoSPolicies();
		}
		return retVal;
	}


	/**
	 * @param document
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public InputStream convertDocumentToInputStream(Document document)
	throws Exception {
		//		String outputString = org.apache.ws.security.util.XMLUtils.PrettyDocumentToString(document);
		//		InputStream in=InputStreamUtil.convertStringToInputStream(outputString);
		//		return in;
		//		ByteArrayInputStream bais=null;
		//		StringWriter writer =getStringWriter();
		//		tf.newTransformer().transform(new DOMSource(document), new StreamResult(writer));  
		//		bais= new ByteArrayInputStream(writer.toString().getBytes(encoding));
		//		releaseStringWriter(writer);
		ByteArrayInputStream bais=null;
		//System.out.println("\t\t\t\t\t\t\t Converting Doc2Stream "+this);
		ByteArrayOutputStream outputStream = getBAOS();
		try{

			//System.out.println("\t\t\t\t\t\t\t Converting Doc2Stream Getting Transformer"+this);
			Transformer transformer= getTransformer();
			transformer.transform(new DOMSource(document), new StreamResult(outputStream));
			//System.out.println("\t\t\t\t\t\t\t Converting Doc2Stream Getting Transformeation done."+this);
			releaseTransformer(transformer);
			bais= new ByteArrayInputStream(outputStream.toByteArray());
		}catch(Exception e){
			//System.out.println("\t\t\t\t\t\t\t Converting Doc2Stream Exception "+e+" "+this);
		}finally{
			//System.out.println("\t\t\t\t\t\t\t Converting Doc2Stream releasing BAOS."+this);
			releaseBAOS(outputStream);
			//System.out.println("\t\t\t\t\t\t\t Converting Doc2Stream releasing BAOS...."+this);
		}
		//System.out.println("\t\t\t\t\t\t\t Converting Doc2Stream Done."+this);
		return bais;
	}





	/**
	 * @param protocolData
	 * @param object
	 * @param policy
	 * @param request 
	 * @return
	 */
	private boolean isPolicyApplicableForThisMessage(
			DPWSProtocolData protocolData, Object object, QoSPolicy policy, Message request) {
		boolean retVal=true;
		if (request!=null && request.getAction()!=null)
		{
			retVal=QoSPolicyUtil.isPolicyApplicableForThisMessage(request.getAction().toString(), policy);		
		}
		return retVal;
	}


	/**
	 * @param interceptors
	 * @return
	 */
	private Iterator filterAndSortInterceptorsForApplication(
			Iterator interceptors) {
		ArrayList xmlDocumentInterceptors=new ArrayList();
		ArrayList utf8TransformationInterceptors=new ArrayList();

		while (interceptors.hasNext())
		{
			Object interceptorO=interceptors.next();


			if (interceptorO instanceof OutboundSOAPXMLInterceptor && !xmlDocumentInterceptors.contains(interceptorO))
			{
				xmlDocumentInterceptors.add(interceptorO);
			}else if (interceptorO instanceof OutboundSOAPUTF8TransformationInterceptor && !utf8TransformationInterceptors.contains(interceptorO)){
				utf8TransformationInterceptors.add(interceptorO);	
			}else{
				//ignore other interceptors add this level
			}
		}
		//first decode encoding -> utf8 xml, second intercept xml document 
		utf8TransformationInterceptors.addAll(xmlDocumentInterceptors);

		return utf8TransformationInterceptors.iterator();
	}




	private final ArrayList documentTransformationPolicies=new ArrayList();
	private final ArrayList utf8TransformationPolicies=new ArrayList();
	/**
	 * @param policies
	 * @return
	 */
	private Iterator filterAndSortPoliciesForApplication(Iterator policies) {
		documentTransformationPolicies.clear();
		utf8TransformationPolicies.clear();
		while (policies.hasNext())
		{
			QoSPolicy policy=(QoSPolicy)policies.next();
			Iterator policyInterceptors=policy.getInterceptors();
			while (policyInterceptors.hasNext())
			{
				Object interceptorO=policyInterceptors.next();
				if (Log.isDebug())
				{
					Log.debug("Interceptor: "+interceptorO);
				}

				if ((interceptorO instanceof InboundSOAPXMLDocumentInterceptor) && !documentTransformationPolicies.contains(policy))
				{	
					documentTransformationPolicies.add(policy);
				}else if ((interceptorO instanceof InboundSOAPUTF8TransformationInterceptor) && !utf8TransformationPolicies.contains(policy))
				{
					utf8TransformationPolicies.add(policy);	
				}else{
					//ignore other policies add this level
				}
			}
		}

		utf8TransformationPolicies.addAll(documentTransformationPolicies);
		return utf8TransformationPolicies.iterator();
	}


	private synchronized DocumentBuilder getDocumentBuilder() throws ParserConfigurationException
	{
		if (this.builder==null){
			if (Log.isDebug())
				Log.debug("Create new builder "+Thread.currentThread());

			this.builder = docBuilderFactory.newDocumentBuilder();
		}

		return this.builder;
	}

	/**
	 * @param inboundSOAPStream
	 */
	private void closeOldInputStream(InputStream inboundSOAPStream) {
		if (inboundSOAPStream instanceof InputStreamWrapper)
		{
			((InputStreamWrapper)inboundSOAPStream).setClosingAllowed(true);	
		}else {
			//void
		}


	}

	/**
	 * @param protocolData
	 * @return
	 */
	protected QoSInterceptionAttributeMap getAttributeMap(
			DPWSProtocolData protocolData, boolean createIfNotAvailable) {
		QoSInterceptionAttributeMap attributes= (QoSInterceptionAttributeMap) QoSInterceptionAttributesRegistry.getInstance().get(protocolData);
		if (attributes==null && createIfNotAvailable)
		{
			attributes=new QoSInterceptionAttributeMap();
			QoSInterceptionAttributesRegistry.getInstance().put(protocolData, attributes);
		}
		return attributes;
	}

	/**
	 * @param protocolData
	 * @return
	 */
	protected QoSInterceptionAttributeMap removeAttributeMap(
			DPWSProtocolData protocolData) {
		QoSInterceptionAttributeMap attributes= (QoSInterceptionAttributeMap) QoSInterceptionAttributesRegistry.getInstance().remove(protocolData);
		return attributes;
	}




	@Override
	protected void sendInvokeMessageToReceiver(MessageReceiver to,
			DPWSProtocolData protocolData, InvokeMessage msg) {
		MDPWSInvokeMessage mdpwsInvokeMsg=null;
		if (!(msg instanceof MDPWSInvokeMessage))
		{
			mdpwsInvokeMsg=new MDPWSInvokeMessage(msg.getHeader());
			mdpwsInvokeMsg.setContent(msg.getContent());
		}
		super.sendInvokeMessageToReceiver(to, protocolData, mdpwsInvokeMsg);
	}


	@Override
	protected void beforeReceive(Message msg, DPWSProtocolData protocolData,
			MessageReceiver to) {
		super.beforeReceive(msg, protocolData, to);
		QoSInterceptionAttributeMap attributes = removeAttributeMap(protocolData);
		if (attributes!=null)
		{
			QoSMessageContext qosMessageCtxt= (QoSMessageContext) attributes.get(QoSMessageContext.class);

			if (qosMessageCtxt!=null && msg instanceof MDPWSMessage)
			{
				((MDPWSMessage)msg).addMessageContext(QoSMessageContext.class, qosMessageCtxt);
			}else if (qosMessageCtxt !=null){
				if (Log.isDebug())Log.debug("Could not attach message context to msg of type "+msg);
			}
		}
	}




	@Override
	protected boolean handleMessage(ElementParser parser, String actionName,
			SOAPHeader header, MessageReceiver to, DPWSProtocolData protocolData)
	throws XmlPullParserException, IOException,
	VersionMismatchException {

		if (MEXConstants.WSX_ACTION_GETMETADATA_RESPONSE.equals(actionName)) {
			to.receive(MDPWSGetMetadataResponseMessage.parse(messageConverter, actionName,header, parser,protocolData, helper), protocolData);
			return true;
		} else if (WXFConstants.WXF_ACTION_GETRESPONSE.equals(actionName)) {
			HashSet check = (HashSet) DPWSCommunicationManager.getMessageIDsForGetMetadataMapping();
			if (check.contains(header.getRelatesTo())) 
			{
				//TODO SSch Move to Parser/Serializer
				to.receive(MDPWSGetMetadataResponseMessage.parse(messageConverter,actionName,header, parser,protocolData, helper), protocolData);
				DPWSCommunicationManager.getMessageIDsForGetMetadataMapping().remove(header.getMessageId());
			} else {
				to.receive(messageConverter.parseGetResponseMessage(actionName,header, parser, protocolData,helper), protocolData);
			}
			return true;
		}
		return super.handleMessage(parser, actionName, header, to, protocolData);
	}

	private class FilterHelper
	{
		//		private final XPathNamespaceContext nameSpaceContext = new SOAPNameSpaceContext();
		//		private final XPathInfo actionXPathInfo = new XPathInfo("//s12:Header/wsa:Action/text()",nameSpaceContext);
		//		private final XPathInfo action2004XPathInfo = new XPathInfo("//s12:Header/wsa2004:Action/text()",nameSpaceContext);

		private final String S12=SOAPConstants.SOAP12_NAMESPACE_NAME;
		private final String S11=SOAPConstants.SOAP11_OLD_NAMESPACE_NAME;
		private final String WSA=WSAConstants.WSA_NAMESPACE_NAME;
		private final String WSA2004=WSAConstants2006.WSA_NAMESPACE_NAME;
		private final String HEADER=SOAPConstants.SOAP_ELEM_HEADER;
		private final String ACTION=WSAConstants.WSA_ELEM_ACTION;
		private String getSOAPAction(Document soapDoc) throws XPathExpressionException{
			String retVal=null;
			if (soapDoc!=null)
			{
				Element e= soapDoc.getDocumentElement();
				if (e!=null)
				{
					NodeList envChilds = e.getChildNodes();
					if (envChilds!=null){
						int l=envChilds.getLength();
						for (int i=0;i<l;i++){
							Node h= envChilds.item(i);
							if (h!=null && h.getNodeType()==Node.ELEMENT_NODE){
								String nsh=h.getNamespaceURI();
								if (HEADER.equals(h.getLocalName()) && (S12.equals(nsh) || S11.equals(nsh)))
								{
									NodeList hChilds = h.getChildNodes();
									int lh=hChilds.getLength();
									for (int ih=0;ih<lh;ih++){
										Node he= hChilds.item(ih);
										if (he!=null && he.getNodeType()==Node.ELEMENT_NODE)
										{
											String nshe=he.getNamespaceURI();
											if (ACTION.equals(he.getLocalName()) && (WSA.equals(nshe) || WSA2004.equals(nshe)))
											{
												retVal=he.getTextContent();
												break;
											}
										}
									}
									break;
								}
							}
						}
					}
				}
			}
			return retVal;
		}
	}


	/**
	 * 
	 */
	private void releaseTransformer(Transformer transformer) {
		try {
			transformerPool.returnObject(transformer);
		} catch (Exception e) {
			Log.error(e);
		}

	}

	/**
	 * @return
	 * @throws TransformerFactoryConfigurationError 
	 * @throws TransformerConfigurationException 
	 */
	private Transformer getTransformer() throws TransformerConfigurationException, TransformerFactoryConfigurationError {
		Transformer retVal=null;
		try {
			retVal= (Transformer) transformerPool.borrowObject();
		} catch (Exception e) {
			Log.error(e);
		}
		return retVal;
	}


	private static class TransformerPoolFactory extends BasePoolableObjectFactory{

		private TransformerFactory factory= TransformerFactory.newInstance();
		/* (non-Javadoc)
		 * @see org.apache.commons.pool.BasePoolableObjectFactory#makeObject()
		 */
		@Override
		public synchronized Object makeObject() throws Exception {
			return factory.newTransformer();
		}

		@Override
		public synchronized void passivateObject(Object obj) throws Exception {
			((Transformer) obj).reset();
		}
	}


	/**
	 * 
	 */
	private void releaseBAOS(ByteArrayOutputStream baos) {
		if (baos!=null){
			baos.reset();
			try {
				baosPool.returnObject(baos);
			} catch (Exception e) {
				Log.error(e);
			}
		}
	}


	/**
	 * @return
	 */
	private  ByteArrayOutputStream getBAOS() {
		ByteArrayOutputStream retVal=null;
		try {
			retVal= (ByteArrayOutputStream) baosPool.borrowObject();
		} catch (Exception e) {
			Log.error(e);
		}finally{
//			baosPoolLock.releaseExclusiveLock();
		}
		return retVal;
	}

	private static class ByteArrayOutputStreamFactory extends BasePoolableObjectFactory{

		/* (non-Javadoc)
		 * @see org.apache.commons.pool.BasePoolableObjectFactory#makeObject()
		 */
		@Override
		public synchronized Object makeObject() throws Exception {
			return new ByteArrayOutputStream();
		}

		@Override
		public synchronized void passivateObject(Object obj) throws Exception {
			((ByteArrayOutputStream) obj).reset();
		}
	}

	



	@Override
	protected synchronized void releaseParser(XmlPullParser parser) throws XmlPullParserException{
		super.releaseParser(parser);
		try {
			if (parser!=null)
				parserPool.returnObject(parser);
		} catch (Exception e) {
			Log.error(e);
		}
	}

	/**
	 * @return
	 */
	@Override
	protected  XmlPullParser createParser() {
		XmlPullParser retVal=null;
		try {
			retVal= (XmlPullParser) parserPool.borrowObject();
		} catch (Exception e) {
			Log.error(e);
		}
		return retVal;
	}

	private static class ParserPoolFactory extends BasePoolableObjectFactory{

		/* (non-Javadoc)
		 * @see org.apache.commons.pool.BasePoolableObjectFactory#makeObject()
		 */
		@Override
		public synchronized Object makeObject() throws Exception {
			XmlPullParser parser = null;
			try {
				parser = XmlPullParserSupport.getFactory().newPullParser();
			} catch (XmlPullParserException e) {
				Log.error("Could not create XmlPullParser: " + e);
				e.printStackTrace();
				throw new RuntimeException("Could not create XmlPullParser: " + e);
			}
			return parser;
		}

		@Override
		public void passivateObject(Object obj) throws Exception {
			((XmlPullParser) obj).setInput(null);
		}
	}


	/**
	 * 
	 */
	public void reset() 
	{
		//SSch void
	}

}
