/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.streaming;

import org.ws4d.java.communication.CommunicationBinding;

public interface StreamBinding extends CommunicationBinding{
	public static final int	STREAM_BINDING	= 0x3;

	public abstract int getRoutingScheme();
}
