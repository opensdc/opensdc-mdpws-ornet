/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.configuration.streaming.soapoverudp;

import org.ws4d.java.constants.SOAPConstants;
import org.ws4d.java.types.URI;

import com.draeger.medical.mdpws.framework.configuration.streaming.StreamTransmissionConfiguration;


public class SOAPoverUDPStreamTransmissionConfiguration implements StreamTransmissionConfiguration {

	private final String destinationAddress; 
	private final URI historyEPR ; 		
	private final Object streamPeriod; 		
	private final String streamType=SOAPConstants.SOAP_OVER_UDP_SCHEMA_URI;
	
	
	public SOAPoverUDPStreamTransmissionConfiguration() {
		super();
		//TODO SSch Remove only for debugging purposes
		//TODO SSch read from config
		destinationAddress= "soap.udp://239.239.239.239:5555"; 
		historyEPR =null; 
		streamPeriod = new Integer(60); 
	}

	public SOAPoverUDPStreamTransmissionConfiguration(
			String destinationAddress, Object streamPeriod, String historyEPR) {
		super();
		this.destinationAddress = destinationAddress;
		if (historyEPR!=null)
			this.historyEPR = new URI(historyEPR);
		else
			this.historyEPR =null;
		this.streamPeriod = streamPeriod;
	}

	@Override
	public String getDestinationAddress() {
		return destinationAddress;
	}

	public URI getHistoryEPR() {
		return historyEPR;
	}

	public Object getStreamPeriod() {
		return streamPeriod;
	}

	@Override
	public boolean isValid() {
		return destinationAddress!=null;
	}
	
	@Override
	public String getStreamTransmissionType()
	{
		return this.streamType;
	}

	@Override
	public String toString() {
		return "SOAPoverUDPStreamTransmissionConfiguration [destinationAddress="
				+ destinationAddress
				+ ", historyEPR="
				+ historyEPR
				+ ", streamPeriod="
				+ streamPeriod
				+ ", streamType="
				+ streamType + "]";
	}
	
	
}
