/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.protocol.constants;


/**
 * Constants used by WS-Streaming.
 */
public interface WSSTMConstants {

	/** The namespace name for WS Streaming. */
	public static final String	WSSTM_NAMESPACE_NAME							= "http://standardized.namespace.org/ws-streaming";
	


	/** The default prefix for the WSSTM namespace. */
	public static final String	WSSTM_NAMESPACE_PREFIX							= "wsstm";

	public static final String	WSSTM_ELEM_STREAMDESCRIPTIONS					= "StreamDescriptions";
	public static final String	WSSTM_ELEM_STREAMSOURCE							= "StreamSource";
	public static final String	WSSTM_ELEM_STREAMTRANSMISSION					= "StreamTransmission";
	public static final String	WSSTM_ELEM_TYPES								= "types";
	public static final String	WSSTM_ELEM_STREAMTYPE							= "streamType";
	public static final String	WSSTM_ELEM_STREAMPERIOD							= "streamPeriod";
	public static final String	WSSTM_ELEM_STREAMADDRESS						= "streamAddress";
	public static final String	WSSTM_ELEM_HISTORYEPR							= "historyEPR";

	public static final String WSSTM_ATTRIB_NAME  								= "name";

	public static final String WSSTM_ATTRIB_ELEMENT 							= "element";

	public static final String WSSTM_ATTRIB_ACTIONURI 							= "actionURI";
	
	public static final String WSSTM_ATTRIB_TARGETNAMESPACE 					= "targetNamespace";
	
	public static final String WSSTM_ATTRIB_ID 									= "id";
	
	public static final String WSSTM_ATTRIB_STREAMTYPE 							= "streamType";
	
	public static final String WSSTM_ATTRIB_TYPE 								= "type";
	

	
	public static final String WSSTM_METADATA_DIALECT_NAME						= WSSTM_NAMESPACE_NAME+"/"+WSSTM_ELEM_STREAMDESCRIPTIONS;

	public static final String WSSTM_ATTRIB_DEVICE_EPR_ADDRESS					= "deviceEprAddress";
	public static final String WSSTM_ATTRIB_SERVICE_ID							= "serviceId";
	public static final String WSSTM_ATTRIB_STREAM_DESC_TNS						= "streamDescriptionTns";
	public static final String WSSTM_ATTRIB_STREAM_TYPE_ID						= "streamTypeId";

	
}
