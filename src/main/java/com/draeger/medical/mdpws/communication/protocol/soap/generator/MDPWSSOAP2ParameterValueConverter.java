/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.protocol.soap.generator;

import java.io.IOException;

import org.ws4d.java.communication.protocol.soap.generator.DefaultSOAP2ParameterValueConverter;
import org.ws4d.java.communication.protocol.soap.generator.SOAP2BasicTypesConverter;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.schema.Element;
import org.ws4d.java.service.parameter.IParameterValue;


public class MDPWSSOAP2ParameterValueConverter extends
DefaultSOAP2ParameterValueConverter {

	/**
	 * @param basicTypesConverter
	 */
	public MDPWSSOAP2ParameterValueConverter(
			SOAP2BasicTypesConverter basicTypesConverter) {
		super(basicTypesConverter);
	}

	@Override
	public  IParameterValue parseParameterValue(ElementParser parser, Element base)
	throws IOException {
		IParameterValue pv= super.parseParameterValue(parser, base);
		return pv;
	}
	
}
