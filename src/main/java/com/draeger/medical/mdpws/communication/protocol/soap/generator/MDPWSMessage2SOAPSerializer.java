/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.protocol.soap.generator;

import java.io.IOException;

import org.ws4d.java.communication.ConstantsHelper;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.protocol.soap.generator.BasicTypes2SOAPConverter;
import org.ws4d.java.communication.protocol.soap.generator.DefaultMessage2SOAPSerializer;
import org.ws4d.java.communication.protocol.soap.generator.ParameterValue2SOAPConverter;
import org.ws4d.java.communication.protocol.soap.generator.SerializeUtil;
import org.ws4d.java.constants.MEXConstants;
import org.ws4d.java.constants.SOAPConstants;
import org.ws4d.java.io.xml.XmlSerializer;
import org.ws4d.java.message.Message;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.HashMap.Entry;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.LinkedList;
import org.ws4d.java.structures.List;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.wsdl.WSDLOperation;
import org.ws4d.java.wsdl.WSDLPortType;

import com.draeger.medical.mdpws.communication.protocol.constants.WSSTMConstants;
import com.draeger.medical.mdpws.domainmodel.wsdl.MDPWSWSDLSerializer;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLPortType;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLStreamOperation;
import com.draeger.medical.mdpws.message.metadata.MDPWSGetMetadataResponseMessage;

public class MDPWSMessage2SOAPSerializer extends DefaultMessage2SOAPSerializer {

	protected static final boolean JMEDS_DPWS_EXPLORER_FALLBACK_ENABLED=Boolean.parseBoolean(System.getProperty("MDPWS.JMEDS_DPWS_EXPLORER_FALLBACK_ENABLED", "true"));
	protected static final boolean SERIALIZE_SCHEMA_FOR_STREAMING_IN_WSDL=Boolean.parseBoolean(System.getProperty("MDPWS.SERIALIZE_SCHEMA_FOR_STREAMING_IN_WSDL", "true"));

	public MDPWSMessage2SOAPSerializer(
			BasicTypes2SOAPConverter basicTypesConverter,
			ParameterValue2SOAPConverter pvSerializer) {
		super(basicTypesConverter, pvSerializer);
	}

	@Override
	public void serialize(Message msg, XmlSerializer serializer,
			ConstantsHelper helper, ProtocolData pd) throws IOException 
			{
		if (msg==null) return;
		if (msg instanceof MDPWSGetMetadataResponseMessage){
			serializeMDPWSGetMetadataResponseMessage((MDPWSGetMetadataResponseMessage)msg, serializer, helper,pd);
			return;
		}else{
			super.serialize(msg, serializer, helper, pd);
		}
			}

	protected void serializeMDPWSGetMetadataResponseMessage(
			MDPWSGetMetadataResponseMessage msg, XmlSerializer serializer,
			ConstantsHelper helper, ProtocolData pd) throws IOException {
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
		// Start Tag
		serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATA);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		if (msg.getMetadataReferences() != null) {
			for (Iterator it = msg.getMetadataReferences().iterator(); it.hasNext();) {
				EndpointReference ref = (EndpointReference) it.next();
				if (JMEDS_DPWS_EXPLORER_FALLBACK_ENABLED)
				{
					if (msg.getRequestedIdentifier()==null && (msg.getRequestedDialect()==null ||  MEXConstants.WSX_DIALECT_WSDL_DEPRECATED.equals(msg.getRequestedDialect().toString())))
					{
						// Start MetadataSection for WSDL
						serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
						// Dialect adden
						serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, MEXConstants.WSX_DIALECT_WSDL_DEPRECATED);
						// EndpointReference(s) adden
						basicTypesConverter.serializeEndpointReference(ref,serializer, helper, MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATAREFERENCE);
						// End Tag
						serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
					}
				}
				if (msg.getRequestedIdentifier()==null && (msg.getRequestedDialect()==null ||  MEXConstants.WSX_DIALECT_WSDL.equals(msg.getRequestedDialect().toString())))
				{
					// Start MetadataSection for WSDL
					serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
					// Dialect adden
					serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, MEXConstants.WSX_DIALECT_WSDL);
					// EndpointReference(s) adden
					basicTypesConverter.serializeEndpointReference(ref,serializer, helper, MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATAREFERENCE);
					// End Tag
					serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
				}
			}
		}

		if (msg.getMetadataLocations() != null) {
			for (Iterator it = msg.getMetadataLocations().iterator(); it.hasNext();) {
				URI location = (URI) it.next();
				if (JMEDS_DPWS_EXPLORER_FALLBACK_ENABLED)
				{
					if (msg.getRequestedIdentifier()==null && (msg.getRequestedDialect()==null ||  MEXConstants.WSX_DIALECT_WSDL_DEPRECATED.equals(msg.getRequestedDialect().toString())))
					{
						// Start MetadataSection for WSDL
						serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
						// Dialect adden
						serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, MEXConstants.WSX_DIALECT_WSDL_DEPRECATED);
						// URI(s) adden
						SerializeUtil.serializeTag(serializer, MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_LOCATION, (location.toString() == null ? "" : location.toString()));
						// End Tag
						serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
					}
				}
				if (msg.getRequestedIdentifier()==null && (msg.getRequestedDialect()==null ||  MEXConstants.WSX_DIALECT_WSDL.equals(msg.getRequestedDialect().toString())))
				{
					// Start MetadataSection for WSDL
					serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
					// Dialect adden
					serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, MEXConstants.WSX_DIALECT_WSDL);
					// URI(s) adden
					SerializeUtil.serializeTag(serializer, MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_LOCATION, (location.toString() == null ? "" : location.toString()));
					// End Tag
					serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
				}
			}
		}

		if (msg.getRequestedIdentifier()==null && (msg.getRequestedDialect()==null || helper.getMetatdataDialectRelationship().equals(msg.getRequestedDialect().toString())))
		{
			// Start MetadataSection for Relationship
			serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
			// RelationshipMData adden
			serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, helper.getMetatdataDialectRelationship());
			if (msg.getRelationship() != null) {
				basicTypesConverter.serializeRelationshipMData(msg.getRelationship(),serializer, helper);
			}
			// End Tags
			serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
		}

		//MDPWSGetMetaDataResponse
		DataStructure portTypes=msg.getPortTypes();
		HashMap schemaMap=msg.getSchemaMap();
		if (portTypes!=null && schemaMap!=null)
		{
			addStreamSourcePolicySection(msg, serializer, helper, portTypes,schemaMap,pd);
		}


		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATA);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}



	protected void addStreamSourcePolicySection(MDPWSGetMetadataResponseMessage msg, XmlSerializer serializer,ConstantsHelper helper,DataStructure portTypes, HashMap schemaMap, ProtocolData pd) throws IllegalArgumentException, IllegalStateException, IOException {
		if (msg.getRequestedDialect()==null || WSSTMConstants.WSSTM_METADATA_DIALECT_NAME.equals(msg.getRequestedDialect().toString()))
		{
			if (Log.isDebug())
				Log.debug("Serializing Stream Source Policy Section "+portTypes+" "+schemaMap);

			HashMap map=new HashMap();
			if (!portTypes.isEmpty())
			{
				Iterator ptIt=portTypes.iterator();

				while(ptIt.hasNext())
				{
					WSDLPortType portType=(WSDLPortType)ptIt.next();
					if (portType instanceof MDPWSWSDLPortType)
					{
						DataStructure operations= ((MDPWSWSDLPortType)portType).getOperations();
						Iterator opIter= operations.iterator();
						List stmsList=new LinkedList();
						while (opIter.hasNext()) {
							WSDLOperation operation = (WSDLOperation) opIter.next();
							if (operation instanceof MDPWSWSDLStreamOperation)
							{
								stmsList.add(operation);
							}
						}
						if (!stmsList.isEmpty())
							map.put(portType, stmsList);
					}
				}

				if (map.isEmpty()) return;



				//add StreamSource Information....
				for (Iterator it = map.entrySet().iterator(); it.hasNext();) 
				{
					Entry e = (Entry) it.next();
					MDPWSWSDLPortType pt=(MDPWSWSDLPortType)e.getKey();
					if (msg.getRequestedIdentifier()==null || msg.getRequestedIdentifier().toString().equals(pt.getName().toStringPlain()))
					{
						// Start MetadataSection for Relationship
						serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
						// RelationshipMData adden
						serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, WSSTMConstants.WSSTM_METADATA_DIALECT_NAME);




						serializer.attribute(null, MEXConstants.WSX_ELEM_IDENTIFIER,pt.getName().toStringPlain() );

						List stmsList=(List) e.getValue();
						//SSch Changed due to beta 4
						if (Log.isDebug())
							Log.debug("Serialize Streamsources 4 PortType "+pt+" "+stmsList);

						new MDPWSWSDLSerializer().serializeStreamSources(pt, stmsList, serializer, false, SERIALIZE_SCHEMA_FOR_STREAMING_IN_WSDL?schemaMap: null,pd);

						serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
					}
				}

			}
		}
	}

}
