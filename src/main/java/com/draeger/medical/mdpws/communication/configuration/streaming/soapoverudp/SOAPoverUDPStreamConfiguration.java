/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.configuration.streaming.soapoverudp;

import com.draeger.medical.mdpws.communication.streaming.soapoverudp.SOAPoverUDPStreamBinding;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamConfiguration;

public class SOAPoverUDPStreamConfiguration extends StreamConfiguration {

	public SOAPoverUDPStreamConfiguration(SOAPoverUDPStreamBinding binding,
			SOAPoverUDPStreamTransmissionConfiguration transmisson) {
		super(binding, transmisson);
	}

}
