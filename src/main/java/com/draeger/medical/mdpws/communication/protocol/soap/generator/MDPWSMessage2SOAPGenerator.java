/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.protocol.soap.generator;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.protocol.soap.generator.DefaultMessage2SOAPGenerator;
import org.ws4d.java.constants.XMLConstants;
import org.ws4d.java.dispatch.ProtocolVersionInfoRegistry;
import org.ws4d.java.message.Message;
import org.ws4d.java.schema.Element;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.ByteArrayBuffer;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.util.Log;

import com.draeger.medical.mdpws.communication.util.MDPWSOutStreamfactory;
import com.draeger.medical.mdpws.communication.util.MDPWSOutputStream;
import com.draeger.medical.mdpws.message.MDPWSMessage;
import com.draeger.medical.mdpws.qos.QoSMessageContext;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.QoSPolicyUtil;
import com.draeger.medical.mdpws.qos.interception.InterceptionException;
import com.draeger.medical.mdpws.qos.interception.OutboundSOAPUTF8TransformationInterceptor;
import com.draeger.medical.mdpws.qos.interception.OutboundSOAPXMLInterceptor;
import com.draeger.medical.mdpws.qos.management.QoSPolicyManager;

public class MDPWSMessage2SOAPGenerator extends DefaultMessage2SOAPGenerator {

	//private final boolean initQoSFramework=Boolean.parseBoolean(System.getProperty("MDPWS.InitQoSFramework"));
	protected OutputStream outputStream;

	public MDPWSMessage2SOAPGenerator() {
		super();
		messageSerializer=new MDPWSMessage2SOAPSerializer(basicTypesSerializer, pvSerializer);
	}



	public ByteArrayBuffer generateSOAPMessage(Message msg, boolean unlimited, ProtocolData pd) throws IOException {

		if (msg == null) {
			return null;
		}

		// reuse byte arrays/streams
		MDPWSOutputStream tmpOutput = MDPWSOutStreamfactory.getInstance().getOutputByteStream();
		// this sets the pointer to zero
		tmpOutput.reset();

		super.generateSOAPMessage(tmpOutput, msg, pd);
		ByteArrayBuffer retVal= new ByteArrayBuffer(tmpOutput.getBuffer(), tmpOutput.getCurrentSize());
		
		MDPWSOutStreamfactory.getInstance().releaseOutputStream(tmpOutput);
		return retVal;
	}

	//Override SSch Allows special handling for invoke messages, e.g. apply QoS attributes
	@Override
	protected void internalGenerateSOAPMessage(Message msg, ProtocolData pd) throws IOException 
	{


		try{
			if (msg.getVersion()==null)
			{
				msg.setVersion(ProtocolVersionInfoRegistry.getInstance().get(new EndpointReference( msg.getHeader().getTo())));
			}

			//		if (!initQoSFramework)
			//			super.internalGenerateSOAPMessage(msg, pd);

			if (msg instanceof MDPWSMessage)
			{
				msg=applyQoSMessagePolicies(msg,pd);
				applyQoSSOAPMessagePolicies(msg,pd);
				serializer.flushCache();
				serializer.endDocument();
			}
			else{
				//Fallback to default behavior
				super.internalGenerateSOAPMessage(msg, pd);	
			}
		}catch(IOException e){
			Log.info(e);
			throw e;
		}catch(Exception e){
			Log.warn(e);
			throw new IOException(e.getMessage());
		}
	}

	private Message applyQoSMessagePolicies(Message msg, ProtocolData pd) 
	{
		return msg;
	}



	@Override
	protected void setOutput(OutputStream out) throws IOException {
		super.setOutput(out);
		this.outputStream=out;
		//		serializer.setOutput(out, XMLConstants.ENCODING);
	}

	private void applyQoSSOAPMessagePolicies(Message msg, ProtocolData pd) throws Exception {
		if (Log.isDebug())
			Log.debug("##### Apply QoS Policies #####");

		QoSMessageContext qosMsgCtxt=null;
		Element inputMessageElement=null;

		if (msg.getHeader().getMessageEndpoint()!=null)
		{
			inputMessageElement=msg.getHeader().getMessageEndpoint().getOutput();
		}


		//save the writer of the serializer, no soap xml has been appended to the writer before this point
		Writer generatorOutputWriter=serializer.getOutput();

		//Create a reference for the temporary interception output, a non-null output will be appended to the writer at the end
		ByteArrayOutputStream temporaryInterceptionOutput=null;

		long tBeforePolices=System.currentTimeMillis();

		//Get all policies that possess a service subject
		//Iterator servicePolicies=QoSPolicyManager.getInstance().getQoSPoliciesForSubjectType(ServicePolicySubject.class);
		Iterator policies=QoSPolicyManager.getInstance().getQoSPolicies();

		//sort policies with respect to the order of application 
		policies=filterAndSortPoliciesForApplication(policies);
		if (policies.hasNext())
		{
			
			Iterator msgCtxtIt=((MDPWSMessage) msg).getMessageContexts();
			//find QoSMessageContext object
			while (msgCtxtIt.hasNext())
			{
				Object msgCtxtO=msgCtxtIt.next();
				if (!(msgCtxtO instanceof QoSMessageContext)) continue;
				qosMsgCtxt=(QoSMessageContext)msgCtxtO;
				break;
			}
			
			//if there has not been any qos message context created, we create one
			if (qosMsgCtxt==null){
				qosMsgCtxt=new QoSMessageContext();
				((MDPWSMessage) msg).addMessageContext(QoSMessageContext.class,qosMsgCtxt);
			}
			while(policies.hasNext())
			{
				Object qosPolicyO=policies.next();
				if (!(qosPolicyO instanceof QoSPolicy)){continue;}

				QoSPolicy policy=(QoSPolicy)qosPolicyO;

				//find out if the policy is applicable for this message. 
				//A policy is applicable if the message has been created from an operation 
				//that belongs to the service the policies subject is associated with
				if (!QoSPolicyUtil.isPolicyApplicableForThisMessage(msg, policy)) continue;

				if (Log.isDebug())
				{
					Log.debug("Applying Policy "+policy);
				}

				Iterator interceptors= policy.getInterceptors();

				//sort interceptors with respect to their type
				interceptors=filterAndSortInterceptorsForApplication(interceptors);

				while (interceptors.hasNext())
				{
					Object interceptorO=interceptors.next();
					//Do we need to generate the pure SOAP message into a temp output as starting point
					if (temporaryInterceptionOutput==null)
						temporaryInterceptionOutput=getInitialTemporaryInterceptionOutputFromMessage(msg,pd);

					if (interceptorO instanceof OutboundSOAPXMLInterceptor || interceptorO instanceof OutboundSOAPUTF8TransformationInterceptor)
					{
						if (Log.isDebug())
							Log.debug("Applying Interceptor "+interceptorO);
						try {

							InputStream interceptionInput=prepareInputStream(temporaryInterceptionOutput);

							//create new output container
							releaseBAOS(temporaryInterceptionOutput);
							temporaryInterceptionOutput=getBAOS();

							//intercept
							boolean finalInterception=callSpecificInterceptor(interceptorO,interceptionInput,temporaryInterceptionOutput ,policy,  pd, qosMsgCtxt, inputMessageElement);

							postInterceptionStreamHandling(interceptionInput,temporaryInterceptionOutput);

							if (Log.isDebug())
							{
								Log.debug("Byte Length after:"+temporaryInterceptionOutput.toByteArray().length);
							}

							//TODO SSch check if final interceptor, boolean flag????
							if (finalInterception)
								break;

						} catch (InterceptionException e) {
							Log.error(e);
						}
					}
				}
				if (Log.isDebug())
					Log.debug("No more interceptors :-(");

			}
		}

		//if an interceptor writer has been created get back the content and append it to the generator output
		if (temporaryInterceptionOutput!=null)
		{
			this.outputStream.write(temporaryInterceptionOutput.toByteArray());
			serializer.setOutput(this.outputStream, XMLConstants.ENCODING);
			releaseBAOS(temporaryInterceptionOutput);
		}
		else
		{
			//we did not change anything so we just reset the old writer and call the super method to serialize the xml
			serializer.setOutput(generatorOutputWriter);
			super.internalGenerateSOAPMessage(msg, pd);
		}
		long tAfterPolicies=System.currentTimeMillis();

		if (Log.isDebug())
		{
			Log.debug("Time for Policy Interception Handling: "+(tAfterPolicies-tBeforePolices));
		}
	}





	/**
	 * @param temporaryInterceptionOutput 
	 * @param policy 
	 * @param qosMsgCtxt 
	 * @param pd 
	 * @param interceptionInput 
	 * @param interceptorO 
	 * @return
	 * @throws InterceptionException 
	 */
	private boolean callSpecificInterceptor(Object interceptorO, InputStream interceptionInput,ByteArrayOutputStream temporaryInterceptionOutput,QoSPolicy policy,  ProtocolData pd, QoSMessageContext qosMsgCtxt, Element e) throws Exception {
		boolean finalInterception=false;
		if (interceptorO instanceof OutboundSOAPUTF8TransformationInterceptor){
			OutboundSOAPUTF8TransformationInterceptor soapTransfInterceptor=(OutboundSOAPUTF8TransformationInterceptor)interceptorO;
			finalInterception=soapTransfInterceptor.interceptOutbound(interceptionInput, pd, qosMsgCtxt, policy, temporaryInterceptionOutput,e);
		}else if (interceptorO instanceof OutboundSOAPXMLInterceptor)
		{
			OutboundSOAPXMLInterceptor soapOutinterceptor=(OutboundSOAPXMLInterceptor)interceptorO;
			finalInterception=soapOutinterceptor.interceptOutbound(interceptionInput,pd,qosMsgCtxt, policy, temporaryInterceptionOutput);
		}
		return finalInterception;
	}



	/**
	 * @param temporaryInterceptionOutput 
	 * @param interceptionInput 
	 * @throws IOException 
	 * 
	 */
	private void postInterceptionStreamHandling(InputStream interceptionInput, OutputStream temporaryInterceptionOutput) throws IOException {
		//close input stream as it is not needed any more
		interceptionInput.close();

		//make sure data has been written
		temporaryInterceptionOutput.flush();		
	}



	/**
	 * @param temporaryInterceptionOutput 
	 * @throws IOException 
	 * 
	 */
	private InputStream prepareInputStream(ByteArrayOutputStream temporaryInterceptionOutput) throws IOException {
		//create input
		String tempString= temporaryInterceptionOutput.toString();
		InputStream interceptionInput=new ByteArrayInputStream(tempString.getBytes());

		if (Log.isDebug())
		{
			Log.debug("String Before:"+tempString);
			Log.debug("Byte Length Before:"+tempString.getBytes().length);
		}
		//close old output container before creating a new one
		temporaryInterceptionOutput.close();

		return interceptionInput;
	}



	/**
	 * @param interceptors
	 * @return
	 */
	private Iterator filterAndSortInterceptorsForApplication(Iterator interceptors) {
		if (Log.isDebug())
			Log.debug("Filter & Sort Interceptors");
		ArrayList xmlTransformationInterceptors=new ArrayList();
		ArrayList soapUTF8TransformationInterceptors=new ArrayList();

		while (interceptors.hasNext())
		{
			Object interceptorO=interceptors.next();
			if (Log.isDebug())
			{
				Log.debug("Interceptor: "+interceptorO);
			}

			if (interceptorO instanceof OutboundSOAPXMLInterceptor && !xmlTransformationInterceptors.contains(interceptorO))
			{
				xmlTransformationInterceptors.add(interceptorO);
			}else if (interceptorO instanceof OutboundSOAPUTF8TransformationInterceptor && !soapUTF8TransformationInterceptors.contains(interceptorO)){
				soapUTF8TransformationInterceptors.add(interceptorO);	
			}else{
				//ignore other interceptors add this level
				if (Log.isDebug())
					Log.debug("Interceptor ignored");
			}
		}
		//first transformation, second encoding
		xmlTransformationInterceptors.addAll(soapUTF8TransformationInterceptors);

		return xmlTransformationInterceptors.iterator();
	}

	/**
	 * @param servicePolicies
	 * @return 
	 */
	private Iterator filterAndSortPoliciesForApplication(Iterator servicePolicies) 
	{
		if (Log.isDebug())
			Log.debug("Msg->SOAP: Policies before filtering available "+servicePolicies.hasNext());

		ArrayList soapWriterPolicies=new ArrayList();
		ArrayList soapUTF8TransformationPolcies=new ArrayList();
		while (servicePolicies.hasNext())
		{
			QoSPolicy policy=(QoSPolicy)servicePolicies.next();
			if (Log.isDebug())
				Log.debug("Policy "+policy);
			Iterator policyInterceptors=policy.getInterceptors();
			while (policyInterceptors.hasNext())
			{
				Object interceptorO=policyInterceptors.next();
				if (Log.isDebug())
				{
					Log.debug("Interceptor: "+interceptorO);
				}

				if (interceptorO instanceof OutboundSOAPXMLInterceptor && !soapWriterPolicies.contains(policy))
				{
					soapWriterPolicies.add(policy);
				}else if (interceptorO instanceof OutboundSOAPUTF8TransformationInterceptor && !soapUTF8TransformationPolcies.contains(policy)){
					soapUTF8TransformationPolcies.add(policy);	
				}else{
					//
					if (Log.isDebug())
						Log.debug("ignore other interceptor add this level ");
				}
			}
		}

		soapUTF8TransformationPolcies.addAll(soapWriterPolicies);
		if (Log.isDebug())
		{
			Log.debug("Msg->SOAP: Policies after filtering available "+soapUTF8TransformationPolcies.size());
			if (soapUTF8TransformationPolcies.size()>0)
			{
				for (Iterator it=soapUTF8TransformationPolcies.iterator();it.hasNext();)
					Log.debug("\t"+it.next().toString());
			}
		}
		return soapUTF8TransformationPolcies.iterator();
	}



	private ByteArrayOutputStream getInitialTemporaryInterceptionOutputFromMessage(Message msg,
			ProtocolData pd) throws IOException {
		ByteArrayOutputStream interceptionOutput=getBAOS();

		BufferedWriter interceptorSOAPOutputWriter=null;
		interceptorSOAPOutputWriter=new BufferedWriter(new OutputStreamWriter(interceptionOutput));
		serializer.setOutput(interceptorSOAPOutputWriter);
		super.internalGenerateSOAPMessage(msg, pd);
		interceptorSOAPOutputWriter.flush();

		return interceptionOutput;

	}


	private final int 			maxProvider=Integer.parseInt(System.getProperty("MPDWS.MDPWSMessage2SOAPGenerator.PoolSize", "5"));
	private final int 			maxBlockTime=Integer.parseInt(System.getProperty("MPDWS.MDPWSMessage2SOAPGenerator.maxBlockTime", "1000"));
	private final byte 			whenExhaustedAction=(byte)Integer.parseInt(System.getProperty("MPDWS.MDPWSMessage2SOAPGenerator.whenExhaustedAction", String.valueOf(GenericObjectPool.WHEN_EXHAUSTED_GROW)));



	private final ObjectPool pool=new GenericObjectPool(new ByteArrayOutputStreamFactory(),maxProvider,whenExhaustedAction,maxBlockTime);
	/**
	 * 
	 */
	private void releaseBAOS(ByteArrayOutputStream baos) {
		try {
			//Added by SSch 20120801: Reset the baos when returned to pool
			baos.reset();
			pool.returnObject(baos);
		} catch (Exception e) {
			Log.error(e);
		}

	}

	/**
	 * @return
	 */
	private  ByteArrayOutputStream getBAOS() {
		ByteArrayOutputStream retVal=null;
		try {
			retVal= (ByteArrayOutputStream) pool.borrowObject();
		} catch (Exception e) {
			Log.error(e);
		}
		return retVal;
	}

	private class ByteArrayOutputStreamFactory extends BasePoolableObjectFactory{

		/* (non-Javadoc)
		 * @see org.apache.commons.pool.BasePoolableObjectFactory#makeObject()
		 */
		@Override
		public Object makeObject() throws Exception {
			return new ByteArrayOutputStream();
		}

		@Override
		public void passivateObject(Object obj) throws Exception {
			((ByteArrayOutputStream) obj).reset();
		}
		
		
	}

	/**
	 * 
	 */
	public void reset() {
		//SSch void
		
	}
}
