/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.configuration.streaming.soapoverudp;

import org.ws4d.java.types.QNameSet;
import org.ws4d.java.util.Log;

import com.draeger.medical.mdpws.communication.streaming.soapoverudp.SOAPoverUDPStreamBinding;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamConfiguration;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamConfigurationBuilder;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamConfigurationWrapper;

public class SOAPoverUDPStreamConfigurationBuilder implements
		StreamConfigurationBuilder {
	
	private static final String PROP_BINDING_IFACE				= "Binding.Interface";
	private static final String PROP_BINDING_PORT				= "Binding.Port";
	private static final String PROP_BINDING_APPLIES_TO			= "Binding.AppliesTo";
	private static final String PROP_DESCRIPTION_TADDRESS		= "Description.TransportAddress";
	private static final String PROP_DESCRIPTION_PERIOD			= "Description.Period";
	private static final String PROP_DESCRIPTION_HISTORYEPR		= "Description.HistoryEPR";

	@Override
	public StreamConfiguration createStreamConfiguration(
			StreamConfigurationWrapper wrapper) {
		if (wrapper==null) return null;
		
		SOAPoverUDPStreamBinding sbinding=null;
		String iFace=null;
		int port=-1;
		if (wrapper.map.containsKey(PROP_BINDING_IFACE))
			iFace=(String) wrapper.map.get(PROP_BINDING_IFACE);
		try{
		if (wrapper.map.containsKey(PROP_BINDING_PORT))
			port=Integer.parseInt((String) wrapper.map.get(PROP_BINDING_PORT));
			if (port<0 && port>0xffff)
				port=-1;
		}catch(NumberFormatException e){
			if (Log.isDebug())
				Log.debug("Port format could not be parsed! "+wrapper.map.get(PROP_BINDING_PORT));
		}
		
		//create the binding
		if (iFace!=null && port!=-1)
		{
			sbinding=new SOAPoverUDPStreamBinding(iFace, port);
		}else if (iFace!=null){
			sbinding=new SOAPoverUDPStreamBinding(iFace);
		}else if (port!=port-1){
			sbinding=new SOAPoverUDPStreamBinding(port);
		}else{
			sbinding=new SOAPoverUDPStreamBinding();
		}
		//add applies to filter if available
		if (wrapper.map.containsKey(PROP_BINDING_APPLIES_TO))
		{
			String aTStr=(String) wrapper.map.get(PROP_BINDING_APPLIES_TO);
			if (aTStr!=null)
			{
				aTStr=aTStr.trim();
				if (aTStr.length()>0)
				{
					QNameSet appliesToSet= QNameSet.construct(aTStr);
					sbinding.addAllStreamnames(appliesToSet);
				}
			}
		}
		
		//Create Stream Description
		SOAPoverUDPStreamTransmissionConfiguration tConfig=
			new SOAPoverUDPStreamTransmissionConfiguration(
					(String)wrapper.map.get(PROP_DESCRIPTION_TADDRESS), 
					(String)wrapper.map.get(PROP_DESCRIPTION_PERIOD), 
					(String)wrapper.map.get(PROP_DESCRIPTION_HISTORYEPR));
		
		SOAPoverUDPStreamConfiguration retVal=new SOAPoverUDPStreamConfiguration(sbinding, tConfig);
		retVal.setConfigId(wrapper.configId);
		return retVal;
	}

}
