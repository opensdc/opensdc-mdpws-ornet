/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.streaming.soapoverudp;

import java.io.IOException;

import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.connection.udp.Datagram;
import org.ws4d.java.communication.protocol.soap.generator.MessageDiscarder;
import org.ws4d.java.communication.protocol.soap.server.SOAPoverUDPServer.SOAPoverUDPDatagramHandler;
import org.ws4d.java.dispatch.MessageInformer;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.discovery.ByeMessage;
import org.ws4d.java.message.discovery.HelloMessage;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ProbeMessage;
import org.ws4d.java.message.discovery.ResolveMatchesMessage;
import org.ws4d.java.message.discovery.ResolveMessage;
import org.ws4d.java.message.eventing.GetStatusMessage;
import org.ws4d.java.message.eventing.GetStatusResponseMessage;
import org.ws4d.java.message.eventing.RenewMessage;
import org.ws4d.java.message.eventing.RenewResponseMessage;
import org.ws4d.java.message.eventing.SubscribeMessage;
import org.ws4d.java.message.eventing.SubscribeResponseMessage;
import org.ws4d.java.message.eventing.SubscriptionEndMessage;
import org.ws4d.java.message.eventing.UnsubscribeMessage;
import org.ws4d.java.message.eventing.UnsubscribeResponseMessage;
import org.ws4d.java.message.metadata.GetMessage;
import org.ws4d.java.message.metadata.GetMetadataMessage;
import org.ws4d.java.message.metadata.GetMetadataResponseMessage;
import org.ws4d.java.message.metadata.GetResponseMessage;
import org.ws4d.java.service.OperationDescription;
import org.ws4d.java.structures.MessageIdBuffer;

public class MDPWSSOAPoverUDPServerHandler extends SOAPoverUDPDatagramHandler {
	
	protected static final MessageInformer	MESSAGE_INFORMER	= MessageInformer.getInstance();

	public MDPWSSOAPoverUDPServerHandler(MessageIdBuffer sentMessageIds) {
		super(sentMessageIds);
	}

	
	@Override
	public void handle(Datagram datagram, DPWSProtocolData protocolData)
			throws IOException {
				// void
		super.handle(datagram, protocolData);
	}


	@Override
	public void receive(HelloMessage hello, DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(ByeMessage bye, DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(ProbeMessage probe, DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(ProbeMatchesMessage probeMatches,
			DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(ResolveMessage resolve, DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(ResolveMatchesMessage resolveMatches,
			DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(GetMessage get, DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(GetResponseMessage getResponse,
			DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(GetMetadataMessage getMetadata,
			DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(GetMetadataResponseMessage getMetadataResponse,
			DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(SubscribeMessage subscribe,
			DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(SubscribeResponseMessage subscribeResponse,
			DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(GetStatusMessage getStatus,
			DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(GetStatusResponseMessage getStatusResponse,
			DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(RenewMessage renew, DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(RenewResponseMessage renewResponse,
			DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(UnsubscribeMessage unsubscribe,
			DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(UnsubscribeResponseMessage unsubscribeResponse,
			DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(SubscriptionEndMessage subscriptionEnd,
			DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(InvokeMessage invoke, DPWSProtocolData protocolData) {
				// void

	}

	
	@Override
	public void receive(FaultMessage fault, DPWSProtocolData protocolData) {
				// void

	}

	
	public void receiveFailed(Exception e) {
				// void

	}

	
	public void sendFailed(Exception e) {
				// void

	}

	@Override
	public void receiveFailed(Exception e, DPWSProtocolData protocolData) {
//void
		
	}


	@Override
	public void sendFailed(Exception e, DPWSProtocolData protocolData) {
//void
		
	}


	@Override
	public OperationDescription getOperation(String action) {
		return null;
	}


	@Override
	protected MessageDiscarder getDiscarder() {
		return null;
	}

}
