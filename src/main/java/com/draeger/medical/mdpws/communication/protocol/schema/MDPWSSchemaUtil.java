/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.protocol.schema;

import java.io.IOException;
import java.io.InputStream;

import org.ws4d.java.constants.SchemaConstants;
import org.ws4d.java.schema.Schema;
import org.ws4d.java.schema.SchemaException;
import org.ws4d.java.util.Log;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class MDPWSSchemaUtil 
{

	private static final XmlPullParserFactory XPP_FACTORY;
	private static Schema wsdcSchema;

	static {
		XmlPullParserFactory factory = null;
		try {
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			// the next is the default
			// factory.setValidating(false);
		} catch (XmlPullParserException e) {
			Log.error("Could not create XmlPullParserFactory: " + e);
			e.printStackTrace();
			throw new RuntimeException("Could not create XmlPullParserFactory: " + e);
		}
		XPP_FACTORY = factory;
	}

//	private static Schema parseSchema(InputStream stream)
//	{
//		try {
//			XmlPullParser parser = XPP_FACTORY.newPullParser();
//
//			parser.setInput(stream, null);
//
//			parser.nextTag(); //skip <?xml version="1.0" encoding="UTF-8" standalone="no"?>
//			String schemaTNS = parser.getAttributeValue(null, SchemaConstants.SCHEMA_TARGETNAMESPACE);
//			if (schemaTNS == null) {
//				/*
//				 * no explicit namespace set? use the targetNamespace from the
//				 * WSDL?
//				 */
//				//TODO Check
//			}
//			Schema schema = Schema.parse(parser,null, schemaTNS);
//			return schema;
//		} catch (SchemaException e) {
//			Log.error(e.getMessage());
//		} catch (XmlPullParserException e) {
//			// XXX Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// XXX Auto-generated catch block
//			e.printStackTrace();
//		}
//		return null;
//
//	}
//
//	public static synchronized Schema getWSDCSchema()
//	{
//		if (wsdcSchema==null)
//		{
//			String wsdcSchemaFile = System.getProperty("MDPWS.WSDCSchema", "/org/ws4d/java/schema/WSDC.xsd");
//			InputStream stream=MDPWSSchemaUtil.class.getResourceAsStream(wsdcSchemaFile);
//			if (stream==null)
//				throw new RuntimeException("VM Arg: MDPWS.WSDCSchema="+wsdcSchemaFile+" is not valid!" );
//
//			wsdcSchema=parseSchema(stream);
//		}
//		return wsdcSchema;
//
//	}
}
