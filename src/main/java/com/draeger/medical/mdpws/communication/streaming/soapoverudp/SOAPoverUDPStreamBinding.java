/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.streaming.soapoverudp;

import org.ws4d.java.communication.CommunicationManagerID;
import org.ws4d.java.communication.IPBinding;
import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.ip.NetworkDetection;
import org.ws4d.java.message.Message;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

import com.draeger.medical.mdpws.communication.MDPWSCommunicationManagerID;
import com.draeger.medical.mdpws.communication.streaming.StreamBinding;



public class SOAPoverUDPStreamBinding extends IPBinding implements StreamBinding{

	protected DataStructure appliesTo=new ArrayList();
	private URI transportAddress;
	private String iFace;

	public SOAPoverUDPStreamBinding(int port)
	{
		//TODO SSch Read from config for client
		//Bugfix SSch: Maybe there is no eth0
		this(System.getProperty("MDPWS.SOAPoverUDPStreamBinding.FallbackIFace"),port); 

	}

	public SOAPoverUDPStreamBinding(String iFace, int port) {
		//TODO SSch sinnvolle Exception schmeißen, wenn kein interface mit namen iface verhanden
		//TODO SSch Read "inet4" from config 
		super(getIPAddress(iFace), port);

		iFace=PlatformSupport.getInstance().getToolkit().getNetworkDetection(NetworkDetection.IP_NETWORK).getInterfaceName(this.ipAddress.getAddress());
		this.iFace=iFace;
	}

	/**
	 * @return
	 */
	private static IPAddress getIPAddress(String iFace) {
		IPAddress ipAddr=null;
		NetworkDetection detection=PlatformSupport.getInstance().getToolkit().getNetworkDetection(NetworkDetection.IP_NETWORK);
		if (detection!=null)
		{
			String fallbackProtocol = System.getProperty("MDPWS.SOAPoverUDPStreamBinding.FallbackProtocol");
			boolean noLoopbackAllowed=Boolean.parseBoolean(System.getProperty("MDPWS.SOAPoverUDPStreamBinding.noLoopbackAllowed",Boolean.toString(true)));
			Iterator addressIt=detection.getAddresses(fallbackProtocol,iFace,noLoopbackAllowed );
			if (addressIt.hasNext())
				ipAddr=(IPAddress) addressIt.next();
			else{
				if (Log.isWarn())Log.warn("Could not determine streaming ip address for given parameters. Interface:"+iFace+" protocol:"+fallbackProtocol+", No Loopback interfaces:"+noLoopbackAllowed);
			}
		}
		return ipAddr;
	}

	public SOAPoverUDPStreamBinding(String interfaceName)
	{
		this(interfaceName,org.ws4d.java.util.Math.getRandomPortNumber());
	}

	public SOAPoverUDPStreamBinding() 
	{
		this(org.ws4d.java.util.Math.getRandomPortNumber());
	}

	@Override
	public int getType() {
		return StreamBinding.STREAM_BINDING;
	}

	public boolean addStreamname(QName streamname) {
		return appliesTo.add(streamname);
	}

	public boolean addAllStreamnames(QNameSet streamnames) {
		boolean addedValue=false;
		if (streamnames !=null && !streamnames.isEmpty())
		{
			for (Iterator it=streamnames.iterator();it.hasNext();)
			{
				QName streamName=(QName) it.next();
				addedValue=addStreamname(streamName);
			}
		}
		return addedValue;

	}

	public void clearStreamnames() {
		appliesTo.clear();
	}

	public boolean containsStreamname(QName streamname) {
		return appliesTo.contains(streamname);
	}

	public boolean removeStreamname(QName streamname) {
		return appliesTo.remove(streamname);
	}

	public Iterator streamnamesIterator() {
		return appliesTo.iterator();
	}

	@Override
	public int getRoutingScheme() {
		return Message.MULTICAST_ROUTING_SCHEME;
	}

	@Override
	public CommunicationManagerID getCommunicationManagerId() {
		return MDPWSCommunicationManagerID.INSTANCE;
	}

	@Override
	public URI getTransportAddress() {
		return this.transportAddress; 
	}

	//Used for remote Communication Manager to open the multicast stream
	public void setTransportAddress(URI transportAddress) {
		this.transportAddress = transportAddress;
	}

	public boolean isListenerBinding()
	{
		return (this.transportAddress!=null);
	}

	public String getInterface() {
		return this.iFace;
	}

	@Override
	public String toString() {
		return "SOAPoverUDPStreamBinding [appliesTo=" + appliesTo
				+ ", transportAddress=" + transportAddress + ", iFace=" + iFace
				+ "]";
	}
	
	
}
