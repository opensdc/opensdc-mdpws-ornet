/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication;

import java.io.IOException;

import org.ws4d.java.communication.CommunicationBinding;
import org.ws4d.java.communication.CommunicationManagerID;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.DPWSProtocolVersionInfo;
import org.ws4d.java.communication.DiscoveryBinding;
import org.ws4d.java.communication.IncomingMessageListener;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.ProtocolDomain;
import org.ws4d.java.communication.Resource;
import org.ws4d.java.communication.ResponseCallback;
import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.udp.UDPClient;
import org.ws4d.java.communication.protocol.soap.generator.Message2SOAPGenerator;
import org.ws4d.java.communication.protocol.soap.generator.SOAPMessageGeneratorFactory;
import org.ws4d.java.communication.protocol.soap.generator.WSDLBindingBuilder;
import org.ws4d.java.communication.protocol.soap.generator.WSDLBindingBuilderRegistry;
import org.ws4d.java.communication.protocol.soap.server.SOAPoverUDPServer;
import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.constants.WSDLConstants;
import org.ws4d.java.message.IMessageEndpoint;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageException;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.EmptyStructures;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.MessageIdBuffer;
import org.ws4d.java.types.ByteArrayBuffer;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.WS4DIllegalStateException;
import org.ws4d.java.wsdl.WSDL;
import org.ws4d.java.wsdl.WSDLPortType;

import com.draeger.medical.mdpws.communication.configuration.streaming.soapoverudp.SOAPoverUDPStreamConfiguration;
import com.draeger.medical.mdpws.communication.configuration.streaming.soapoverudp.SOAPoverUDPStreamTransmissionConfiguration;
import com.draeger.medical.mdpws.communication.protocol.soap.wsdl.MDPWSSOAP12DocumentLiteralHTTPBindingBuilder;
import com.draeger.medical.mdpws.communication.streaming.soapoverudp.SOAPoverUDPDatagramHandlerAdapter;
import com.draeger.medical.mdpws.communication.streaming.soapoverudp.SOAPoverUDPStreamBinding;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamConfiguration;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamingConfigurationPropertiesHandler;
import com.draeger.medical.mdpws.message.MDPWSMessageContext;
import com.draeger.medical.mdpws.message.streaming.StreamFrameMessage;
import com.draeger.medical.mdpws.message.streaming.msgCtxt.StreamConfigurationMessageContext;

public class MDPWSCommunicationManager extends DPWSCommunicationManager {

	private final HashMap						udpClientsPerStreamBinding				= new HashMap();
	private final HashMap						udpClientsPerAction						= new HashMap();
	private final HashMap						udpStreamingServers						= new HashMap();

	private final boolean cleanupUnusableClients = Boolean.parseBoolean(System.getProperty("MDPWS.CleanupUnusableClients", "true"));
	
	@Override
	public CommunicationManagerID getCommunicationManagerId() {
		return MDPWSCommunicationManagerID.INSTANCE;
	}




	@Override
	public void start() throws IOException {
		super.start();
		
		WSDLBindingBuilderRegistry.getInstance().addBindingBuilder(WSDLConstants.SOAP12_BINDING_NAMESPACE_NAME, new MDPWSSOAP12DocumentLiteralHTTPBindingBuilder());
		
		StreamingConfigurationPropertiesHandler handler= StreamingConfigurationPropertiesHandler.getInstance();
		if (handler!=null)
		{
		for (Iterator it= handler.getStreamConfigurations();it.hasNext();)
		{
			StreamConfiguration sConfig=(StreamConfiguration) it.next();
			openStreamSocket(sConfig.getBinding(),null);
		}
		}
	}




	@Override
	public void stop() {
		super.stop();
		//TODO SSch 2011-01-20 implement stop and close all udp clients
	}




	@Override
	protected void prepareRessource(Resource resource,
			CommunicationBinding binding) {
		//if it is a wsdl we need to attach a binding for each porttype
		
		if (resource instanceof WSDL)
		{
			WSDL wsdl=(WSDL) resource;
			for (Iterator it=wsdl.getPortTypes();it.hasNext();)
			{
				WSDLPortType wsdlPortType=(WSDLPortType) it.next();
				QName portTypeName= wsdlPortType.getName();
				//Changed Ssch wsdl.addBinding(new SOAP12DocumentLiteralHTTPBinding(QNameFactory.getInstance().getQName(portTypeName.getLocalPart() + "Binding", portTypeName.getNamespace()), portTypeName));
				WSDLBindingBuilder builder= WSDLBindingBuilderRegistry.getInstance().getBuilder(WSDLConstants.SOAP12_BINDING_NAMESPACE_NAME);
				wsdl.addBinding(builder.buildBinding(QNameFactory.getInstance().getQName(portTypeName.getLocalPart() + "Binding", portTypeName.getNamespace()), portTypeName));
			}
			//TODO SSch Maybe we want to add a wsdl service also...
		}
	}




	@Override
	public void send(Message message, ProtocolDomain domain,
			ResponseCallback callback) throws WS4DIllegalStateException {
		if (message instanceof StreamFrameMessage)
		{
			StreamFrameMessage sfMsg=(StreamFrameMessage)message;
			sfMsg.setVersion(DPWSProtocolVersionInfo.getDPWSProtocolVersionInfo(DPWSConstants.DPWS_VERSION2009));
			
			StreamConfiguration config=null;
			MDPWSMessageContext msgContext= sfMsg.getMessageContext(StreamConfigurationMessageContext.class);
			if (msgContext instanceof StreamConfigurationMessageContext)
			{
				config=((StreamConfigurationMessageContext)msgContext).getStreamConfiguration();
			}
//			StreamConfiguration config= sfMsg.getStreamConfiguration();
			
			//TODO SSch Extract dependency. Use a Registry instead.
			if (config instanceof SOAPoverUDPStreamConfiguration)
			{
				UDPClient client = null;
				try{

					synchronized (udpClientsPerStreamBinding) {
						client = (UDPClient) udpClientsPerStreamBinding.get(config.getBinding());
					}
					if (client == null || client.isClosed()) return;

					SOAPoverUDPStreamTransmissionConfiguration tConfig=(SOAPoverUDPStreamTransmissionConfiguration)config.getTransmissonConfiguration();
					URI target=	new URI(tConfig.getDestinationAddress());
					Message2SOAPGenerator generator= SOAPMessageGeneratorFactory.getInstance().getMessage2SOAPGeneratorForCurrentThread();
					ByteArrayBuffer b = generator.generateSOAPMessage(message);
					SOAPMessageGeneratorFactory.getInstance().releaseMessage2SOAPGenerator(generator);
					client.send(new IPAddress(target.getHost()),target.getPort(), b.getBuffer(), b.getContentLength(), null);
					return;
				} catch (IOException e) {
					
					if (Log.isWarn())
					{
						Log.warn("Sending StreamFrame to UDP client "+client.getIPAddress().getAddress()+" failed: " + e.getMessage());
						Log.warn(e);
					}
					
					if (cleanupUnusableClients)
					{					
						// cleanup unusable client
						try {
							client.close();
						} catch (IOException ex) {
							Log.warn("Unable to close unusable UDP client");
						}
					
						synchronized (udpClientsPerStreamBinding) {
							udpClientsPerStreamBinding.remove(config.getBinding());
						}
					}

					return;
				}
			}else{
				Log.warn("Could not send stream due to missing stream configuration.");
			}
		}
		super.send(message, domain, callback);
	}

	@Override
	public void registerService(int[] messageTypes,
			CommunicationBinding binding, IncomingMessageListener listener)
	throws IOException, WS4DIllegalStateException 
	{
		openStreamSocket(binding, listener);
	}

	@Override
	public void unregisterService(int[] messageTypes,
			CommunicationBinding binding, IncomingMessageListener listener)
	throws IOException, WS4DIllegalStateException {
		closeStreamSocket(binding, listener);
	}

	
	

	@Override
	public DataStructure getDiscoveryBindings() throws IOException {
		//Discovery is done by DPWS Communication Manager
		return EmptyStructures.EMPTY_STRUCTURE;
	}




	@Override
	public DiscoveryBinding getDiscoveryBindingForDomain(ProtocolDomain domain) throws IOException {
		//Discovery is done by DPWS Communication Manager
		return null;
	}




	@Override
	public DataStructure getDiscoveryDomainForBinding(
			CommunicationBinding binding) throws IOException {
		//Discovery is done by DPWS Communication Manager
		return EmptyStructures.EMPTY_STRUCTURE;
	}




	@Override
	public DiscoveryBinding getDiscoveryBindingForProtocolData(ProtocolData data) {
		//Discovery is done by DPWS Communication Manager
		return null;
	}




	protected void closeStreamSocket(CommunicationBinding binding,
			IncomingMessageListener listener) throws IOException {
		
		//TODO SSch Extract dependency. Use a registry instead.
		if (binding instanceof SOAPoverUDPStreamBinding)
		{

			SOAPoverUDPStreamBinding sbinding=(SOAPoverUDPStreamBinding) binding;
			if (!sbinding.isListenerBinding())
			{
				UDPClient client = null;

				synchronized (udpClientsPerStreamBinding) 
				{
					client = (UDPClient) udpClientsPerStreamBinding.get(sbinding);
					if (client != null && !client.isClosed()) 
					{
						client.close();
					}
				}
				if (client!=null)
				{
					synchronized(udpClientsPerAction)
					{
						for(Iterator it= sbinding.streamnamesIterator(); it.hasNext();)
						{
							QName streamName=(QName) it.next();
							DataStructure aClientList =(DataStructure) udpClientsPerAction.get(streamName.toStringPlain());
							if (aClientList!=null){
								if (aClientList.contains(client))
								{
									aClientList.remove(client);
								}
								udpClientsPerAction.put(streamName.toStringPlain(), aClientList);
							}
						}
					}
				}
			}else{
				SOAPoverUDPServer server = getSOAPoverUDPServerStreaming(new IPAddress(sbinding.getTransportAddress().getHost()), sbinding.getTransportAddress().getPort(), sbinding.getInterface());
				IncomingUDPReceiver receiver = (IncomingUDPReceiver) server.getHandler();
				receiver.unregister(listener);
				server.stop();
			}
		}
	}
	
	


	@Override
	public void getAutobindings(String descriptor, DataStructure bindings)
			throws IOException {
		// void Done by DPWS Communication Manager
		//super.getAutobindings(descriptor, bindings);
	}




	@Override
	public void registerDevice(int[] messageTypes,
			CommunicationBinding binding, IncomingMessageListener listener)
	throws IOException, WS4DIllegalStateException {
		//void
		//Discovery is done by DPWS Communication Manager
	}

	@Override
	public void registerDeviceReference(int[] messageTypes,
			CommunicationBinding binding, IncomingMessageListener listener)
	throws IOException, WS4DIllegalStateException {
		//void
		//Discovery is done by DPWS Communication Manager
	}

	@Override
	public void registerDiscovery(int[] messageTypes,
			CommunicationBinding binding, IncomingMessageListener listener)
	throws IOException, WS4DIllegalStateException {
		//void
		//Discovery is done by DPWS Communication Manager
	}

	@Override
	public URI registerResource(Resource resource,
			CommunicationBinding binding, String resourcePath)
	throws IOException, WS4DIllegalStateException 
	{
		//Discovery is done by DPWS Communication Manager
		return null;
	}


	protected void openStreamSocket(CommunicationBinding binding,IncomingMessageListener listener)
	{
		if (binding instanceof SOAPoverUDPStreamBinding)
		{

			SOAPoverUDPStreamBinding sbinding=(SOAPoverUDPStreamBinding) binding;
			if (!sbinding.isListenerBinding())
			{
				UDPClient client = null;

				synchronized (udpClientsPerStreamBinding) 
				{
					client = (UDPClient) udpClientsPerStreamBinding.get(sbinding);
					if (client == null || client.isClosed()) 
					{
//						Iterator it= new IPDetection().getAddresses(null, );
//						if (it.hasNext())
//						{
//							String address=(String) it.next();
						
							client = UDPClient.get(sbinding.getHostAddress(), sbinding.getPort(), sbinding.getInterface());
							udpClientsPerStreamBinding.put(sbinding, client);
//						}
					}
				}
				if (client!=null)
				{
					//TODO SSch Use UDPClientsPerAction ????
					synchronized(udpClientsPerAction)
					{
						for(Iterator it= sbinding.streamnamesIterator(); it.hasNext();)
						{
							QName streamName=(QName) it.next();
							DataStructure aClientList =(DataStructure) udpClientsPerAction.get(streamName.toStringPlain());
							if (aClientList==null){
								aClientList=new ArrayList();
							}
							if (!aClientList.contains(client))
							{
								aClientList.add(client);
							}
							udpClientsPerAction.put(streamName.toStringPlain(), aClientList);
						}
					}
				}
			}else{
				SOAPoverUDPServer server = getSOAPoverUDPServerStreaming(new IPAddress(sbinding.getTransportAddress().getHost()), sbinding.getTransportAddress().getPort(), sbinding.getInterface());
				IncomingUDPReceiver receiver = (IncomingUDPReceiver) server.getHandler();
				receiver.register(listener);
				if (Log.isDebug()) {
					Log.debug("StreamBinding: " + sbinding);
				}
			}
		}
	}

	protected SOAPoverUDPServer getSOAPoverUDPServerStreaming(IPAddress streamAddress,
			int port, String iFace) {
		String key = streamAddress + ":" + port + "%" + iFace;
		SOAPoverUDPServer server = null;
		synchronized (udpStreamingServers) {
			server = (SOAPoverUDPServer) udpStreamingServers.get(key);
			if ((server == null || !server.isRunning())) {
				try {
					server = new SOAPoverUDPServer(streamAddress, port, iFace, new IncomingUDPReceiver(sentMulticastIds), true);
					udpStreamingServers.put(key, server);
				} catch (IOException e) {
					Log.warn("Unable to create SOAP-over-UDP server for multicast address " + key + ". " + e.getMessage());
				}
			}
		}
		return server;
	}


	protected class IncomingUDPReceiver extends SOAPoverUDPDatagramHandlerAdapter
	{

		private IncomingMessageListener listener;

		public IncomingUDPReceiver(MessageIdBuffer sentMessageIds) {
			super(sentMessageIds);
		}

		public void unregister(IncomingMessageListener listener2) 
		{
			listener=null;
		}

		public void register(IncomingMessageListener listener) {
			this.listener=listener;
		}

		@Override
		public void receive(InvokeMessage invoke, DPWSProtocolData protocolData) {
			if (listener!=null)
				try {
					listener.handle(invoke, protocolData);
				} catch (MessageException e) {
					Log.error(e);
				} catch(NullPointerException npe){
					Log.error(npe);
				}
		}

		@Override
		public IMessageEndpoint getOperation(String action) {
			return listener.getOperation(action);
		}



	}

}
