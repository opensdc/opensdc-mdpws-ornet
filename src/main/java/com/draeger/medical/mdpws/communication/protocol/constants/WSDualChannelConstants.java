/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.protocol.constants;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

public interface WSDualChannelConstants 
{
	public static final String WSDC_NAMESPACE = "http://standardized.org/ws-dc";
	
	public static final String WSDC_SECOND_CHANNEL_POLICY = "SecondChannelPolicy";

	public static final String WSDC_REQVALUE = "ReqValue";
	public static final String WSDC_REQVALUEREF = "ReqValueRef";
	public static final String WSDC_ATTR_XPATH = "xpath";
	public static final String WSDC_ATTR_ALGORITHM = "algorithm";
	public static final String WSDC_ATTR_TRANSFORM = "transform";
	public static final String WSDC_DEFAULT_ALGORITHM = "identity";
	
	public static final String WSDC_MESSAGE_NAME = "DualChannel";
	
	
	public static final String WSDC_MESSAGEPART_NAME="headerDC";
	
	
	public static final QName WSDC_DUALCHANNEL_MESSAGE=QNameFactory.getInstance().getQName("DualChannelMsg", WSDC_NAMESPACE);
	public static final QName WSDC_DUALCHANNEL_MESSAGE_ELEMENT=QNameFactory.getInstance().getQName("DualChannel", WSDC_NAMESPACE);
	
	/* 
	 * In wsdl11:binding/wsdl11:operation/wsdl11:input einfügen
	 * <SecondChannelPolicy>
	 * 		<ReqValue algorithm="identity" transform="none" xpath="./n1:SetValue/n1:ReferenceId/text()">xyz</ReqValue>
	 * 		<ReqValue algorithm="identity" transform="http://www.w3.org/2001/10/xml-exc-c14n#" xpath="./n1:SetValue/n1:Value/text()">rrr</ReqValue>
	 * 		<ReqValueRef algorithm="identity" transform="http://www.w3.org/2001/10/xml-exc-c14n#" xpath="./n1:SetValue/n1:Value/text()" idref="id17"/>
	 * </SecondChannelPolicy>
	 * 
	 * Falls kein bestimmtes Element gesichert werden soll, dann wird der ganze Body dem algorithmus als parameter übergeben, 
	 * ansonsten sequentiell die angegebenen Werte im XPATH- Audruck.
	 * Welcher Transformation des Inhalts, der mit dem XPath-Ausdrucks adressiert wird, durchgeführt werden soll. Alternativ könnte z.B. http://www.w3.org/2001/10/xml-exc-c14n# als Transform angegeben sein.
	 * 
	 * Relativer Bezugspunkt für XPATH ausdrücke ist der SOAP-Body.
	 * 
	 */
	
	/* Dann brauchen wir noch  SOAP:Header und RequiredElement aus Security
	 * für die Input message im Binding
	 *         <wsdlsoap:header message="wsdc:DualChannel" part="headerDC" use="literal"/>
	 *         
	 *         Dies führt dann dazu, dass der Request einen Header enthalten muss, der wie folgt aussieht
	 *         <wsdc:DualChannel
	 *         		<Value algorithm="identity" transform="none" xpath="./n1:SetValue/n1:ReferenceId/text()">0xah</Value>
	 *         		<Value algorithm="identity" xpath="./n1:SetValue/n1:ReferenceId/text()">42</Value>
	 *         <wsdc:DualChannel>
	 */
}
