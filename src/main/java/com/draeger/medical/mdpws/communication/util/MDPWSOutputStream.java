/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.communication.util;

import java.io.IOException;
import java.io.OutputStream;

import org.ws4d.java.constants.FrameworkConstants;

public class MDPWSOutputStream extends OutputStream {

//	private static final int	BYTE_BUF_SIZE	= FrameworkConstants.DGRAM_MAX_SIZE;
	
	public static final int	BYTE_BUF_SIZE	= FrameworkConstants.DGRAM_MAX_SIZE;

	private final byte[]		buf				= new byte[BYTE_BUF_SIZE];

	private int					pointer			= 0;

	/*
	 * (non-Javadoc)
	 * @see java.io.OutputStream#write(int)
	 */
	@Override
	public void write(int b) throws IOException {
		if (pointer == buf.length) {
			throw new IOException("Buffer size exceeded");
		}
		buf[pointer++] = (byte) b;
	}

	/*
	 * (non-Javadoc)
	 * @see java.io.OutputStream#write(byte[])
	 */
	@Override
	public void write(byte[] b) throws IOException {
		write(b, 0, b.length);
	}

	/*
	 * (non-Javadoc)
	 * @see java.io.OutputStream#write(byte[], int, int)
	 */
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		if (pointer + (len - off) >= buf.length) {
			throw new IOException("Buffer size exceeded (current=" + buf.length + ", new to store=" + (len - off));
		}
		System.arraycopy(b, off, buf, pointer, len);
		pointer += len;
	}

	/*
	 * (non-Javadoc)
	 * @see java.io.OutputStream#close()
	 */
	@Override
	public void close() throws IOException {
		reset();
	}

	public void reset() {
		// reset pointer
		pointer = 0;
	}

	public byte[] getBuffer() {
		return buf;
	}

	public int getCurrentSize() {
		return pointer;
	}

}
