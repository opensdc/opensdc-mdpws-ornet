/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.message.metadata;

import java.io.IOException;

import org.ws4d.java.communication.ConstantsHelper;
import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.ProtocolVersionInfo;
import org.ws4d.java.communication.protocol.soap.generator.SOAP2MessageConverter;
import org.ws4d.java.communication.protocol.soap.generator.VersionMismatchException;
import org.ws4d.java.concurrency.LockSupport;
import org.ws4d.java.constants.MEXConstants;
import org.ws4d.java.io.xml.AbstractElementParser;
import org.ws4d.java.io.xml.ElementHandler;
import org.ws4d.java.io.xml.ElementHandlerRegistry;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.io.xml.XmlSerializer;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.message.SOAPHeader;
import org.ws4d.java.message.metadata.GetMetadataMessage;
import org.ws4d.java.message.metadata.GetMetadataResponseMessage;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.EmptyStructures;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.List;
import org.ws4d.java.types.AppSequence;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.EndpointReferenceSet;
import org.ws4d.java.types.HostMData;
import org.ws4d.java.types.HostedMData;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.types.RelationshipMData;
import org.ws4d.java.types.URI;
import org.ws4d.java.types.URISet;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.WS4DIllegalStateException;
import org.ws4d.java.wsdl.WSDL;
import org.ws4d.java.wsdl.WSDLPortType;
import org.xmlpull.v1.XmlPullParserException;

import com.draeger.medical.mdpws.common.util.XMLParserUtil;
import com.draeger.medical.mdpws.communication.protocol.constants.WSSTMConstants;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLPortType;

public class MDPWSGetMetadataResponseMessage extends GetMetadataResponseMessage {

	private static final MDPWSGetMetaDataElementHandler elementHandler=new MDPWSGetMetaDataElementHandler();
	private static final LockSupport lock=new LockSupport();

	private HashMap								portTypes;
	private HashMap 							schemaMap;
	private GetMetadataResponseMessage 			getMetadataResponseMessage;


	public MDPWSGetMetadataResponseMessage(MessageHeader header) {
		super(header);

	}

	public MDPWSGetMetadataResponseMessage(
			GetMetadataResponseMessage responseMsg) {
		this.getMetadataResponseMessage=responseMsg;
	}

	public void setGetMetadataResponseMessage(
			GetMetadataResponseMessage getMetadataResponseMessage) {
		this.getMetadataResponseMessage = getMetadataResponseMessage;
	}

	/**
	 * Returns a data structure containing all port types defined within this
	 * WSDL instance.
	 * 
	 * @return a data structure of all defined port types
	 */
	public DataStructure getPortTypes() {
		return portTypes == null ? EmptyStructures.EMPTY_STRUCTURE : portTypes.values();
	}

	public void addPortType(WSDLPortType portType) {
		if (portType == null) {
			return;
		}
		if (portTypes == null) {
			portTypes = new HashMap();
		}
		portTypes.put(portType.getName(), portType);
	}

	public void setSchemaMap(HashMap schemaMap) {
		this.schemaMap=schemaMap;
	}

	public HashMap getSchemaMap() {
		return schemaMap==null ? EmptyStructures.EMPTY_MAP:schemaMap;
	}

	@Override
	public void addMetadataLocation(URI metadataLocation) {
		getMetadataResponseMessage.addMetadataLocation(metadataLocation);
	}

	@Override
	public void addMetadataReference(EndpointReference metadataReference) {
		getMetadataResponseMessage.addMetadataReference(metadataReference);
	}

	@Override
	public void addRelationship(RelationshipMData relationship) {
		getMetadataResponseMessage.addRelationship(relationship);
	}

	@Override
	public void addUnknownAttribute(QName attributeName, String value) {
		getMetadataResponseMessage.addUnknownAttribute(attributeName, value);
	}

	@Override
	public void addUnknownElement(QName elementName, Object element) {
		getMetadataResponseMessage.addUnknownElement(elementName, element);
	}

	@Override
	public boolean equals(Object arg0) {
		return getMetadataResponseMessage.equals(arg0);
	}

	@Override
	public AttributedURI getAction() {
		return getMetadataResponseMessage.getAction();
	}

	@Override
	public AppSequence getAppSequence() {
		return getMetadataResponseMessage.getAppSequence();
	}

	@Override
	public Object getCertificate() {
		return getMetadataResponseMessage.getCertificate();
	}

	@Override
	public MessageHeader getHeader() {
		return getMetadataResponseMessage.getHeader();
	}

	@Override
	public HostMData getHost() {
		return getMetadataResponseMessage.getHost();
	}

	@Override
	public HostedMData getHosted(AttributedURI address) {
		return getMetadataResponseMessage.getHosted(address);
	}

	@Override
	public AttributedURI getMessageId() {
		return getMetadataResponseMessage.getMessageId();
	}

	@Override
	public URISet getMetadataLocations() {
		return getMetadataResponseMessage.getMetadataLocations();
	}

	@Override
	public EndpointReferenceSet getMetadataReferences() {
		return getMetadataResponseMessage.getMetadataReferences();
	}

	@Override
	public Object getPrivateKey() {
		return getMetadataResponseMessage.getPrivateKey();
	}

	@Override
	public AttributedURI getRelatesTo() {
		return getMetadataResponseMessage.getRelatesTo();
	}

	@Override
	public RelationshipMData getRelationship() {
		return getMetadataResponseMessage.getRelationship();
	}

	@Override
	public EndpointReference getReplyTo() {
		return getMetadataResponseMessage.getReplyTo();
	}

	@Override
	public URI getTargetAddress() {
		return getMetadataResponseMessage.getTargetAddress();
	}

	@Override
	public AttributedURI getTo() {
		return getMetadataResponseMessage.getTo();
	}

	@Override
	public int getType() {
		return getMetadataResponseMessage.getType();
	}

	@Override
	public String getUnknownAttribute(QName elementName) {
		return getMetadataResponseMessage.getUnknownAttribute(elementName);
	}

	@Override
	public HashMap getUnknownAttributes() {
		return getMetadataResponseMessage.getUnknownAttributes();
	}

	@Override
	public Object getUnknownElement(QName elementName) {
		return getMetadataResponseMessage.getUnknownElement(elementName);
	}

	@Override
	public HashMap getUnknownElements() {
		return getMetadataResponseMessage.getUnknownElements();
	}

	@Override
	public List getUnknownElements(QName elementName) {
		return getMetadataResponseMessage.getUnknownElements(elementName);
	}

	@Override
	public int hashCode() {
		return getMetadataResponseMessage.hashCode();
	}

	@Override
	public boolean isInbound() {
		return getMetadataResponseMessage.isInbound();
	}

	@Override
	public boolean isSecure() {
		return getMetadataResponseMessage.isSecure();
	}

	@Override
	public void setCertificate(Object certificate) {
		getMetadataResponseMessage.setCertificate(certificate);
	}

	@Override
	public void setInbound(boolean inbound) {
		getMetadataResponseMessage.setInbound(inbound);
	}

	@Override
	public void setPrivateKey(Object privKey) {
		getMetadataResponseMessage.setPrivateKey(privKey);
	}

	@Override
	public void setResponseTo(Message request) {
		getMetadataResponseMessage.setResponseTo(request);
	}

	public void setResponseTo(SOAPHeader requestHeader) {
		getMetadataResponseMessage.setResponseTo(requestHeader);
	}

	@Override
	public void setSecure(boolean b) {
		getMetadataResponseMessage.setSecure(b);
	}

	@Override
	public void setTargetAddress(URI targetAddress) {
		getMetadataResponseMessage.setTargetAddress(targetAddress);
	}

	@Override
	public void setUnknownAttributes(HashMap attributes) {
		getMetadataResponseMessage.setUnknownAttributes(attributes);
	}

	@Override
	public void setUnknownElements(HashMap elements) {
		getMetadataResponseMessage.setUnknownElements(elements);
	}

	@Override
	public String toString() {
		return getMetadataResponseMessage.toString();
	}



	@Override
	public DataStructure getWSDLs() {
		return getMetadataResponseMessage.getWSDLs();
	}

	@Override
	public void addWSDL(WSDL wsdl) {
		getMetadataResponseMessage.addWSDL(wsdl);
	}


	@Override
	public void setVersion(ProtocolVersionInfo Version) {
		getMetadataResponseMessage.setVersion(Version);
	}

	@Override
	public ProtocolVersionInfo getVersion() {
		return getMetadataResponseMessage.getVersion();
	}

	@Override
	public int getRoutingScheme() {
		return getMetadataResponseMessage.getRoutingScheme();
	}

	@Override
	public void setRoutingScheme(int routingScheme) {
		getMetadataResponseMessage.setRoutingScheme(routingScheme);
	}
	
	@Override
	public void setResponseTo(MessageHeader requestHeader) {
		getMetadataResponseMessage.setResponseTo(requestHeader);
	}

	@Override
	public void setHeader(MessageHeader header) {
		getMetadataResponseMessage.setHeader(header);
	}
	
	@Override
	public void setFilter(GetMetadataMessage getMetadata) {
		System.out.println("Set Filter in MDPWS "+getMetadata);
		getMetadataResponseMessage.setFilter(getMetadata);
	}

	@Override
	public URI getRequestedDialect() {
		return getMetadataResponseMessage.getRequestedDialect();
	}

	@Override
	public URI getRequestedIdentifier() {
		return getMetadataResponseMessage.getRequestedIdentifier();
	}

	// ########### Parser ##############
	/**
	 * @param actionName 
	 * @param messageConverter2 
	 * @param header
	 * @param parser
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 * @throws VersionMismatchException
	 */
	public static MDPWSGetMetadataResponseMessage parse(SOAP2MessageConverter messageConverter2, String actionName, SOAPHeader header, ElementParser parser, DPWSProtocolData pd, ConstantsHelper helper) throws XmlPullParserException, IOException, VersionMismatchException {
		MDPWSGetMetadataResponseMessage getMetadataResponseMessage = new MDPWSGetMetadataResponseMessage(header);

		try{
			lock.exclusiveLock();
			ElementHandler handler= ElementHandlerRegistry.getRegistry().getElementHandler(MDPWSGetMetaDataElementHandler.elementName);
			if (!(handler instanceof MDPWSGetMetaDataElementHandler))
			{
				ElementHandlerRegistry.getRegistry().registerElementHandler(MDPWSGetMetaDataElementHandler.elementName,elementHandler );
				handler=elementHandler;
			}
			((MDPWSGetMetaDataElementHandler)handler).setMessage(getMetadataResponseMessage);
		}finally{
			lock.releaseExclusiveLock();
		}
		GetMetadataResponseMessage origMsg=messageConverter2.parseGetMetadataResponseMessage(actionName,header, parser,pd, helper);
		getMetadataResponseMessage.setGetMetadataResponseMessage(origMsg);
		
		return getMetadataResponseMessage;
	}

	protected static class MDPWSGetMetaDataElementHandler implements ElementHandler
	{
		protected static QName elementName=QNameFactory.getInstance().getQName(MEXConstants.WSX_ELEM_METADATASECTION,MEXConstants.WSX_NAMESPACE_NAME);

		private MDPWSGetMetadataResponseMessage getMetadataResponseMessage;

		public void setMessage(MDPWSGetMetadataResponseMessage getMetadataResponseMessage) 
		{
			this.getMetadataResponseMessage=getMetadataResponseMessage;
		}

		@Override
		public Object handleElement(QName elementName,
				AbstractElementParser parser, URI fromURI) throws XmlPullParserException,
				IOException {
			if (this.getMetadataResponseMessage==null) return null;

			String dialect = parser.getAttributeValue(null, MEXConstants.WSX_ELEM_DIALECT);

			if (WSSTMConstants.WSSTM_METADATA_DIALECT_NAME.equals(dialect))
			{

				//Parse the DIALECT section
				MDPWSWSDLPortType portType=new MDPWSWSDLPortType();
				parser.nextTag();
				if (XMLParserUtil.checkElementStart(parser, WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMDESCRIPTIONS))
				{
					if (Log.isDebug())
						Log.debug("Handling Streaming Assertion from GetMetaResponse");
					
					new com.draeger.medical.mdpws.domainmodel.wsdl.MDPWSWSDLParser().handleStreamDescriptions(parser, portType, fromURI);
					getMetadataResponseMessage.addPortType(portType);

					return getMetadataResponseMessage;
				}else{
					return null;
				}
			}else{
				if (Log.isInfo())
					Log.info("Unknown Dialect: "+dialect);
				return null;
			}
		}

		@Override
		public void serializeElement(XmlSerializer serializer, QName qname,
				Object value) throws IllegalArgumentException,
				WS4DIllegalStateException, IOException {
			//void
		}
	}

}
