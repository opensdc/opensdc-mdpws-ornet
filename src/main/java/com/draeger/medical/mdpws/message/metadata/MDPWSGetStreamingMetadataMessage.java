/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.message.metadata;

import org.ws4d.java.types.URI;

import com.draeger.medical.mdpws.communication.protocol.constants.WSSTMConstants;

public class MDPWSGetStreamingMetadataMessage extends MDPWSGetMetadataMessage 
{

	public MDPWSGetStreamingMetadataMessage() {
		super();
		this.setDialect(new URI(WSSTMConstants.WSSTM_METADATA_DIALECT_NAME));
	}

	@Override
	public String toString() {
		return "MDPWSGetStreamingMetadataMessage [getDialect()=" + getDialect()
				+ ", getIdentifier()=" + getIdentifier() + ", getMessageId()="
				+ getMessageId() + ", getTo()=" + getTo() + "]";
	}
	
}
