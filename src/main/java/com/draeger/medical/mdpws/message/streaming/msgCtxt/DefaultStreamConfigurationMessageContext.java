/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.message.streaming.msgCtxt;

import com.draeger.medical.mdpws.framework.configuration.streaming.StreamConfiguration;
import com.draeger.medical.mdpws.message.MDPWSMessageContext;

public class DefaultStreamConfigurationMessageContext implements StreamConfigurationMessageContext {

	private StreamConfiguration streamConfiguration;

	@Override
	public StreamConfiguration getStreamConfiguration() 
	{
		return streamConfiguration;
	}

	@Override
	public void setStreamConfiguration(StreamConfiguration streamConfiguration) 
	{
		this.streamConfiguration = streamConfiguration;
	}

	@Override
	public void merge(MDPWSMessageContext msgContext) 
	{
		//void
	}
}
