/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.message.streaming;

import org.ws4d.java.message.MessageHeader;

import com.draeger.medical.mdpws.message.invoke.MDPWSInvokeMessage;

public class StreamFrameMessage extends MDPWSInvokeMessage {

	public StreamFrameMessage(MessageHeader header) {
		super(header);
	}

	public StreamFrameMessage(String action) {
		super(action,false);
	}	
}
