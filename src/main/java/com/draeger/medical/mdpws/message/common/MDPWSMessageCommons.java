/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.message.common;

import org.ws4d.java.concurrency.LockSupport;
import org.ws4d.java.concurrency.Lockable;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;

import com.draeger.medical.mdpws.message.MDPWSMessage;
import com.draeger.medical.mdpws.message.MDPWSMessageContext;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;

public class MDPWSMessageCommons implements MDPWSMessage {
	private final MDPWSMessageContextMap messageContexts=new MDPWSMessageContextMap(); 
	private final Lockable lock=new LockSupport();

	@Override
	public MDPWSMessageContextMap getMessageContextMap() {
		return messageContexts;
	}

	@Override
	public Iterator getMessageContexts() {
		return new ReadOnlyIterator(messageContexts.values().iterator());
	}

	@Override
	public MDPWSMessageContext getMessageContext(Class<?> cls) 
	{
		return (MDPWSMessageContext) messageContexts.get(cls);
	}

	@Override
	public void addMessageContext(Class<?> cls, MDPWSMessageContext context) 
	{
		if (context==null) return;
		if (cls==null) throw new IllegalArgumentException("Class must not be null.");


		try{
			lock.exclusiveLock();
			if (cls.isInstance(context)){
				messageContexts.put(cls, context);
			}else{
				throw new IllegalArgumentException("Class must be instance of "+context.getClass());
			}
		}finally{
			lock.releaseExclusiveLock();
		}
	}

	public void addMessageContextMapContent(MDPWSMessageContextMap ctxtMap)
	{
		if (ctxtMap==null) return;
		try{
			lock.exclusiveLock();
			messageContexts.putAll(ctxtMap);
		}finally{
			lock.releaseExclusiveLock();
		}
	}

	@Override
	public void removeMessageContext(Class<?> cls) 
	{
		if (cls!=null)
		{
			try{
				lock.exclusiveLock();
				messageContexts.remove(cls);
			}finally{
				lock.releaseExclusiveLock();
			}
		}
	}

}
