/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.message.invoke;

import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.structures.Iterator;

import com.draeger.medical.mdpws.message.MDPWSMessage;
import com.draeger.medical.mdpws.message.MDPWSMessageContext;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;
import com.draeger.medical.mdpws.message.common.MDPWSMessageCommons;

public class MDPWSInvokeMessage extends InvokeMessage implements MDPWSMessage{

	protected MDPWSMessageCommons mdpwsMessageDelegate=new MDPWSMessageCommons();
	protected boolean signMessage=false;
	
	public MDPWSInvokeMessage(MessageHeader header) {
		super(header);
	}

	public MDPWSInvokeMessage(String action, boolean request) {
		super(action, request);
	}

	public MDPWSInvokeMessage(String action) {
		super(action);
	}

	public boolean isSignMessage() {
		return signMessage;
	}

	public void setSignMessage(boolean signMessage) {
		this.signMessage = signMessage;
	}

	@Override
	public Iterator getMessageContexts() {
		return mdpwsMessageDelegate.getMessageContexts();
	}

	@Override
	public MDPWSMessageContext getMessageContext(Class<?> cls) {
		return mdpwsMessageDelegate.getMessageContext(cls);
	}

	@Override
	public void addMessageContext(Class<?> cls, MDPWSMessageContext context) {
		mdpwsMessageDelegate.addMessageContext(cls, context);
	}

	@Override
	public void removeMessageContext(Class<?> cls) {
		mdpwsMessageDelegate.removeMessageContext(cls);
	}

	@Override
	public MDPWSMessageContextMap getMessageContextMap() {
		return mdpwsMessageDelegate.getMessageContextMap();
	}

	public void addMessageContextMapContent(MDPWSMessageContextMap ctxtMap) {
		mdpwsMessageDelegate.addMessageContextMapContent(ctxtMap);
	}

}
