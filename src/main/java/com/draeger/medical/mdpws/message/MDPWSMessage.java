package com.draeger.medical.mdpws.message;

import org.ws4d.java.structures.Iterator;

public interface MDPWSMessage  //TODO SSch extract Message interface in jmeds stack and derive this interface from base message interface 
{
	//Interface of the message context
	public abstract MDPWSMessageContextMap getMessageContextMap(); 
	public abstract Iterator getMessageContexts();
	public abstract MDPWSMessageContext getMessageContext(Class<?> cls);		
	public abstract void addMessageContext(Class<?> cls, MDPWSMessageContext context);
	public abstract void removeMessageContext(Class<?> cls);	
}
