/**
 * 
 */
package com.draeger.medical.mdpws.message;

import org.ws4d.java.structures.HashMap;

/**
 *
 *
 */
public class MDPWSMessageContextMap extends HashMap{

	public MDPWSMessageContextMap() {
		super();
	}

	public MDPWSMessageContextMap(HashMap map) {
		super(map);
	}

	public MDPWSMessageContextMap(int initialCapacity) {
		super(initialCapacity);
	}

}
