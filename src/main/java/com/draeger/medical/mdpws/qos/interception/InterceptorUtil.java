/**
 * 
 */
package com.draeger.medical.mdpws.qos.interception;

import org.ws4d.java.communication.protocol.soap.generator.MessageReceiver;
import org.ws4d.java.message.IMessageEndpoint;

/**
 *
 *
 */
public class InterceptorUtil {
	
	/**
	 * @param to
	 * @param action
	 * @return
	 */
	public static org.ws4d.java.schema.Element getInboundElement(MessageReceiver to,
			String action) 
	{
		org.ws4d.java.schema.Element e=null;
		if (to!=null)
		{
			IMessageEndpoint endpoint= to.getOperation(action);
			
			if (endpoint!=null)
				e=endpoint.getInput();
			
		}
		return e;
	}
	
	/**
	 * @param to
	 * @param action
	 * @return
	 */
	public static org.ws4d.java.schema.Element getOutboundElement(MessageReceiver to,
			String action) 
	{
		org.ws4d.java.schema.Element e=null;
		if (to!=null)
		{
			IMessageEndpoint endpoint= to.getOperation(action);
			
			if (endpoint!=null)
				e=endpoint.getOutput();
			
		}
		return e;
	}
}
