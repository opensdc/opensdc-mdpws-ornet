/**
 * 
 */
package com.draeger.medical.mdpws.qos.dualchannel;

import org.w3c.dom.Node;
import org.ws4d.java.service.parameter.IParameterValue;


/**
 *
 *
 */
public interface DualChannelLocal2MDPWSConverter 
{
	//object is in local representation
	//public abstract Object createChannelValueFromBytes(byte[] serializedData, DualChannelPolicyAttributes attributes);
	
	//the object is in local representation
	public abstract Object createObjectFromParameterValue(IParameterValue pv,DualChannelPolicyAttributes dualChannelPolicyAttributes);

	public abstract IParameterValue getParameterValue(Object channelValue, DualChannelPolicyAttributes policyAttributes, Node exemplaryChannelDomNode);


	/**
	 * Channel value in the same representation as the first channel, e.g. fc=1 & sc=1 ==> convertSecondChannel2LocalRepresentation(sc,something) ==> sc=42.
	 * 
	 * @param channel 
	 * @param policyAttributes
	 * @return
	 */
	public abstract Object convertSecondChannel2LocalRepresentation(
			Object channel,
			DualChannelPolicyAttributes policyAttributes);



}
