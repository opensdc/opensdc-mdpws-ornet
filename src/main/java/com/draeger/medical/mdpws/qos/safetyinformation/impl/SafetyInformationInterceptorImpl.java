/**
 * 
 */
package com.draeger.medical.mdpws.qos.safetyinformation.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.protocol.soap.generator.DefaultBasicTypes2SOAPConverter;
import org.ws4d.java.communication.protocol.soap.generator.DefaultParameterValue2SOAPSerializer;
import org.ws4d.java.communication.protocol.soap.generator.Message2SOAPConverter;
import org.ws4d.java.communication.protocol.soap.generator.Message2SOAPGenerator;
import org.ws4d.java.communication.protocol.soap.generator.MessageReceiver;
import org.ws4d.java.communication.protocol.soap.generator.SOAP2MessageGenerator;
import org.ws4d.java.communication.protocol.soap.generator.SOAPMessageGeneratorFactory;
import org.ws4d.java.constants.XMLConstants;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.StringValue;
import org.ws4d.java.service.parameter.StringValueFactory;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.HashMap.Entry;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.structures.Set;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.util.Log;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.draeger.medical.mdpws.common.util.SOAPNameSpaceContext;
import com.draeger.medical.mdpws.common.util.XPathInfo;
import com.draeger.medical.mdpws.common.util.XPathNamespaceContext;
import com.draeger.medical.mdpws.qos.QoSMessageContext;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.QoSPolicyExceptionHandlerManager;
import com.draeger.medical.mdpws.qos.interception.InboundSOAPXMLDocumentInterceptor;
import com.draeger.medical.mdpws.qos.interception.InterceptionException;
import com.draeger.medical.mdpws.qos.interception.InterceptorUtil;
import com.draeger.medical.mdpws.qos.interception.OutboundSOAPUTF8TransformationInterceptor;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptionDirection;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyOrdinalNumber;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyToken;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicy;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicyAttributes;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicyInterceptor;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicyToken;
import com.draeger.medical.mdpws.qos.safetyinformation.def.DualChannelSelector;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelComparator;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelLocal2MDPWSConverter;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelProtocolConverter;
import com.draeger.medical.mdpws.qos.safetyinformation.transmission.DualChannel;
import com.draeger.medical.mdpws.qos.safetyinformation.transmission.SafetyContext;
import com.draeger.medical.mdpws.qos.safetyinformation.transmission.SafetyContextValue;
import com.draeger.medical.mdpws.qos.safetyinformation.transmission.SafetyInformation;
import com.draeger.medical.mdpws.qos.security.SecurityEngine;
import com.draeger.medical.mdpws.qos.subjects.MessagePolicySubject;
import com.draeger.medical.mdpws.qos.subjects.OperationPolicySubject;
import com.draeger.medical.mdpws.qos.subjects.ServicePolicySubject;
import com.draeger.medical.mdpws.qos.wsdl.SafetyInformationConstants;



/**
 *
 *
 */
public class SafetyInformationInterceptorImpl implements
SafetyInformationPolicyInterceptor,
OutboundSOAPUTF8TransformationInterceptor,
InboundSOAPXMLDocumentInterceptor {
	private DocumentBuilderFactory docBuilderFactory;
	private DocumentBuilder builder;
	private TransformerFactory tFactory;
	private Transformer transformer;

	private final ArrayList associatedPolicies = new ArrayList();
	private final ArrayList associatedSubjectClasses = new ArrayList();
	private final XPathNamespaceContext nameSpaceContext = new SOAPNameSpaceContext();
	private final XPathInfo soapHeaderXPathInfo = new XPathInfo("//s12:Header",nameSpaceContext);
	private final XPathInfo dualChannelValuesHeaderXPathInfo = new XPathInfo("//s12:Header/si:SafetyInfo/si:DualChannel/si:DCValue",nameSpaceContext);
	private final XPathInfo siCtxtValuesHeaderXPathInfo = new XPathInfo("//s12:Header/si:SafetyInfo/si:SafetyContext",nameSpaceContext);

	private final static XmlPullParserFactory XPP_FACTORY;





	static
	{
		XmlPullParserFactory factory = null;
		try
		{
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
		}
		catch (XmlPullParserException e)
		{
			Log.error("Could not create XmlPullParserFactory: " + e);
			e.printStackTrace();
			throw new RuntimeException("Could not create XmlPullParserFactory: " + e);
		}

		XPP_FACTORY = factory;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.interception.OutboundSOAPUTF8TransformationInterceptor#interceptOutbound(java.io.InputStream, org.ws4d.java.communication.ProtocolData, com.draeger.medical.mdpws.qos.QoSMessageContext, com.draeger.medical.mdpws.qos.QoSPolicy, java.io.OutputStream)
	 */
	public SafetyInformationInterceptorImpl() {
		super();
		this.docBuilderFactory = DocumentBuilderFactory.newInstance();
		this.docBuilderFactory.setNamespaceAware(true);
		// Use a Transformer for output
		this.tFactory = TransformerFactory.newInstance();

		//tick the security engine
		SecurityEngine.getInstance().tick();

		nameSpaceContext.addNamespace(SafetyInformationConstants.SI_DEFAULT_NAMESPACE_PREFIX, SafetyInformationConstants.SI_NAMESPACE);

		associatedPolicies.add(InOutboundSafetyInformationQoSPolicy.class);
		associatedPolicies.add(InboundSafetyInformationQoSPolicy.class);
		associatedPolicies.add(OutboundSafetyInformationQoSPolicy.class);
		associatedSubjectClasses.add(ServicePolicySubject.class);
		associatedSubjectClasses.add(OperationPolicySubject.class);
		associatedSubjectClasses.add(MessagePolicySubject.class);
	}

	@Override
	public boolean interceptOutbound(
			InputStream interceptableSOAPUTF8InputStream, ProtocolData protocolData,
			QoSMessageContext qosMsgCtxt, QoSPolicy policy,
			OutputStream soapUTF8OutputStream, org.ws4d.java.schema.Element schemaElement) throws InterceptionException {

		if (policy instanceof SafetyInformationPolicy) {
			SafetyInformationPolicy siPolicy = (SafetyInformationPolicy) policy;
			try{
				Document inputDocument = getDocumentBuilder().parse(new InputSource(interceptableSOAPUTF8InputStream));
				try {


					Document siPreparedDocument = prepareDocumentForSafetyInformation(inputDocument, qosMsgCtxt);

					if (Log.isDebug()) {
						String outputString = org.apache.ws.security.util.XMLUtils.PrettyDocumentToString(siPreparedDocument);
						Log.debug("SafetyInformation prepared Document:"+outputString);
						Log.debug("Add Safety Information to message [Policy: " + policy+ "]");
					}


					Node soapHeader = getSOAPHeaderNode(siPreparedDocument);

					if (soapHeader != null) {
						appendSafetyInformationNode(siPreparedDocument, soapHeader,siPolicy, qosMsgCtxt, protocolData, schemaElement);
					}

					//Creating the resulting document and stream it to the soapUTF8OutputStream
					DOMSource source = new DOMSource(siPreparedDocument);
					StreamResult result = new StreamResult(soapUTF8OutputStream);
					getTransformer().transform(source, result);

				} catch (Exception e) {
					callQoSExceptionHandler(siPolicy, inputDocument, protocolData, qosMsgCtxt, null, "Could not add safety info! "+e.getMessage(), e);

					DOMSource source = new DOMSource(inputDocument);
					StreamResult result = new StreamResult(soapUTF8OutputStream);
					getTransformer().transform(source, result);
				}
			}catch(Exception e)
			{
				Log.error("Fatal error! "+e.getMessage());
			}
		}

		return false;
	}

	/**
	 * @param siPreparedDocument 
	 * @param policy 
	 * @param qosMsgCtxt 
	 * @param protocolData 
	 * @param iMessageEndpoint 
	 * @return
	 * @throws Exception 
	 */
	private Node appendSafetyInformationNode(Document siPreparedDocument,
			Node soapHeader, SafetyInformationPolicy policy,
			QoSMessageContext qosMsgCtxt, ProtocolData protocolData, org.ws4d.java.schema.Element element) throws Exception {


		HashMap declaredNameSpaces = XPathInfo.getDeclaredNamesSpaces(siPreparedDocument);

		String currentSINamespacePrefix = (String) declaredNameSpaces.get(SafetyInformationConstants.SI_NAMESPACE);
		if (currentSINamespacePrefix == null) {
			currentSINamespacePrefix = SafetyInformationConstants.SI_DEFAULT_NAMESPACE_PREFIX;
			declaredNameSpaces.put(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix);
			siPreparedDocument.getDocumentElement().setAttributeNS(XMLConstants.XMLNS_NAMESPACE_NAME,XMLConstants.XMLNS_NAMESPACE_PREFIX+":"+currentSINamespacePrefix,SafetyInformationConstants.SI_NAMESPACE);
		}

		XPathNamespaceContext ctxt= XPathInfo.createXPathSpaceContext(declaredNameSpaces);

		Element siTransmissionSOAPHeaderElement = siPreparedDocument.createElementNS(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix + ":"+SafetyInformationConstants.SI_ELEM_TRANSM_SAFETY_INFO);
		soapHeader.appendChild(siTransmissionSOAPHeaderElement);

		Iterator qosTokens = qosMsgCtxt.getQoSPolicyToken();
		boolean dualChannelHeaderHasContent=false;

		if (Log.isDebug()) Log.debug("QoSTokens available: "+qosTokens.hasNext());

		while (qosTokens.hasNext()) {
			try{
				Object token = qosTokens.next();

				if (token instanceof SafetyInformationPolicyToken) {

					@SuppressWarnings("unchecked")
					SafetyInformationPolicyToken<SafetyInformation, SafetyInformationPolicy> safetyInformationPolicyToken = (SafetyInformationPolicyToken<SafetyInformation, SafetyInformationPolicy>) token;

					if (!policy.equals(safetyInformationPolicyToken.getQoSPolicy()))
					{
						if (Log.isDebug()) Log.debug(policy+" "+safetyInformationPolicyToken.getQoSPolicy());

						continue;
					}

					SafetyInformationPolicyAttributes attributes = policy.getSafetyInformationPolicyAttributes();
					QName transformQName = attributes.getTransform();
					QName algorithmQName = attributes.getAlgorithm();

					//TODO SSch DualChannel
					//XPathInfo xpath = createIdentifyingXPath(attributes.getXPath(),safetyInformationPolicyToken.getValue().getDualChannels().get(0).getIdentifyingInformation());
					HashMap messageIDMap=null;
					if (attributes.getDualChannelSelectors()!=null && attributes.getDualChannelSelectors().size()>0)
					{
						messageIDMap=augmentMessageBodyWithIDs(attributes.getDualChannelSelectors(),siPreparedDocument, currentSINamespacePrefix);	
					}

					if (Log.isDebug())Log.debug("Created identifying IDs: "+(messageIDMap!=null?messageIDMap.size():0));

					if (messageIDMap!=null && !messageIDMap.isEmpty())
					{

						Element dualChannelTransmissionElement = siPreparedDocument.createElementNS(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix + ":"+SafetyInformationConstants.SI_ELEM_TRANSM_DUALCHANNEL);
						siTransmissionSOAPHeaderElement.appendChild(dualChannelTransmissionElement);

						Set dualChannelItemsEntries = messageIDMap.entrySet();
						for (Iterator dualChannelItemsIterator=dualChannelItemsEntries.iterator();dualChannelItemsIterator.hasNext();)
						{
							Entry dualChannelItem = (Entry) dualChannelItemsIterator.next();
							String documentID=(String) dualChannelItem.getKey();
							DualChannelSelector selector=(DualChannelSelector) dualChannelItem.getValue();

							String dualChannelFirstValueXPathQuery="//*[@"+currentSINamespacePrefix+":"+SafetyInformationConstants.SI_ATTRIB_TRANSM_ID+"='"+documentID+"']/text()"; 
							dualChannelFirstValueXPathQuery=transformXPathExpression(null, dualChannelFirstValueXPathQuery,declaredNameSpaces,ctxt);
							XPathInfo xPathInfoForFirstChannel = new XPathInfo(dualChannelFirstValueXPathQuery, ctxt);

							if (Log.isDebug()) Log.debug("DualChannel-XML: " + org.apache.ws.security.util.XMLUtils.PrettyDocumentToString(siPreparedDocument));
							TransformedChannelInfos transformedFirstChannelInfos = getTransformedFirstChannelInfosFromDocument(siPreparedDocument, xPathInfoForFirstChannel, policy);

							//TODO SSch Sollte eigentlich genau ein Knoten sein, oder?
							Set transformedSet= transformedFirstChannelInfos.entrySet();

							if (!transformedSet.isEmpty())
							{
								for (Iterator transformedNodes=transformedSet.iterator();transformedNodes.hasNext();)
								{
									Entry transformedNodesEntry=(Entry) transformedNodes.next();

									byte[] serializedData=(byte[]) transformedNodesEntry.getValue();

									//System.out.println(new String(serializedData,"UTF-8"));

									//create the value elements
									Element value = siPreparedDocument.createElementNS(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix + ":"+SafetyInformationConstants.SI_ELEM_TRANSM_DUALCHANNEL_VALUE);


									addDualChannelQNameAttribute(SafetyInformationConstants.SI_NAMESPACE, value,
											algorithmQName, declaredNameSpaces,ctxt, currentSINamespacePrefix
											+ ":"+SafetyInformationConstants.SI_ATTRIB_DUALCHANNEL_ALGORITHM);

									addDualChannelQNameAttribute(SafetyInformationConstants.SI_NAMESPACE, value,
											transformQName, declaredNameSpaces,ctxt, currentSINamespacePrefix
											+ ":"+SafetyInformationConstants.SI_ATTRIB_DUALCHANNEL_TRANSFORM);


									addDualChannelURIAttributes(SafetyInformationConstants.SI_NAMESPACE, value, documentID, currentSINamespacePrefix + ":"+SafetyInformationConstants.SI_ATTRIB_TRANSM_URI);

									//TODO SSch add //s12:body where necessary???
									if (isFirstChannelValid(safetyInformationPolicyToken, serializedData, policy, element, selector)) 
									{
										String secChannelRepresentation = computeSecondChannelRepresentation(serializedData, policy);
										value.setTextContent(secChannelRepresentation);
										dualChannelTransmissionElement.appendChild(value);
										dualChannelHeaderHasContent=true;
									} else {
										String outputString = org.apache.ws.security.util.XMLUtils.PrettyDocumentToString(siPreparedDocument);
										callQoSExceptionHandler(policy,siPreparedDocument,protocolData,qosMsgCtxt,safetyInformationPolicyToken,"Required 1st channel not valid. ["+outputString+"] ["+transformedFirstChannelInfos+"] ["+policy+"]",null);
									}
								}
							}else{
								callQoSExceptionHandler(policy, siPreparedDocument, protocolData, qosMsgCtxt,safetyInformationPolicyToken,"No such element in message! "+selector.getSelectedElementInMessage(),null);
							}
						}//end of while
					}else{
						//No Selectors
					}

					if ( safetyInformationPolicyToken.getValue()!=null && safetyInformationPolicyToken.getValue().getSafetyContexts()!=null)
					{
						List<SafetyContext> safetyContexts = safetyInformationPolicyToken.getValue().getSafetyContexts();
						int safetyContextsCnt= safetyContexts.size();
						if (safetyContextsCnt!=0)
						{
							java.util.HashMap<String, SafetyContext> safetyContextMap=new java.util.HashMap<String, SafetyContext> ();
							java.util.HashMap<String, Element> safetyContextDOMElementMap=new java.util.HashMap<String, Element> ();
							if (Log.isInfo())Log.info("Safety Contexts Cnt:"+safetyContextsCnt);

							for (SafetyContext safetyContext : safetyContexts) {
								SafetyContext mergedSafetyContext = safetyContextMap.get(safetyContext.getTargetIdentifier());
								Element safetyContextSOAPHeaderElement =safetyContextDOMElementMap.get(safetyContext.getTargetIdentifier());
								if (mergedSafetyContext==null)
								{

									safetyContextSOAPHeaderElement = siPreparedDocument.createElementNS(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix + ":"+SafetyInformationConstants.SI_ELEM_TRANSM_SAFETYCTXT);
									safetyContextSOAPHeaderElement.setAttributeNS(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix+":"+SafetyInformationConstants.SI_ATTRIB_TARGET, safetyContext.getTargetIdentifier());
									siTransmissionSOAPHeaderElement.appendChild(safetyContextSOAPHeaderElement);
									mergedSafetyContext=safetyContext;
									safetyContextMap.put(safetyContext.getTargetIdentifier(), mergedSafetyContext);
									safetyContextDOMElementMap.put(safetyContext.getTargetIdentifier(), safetyContextSOAPHeaderElement);
								}
								if (safetyContextSOAPHeaderElement!=null)
								{
									if (safetyContext.getValues()!=null)
									{
										for(SafetyContextValue val: safetyContext.getValues())
										{
											if (safetyContext==mergedSafetyContext || !mergedSafetyContext.containsReferencedElement(val.getReferencedElementQName()))
											{
												Element contextValueSOAPHeaderElement = siPreparedDocument.createElementNS(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix + ":"+SafetyInformationConstants.SI_ELEM_TRANSM_SCTXT_VALUE);
												safetyContextSOAPHeaderElement.appendChild(contextValueSOAPHeaderElement);
												addDualChannelQNameAttribute(SafetyInformationConstants.SI_NAMESPACE,contextValueSOAPHeaderElement, val.getReferencedElementQName(),declaredNameSpaces,ctxt, currentSINamespacePrefix+ ":"+SafetyInformationConstants.SI_ATTRIB_TRANSM_SELECTOR_REF);
												ByteArrayOutputStream baos=new ByteArrayOutputStream();
												getXMLFragment(val.getCtxtParameterValue(),baos);
												appendXmlFragment(getDocumentBuilder(),contextValueSOAPHeaderElement,baos.toString(XMLConstants.ENCODING));
											}else{
												if (Log.isWarn())Log.warn("Ignoring duplicate safety context value for "+val.getReferencedElementQName()+".");
											}
										}
									}
								}
							}


							//							for (SafetyContext safetyContext : safetyContexts) {
							//								Element safetyContextSOAPHeaderElement = siPreparedDocument.createElementNS(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix + ":"+SafetyInformationConstants.SI_ELEM_TRANSM_SAFETYCTXT);
							//								safetyContextSOAPHeaderElement.setAttributeNS(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix+":"+SafetyInformationConstants.SI_ATTRIB_TARGET, safetyContext.getTargetIdentifier());
							//								siTransmissionSOAPHeaderElement.appendChild(safetyContextSOAPHeaderElement);
							//
							//								for(SafetyContextValue val: safetyContext.getValues())
							//								{
							//									Element contextValueSOAPHeaderElement = siPreparedDocument.createElementNS(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix + ":"+SafetyInformationConstants.SI_ELEM_TRANSM_SCTXT_VALUE);
							//									safetyContextSOAPHeaderElement.appendChild(contextValueSOAPHeaderElement);
							//									addDualChannelQNameAttribute(SafetyInformationConstants.SI_NAMESPACE,contextValueSOAPHeaderElement, val.getReferencedElementQName(),declaredNameSpaces,ctxt, currentSINamespacePrefix+ ":"+SafetyInformationConstants.SI_ATTRIB_TRANSM_SELECTOR_REF);
							//									ByteArrayOutputStream baos=new ByteArrayOutputStream();
							//									getXMLFragment(val.getCtxtParameterValue(),baos);
							//									appendXmlFragment(getDocumentBuilder(),contextValueSOAPHeaderElement,baos.toString(XMLConstants.ENCODING));
							//								}
							//
							//							}
						}else{
							if (Log.isDebug())Log.debug("No safety context provided.");
						}
					}



				}
			}catch(Exception e)
			{
				callQoSExceptionHandler(policy,siPreparedDocument,protocolData,qosMsgCtxt,null,"Could not add Dual channel node! "+e.getMessage(),e);
			}

		}

		if (!dualChannelHeaderHasContent)
			soapHeader.removeChild(siTransmissionSOAPHeaderElement);

		if (Log.isDebug()) Log.debug("Final Document after Safety Information should have si content "+dualChannelHeaderHasContent+": " + org.apache.ws.security.util.XMLUtils.PrettyDocumentToString(siPreparedDocument));

		return siTransmissionSOAPHeaderElement;
	}

	private void getXMLFragment(IParameterValue parameterValue, OutputStream os) throws IOException 
	{
		DefaultBasicTypes2SOAPConverter btc = new DefaultBasicTypes2SOAPConverter();
		DefaultParameterValue2SOAPSerializer converter = new DefaultParameterValue2SOAPSerializer(btc);
		converter.serializeParameterValue(parameterValue, os, null);  
	}

	public static void appendXmlFragment(DocumentBuilder docBuilder, Node parent, String fragment) throws IOException, SAXException {
		Document doc = parent.getOwnerDocument();
		Node fragmentNode = docBuilder.parse(new InputSource(new StringReader(fragment))).getDocumentElement();
		fragmentNode = doc.importNode(fragmentNode, true);
		parent.appendChild(fragmentNode);
	}

	private HashMap augmentMessageBodyWithIDs(java.util.ArrayList<DualChannelSelector> dualChannelSelectors, Document siPreparedDocument, String currentSINamespacePrefix) throws XPathExpressionException {
		HashMap result=new HashMap();

		for (DualChannelSelector dualChannelSelector : dualChannelSelectors) {
			XPathInfo idXPath= dualChannelSelector.getSelectedElementInMessage();
			org.w3c.dom.NodeList  selectedNodes=(NodeList) idXPath.evaluate(siPreparedDocument,XPathConstants.NODESET);

			int nodeCnt=0;
			if (selectedNodes!=null && (nodeCnt=selectedNodes.getLength())>0)
			{
				String idStr="sid-";
				for(int selectedNodeCnt=0;selectedNodeCnt<nodeCnt;selectedNodeCnt++)
				{
					Node firstChannelValueNode=selectedNodes.item(selectedNodeCnt);
					if (firstChannelValueNode instanceof Element)
					{
						Element firstChannelValueElem=(Element)firstChannelValueNode;
						String id=idStr+selectedNodeCnt;
						result.put(id, dualChannelSelector);
						firstChannelValueElem.setAttributeNS(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix+":"+SafetyInformationConstants.SI_ATTRIB_TRANSM_ID, id);
					}
				}
			}
		}

		return result;
	}

	//	/**
	//	 * @param xPath
	//	 * @param object 
	//	 * @return
	//	 */
	//	private XPathInfo createIdentifyingXPath(XPathInfo xPath, Object identifyingInformation) {
	//		XPathInfo xpi=xPath;
	//		if (identifyingInformation instanceof XPathInfo)
	//		{
	//			XPathInfo idXPath=(XPathInfo)identifyingInformation;
	//			String idXPExpr=idXPath.getExpression().trim();
	//			if (!idXPExpr.startsWith("[")&& idXPExpr.endsWith("]"))
	//			{
	//				idXPExpr="["+idXPExpr+"]";
	//			}
	//
	//			if (idXPExpr.length()<3)
	//			{
	//				if (Log.isDebug())
	//					Log.debug("XPATH predicate "+idXPExpr+" not valid.");
	//
	//				throw new IllegalArgumentException("XPATH predicate "+idXPExpr+" not valid.");
	//			}
	//			if (Log.isDebug())
	//				Log.debug("ID predicate:"+idXPExpr);
	//
	//			String expression="("+xPath.getExpression()+")"+idXPExpr;
	//			XPathNamespaceContext context= xPath.getContext();
	//			context.addNamespaceContext(idXPath.getContext());
	//			xpi=new XPathInfo(expression, context);
	//		}
	//		return xpi;
	//	}

	/**
	 * @param firstChannelInfos
	 * @param policy
	 * @return
	 */
	private String computeSecondChannelRepresentation(
			byte[] serializedData,
			SafetyInformationPolicy policy) {
		DualChannelProtocolConverter converter = policy.getDualChannelProtocolConverter(policy.getSafetyInformationPolicyAttributes());
		return converter.getDualChannelRepresentation(serializedData);
	}

	/**
	 * @param safetyInformationPolicyToken
	 * @param policy 
	 * @param selector 
	 * @param transformedFirstChannelInfos 
	 * @param msgEndpoint 
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings("rawtypes")
	private boolean isFirstChannelValid(
			SafetyInformationPolicyToken<SafetyInformation, SafetyInformationPolicy> safetyInformationPolicyToken,
			byte[] serializedData,
			SafetyInformationPolicy policy, org.ws4d.java.schema.Element element, DualChannelSelector selector) throws Exception {
		DualChannelLocal2MDPWSConverter converter = policy.getDualChannelLocalConverter();

		boolean retVal = false;
		IParameterValue pv=createPVFromBytes(serializedData,policy.getSafetyInformationPolicyAttributes(), element);
		Object computedFirstChannel =converter.createObjectFromParameterValue(pv,policy.getSafetyInformationPolicyAttributes());
		SafetyInformation safetyInformation = safetyInformationPolicyToken.getValue();
		DualChannelComparator comparator = policy.getDualChannelComparator();

		//TODO SSch more than one dual Channel???
		//if (safetyInformation.getDualChannels()!=null && safetyInformation.getDualChannels().size()>0)
		//	retVal = comparator.compare(computedFirstChannel, safetyInformation.getDualChannels().get(0));

		if (safetyInformation.getDualChannels()!=null && safetyInformation.getDualChannels().size()>0)
		{
			for (DualChannel dc : safetyInformation.getDualChannels()) {
				if (selector.getElementQName().equals(dc.getIdentifyingInformation())) {
					retVal = comparator.compare(computedFirstChannel, dc);break;
				}
			}

		}



		return retVal;
	}

	/**
	 * @param serializedData
	 * @param dualChannelPolicyAttributes
	 * @param msgEndpoint 
	 * @return
	 * @throws XmlPullParserException 
	 */
	private IParameterValue createPVFromBytes(byte[] serializedData,
			SafetyInformationPolicyAttributes dualChannelPolicyAttributes, org.ws4d.java.schema.Element element) throws Exception {
		return createParameterValueFromByteArray(serializedData,element);
	}

	private IParameterValue createParameterValueFromByteArray(byte[] serializedData, org.ws4d.java.schema.Element element) throws Exception {
		IParameterValue retVal=null;
		String val=null;
		if (serializedData!=null)
			val=new String(serializedData,XMLConstants.ENCODING);

		if (Log.isDebug())
			Log.debug("Create PV from: "+val);

		if (element!=null)
		{
			//Determine if the string contains valid xml...
			if (val!=null && val.startsWith("<"))
			{
				XmlPullParser parser = XPP_FACTORY.newPullParser();
				parser.setInput(new StringReader(val));

				if (parser.getEventType()==XmlPullParser.START_DOCUMENT)
				{
					parser.nextTag();
				}

				if (val.startsWith("<xml"))
					parser.nextTag(); // Skip <xml...>

				SOAP2MessageGenerator generator= SOAPMessageGeneratorFactory.getInstance().getSOAP2MessageGeneratorForCurrentThread();



				retVal= generator.getParameterValueParser().parseParameterValue(new ElementParser(parser,null),element);

				//				if (retVal instanceof ParameterDefinition)
				//				{
				//					t.add(retVal);
				//					retVal=t;
				//				}
				SOAPMessageGeneratorFactory.getInstance().releaseSOAP2MessageGenerator(generator);
			}else if (val!=null){
				retVal=StringValueFactory.getInstance().getStringValue();
				((StringValue)retVal).set(val); 
			}
		}
		return retVal;
	}

	/**
	 * @param dualChannelDoc
	 * @param xpath
	 * @param object 
	 * @return
	 * @throws XPathExpressionException 
	 */
	private TransformedChannelInfos getTransformedFirstChannelInfosFromDocument(Document dualChannelDoc, XPathInfo xpath, SafetyInformationPolicy policy) throws Exception {

		TransformedChannelInfos channelInfos = new TransformedChannelInfos();

		if (xpath!=null)
		{
			Object xpathResult=xpath.evaluate(dualChannelDoc);	
			Node[] nodes=getNodes(xpathResult,dualChannelDoc);
			if (nodes != null && nodes.length > 0) 
			{
				transformFirstChannelNodes( policy, channelInfos, nodes);
			}
		}


		return channelInfos;
	}

	private Node[] getNodes(Object xpathResult, Document doc)
	{
		Node[] nodes=null;

		NodeList nodeList=null;
		if (xpathResult!=null)
		{
			if (xpathResult instanceof NodeList)
			{
				nodeList=(NodeList)xpathResult;
			}else if (xpathResult instanceof String){
				Text t=doc.createTextNode((String)xpathResult);
				nodes=new Node[]{t};
			}else{
				// TODO SSch other xpath result types...
				Log.warn("Could not handle xpath result");
			}
		}

		if (nodeList != null && nodeList.getLength() > 0) {
			nodes=new Node[nodeList.getLength()];
			for(int j=0;j<nodeList.getLength();j++)
			{
				nodes[j]=nodeList.item(j);
			}
		}

		return nodes;
	}

	/**
	 * @param xpath
	 * @param policy
	 * @param channelInfos
	 * @param nodes
	 */
	protected void transformFirstChannelNodes(
			SafetyInformationPolicy policy, TransformedChannelInfos channelInfos,
			Node[] nodes) {
		if (nodes != null && nodes.length > 0) {

			//			if (nodes.length > 1)
			//				throw new IllegalArgumentException(
			//				"NodeList contains more than one node.");

			DualChannelProtocolConverter transformator = policy.getDualChannelProtocolConverter(policy.getSafetyInformationPolicyAttributes());

			if (transformator == null)
				throw new IllegalStateException("Could not get a transformator "+ policy.getSafetyInformationPolicyAttributes().getTransform());

			transformator.transformForSerialization(nodes, channelInfos);
		}
	}

	/**
	 * @param dualChannelNS
	 * @param value
	 * @param xpath
	 * @param declaredNameSpaces
	 * @param attributeNamePrefixed
	 */
	private void addDualChannelURIAttributes(String dualChannelNS,
			Element value, String referencedID, String attributeNamePrefixed) 
	{
		value.setAttributeNS(dualChannelNS, attributeNamePrefixed,referencedID);
	}

	/**
	 * @param value
	 * @param xPathInfo
	 * @param declaredNameSpaces
	 * @param ctxt
	 * @return
	 */
	protected String transformXPathExpression(Element value,
			String expr, HashMap declaredNameSpaces,
			XPathNamespaceContext ctxt) {
		if (expr!=null && ctxt!=null)
		{
			java.util.Iterator<java.util.Map.Entry<String, String>> entries= ctxt.getAllEntries();
			while(entries.hasNext())
			{
				java.util.Map.Entry<String, String> entry=entries.next();
				String prefixOrig=entry.getKey();
				if (expr.contains(prefixOrig+":"))
				{
					String prefix=getNameSpacePrefix(value,declaredNameSpaces,entry.getValue(),ctxt);

					if (Log.isDebug())
						Log.debug("Replace "+prefixOrig+" with "+prefix+" "+entry.getValue()+" in "+expr);

					expr=expr.replace(prefixOrig+":", prefix+":");
				}
			}
		}
		return expr;
	}

	/**
	 * @param dualChannelNS
	 * @param attrValueNSPrefix
	 * @param valueElement
	 * @param declaredNameSpaces 
	 * @param ctxt 
	 * @param attributeNamePrefixed 
	 */
	protected void addDualChannelQNameAttribute(String dualChannelNS,
			Element valueElement, QName attrValue, HashMap declaredNameSpaces,
			XPathNamespaceContext ctxt, String attributeNamePrefixed) {
		String nameSpace = attrValue.getNamespace();
		String nameSpacePrefix = getNameSpacePrefix(valueElement, declaredNameSpaces,nameSpace, ctxt);

		valueElement.setAttributeNS(dualChannelNS,attributeNamePrefixed,(nameSpace == null ? "" : (nameSpacePrefix + ":"))+ attrValue.getLocalPart());
	}

	/**
	 * @param valueElement
	 * @param declaredNameSpaces
	 * @param nameSpace
	 * @param ctxt 
	 * @return
	 */
	protected String getNameSpacePrefix(Element valueElement,
			HashMap declaredNameSpaces, String nameSpace, XPathNamespaceContext ctxt) {
		//		String nameSpacePrefix = (String) declaredNameSpaces.get(nameSpace);
		String nameSpacePrefix=null;
		if (valueElement!=null && valueElement.getParentNode()!=null){nameSpacePrefix=valueElement.getParentNode().lookupPrefix(nameSpace);}
		else{nameSpacePrefix=(String) declaredNameSpaces.get(nameSpace);}

		if ((nameSpacePrefix == null || XMLConstants.XMLNS_NAMESPACE_PREFIX.equals(nameSpacePrefix)) && nameSpace != null && valueElement!=null) 
		{
			if (ctxt!=null){
				nameSpacePrefix= ctxt.getPrefix(nameSpace);
			}
			if (nameSpacePrefix==null || XMLConstants.XMLNS_NAMESPACE_PREFIX.equals(nameSpacePrefix)) nameSpacePrefix = "i" + (System.nanoTime()+""+PlatformSupport.getInstance().getToolkit().getRandom()).hashCode();
			declaredNameSpaces.put(nameSpace, nameSpacePrefix);

			//for the attribute qname value we have to explicitly add the namespace
			valueElement
			.setAttributeNS(XMLConstants.XMLNS_NAMESPACE_NAME,
					XMLConstants.XMLNS_NAMESPACE_PREFIX + ":"
					+ nameSpacePrefix, nameSpace);
		}
		return nameSpacePrefix;
	}

	/**
	 * @param dualChannelDoc
	 * @return
	 * @throws XPathExpressionException 
	 */
	private Node getSOAPHeaderNode(Document dualChannelDoc)
	throws XPathExpressionException {
		return (Node) soapHeaderXPathInfo.evaluate(dualChannelDoc,XPathConstants.NODE);
	}

	private NodeList getDualChannelValueNodes(Document dualChannelDoc)
	throws XPathExpressionException {
		return (NodeList) dualChannelValuesHeaderXPathInfo.evaluate(dualChannelDoc,XPathConstants.NODESET);
	}

	private NodeList getSafetyCtxtNodes(Document dualChannelDoc)
	throws XPathExpressionException {
		return (NodeList) siCtxtValuesHeaderXPathInfo.evaluate(dualChannelDoc,XPathConstants.NODESET);
	}

	/**
	 * @param inputDocument
	 * @param qosMsgCtxt
	 * @return
	 */
	private Document prepareDocumentForSafetyInformation(Document inputDocument,
			QoSMessageContext qosMsgCtxt) {
		return inputDocument;
	}

	private synchronized DocumentBuilder getDocumentBuilder()
	throws ParserConfigurationException {
		if (this.builder == null)
			this.builder = this.docBuilderFactory.newDocumentBuilder();

		return this.builder;
	}

	private synchronized Transformer getTransformer()
	throws TransformerConfigurationException {
		if (this.transformer == null)
			this.transformer = tFactory.newTransformer();
		return this.transformer;
	}

	@Override
	public QoSPolicyOrdinalNumber getOrdinalNumberForSubject(Class<?> subjectClass) {
		if (ServicePolicySubject.class.isAssignableFrom(subjectClass))
			return new QoSPolicyOrdinalNumber(0);
		else
			return QoSPolicyOrdinalNumber.NOT_APPLICABLE;
	}

	@Override
	public Iterator getQoSPolicyClasses() {
		return new ReadOnlyIterator(associatedPolicies.iterator());
	}

	@Override
	public Iterator getInterceptorSubjectClasses() {
		return new ReadOnlyIterator(associatedSubjectClasses.iterator());
	}

	@Override
	public QoSPolicyInterceptionDirection getInterceptionDirection() {
		return QoSPolicyInterceptionDirection.OUTBOUND;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.interception.InboundSOAPXMLInputStreamInterceptor#interceptInbound(java.io.InputStream, org.ws4d.java.communication.DPWSProtocolData, org.ws4d.java.communication.protocol.soap.generator.MessageReceiver, com.draeger.medical.mdpws.qos.QoSMessageContext, com.draeger.medical.mdpws.qos.QoSPolicy)
	 */
	@Override
	public Document interceptInbound(Document inboundSOAPDocument,
			DPWSProtocolData protocolData, MessageReceiver to,
			QoSMessageContext qosMessageCtxt, QoSPolicy policy, String action)
	throws InterceptionException {

		Document xmlOutputDocument = inboundSOAPDocument;
		if (policy instanceof SafetyInformationPolicy)
		{
			SafetyInformationPolicy siPolicy=(SafetyInformationPolicy)policy;
			try {
				if (inboundSOAPDocument != null) {


					org.ws4d.java.schema.Element element=InterceptorUtil.getInboundElement(to,action);

					SafetyInformationPolicyAttributes policyAttributes=siPolicy.getSafetyInformationPolicyAttributes();
					if (policyAttributes==null) { if (Log.isWarn()) Log.warn("No Policy attributes available");}
					else if (policyAttributes.isTransmitDualChannel() || policyAttributes.isTransmitSafetyContext()){
						SafetyInformation si=new SafetyInformation();
						if (policyAttributes.isTransmitDualChannel())
						{
							@SuppressWarnings("rawtypes")
							List<DualChannel> dualChannels = extractDualChannelInformation(inboundSOAPDocument, siPolicy,policyAttributes, qosMessageCtxt, protocolData, element);

							if (dualChannels!=null && !dualChannels.isEmpty())
							{
								si.setDualChannels(dualChannels);
							}
						}
						if (policyAttributes.isTransmitSafetyContext())
						{
							List<SafetyContext> safetyContexts=extractSafetyContextInformation(inboundSOAPDocument, siPolicy,policyAttributes, qosMessageCtxt, protocolData, element);
							si.setSafetyContexts(safetyContexts);
						}
						SafetyInformationPolicyToken<SafetyInformation, SafetyInformationPolicy> token=new SafetyInformationPolicyToken<SafetyInformation, SafetyInformationPolicy>(siPolicy, si, QoSPolicyInterceptionDirection.INBOUND);
						qosMessageCtxt.addQoSPolicyToken(token);	
					}

					xmlOutputDocument = inboundSOAPDocument;
				}
			} catch (Exception ex) {
				callQoSExceptionHandler(policy,inboundSOAPDocument,protocolData,qosMessageCtxt,null,ex.getMessage(),ex);
				throw new InterceptionException(ex);
			}
		}

		return xmlOutputDocument;
	}





	/**
	 * @param inboundSOAPDocument
	 * @param policy
	 * @param policyAttributes 
	 * @param qosMessageCtxt 
	 * @param protocolData 
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings("rawtypes")
	private List<DualChannel> extractDualChannelInformation(
			Document inboundSOAPDocument, SafetyInformationPolicy policy, SafetyInformationPolicyAttributes policyAttributes, QoSMessageContext qosMessageCtxt, DPWSProtocolData protocolData, org.ws4d.java.schema.Element element) throws Exception {
		java.util.ArrayList<DualChannel> extractedDualChannels = new java.util.ArrayList<DualChannel>();



		NodeList valueNodes = getDualChannelValueNodes(inboundSOAPDocument);

		if (valueNodes!=null && valueNodes.getLength()>0)
		{
			String safetInformationNamespace = SafetyInformationConstants.SI_NAMESPACE;
			HashMap declaredNameSpaces = XPathInfo.getDeclaredNamesSpaces(inboundSOAPDocument);


			String currentSINamespacePrefix = (String) declaredNameSpaces.get(SafetyInformationConstants.SI_NAMESPACE);
			if (currentSINamespacePrefix == null) {
				currentSINamespacePrefix = SafetyInformationConstants.SI_DEFAULT_NAMESPACE_PREFIX;
				declaredNameSpaces.put(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix);
			}

			XPathNamespaceContext ctxt= XPathInfo.createXPathSpaceContext(declaredNameSpaces);

			int nodeCnt=valueNodes.getLength();
			for(int valueNodeCnt=0;valueNodeCnt<nodeCnt;valueNodeCnt++)
			{

				Node dualChannelValueNode=valueNodes.item(valueNodeCnt);
				NamedNodeMap attributes=dualChannelValueNode.getAttributes();
				QName algorithm=null;
				QName transform=null;
				XPathInfo xPathInfoForFirstChannel=null;

				String secondChannelValue=dualChannelValueNode.getTextContent();

				if (secondChannelValue==null)
				{
					callQoSExceptionHandler(policy,inboundSOAPDocument,protocolData,qosMessageCtxt,null,"Second Channel has no value !",null);
				}

				if (attributes!=null && attributes.getLength()>0)
				{
					int attrCnt=attributes.getLength();
					for (int a=0;a<attrCnt && (algorithm==null || transform==null || xPathInfoForFirstChannel==null);a++)
					{
						Attr attrNode=(Attr) attributes.item(a);
						String attrNamespaceURI = attrNode.getNamespaceURI();
						if (attrNamespaceURI==null || safetInformationNamespace.equals(attrNamespaceURI))
						{
							String tempValue=attrNode.getValue();
							if (SafetyInformationConstants.SI_ATTRIB_DUALCHANNEL_ALGORITHM.equals(attrNode.getLocalName()))
							{
								algorithm=parsePrefixValue(declaredNameSpaces, tempValue,dualChannelValueNode);
							}else if (SafetyInformationConstants.SI_ATTRIB_DUALCHANNEL_TRANSFORM.equals(attrNode.getLocalName())){
								transform=parsePrefixValue(declaredNameSpaces,tempValue,dualChannelValueNode);
							}else if (SafetyInformationConstants.SI_ATTRIB_TRANSM_URI.equals(attrNode.getLocalName())){
								String dualChannelFirstValueXPathQuery="//*[@"+currentSINamespacePrefix+":"+SafetyInformationConstants.SI_ATTRIB_TRANSM_ID+"='"+tempValue+"']/text()"; 
								dualChannelFirstValueXPathQuery=transformXPathExpression(null, dualChannelFirstValueXPathQuery,declaredNameSpaces,ctxt);
								xPathInfoForFirstChannel=new XPathInfo(dualChannelFirstValueXPathQuery, ctxt);
							}
						}
					}
				}
				if (Log.isDebug()) Log.debug("Dual Channel Information "+algorithm+" "+transform+" "+xPathInfoForFirstChannel+" "+dualChannelValueNode.getTextContent());


				//set default values if necessary
				if (algorithm==null)
					algorithm=SafetyInformationConstants.SI_ATTRIB_ALGORITHM_DUALCHANNEL_DEFAULT_VALUE;
				if (transform==null)
					transform=SafetyInformationConstants.SI_ATTRIB_TRANSFORM_DUALCHANNEL_DEFAULT_VALUE;
				if (xPathInfoForFirstChannel==null)
					xPathInfoForFirstChannel=new XPathInfo(null, new SOAPNameSpaceContext());

				//TODO SSch Per Message Filter
				//				String policyXPath=transformXPathExpression(null, policyAttributes.getXPath().getExpression(),declaredNameSpaces,policyAttributes.getXPath().getContext());
				//
				//				String xPathInfoModified=createModifiedXPathString(xPathInfoForFirstChannel.getExpression());
				//
				//				//compare attributes with policy attributes, validate content and recreate second channel
				//				if (!algorithm.equals(policyAttributes.getAlgorithm()) || !transform.equals(policyAttributes.getTransform())
				//						|| (!xPathInfoForFirstChannel.getExpression().equals(policyXPath) && !xPathInfoModified.equals(policyXPath)))
				//				{
				//					if (Log.isDebug())
				//						Log.debug(algorithm+" "+transform+" "+xPathInfoForFirstChannel+" does mot match "+policyAttributes+" "+policyXPath);
				//
				//					continue;
				//				}

				//get the referenced content nodes
				Object xPathresult=xPathInfoForFirstChannel.evaluate(inboundSOAPDocument);
				//				NodeList contentNodesList=(NodeList) xPathresult;
				Node[] contentNodes=getNodes(xPathresult, inboundSOAPDocument); 
				if (contentNodes==null || contentNodes.length==0)
				{
					callQoSExceptionHandler(policy,inboundSOAPDocument,protocolData,qosMessageCtxt,null,"Referenced content not found in document: "+xPathInfoForFirstChannel,null);
				}else{

					//copy dom nodes, transform the nodes and run the algorithm on the copied dom nodes 
					Node[] contentNodesCopy=new Node[contentNodes.length];
					for(int j=0;j<contentNodes.length;j++)
					{
						contentNodesCopy[j]=contentNodes[j].cloneNode(true); //deep-clone the content node
					}

					TransformedChannelInfos channelInfos = new TransformedChannelInfos();
					transformFirstChannelNodes(policy, channelInfos, contentNodesCopy);

					String secChannelRepresentation = computeSecondChannelRepresentation((byte[])channelInfos.get(contentNodesCopy[0]), policy);

					//Validate if the copied dom nodes are not corrupted
					if (!secondChannelValue.equals(secChannelRepresentation))
					{
						callQoSExceptionHandler(policy,inboundSOAPDocument,protocolData,qosMessageCtxt,null,"Checksum comparison failed. In document ["+secondChannelValue+"] vs. calculated from copy ["+secChannelRepresentation+"]",null);
					}else{

						//ok, lets convert the data to a native struct
						DualChannelLocal2MDPWSConverter converter2Local = policy.getDualChannelLocalConverter();

						byte[] serializedData=null;
						Node copiedDOMNode=contentNodesCopy[0]; //should be only one node in the list otherwise the xpath expression is not clear enough
						try{
							Transformer t = TransformerFactory.newInstance().newTransformer();
							t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
							StringWriter sw = new StringWriter();
							t.transform(new DOMSource(copiedDOMNode), new StreamResult(sw));
							serializedData= sw.toString().getBytes(XMLConstants.ENCODING);
						}catch(Exception ex)
						{
							callQoSExceptionHandler(policy,inboundSOAPDocument,protocolData,qosMessageCtxt,null,"Could not create serializedData !",null);
						}

						org.ws4d.java.schema.Element pvElement=element;

						String eName=null;
						if (contentNodes[0].getLocalName()==null)
						{
							eName=contentNodes[0].getParentNode().getNamespaceURI()+"/"+contentNodes[0].getParentNode().getLocalName();
						}else{
							eName=contentNodes[0].getNamespaceURI()+"/"+contentNodes[0].getLocalName();
						}
						QName eNameQN=QNameFactory.getInstance().getQName(eName);
						if (!element.getName().equals(eNameQN) && element.getType().isComplexType())
						{
							ComplexType complex = (ComplexType) element.getType();

							pvElement = ParameterValue.searchElement(complex,eNameQN );
						}


						IParameterValue pv=createPVFromBytes(serializedData,policyAttributes, pvElement);
						Object computedFirstChannel =converter2Local.createObjectFromParameterValue(pv,policyAttributes);
						IParameterValue pv2Channel=createPVFromBytes(serializedData,policyAttributes, pvElement);
						Object computedSecondChannel =converter2Local.createObjectFromParameterValue(pv,policyAttributes);

						//create a dom from the copy and compare it with the copy of the dom nodes that have been validated beforehand
						try {
							Node temptRootElement = createDOMNode(pv2Channel, inboundSOAPDocument);
							if (temptRootElement==null || !copiedDOMNode.isEqualNode(temptRootElement))
							{
								//TODO SSch is this an error ????
								Log.warn("DOM node and copy are not equal: "+copiedDOMNode+" != "+temptRootElement);
							}

						} catch (Exception e) 
						{
							callQoSExceptionHandler(policy,inboundSOAPDocument,protocolData,qosMessageCtxt,null,e.getMessage(),e);
						}

						//transform the struct copy to the machine-dependent second channel format
						Object computedSecondChannelInLocalRep=converter2Local.convertSecondChannel2LocalRepresentation(computedSecondChannel, policyAttributes);
						//String identifyingExpression=getIdentifyingPredicate(xPathInfoForFirstChannel, valueNodeCnt+1);
						DualChannel<Object,Object,XPathInfo> dualChannel=new DualChannel<Object,Object,XPathInfo>(computedFirstChannel, computedSecondChannelInLocalRep,xPathInfoForFirstChannel);
						extractedDualChannels.add(dualChannel);
						Log.info("Dual Channel was OK. Added to extracted Dual Channels.");
					}
				}
			}
		}else{
			callQoSExceptionHandler(policy,inboundSOAPDocument,protocolData,qosMessageCtxt,null,"Could not determine 'dc:DCValue'-nodes in the message for "+policyAttributes,null);
		}

		return extractedDualChannels;
	}


	private List<SafetyContext> extractSafetyContextInformation(Document inboundSOAPDocument,
			SafetyInformationPolicy siPolicy,
			SafetyInformationPolicyAttributes policyAttributes,
			QoSMessageContext qosMessageCtxt, DPWSProtocolData protocolData,
			org.ws4d.java.schema.Element element) throws Exception {

		java.util.ArrayList<SafetyContext> safetyContexts=new java.util.ArrayList<SafetyContext>();

		if(element.getBelongingSchema()==null){
			Log.warn("Element "+element+" does not  belong to a schema!");
			return safetyContexts;
		}

		NodeList safetyContextNodes = getSafetyCtxtNodes(inboundSOAPDocument);

		if (safetyContextNodes!=null && safetyContextNodes.getLength()>0)
		{
			String safetInformationNamespace = SafetyInformationConstants.SI_NAMESPACE;
			HashMap declaredNameSpaces = XPathInfo.getDeclaredNamesSpaces(inboundSOAPDocument);

			int nodeCnt=safetyContextNodes.getLength();
			for(int siCtxtNodeCnt=0;siCtxtNodeCnt<nodeCnt;siCtxtNodeCnt++)
			{
				Node siCtxtNode=safetyContextNodes.item(siCtxtNodeCnt);
				NamedNodeMap attributes=siCtxtNode.getAttributes();

				String targetIdentifier=null;
				if (attributes!=null && attributes.getLength()>0)
				{
					int attrCnt=attributes.getLength();
					for (int a=0;a<attrCnt;a++)
					{
						Attr attrNode=(Attr) attributes.item(a);
						String attrNamespaceURI = attrNode.getNamespaceURI();
						if (attrNamespaceURI==null || safetInformationNamespace.equals(attrNamespaceURI))
						{
							String tempValue=attrNode.getValue();
							if (SafetyInformationConstants.SI_ATTRIB_TARGET.equals(attrNode.getLocalName()))
							{
								targetIdentifier=tempValue;
								break;
							}
						}
					}

					if (targetIdentifier!=null)
					{
						SafetyContext currentSafetyContext=new SafetyContext(targetIdentifier);
						safetyContexts.add(currentSafetyContext);

						NodeList ctxtValueNodes = siCtxtNode.getChildNodes();
						if (ctxtValueNodes!=null)
						{
							int ctxtValueNodeCnt=ctxtValueNodes.getLength();
							if (ctxtValueNodeCnt>0)
							{
								for(int ctxtValueIndex=0;ctxtValueIndex<ctxtValueNodeCnt;ctxtValueIndex++)
								{
									Node ctxtValueWrapperNode=ctxtValueNodes.item(ctxtValueIndex);

									if (safetInformationNamespace.equals(ctxtValueWrapperNode.getNamespaceURI()) && SafetyInformationConstants.SI_ELEM_TRANSM_SCTXT_VALUE.equals(ctxtValueWrapperNode.getLocalName()))
									{
										NamedNodeMap ctxtValueWrapperNodeAttributes=ctxtValueWrapperNode.getAttributes();
										//Attr selectorAttributeNode=(Attr) ctxtValueWrapperNodeAttributes.getNamedItemNS(safetInformationNamespace, SafetyInformationConstants.SI_ATTRIB_TRANSM_SELECTOR_REF);

										String selectorValue=null;
										int ctxtAttrCnt=ctxtValueWrapperNodeAttributes.getLength();
										for (int attributeIndex=0;attributeIndex<ctxtAttrCnt;attributeIndex++)
										{
											Attr attrNode=(Attr) ctxtValueWrapperNodeAttributes.item(attributeIndex);
											String attrNamespaceURI = attrNode.getNamespaceURI();
											if (attrNamespaceURI==null || safetInformationNamespace.equals(attrNamespaceURI))
											{
												String tempValue=attrNode.getValue();
												if (SafetyInformationConstants.SI_ATTRIB_TRANSM_SELECTOR_REF.equals(attrNode.getLocalName()))
												{
													selectorValue=tempValue;
													break;
												}
											}
										}

										if (selectorValue!=null)
										{

											QName referencedElementQName=parsePrefixValue(declaredNameSpaces,selectorValue, ctxtValueWrapperNode);
											org.ws4d.java.schema.Element referencedElement=element.getBelongingSchema().getElement(referencedElementQName);

											if (referencedElement!=null)
											{
												Node ctxtValue=ctxtValueWrapperNode.getFirstChild();

												while(ctxtValue!=null)
												{
													if(ctxtValue.getNodeType()==Node.ELEMENT_NODE)
													{
														Transformer t = TransformerFactory.newInstance().newTransformer();
														t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
														t.setOutputProperty(OutputKeys.INDENT, "no");
														StringWriter sw = new StringWriter();
														t.transform(new DOMSource(ctxtValue), new StreamResult(sw));
														byte[] serializedData = sw.toString().replaceAll(">\\s*<", "><").getBytes(XMLConstants.ENCODING);
														IParameterValue ctxtParameterValue = createParameterValueFromByteArray(serializedData,referencedElement);

														if (Log.isDebug())Log.debug("Found serialized si:CtxtValue for target '"+targetIdentifier+"' and selector '"+referencedElementQName+"' :"+ctxtParameterValue);

														currentSafetyContext.addContextValue(referencedElementQName, new SafetyContextValue(referencedElementQName, ctxtParameterValue));

														break;
													}
													ctxtValue=ctxtValue.getNextSibling();
												}
											}
										}
									}
								}
							}
						}

					}

				}
			}
		}

		return safetyContexts;
	}

	//	/**
	//	 * @param xPathInfo 
	//	 * @param valueNodeCnt 
	//	 * @return
	//	 */
	//	private String getIdentifyingPredicate(XPathInfo xPathInfo, int valueNodeCnt) 
	//	{
	//		return "["+Integer.toString(valueNodeCnt)+"]";
	//	}
	//
	//	/**
	//	 * @param expression
	//	 * @return
	//	 */
	//	private String createModifiedXPathString(String expression) {
	//		return (expression.length()>1 && expression.lastIndexOf(")")>-1)?(expression.substring(1).substring(0,expression.lastIndexOf(")")-1)):expression;
	//	}

	/**
	 * @param pv
	 * @param inboundSOAPDocument 
	 * @return
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	protected Node createDOMNode(IParameterValue pv, Document inboundSOAPDocument) throws IOException,
	UnsupportedEncodingException, SAXException,
	ParserConfigurationException {

		Node temptRootElement=null;
		if (pv instanceof StringValue)
		{
			StringValue sValue=(StringValue)pv;
			temptRootElement=inboundSOAPDocument.createTextNode(sValue.get());
		}else if (pv !=null){
			Message2SOAPGenerator msgGenerator= SOAPMessageGeneratorFactory.getInstance().getMessage2SOAPGeneratorForCurrentThread();
			Message2SOAPConverter msgConverter= msgGenerator.getMessageSerializer(); 

			ByteArrayOutputStream bout = new ByteArrayOutputStream(); 

			msgConverter.getParameterValueSerializer().serializeParameterValue(pv, bout, null);
			SOAPMessageGeneratorFactory.getInstance().releaseMessage2SOAPGenerator(msgGenerator);

			if (Log.isDebug()){
				String pvString=bout.toString(XMLConstants.ENCODING);
				Log.debug("PVString: "+pvString);
			}
			//			InputStream in=InputStreamUtil.convertStringToInputStream(pvString);
			bout.flush();
			InputStream in=new ByteArrayInputStream(bout.toByteArray());
			bout.close();

			Document tempDocument= docBuilderFactory.newDocumentBuilder().parse(in);
			temptRootElement=tempDocument.getDocumentElement();
		}

		return temptRootElement;
	}

	/**
	 * @param declaredNameSpaces
	 * @param prefixedString
	 * @param ctxtValueWrapperNode 
	 */
	protected QName parsePrefixValue(HashMap declaredNameSpaces, String prefixedString, Node ctxtValueWrapperNode) {
		QName qNameValue;

		StringTokenizer tokenizer=new StringTokenizer(prefixedString,":");
		if (tokenizer.countTokens()==2)
		{
			String prefix=tokenizer.nextToken();

			String namespace=null;
			if (ctxtValueWrapperNode!=null)
			{
				namespace=ctxtValueWrapperNode.lookupNamespaceURI(prefix);

			}else{
				namespace=findNameSpaceForPrefix(declaredNameSpaces,prefix);
			}
			
			if (namespace!=null)
			{
				qNameValue=QNameFactory.getInstance().getQName(tokenizer.nextToken(), namespace);
			}else{
				qNameValue=QNameFactory.getInstance().getQName(prefixedString);	
			}

		}else{
			qNameValue=QNameFactory.getInstance().getQName(prefixedString);
		}


		return qNameValue;
	}

	/**
	 * @param declaredNameSpaces
	 * @param prefix
	 * @return
	 */
	private String findNameSpaceForPrefix(HashMap declaredNameSpaces,String prefix) {
		if (declaredNameSpaces!=null && !declaredNameSpaces.isEmpty())
		{
			Set entries= declaredNameSpaces.entrySet();
			for (Iterator entIt=entries.iterator();entIt.hasNext();)
			{
				Entry entry= (Entry) entIt.next();
				if (entry.getValue().equals(prefix))
				{
					return entry.getKey().toString();
				}
			}
		}
		return null;
	}

	//	/**
	//	 * @param declaredNameSpaces
	//	 * @return
	//	 */
	//	private XPathNamespaceContext createXPathSpaceContext(
	//			HashMap declaredNameSpaces) 
	//	{
	//		XPathNamespaceContext nsContext=new SOAPNameSpaceContext();
	//		if (declaredNameSpaces!=null && !declaredNameSpaces.isEmpty())
	//		{
	//			Set entries= declaredNameSpaces.entrySet();
	//			for (Iterator entIt=entries.iterator();entIt.hasNext();)
	//			{
	//				Entry entry= (Entry) entIt.next();
	//				nsContext.addNamespace((String)entry.getValue(), (String)entry.getKey());
	//			}
	//		}
	//		return nsContext;
	//	}

	/**
	 * @param policy
	 * @param inboundSOAPDocument
	 * @param protocolData 
	 * @param object 
	 * @param qosMsgCtxt 
	 * @param e 
	 * @throws InterceptionException 
	 */
	private void callQoSExceptionHandler(QoSPolicy policy,
			Document soapDocument, ProtocolData protocolData, QoSMessageContext qosMsgCtxt, QoSPolicyToken<?,?> token, String additionalInfo, Throwable t) throws InterceptionException {
		//		Log.warn(additionalInfo);
		//		if (t!=null)
		//			Log.printStackTrace(t);

		if (!(t instanceof InterceptionException))
		{
			boolean throwException=QoSPolicyExceptionHandlerManager.getInstance().callExceptionHandler(policy, soapDocument, token, qosMsgCtxt, protocolData, additionalInfo);
			if (throwException)
				throw new InterceptionException(additionalInfo);
		}
	}

	public static class TransformedChannelInfos {
		private final HashMap map = new HashMap();

		public Object get(Object key) {
			return map.get(key);
		}

		public boolean isEmpty() {
			return map.isEmpty();
		}

		public Object put(Object key, Object value) {
			return map.put(key, value);
		}

		public Object remove(Object key) {
			return map.remove(key);
		}

		public int size() {
			return map.size();
		}

		public DataStructure values() {
			return map.values();
		}

		public Set entrySet() {
			return map.entrySet();
		}

		@Override
		public String toString() {
			StringBuilder builder=new StringBuilder();
			try {
				Iterator it= map.entrySet().iterator();
				while(it.hasNext())
				{
					byte[] val=(byte[])((Entry) it.next()).getValue();

					builder.append(new String(val,XMLConstants.ENCODING));

				}
			} catch (UnsupportedEncodingException e) {
				Log.warn(e);
			}
			return builder.toString();
		}

	}



}
