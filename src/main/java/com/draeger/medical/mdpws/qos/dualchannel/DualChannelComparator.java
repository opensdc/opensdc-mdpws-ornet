/**
 * 
 */
package com.draeger.medical.mdpws.qos.dualchannel;

/**
 *
 *
 */
public interface DualChannelComparator {

	/**
	 * @param computedFirstChannel
	 * @param dualChannel
	 */
	public abstract boolean compare(Object computedFirstChannel, DualChannel<?, ?, ?> dualChannel);

}
