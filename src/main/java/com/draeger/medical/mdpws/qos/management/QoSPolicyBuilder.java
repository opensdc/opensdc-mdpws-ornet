/**
 * 
 */
package com.draeger.medical.mdpws.qos.management;

import org.ws4d.java.types.QName;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionParser;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionSerializer;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptor;

/**
 *
 *
 */
public interface QoSPolicyBuilder {
	public QoSPolicyInterceptor createInboundInterceptor();
	public QoSPolicyInterceptor createOutboundInterceptor();
	public QoSPolicyInterceptor createInOutboundInterceptor();
	
	public QoSPolicy createOutboundPolicy();
	public QoSPolicy createOutboundPolicy(Object addInfo);
	public QoSPolicy createInboundPolicy();
	public QoSPolicy createInboundPolicy(Object addInfo);
	public QoSPolicy createInOutboundPolicy();
	public QoSPolicy createInOutboundPolicy(Object addInfo);
	public Class<?> getInboundPolicyClass();
	public Class<?> getOutboundPolicyClass();
	public Class<?> getInOutboundPolicyClass();
	
	
	public QName getInboundAssertionElementName();
	public QName getOutboundAssertionElementName();
	public QName getInOutboundAssertionElementName();
	
	public WSDLAssertionSerializer createSerializer();
	public WSDLAssertionParser createParser();
}
