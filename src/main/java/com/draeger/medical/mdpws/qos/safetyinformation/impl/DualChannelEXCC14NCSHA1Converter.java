/**
 * 
 */
package com.draeger.medical.mdpws.qos.safetyinformation.impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.ws.security.util.Base64;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.c14n.InvalidCanonicalizerException;
import org.w3c.dom.Node;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelProtocolConverter;
import com.draeger.medical.mdpws.qos.safetyinformation.impl.SafetyInformationInterceptorImpl.TransformedChannelInfos;
import com.draeger.medical.mdpws.qos.wsdl.SafetyInformationConstants;



/**
 *
 *
 */
public class DualChannelEXCC14NCSHA1Converter implements
DualChannelProtocolConverter {

	private final MessageDigest sha1;

	public DualChannelEXCC14NCSHA1Converter() throws NoSuchAlgorithmException
	{
		org.apache.xml.security.Init.init();
		sha1=MessageDigest.getInstance("SHA1");
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelTransformator#transformForSerialization(org.w3c.dom.NodeList, com.draeger.medical.mdpws.qos.dualchannel.impl.DualChannelInterceptorImpl.TransformedChannelInfos)
	 */
	@Override
	public void transformForSerialization(Node[] identifiedNodes,
			TransformedChannelInfos channelInfos) 
	{
		int nodeCnt=identifiedNodes.length;
		for(int i=0;i<nodeCnt;i++)
		{
			try{
				Node n=identifiedNodes[i];
				byte[] bytes=getBytes(n);
				channelInfos.put(n, bytes);
			}catch (Exception e)
			{
				Log.error(e);
				throw new IllegalStateException(e.getMessage());
			}
		}
	}

	/**
	 * @param n
	 * @return
	 * @throws InvalidCanonicalizerException 
	 * @throws CanonicalizationException 
	 */
	private byte[] getBytes(Node n) throws InvalidCanonicalizerException, CanonicalizationException 
	{
		Canonicalizer c14n = Canonicalizer.getInstance(Canonicalizer.ALGO_ID_C14N_WITH_COMMENTS);
		byte[] canonicalMessage = c14n.canonicalizeSubtree(n);
		return canonicalMessage;
	}

	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelTransformator#createChannelValueFromBytes(byte[])
	//	 */
	//	public Object createChannelValueFromBytes(byte[] serializedData) 
	//	{
	//		try{
	//			return new String(serializedData,"UTF-8");
	//		}catch(Exception e){
	//			Log.error(e.getMessage());
	//			Log.printStackTrace(e);
	//		}
	//		return null;
	//	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelTransformator#getImplementedTransformation()
	 */
	@Override
	public QName getImplementedTransformation() {
		return SafetyInformationConstants.SI_ATTRIB_TRANSFORM_DUALCHANNEL_VALUE_EXCC14NC;
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelConverter#getDualChannelRepresentation(com.draeger.medical.mdpws.qos.dualchannel.impl.DualChannelInterceptorImpl.TransformedChannelInfos)
	 */
	@Override
	public String getDualChannelRepresentation(
			byte[] serializedData) 
	{

		if (serializedData==null) return null;

		StringBuilder builder=new StringBuilder();
		try{
			builder.append(computeDigest(serializedData));
		}catch(Exception e)
		{
			Log.error(e);
			throw new RuntimeException(e);
		}
		return builder.toString();
	}

	/**
	 * @param transformedData
	 * @return
	 */
	private String computeDigest(byte[] transformedData) 
	{
		String digestValue=null;
		byte[] digest=null;
		synchronized(sha1)
		{
			sha1.reset();
			sha1.update(java.nio.ByteBuffer.wrap(transformedData));
			digest=sha1.digest();
		}
		digestValue=new String(Base64.encode(digest));

		return digestValue;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelConverter#getImplementedAlgorithm()
	 */
	@Override
	public QName getImplementedAlgorithm() {
		return SafetyInformationConstants.SI_ATTRIB_ALGORITHM_DUALCHANNEL_VALUE_SHA1D;
	}


}
