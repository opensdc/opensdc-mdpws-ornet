/**
 * 
 */
package com.draeger.medical.mdpws.qos.wsdl.safetyinformation;

import java.io.IOException;

import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.util.Log;
import org.xmlpull.v1.XmlPullParserException;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionParser;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyDirection;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder;
import com.draeger.medical.mdpws.qos.management.QoSPolicyBuilderInstanceRegistry;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicyAttributes;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicyBuilder;
import com.draeger.medical.mdpws.qos.wsdl.SafetyInformationConstants;

/**
 *
 *
 */
public class SafetyInformationAssertionParser implements WSDLAssertionParser {


	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionParser#parse(org.ws4d.java.io.xml.ElementParser, com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint)
	 */
	@Override
	public QoSPolicy parse(ElementParser assertionEParser,
			WSDLPolicyAttachmentPoint attachmentPoint) throws IOException {
		QoSPolicy policy=null;
		try {

			String transmitDualChannelAttrVal=assertionEParser.getAttributeValue(SafetyInformationConstants.SI_NAMESPACE, SafetyInformationConstants.SI_ATTRIB_ASSERTION_TRANSMIT_DC);
			String transmitSafetyContextAttrVal=assertionEParser.getAttributeValue(SafetyInformationConstants.SI_NAMESPACE, SafetyInformationConstants.SI_ATTRIB_ASSERTION_TRANSMIT_SCTXT);
			String algorithmStr=assertionEParser.getAttributeValue(SafetyInformationConstants.SI_NAMESPACE, SafetyInformationConstants.SI_ATTRIB_DUALCHANNEL_ALGORITHM);
			String transformStr=assertionEParser.getAttributeValue(SafetyInformationConstants.SI_NAMESPACE, SafetyInformationConstants.SI_ATTRIB_DUALCHANNEL_TRANSFORM);

			boolean transmitDualChannel=true;
			if (transmitDualChannelAttrVal!=null)
			{
				transmitDualChannel=Boolean.parseBoolean(transmitDualChannelAttrVal);
			}
			boolean transmitSafetyContext=true;
			if (transmitSafetyContextAttrVal!=null)
			{
				transmitSafetyContext=Boolean.parseBoolean(transmitSafetyContextAttrVal);
			}
			
			QName algorithmQN=null;
			if (algorithmStr!=null)
			{
				algorithmQN=getQName(algorithmStr, assertionEParser);
			}else{
				algorithmQN=SafetyInformationConstants.SI_ATTRIB_ALGORITHM_DUALCHANNEL_DEFAULT_VALUE;
			}
			QName transformQN=null;
			if (transformStr!=null)
			{
				transformQN=getQName(transformStr, assertionEParser);
			}else{
				transformQN=SafetyInformationConstants.SI_ATTRIB_TRANSFORM_DUALCHANNEL_DEFAULT_VALUE;
			}

			if (SafetyInformationConstants.SI_ATTRIB_ALGORITHM_DUALCHANNEL_VALUE_HEX.equals(algorithmQN) 
					|| SafetyInformationConstants.SI_ATTRIB_ALGORITHM_DUALCHANNEL_VALUE_SHA1D.equals(algorithmQN))
			{
				policy=createPolicy(attachmentPoint, transmitDualChannel,transmitSafetyContext,algorithmQN,transformQN, assertionEParser);
			}

			assertionEParser.consume();
		} catch (XmlPullParserException e) {
			Log.error(e);
			throw new IOException(e.getMessage());
		}
		return policy;
	}

	protected QoSPolicy createPolicy(WSDLPolicyAttachmentPoint attachmentPoint,boolean transmitDualChannel, boolean transmitSafetyContext,QName algorithm, QName transform, ElementParser eParser ) throws XmlPullParserException{
		SafetyInformationPolicyAttributes attributes=new SafetyInformationPolicyAttributes();
		attributes.setTransmitDualChannel(transmitDualChannel);
		attributes.setTransmitSafetyContext(transmitSafetyContext);
		attributes.setAlgorithm(algorithm);
		attributes.setTransform(transform);

		if (Log.isDebug())
		{
			Log.debug("Parser DC attributes:"+attributes);
		}
		
		QoSPolicy policy=null;

		QoSPolicyBuilder builder=  QoSPolicyBuilderInstanceRegistry.getInstance().getPolicyBuilder(SafetyInformationPolicyBuilder.class);
		if (builder instanceof SafetyInformationPolicyBuilder)
		{
			SafetyInformationPolicyBuilder siBuilder=(SafetyInformationPolicyBuilder) builder;
			if (attachmentPoint.getPolicyDirection().equals(WSDLPolicyDirection.INBOUND)){
				policy=siBuilder.createInboundPolicy(attributes);
			}else if (attachmentPoint.getPolicyDirection().equals(WSDLPolicyDirection.OUTBOUND)){
				policy=siBuilder.createOutboundPolicy(attributes);
			}else if (attachmentPoint.getPolicyDirection().equals(WSDLPolicyDirection.INOUTBOUND)){
				policy=siBuilder.createInOutboundPolicy(attributes);
			}
		}
		return policy;
	}

	private QName getQName(String attributeContent, ElementParser parser)
	{

		String local=attributeContent;
		String nsp=null;
		int index= local.indexOf(":");
		if (index>=0)
		{
			if (index>0){
				nsp=local.substring(0,index);
			}
			local=local.substring(index+1);
		}
		return QNameFactory.getInstance().getQName(local,parser.getNamespace(nsp));
	}
	
//	/**
//	 * @param eParser
//	 * @return
//	 * @throws XmlPullParserException 
//	 */
//	private XPathNamespaceContext getXPathContext(ElementParser eParser) throws XmlPullParserException {
//		Log.info("Creating namespace context");
//		XPathNamespaceContext ctxt=new XPathNamespaceContext();
//
//		//int nsStart = eParser.getNamespaceCount(eParser.getDepth()-1);
//		int nsEnd = eParser.getNamespaceCount(eParser.getDepth());
//		for (int i = 0 ; i < nsEnd; i++) {
//			String prefix = eParser.getNamespacePrefix(i);
//			String ns = eParser.getNamespaceUri(i);
//			Log.info("Adding namespace to context: "+prefix+" "+ns);
//			ctxt.addNamespace(prefix, ns);
//		}
//
//		return ctxt;
//	}
//


}
