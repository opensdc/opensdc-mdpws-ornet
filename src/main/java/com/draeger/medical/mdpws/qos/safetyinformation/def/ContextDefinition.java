package com.draeger.medical.mdpws.qos.safetyinformation.def;

import java.util.ArrayList;

import org.ws4d.java.types.QName;

public class ContextDefinition {
	private final String contextTarget;
	private final ArrayList<QName> attributeSelectors=new ArrayList<QName>();
	
	public ContextDefinition(String contextTarget) {
		super();
		this.contextTarget = contextTarget;
	}

	public String getContextTarget() {
		return contextTarget;
	}

	public ArrayList<QName> getAttributeSelectors() {
		return attributeSelectors;
	}

	@Override
	public String toString() {
		return "ContextDefinition [contextTarget=" + contextTarget
				+ ", attributeSelectors=" + attributeSelectors + "]";
	}
}
