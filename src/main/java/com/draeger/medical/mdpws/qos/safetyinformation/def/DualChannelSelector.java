package com.draeger.medical.mdpws.qos.safetyinformation.def;

import org.ws4d.java.types.QName;

import com.draeger.medical.mdpws.common.util.XPathInfo;

public class DualChannelSelector 
{
	private final XPathInfo selectedElementInMessage;
	private final QName elementQName;

	public DualChannelSelector(XPathInfo selectedElementInMessage,QName elementQName) {
		super();
		this.selectedElementInMessage = selectedElementInMessage;
		this.elementQName=elementQName;
	}

	public XPathInfo getSelectedElementInMessage() {
		return selectedElementInMessage;
	}

	public QName getElementQName() {
		return elementQName;
	}

	@Override
	public String toString() {
		return "DualChannelSelector [selectedElementInMessage="
				+ selectedElementInMessage + ", elementQName=" + elementQName
				+ "]";
	}

}
