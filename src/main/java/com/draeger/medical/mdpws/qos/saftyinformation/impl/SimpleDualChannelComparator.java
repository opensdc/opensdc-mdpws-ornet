/**
 * 
 */
package com.draeger.medical.mdpws.qos.saftyinformation.impl;

import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelComparator;
import com.draeger.medical.mdpws.qos.safetyinformation.transmission.DualChannel;


/**
 * @author Stefan Schlichting
 *
 *
 */
public class SimpleDualChannelComparator implements DualChannelComparator {
	private static final double simpleDualChannelPrecision	= Double.parseDouble(System.getProperty("SimpleDualChannelComparator.simpleDualChannelPrecision", Double.toString(1e-6)));
	
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelComparator#compare(java.lang.Object, com.draeger.medical.mdpws.qos.dualchannel.DualChannel)
	 */
	@Override
	public boolean compare(Object computedFirstChannel,
			DualChannel<?, ?, ?> dualChannel) 
	{
		if (computedFirstChannel==null) return false;
		
		boolean retVal=false;
		if (dualChannel.getFirstChannel() instanceof Number)
		{
			try{
				double diff = Double.parseDouble(computedFirstChannel.toString())-((Number)dualChannel.getFirstChannel()).doubleValue();
				if (diff<simpleDualChannelPrecision && dualChannel.getFirstChannel().equals(dualChannel.getSecondChannel())){
					retVal=true;
				}
			}catch (NumberFormatException nfe){
				//void
			}
		}else{
			retVal= computedFirstChannel.equals(dualChannel.getFirstChannel()) && dualChannel.getFirstChannel().equals(dualChannel.getSecondChannel());
		}
		return retVal;
	}

}
