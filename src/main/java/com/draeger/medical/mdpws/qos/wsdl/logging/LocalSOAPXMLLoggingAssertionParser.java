/**
 * 
 */
package com.draeger.medical.mdpws.qos.wsdl.logging;

import java.io.IOException;

import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.util.Log;
import org.xmlpull.v1.XmlPullParserException;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionParser;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyDirection;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.logging.LoggingPolicyBuilder;
import com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder;
import com.draeger.medical.mdpws.qos.management.QoSPolicyBuilderInstanceRegistry;
import com.draeger.medical.mdpws.qos.wsdl.SPAConstants;

/**
 *
 *
 */
public class LocalSOAPXMLLoggingAssertionParser implements WSDLAssertionParser {

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionParser#parse(org.ws4d.java.io.xml.ElementParser)
	 */
	@Override
	public QoSPolicy parse(ElementParser eParser,
			WSDLPolicyAttachmentPoint attachmentPoint) throws IOException {
		QoSPolicy policy=null;
		try {

			String method=eParser.getAttributeValue(SPAConstants.SPA_NAMESPACE, SPAConstants.SPA_ATTRIB_METHOD);
			if (SPAConstants.SPA_ATTRIB_LOGGING_LOCAL.equals(method))
			{

				policy=createPolicy(attachmentPoint, method);
			}else{
				Log.info("Unknown logging method:"+method);
			}
			eParser.consume();
		} catch (XmlPullParserException e) {
			Log.error(e);
			throw new IOException(e.getMessage());
		}
		return policy;
	}

	public QoSPolicy createPolicy(WSDLPolicyAttachmentPoint attachmentPoint, String method ){
		QoSPolicy policy=null;
		QoSPolicyBuilder builder=  QoSPolicyBuilderInstanceRegistry.getInstance().getPolicyBuilder(LoggingPolicyBuilder.class);
		if (builder instanceof LoggingPolicyBuilder)
		{
			LoggingPolicyBuilder lpBuilder=(LoggingPolicyBuilder)builder;
			if (attachmentPoint.getPolicyDirection().equals(WSDLPolicyDirection.INBOUND)){
				policy=lpBuilder.createInboundPolicy(method);
			}else if (attachmentPoint.getPolicyDirection().equals(WSDLPolicyDirection.OUTBOUND)){
				policy=lpBuilder.createOutboundPolicy(method);
			}else if (attachmentPoint.getPolicyDirection().equals(WSDLPolicyDirection.INOUTBOUND)){
				policy=lpBuilder.createInOutboundPolicy(method);
			}
		}
		return policy;
	}

}
