package com.draeger.medical.mdpws.qos.util;

import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.EmptyStructures;
import org.ws4d.java.structures.Iterator;

public class Util 
{
	public static Iterator findItems(Class<?> itemClz, Iterator collectionIterator) 
	{
		if (itemClz==null) return EmptyStructures.EMPTY_ITERATOR;
		ArrayList compatibleItems=new ArrayList();
		while (collectionIterator.hasNext())
		{
			Object o= collectionIterator.next();
			if (o!=null && itemClz.isInstance(o))
			{
				compatibleItems.add(o);
			}
		}
		return compatibleItems.iterator();	
	}
	
	public static Iterator findClasses(Class<?> clz, Iterator clzCollectionIterator) 
	{
		if (clz==null) return EmptyStructures.EMPTY_ITERATOR;
		ArrayList compatibleItems=new ArrayList();
		while (clzCollectionIterator.hasNext())
		{
			Class<?> entry= (Class<?>)clzCollectionIterator.next();
			if (entry!=null && clz.isAssignableFrom(entry))
			{
				compatibleItems.add(entry);
			}
		}
		return compatibleItems.iterator();	
	}
}
