/**
 * 
 */
package com.draeger.medical.mdpws.qos.safetyinformation.dualchannel;

import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicy;


/**
 *
 *
 */
public  class SafetyInformationManager 
{
	private static final SafetyInformationManager instance=new SafetyInformationManager();


	private SafetyInformationManager()
	{
		//void
	}

	public static SafetyInformationManager getInstance()
	{
		return instance;
	}


	private Local2MDPWSConverterProvider converterProviderImpl=null;

	private DualChannelProtocolConverterProvider transformatorProviderImpl=null;

	private DualChannelComparatorProvider comparatorProviderImpl=null;


	private Local2MDPWSConverterProvider getLocal2MDPWSConverterProviderImpl() {
		return converterProviderImpl;
	}

	private DualChannelProtocolConverterProvider getProtocolConverterProviderImpl() {
		return transformatorProviderImpl;
	}

	private DualChannelComparatorProvider getComparatorProviderImpl() {
		return comparatorProviderImpl;
	}


	public void initializeManager(Local2MDPWSConverterProvider converterProviderImpl, DualChannelProtocolConverterProvider transformatorProviderImpl, DualChannelComparatorProvider comparatorProviderImpl)
	{
		setLocal2MDPWSConverterProviderImpl(converterProviderImpl);
		setComparatorProviderImpl(comparatorProviderImpl);
		setProtocolConverterProviderImpl(transformatorProviderImpl);
	}

	private void setLocal2MDPWSConverterProviderImpl(
			Local2MDPWSConverterProvider converterProviderImpl) {
		this.converterProviderImpl = converterProviderImpl;
	}

	private void setProtocolConverterProviderImpl(
			DualChannelProtocolConverterProvider transformatorProviderImpl) {
		this.transformatorProviderImpl = transformatorProviderImpl;
	}

	private void setComparatorProviderImpl(
			DualChannelComparatorProvider comparatorProviderImpl) {
		this.comparatorProviderImpl = comparatorProviderImpl;
	}

	public void updateDualChanneLocal2MDPWSConverter(SafetyInformationPolicy policy)
	{
		if (getLocal2MDPWSConverterProviderImpl()!=null)
		{
			DualChannelLocal2MDPWSConverter converter=getLocal2MDPWSConverterProviderImpl().getConverter(policy);
			if (converter!=null)
			{
				policy.addDualChannelConverter(converter);
			}
		}
	}

	public void updateDualChannelProtocolConverter(SafetyInformationPolicy policy)
	{
		if (getProtocolConverterProviderImpl()!=null)
		{
			DualChannelProtocolConverter protocolConverter= getProtocolConverterProviderImpl().getDualChannelTransformator(policy);
			if (protocolConverter!=null)
			{
				policy.addDualChannelProtocolConverter( protocolConverter);
			}
		}
	}

	/**
	 * @param policy
	 */
	public void updateDualChannelComparator(SafetyInformationPolicy policy) 
	{
		if (getComparatorProviderImpl()!=null)
		{
			DualChannelComparator comparator=getComparatorProviderImpl().getDualChannelComparator(policy);
			policy.setDualChannelComparator(comparator);
		}
	}
}
