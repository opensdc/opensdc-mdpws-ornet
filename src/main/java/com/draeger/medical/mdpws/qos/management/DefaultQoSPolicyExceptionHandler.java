/**
 * 
 */
package com.draeger.medical.mdpws.qos.management;

import org.w3c.dom.Document;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.util.Log;

import com.draeger.medical.mdpws.qos.QoSMessageContext;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.QoSPolicyExceptionHandler;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyToken;

/**
 *
 *
 */
public class DefaultQoSPolicyExceptionHandler implements
		QoSPolicyExceptionHandler {

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.QoSPolicyExceptionHandler#handle(com.draeger.medical.mdpws.qos.QoSPolicy, org.w3c.dom.Document, com.draeger.medical.mdpws.qos.interception.QoSPolicyToken, com.draeger.medical.mdpws.qos.QoSMessageContext)
	 */
	@Override
	public boolean handle(QoSPolicy policy, Document soapDocument,
			QoSPolicyToken<?,?> token, QoSMessageContext qosMessageCtxt, ProtocolData protocolData, String additionalInfo) {
		boolean throwException=false;
		if (Log.isInfo())
			Log.info("An Exception has occurred during policy interception:"+additionalInfo+" "+policy+" "+token+" "+protocolData );
		
		return throwException;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.QoSPolicyExceptionHandler#isResponsibleFor(com.draeger.medical.mdpws.qos.QoSPolicy)
	 */
	@Override
	public boolean isResponsibleFor(QoSPolicy policy) {
		return true;
	}

}
