package com.draeger.medical.mdpws.qos.safetyinformation;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionParser;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionSerializer;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.SafetyInformationManager;
import com.draeger.medical.mdpws.qos.safetyinformation.impl.InOutboundSafetyInformationQoSPolicy;
import com.draeger.medical.mdpws.qos.safetyinformation.impl.InboundSafetyInformationQoSPolicy;
import com.draeger.medical.mdpws.qos.safetyinformation.impl.OutboundSafetyInformationQoSPolicy;
import com.draeger.medical.mdpws.qos.safetyinformation.impl.SafetyInformationInterceptorImpl;
import com.draeger.medical.mdpws.qos.wsdl.SafetyInformationConstants;
import com.draeger.medical.mdpws.qos.wsdl.safetyinformation.SafetyInformationAssertionParser;
import com.draeger.medical.mdpws.qos.wsdl.safetyinformation.SafetyInformationAssertionSerializer;



public class SafetyInformationPolicyBuilder implements QoSPolicyBuilder{	
	private final SafetyInformationPolicyInterceptor interceptor=new SafetyInformationInterceptorImpl();
	
	
	
	public SafetyInformationPolicyBuilder()
	{
		//void
	}
	
	@Override
	public SafetyInformationPolicyInterceptor createInboundInterceptor()
	{
		return getDualChannelPolicyInterceptor();
	}
	
	@Override
	public SafetyInformationPolicyInterceptor createOutboundInterceptor()
	{
		return getDualChannelPolicyInterceptor();
	}
	
	@Override
	public SafetyInformationPolicyInterceptor createInOutboundInterceptor()
	{
		return getDualChannelPolicyInterceptor();
	}
	
	private SafetyInformationPolicyInterceptor getDualChannelPolicyInterceptor()
	{
		return interceptor;
	}
	
	@Override
	public Class<?> getInboundPolicyClass()
	{
		return InboundSafetyInformationQoSPolicy.class;
	}
	
	@Override
	public Class<?> getInOutboundPolicyClass()
	{
		return InOutboundSafetyInformationQoSPolicy.class;
	}
	
	@Override
	public Class<?> getOutboundPolicyClass()
	{
		return OutboundSafetyInformationQoSPolicy.class;
	}

	/**
	 * @return
	 */
	@Override
	public WSDLAssertionSerializer createSerializer() {
		return new SafetyInformationAssertionSerializer();
	}

	/**
	 * @return
	 */
	@Override
	public QName getInboundAssertionElementName() {
		return QNameFactory.getInstance().getQName(SafetyInformationConstants.SI_ELEM_ASSERTION_SAFETY_REQ, SafetyInformationConstants.SI_NAMESPACE);
	}
	
	@Override
	public QName getOutboundAssertionElementName() {
		return QNameFactory.getInstance().getQName(SafetyInformationConstants.SI_ELEM_ASSERTION_SAFETY_REQ, SafetyInformationConstants.SI_NAMESPACE);
	}
	
	@Override
	public QName getInOutboundAssertionElementName() {
		return QNameFactory.getInstance().getQName(SafetyInformationConstants.SI_ELEM_ASSERTION_SAFETY_REQ, SafetyInformationConstants.SI_NAMESPACE);
	}

	/**
	 * @return
	 */
	@Override
	public WSDLAssertionParser createParser() {
		return new SafetyInformationAssertionParser();
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder#createInboundPolicy()
	 */
	@Override
	public QoSPolicy createInboundPolicy() {
		return createInboundPolicy(null);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder#createInboundPolicy(java.lang.String)
	 */
	@Override
	public QoSPolicy createInboundPolicy(Object addInfo) {
		InboundSafetyInformationQoSPolicy inbound=new InboundSafetyInformationQoSPolicy();
		enrichPolicy(addInfo, inbound);
		return inbound;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder#createInOutboundPolicy()
	 */
	@Override
	public QoSPolicy createInOutboundPolicy() {
		return createInOutboundPolicy(null);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder#createInOutboundPolicy(java.lang.String)
	 */
	@Override
	public QoSPolicy createInOutboundPolicy(Object addInfo) {
		InOutboundSafetyInformationQoSPolicy inOutPolicy=new InOutboundSafetyInformationQoSPolicy();
		enrichPolicy(addInfo, inOutPolicy);
		return inOutPolicy;
	}

	/**
	 * @param addInfo
	 * @param inOutPolicy
	 */
	protected void enrichPolicy(Object addInfo,
			SafetyInformationPolicy policy) {
		if (addInfo instanceof SafetyInformationPolicyAttributes)
		{
			SafetyInformationPolicyAttributes attributes=(SafetyInformationPolicyAttributes)addInfo;
			policy.setSafetyInformationPolicyAttributes(attributes);
		}
		SafetyInformationManager.getInstance().updateDualChannelProtocolConverter(policy);
		SafetyInformationManager.getInstance().updateDualChanneLocal2MDPWSConverter(policy);
		SafetyInformationManager.getInstance().updateDualChannelComparator(policy);
	}
	
	@Override
	public SafetyInformationPolicy createOutboundPolicy()
	{
		return createOutboundPolicy(null);
	}
	
	@Override
	public SafetyInformationPolicy createOutboundPolicy(Object addInfo)
	{
		OutboundSafetyInformationQoSPolicy outPolicy=new OutboundSafetyInformationQoSPolicy();
		enrichPolicy(addInfo, outPolicy);
		return outPolicy;
	}
	
}
