/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/

package com.draeger.medical.mdpws.qos.logging;

import com.draeger.medical.mdpws.qos.AbstractQoSPolicy;
import com.draeger.medical.mdpws.qos.QoSPolicy;


/**
 * Abstract marker class for a {@link QoSPolicy} that deals with logging.
 */
public abstract class LoggingQoSPolicy extends AbstractQoSPolicy {
	//void - abstract marker class
}
