/**
 * 
 */
package com.draeger.medical.mdpws.qos.safetyinformation;

import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelComparator;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelLocal2MDPWSConverter;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelProtocolConverter;

/**
 *
 *
 */
public interface SafetyInformationPolicy extends QoSPolicy
{
	public abstract void setSafetyInformationPolicyAttributes(SafetyInformationPolicyAttributes attributes);
	public abstract SafetyInformationPolicyAttributes getSafetyInformationPolicyAttributes();
	
	public abstract DualChannelLocal2MDPWSConverter getDualChannelLocalConverter();
	public abstract void addDualChannelConverter(DualChannelLocal2MDPWSConverter converter);
	
	public abstract DualChannelComparator getDualChannelComparator();
	public abstract void setDualChannelComparator(DualChannelComparator comparator);

	public abstract DualChannelProtocolConverter getDualChannelProtocolConverter(SafetyInformationPolicyAttributes attributes);
	public abstract void addDualChannelProtocolConverter(DualChannelProtocolConverter converter);
}
