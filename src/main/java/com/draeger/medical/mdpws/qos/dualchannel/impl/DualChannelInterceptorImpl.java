/**
 * 
 */
package com.draeger.medical.mdpws.qos.dualchannel.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.protocol.soap.generator.Message2SOAPConverter;
import org.ws4d.java.communication.protocol.soap.generator.Message2SOAPGenerator;
import org.ws4d.java.communication.protocol.soap.generator.MessageReceiver;
import org.ws4d.java.communication.protocol.soap.generator.SOAP2MessageGenerator;
import org.ws4d.java.communication.protocol.soap.generator.SOAPMessageGeneratorFactory;
import org.ws4d.java.constants.XMLConstants;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.parameter.StringValue;
import org.ws4d.java.service.parameter.StringValueFactory;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.HashMap.Entry;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.structures.Set;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.util.Log;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.draeger.medical.mdpws.common.util.SOAPNameSpaceContext;
import com.draeger.medical.mdpws.common.util.XPathInfo;
import com.draeger.medical.mdpws.common.util.XPathNamespaceContext;
import com.draeger.medical.mdpws.qos.QoSMessageContext;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.QoSPolicyExceptionHandlerManager;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannel;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelComparator;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelLocal2MDPWSConverter;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicy;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicyAttributes;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicyInterceptor;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicyToken;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelProtocolConverter;
import com.draeger.medical.mdpws.qos.interception.InboundSOAPXMLDocumentInterceptor;
import com.draeger.medical.mdpws.qos.interception.InterceptionException;
import com.draeger.medical.mdpws.qos.interception.InterceptorUtil;
import com.draeger.medical.mdpws.qos.interception.OutboundSOAPUTF8TransformationInterceptor;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptionDirection;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyOrdinalNumber;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyToken;
import com.draeger.medical.mdpws.qos.security.SecurityEngine;
import com.draeger.medical.mdpws.qos.subjects.MessagePolicySubject;
import com.draeger.medical.mdpws.qos.subjects.OperationPolicySubject;
import com.draeger.medical.mdpws.qos.subjects.ServicePolicySubject;
import com.draeger.medical.mdpws.qos.wsdl.SPAConstants;

/**
 *
 *
 */
public class DualChannelInterceptorImpl implements
DualChannelPolicyInterceptor,
OutboundSOAPUTF8TransformationInterceptor,
InboundSOAPXMLDocumentInterceptor {
	private DocumentBuilderFactory docBuilderFactory;
	private DocumentBuilder builder;
	private TransformerFactory tFactory;
	private Transformer transformer;

	private final ArrayList associatedPolicies = new ArrayList();
	private final ArrayList associatedSubjectClasses = new ArrayList();
	private final XPathNamespaceContext nameSpaceContext = new SOAPNameSpaceContext();
	private final XPathInfo soapHeaderXPathInfo = new XPathInfo("//s12:Header",nameSpaceContext);
	private final XPathInfo dualChannelValuesHeaderXPathInfo = new XPathInfo("//s12:Header/dc:DualChannel/dc:Value",nameSpaceContext);
	private final QName dualChannelNameSpace = QNameFactory.getInstance().getQName("http://www.itm.uni-luebeck.de/projects/tekomed/dualchannel");

	private static final String ALGORITHM_ATTR="algorithm";
	private static final String TRANSFORM_ATTR="transform";
	private static final String XPATH_ATTR = "xpath";
	private static final String DUALCHANNEL_TAG = "DualChannel";
	private static final String VALUE_TAG = "Value";
	private static final String DEFAULT_DUALCHANNEL_NSPREFIX = "dc";

	private final static XmlPullParserFactory XPP_FACTORY;





	static
	{
		XmlPullParserFactory factory = null;
		try
		{
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
		}
		catch (XmlPullParserException e)
		{
			Log.error("Could not create XmlPullParserFactory: " + e);
			e.printStackTrace();
			throw new RuntimeException("Could not create XmlPullParserFactory: " + e);
		}

		XPP_FACTORY = factory;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.interception.OutboundSOAPUTF8TransformationInterceptor#interceptOutbound(java.io.InputStream, org.ws4d.java.communication.ProtocolData, com.draeger.medical.mdpws.qos.QoSMessageContext, com.draeger.medical.mdpws.qos.QoSPolicy, java.io.OutputStream)
	 */
	public DualChannelInterceptorImpl() {
		super();
		this.docBuilderFactory = DocumentBuilderFactory.newInstance();
		this.docBuilderFactory.setNamespaceAware(true);
		// Use a Transformer for output
		this.tFactory = TransformerFactory.newInstance();

		//tick the security engine
		SecurityEngine.getInstance().tick();

		nameSpaceContext.addNamespace(DEFAULT_DUALCHANNEL_NSPREFIX, dualChannelNameSpace.toStringPlain());

		associatedPolicies.add(InOutboundDualChannelQoSPolicy.class);
		associatedPolicies.add(InboundDualChannelQoSPolicy.class);
		associatedPolicies.add(OutboundDualChannelQoSPolicy.class);
		associatedSubjectClasses.add(ServicePolicySubject.class);
		associatedSubjectClasses.add(OperationPolicySubject.class);
		associatedSubjectClasses.add(MessagePolicySubject.class);
	}

	@Override
	public boolean interceptOutbound(
			InputStream interceptableSOAPUTF8InputStream, ProtocolData pd,
			QoSMessageContext qosMsgCtxt, QoSPolicy policy,
			OutputStream soapUTF8OutputStream, org.ws4d.java.schema.Element element) throws InterceptionException {

		if (policy instanceof DualChannelPolicy) {
			DualChannelPolicy dualChannelPolicy = (DualChannelPolicy) policy;
			try{
				Document inputDocument = getDocumentBuilder().parse(new InputSource(interceptableSOAPUTF8InputStream));
				try {


					Document dualChannelDoc = prepareDualChannelDocument(
							inputDocument, qosMsgCtxt);

					if (Log.isDebug()) {
						String outputString = org.apache.ws.security.util.XMLUtils.PrettyDocumentToString(dualChannelDoc);
						Log.debug("DualChannel outputString:"+outputString);
						Log.debug("Add Dual Channel to message [Policy: " + policy+ "]");
					}


					Node soapHeader = getSOAPHeaderNode(dualChannelDoc);
					//Log.debug("SOAPHeader: "+soapHeader);
					if (soapHeader != null) {
						appendDualChannelNode(dualChannelDoc, soapHeader,
								dualChannelPolicy, qosMsgCtxt, pd, element);
					}

					DOMSource source = new DOMSource(dualChannelDoc);
					StreamResult result = new StreamResult(soapUTF8OutputStream);
					getTransformer().transform(source, result);

				} catch (Exception e) {
					callQoSExceptionHandler(dualChannelPolicy, inputDocument, pd, qosMsgCtxt, null, "Could not add dual channel info! "+e.getMessage(), e);

					DOMSource source = new DOMSource(inputDocument);
					StreamResult result = new StreamResult(soapUTF8OutputStream);
					getTransformer().transform(source, result);
				}
			}catch(Exception e)
			{
				Log.error("Fatal error! "+e.getMessage());
			}
		}

		return false;
	}

	/**
	 * @param dualChannelDoc 
	 * @param policy 
	 * @param qosMsgCtxt 
	 * @param protocolData 
	 * @param iMessageEndpoint 
	 * @return
	 * @throws Exception 
	 */
	private Node appendDualChannelNode(Document dualChannelDoc,
			Node soapHeader, DualChannelPolicy policy,
			QoSMessageContext qosMsgCtxt, ProtocolData protocolData, org.ws4d.java.schema.Element element) throws Exception {

		String dualChannelNS = dualChannelNameSpace.toStringPlain();

		HashMap declaredNameSpaces = XPathInfo
		.getDeclaredNamesSpaces(dualChannelDoc);

		String dualChannelNSPrefix = (String) declaredNameSpaces
		.get(dualChannelNS);
		if (dualChannelNSPrefix == null) {
			dualChannelNSPrefix = DEFAULT_DUALCHANNEL_NSPREFIX;
			declaredNameSpaces.put(dualChannelNS, dualChannelNSPrefix);
		}

		Element dualChannelSOAPHeaderElement = dualChannelDoc.createElementNS(
				dualChannelNS, dualChannelNSPrefix + ":"+DUALCHANNEL_TAG);
		soapHeader.appendChild(dualChannelSOAPHeaderElement);

		Iterator qosTokens = qosMsgCtxt.getQoSPolicyToken();
		boolean dualChannelHeaderHasContent=false;
		if (Log.isDebug())
			Log.debug("QoSToken available: "+qosTokens.hasNext());

		while (qosTokens.hasNext()) {
			try{
				@SuppressWarnings("unchecked")
				QoSPolicyToken<DualChannel<?, ?, ?>, DualChannelPolicy> token = (QoSPolicyToken<DualChannel<?, ?, ?>, DualChannelPolicy>) qosTokens.next();

				if (token instanceof DualChannelPolicyToken) {

					DualChannelPolicyToken<DualChannel<?, ?, ?>, DualChannelPolicy> dualChannelPolicyToken = (DualChannelPolicyToken<DualChannel<?, ?, ?>, DualChannelPolicy>) token;

					if (!policy.equals(dualChannelPolicyToken.getQoSPolicy()))
					{
						if (Log.isDebug())
							Log.debug(policy+" "+dualChannelPolicyToken.getQoSPolicy());

						continue;
					}

					DualChannelPolicyAttributes attributes = policy.getDualChannelPolicyAttributes();
					QName transformQName = attributes.getTransform();
					QName algorithmQName = attributes.getAlgorithm();
					XPathInfo xpath = createIdentifyingXPath(attributes.getXPath(),dualChannelPolicyToken.getValue().getIdentifyingInformation());

					if (Log.isDebug())
						Log.debug("Created identifying XPATH: "+xpath);

					TransformedChannelInfos transformedFirstChannelInfos = getTransformedFirstChannelInfosFromDocument(
							dualChannelDoc, xpath, policy);

					Set transformedSet= transformedFirstChannelInfos.entrySet();

					boolean countNodes=transformedSet.size()>1;
					int identifiedNodesCnt=1;
					if (!transformedSet.isEmpty())
					{
						for (Iterator transformedNodes=transformedSet.iterator();transformedNodes.hasNext();identifiedNodesCnt++)
						{
							Entry transformedNodesEntry=(Entry) transformedNodes.next();

							byte[] serializedData=(byte[]) transformedNodesEntry.getValue();

							//create the value elements
							Element value = dualChannelDoc.createElementNS(dualChannelNS,
									dualChannelNSPrefix + ":"+VALUE_TAG);

							addDualChannelQNameAttribute(dualChannelNS, value,
									algorithmQName, declaredNameSpaces, dualChannelNSPrefix
									+ ":"+ALGORITHM_ATTR);

							addDualChannelQNameAttribute(dualChannelNS, value,
									transformQName, declaredNameSpaces, dualChannelNSPrefix
									+ ":"+TRANSFORM_ATTR);

							String expr=(countNodes?"(":"")+xpath.getExpression()+(countNodes?")["+identifiedNodesCnt+"]":"");
							addDualChannelXPathAttributes(dualChannelNS, value, xpath.getContext(),expr,
									declaredNameSpaces, dualChannelNSPrefix + ":"+XPATH_ATTR);

							//TODO SSch add //s12:body where necessary???
							if (isFirstChannelValid(dualChannelPolicyToken, serializedData, policy, element)) 
							{
								String secChannelRepresentation = computeSecondChannelRepresentation(serializedData, policy);
								value.setTextContent(secChannelRepresentation);
								dualChannelSOAPHeaderElement.appendChild(value);
								dualChannelHeaderHasContent=true;
							} else {
								String outputString = org.apache.ws.security.util.XMLUtils.PrettyDocumentToString(dualChannelDoc);
								callQoSExceptionHandler(policy,dualChannelDoc,protocolData,qosMsgCtxt,token,"Required 1st channel not valid. ["+outputString+"] ["+transformedFirstChannelInfos+"] ["+policy+"]",null);
							}
						}
					}else{
						callQoSExceptionHandler(policy, dualChannelDoc, protocolData, qosMsgCtxt,token,"No such element in message! "+xpath,null);
					}


				}
			}catch(Exception e)
			{
				callQoSExceptionHandler(policy,dualChannelDoc,protocolData,qosMsgCtxt,null,"Could not add Dual channel node! "+e.getMessage(),e);
			}

		}

		if (!dualChannelHeaderHasContent)
			soapHeader.removeChild(dualChannelSOAPHeaderElement);

		if (Log.isDebug()) {
			String outputString = org.apache.ws.security.util.XMLUtils
			.PrettyDocumentToString(dualChannelDoc);
			Log.debug("DualChannel-XML: " + outputString);
		}

		return dualChannelSOAPHeaderElement;
	}

	/**
	 * @param xPath
	 * @param object 
	 * @return
	 */
	private XPathInfo createIdentifyingXPath(XPathInfo xPath, Object identifyingInformation) {
		XPathInfo xpi=xPath;
		if (identifyingInformation instanceof XPathInfo)
		{
			XPathInfo idXPath=(XPathInfo)identifyingInformation;
			String idXPExpr=idXPath.getExpression().trim();
			if (!idXPExpr.startsWith("[")&& idXPExpr.endsWith("]"))
			{
				idXPExpr="["+idXPExpr+"]";
			}

			if (idXPExpr.length()<3)
			{
				if (Log.isDebug())
					Log.debug("XPATH predicate "+idXPExpr+" not valid.");

				throw new IllegalArgumentException("XPATH predicate "+idXPExpr+" not valid.");
			}
			if (Log.isDebug())
				Log.debug("ID predicate:"+idXPExpr);

			String expression="("+xPath.getExpression()+")"+idXPExpr;
			XPathNamespaceContext context= xPath.getContext();
			context.addNamespaceContext(idXPath.getContext());
			xpi=new XPathInfo(expression, context);
		}
		return xpi;
	}

	/**
	 * @param firstChannelInfos
	 * @param policy
	 * @return
	 */
	private String computeSecondChannelRepresentation(
			byte[] serializedData,
			DualChannelPolicy policy) {
		DualChannelProtocolConverter converter = policy.getDualChannelProtocolConverter(policy.getDualChannelPolicyAttributes());
		return converter.getDualChannelRepresentation(serializedData);
	}

	/**
	 * @param dualChannelPolicyToken
	 * @param policy 
	 * @param transformedFirstChannelInfos 
	 * @param msgEndpoint 
	 * @return
	 * @throws Exception 
	 */
	private boolean isFirstChannelValid(
			DualChannelPolicyToken<DualChannel<?, ?, ?>, DualChannelPolicy> dualChannelPolicyToken,
			byte[] serializedData,
			DualChannelPolicy policy, org.ws4d.java.schema.Element element) throws Exception {
		DualChannelLocal2MDPWSConverter converter = policy.getDualChannelLocalConverter();

		boolean retVal = false;
		IParameterValue pv=createPVFromBytes(serializedData,policy.getDualChannelPolicyAttributes(), element);
		Object computedFirstChannel =converter.createObjectFromParameterValue(pv,policy.getDualChannelPolicyAttributes());
		DualChannel<?, ?, ?> dualChannel = dualChannelPolicyToken.getValue();
		DualChannelComparator comparator = policy.getDualChannelComparator();
		retVal = comparator.compare(computedFirstChannel, dualChannel);

		return retVal;
	}

	/**
	 * @param serializedData
	 * @param dualChannelPolicyAttributes
	 * @param msgEndpoint 
	 * @return
	 * @throws XmlPullParserException 
	 */
	private IParameterValue createPVFromBytes(byte[] serializedData,
			DualChannelPolicyAttributes dualChannelPolicyAttributes, org.ws4d.java.schema.Element element) throws Exception {
		IParameterValue retVal=null;
		String val=null;
		if (serializedData!=null)
			val=new String(serializedData,XMLConstants.ENCODING);

		if (Log.isDebug())
			Log.debug("Create PV from: "+val);

		if (element!=null)
		{
			//Determine if the string contains valid xml...
			if (val!=null && val.startsWith("<"))
			{
				XmlPullParser parser = XPP_FACTORY.newPullParser();
				parser.setInput(new StringReader(val));

				if (val.startsWith("<xml"))
					parser.nextTag(); // Skip <xml...>

				SOAP2MessageGenerator generator= SOAPMessageGeneratorFactory.getInstance().getSOAP2MessageGeneratorForCurrentThread();

				retVal= generator.getParameterValueParser().parseParameterValue(new ElementParser(parser,null),element);
				SOAPMessageGeneratorFactory.getInstance().releaseSOAP2MessageGenerator(generator);
			}else if (val!=null){
				retVal=StringValueFactory.getInstance().getStringValue();
				((StringValue)retVal).set(val); 
			}
		}
		return retVal;
	}

	/**
	 * @param dualChannelDoc
	 * @param xpath
	 * @param object 
	 * @return
	 * @throws XPathExpressionException 
	 */
	private TransformedChannelInfos getTransformedFirstChannelInfosFromDocument(Document dualChannelDoc, XPathInfo xpath, DualChannelPolicy policy) throws Exception {

		TransformedChannelInfos channelInfos = new TransformedChannelInfos();

		if (xpath!=null)
		{
			Object xpathResult=xpath.evaluate(dualChannelDoc);	
			Node[] nodes=getNodes(xpathResult,dualChannelDoc);
			if (nodes != null && nodes.length > 0) 
			{


				transformFirstChannelNodes( policy, channelInfos, nodes);
			}
		}


		return channelInfos;
	}

	private Node[] getNodes(Object xpathResult, Document doc)
	{
		Node[] nodes=null;

		NodeList nodeList=null;
		if (xpathResult!=null)
		{
			if (xpathResult instanceof NodeList)
			{
				nodeList=(NodeList)xpathResult;
			}else if (xpathResult instanceof String){
				Text t=doc.createTextNode((String)xpathResult);
				nodes=new Node[]{t};
			}else{
				// TODO SSch other xpath result types...
				Log.warn("Could not handle xpath result");
			}
		}

		if (nodeList != null && nodeList.getLength() > 0) {
			nodes=new Node[nodeList.getLength()];
			for(int j=0;j<nodeList.getLength();j++)
			{
				nodes[j]=nodeList.item(j);
			}
		}

		return nodes;
	}

	/**
	 * @param xpath
	 * @param policy
	 * @param channelInfos
	 * @param nodes
	 */
	protected void transformFirstChannelNodes(
			DualChannelPolicy policy, TransformedChannelInfos channelInfos,
			Node[] nodes) {
		if (nodes != null && nodes.length > 0) {

			//			if (nodes.length > 1)
			//				throw new IllegalArgumentException(
			//				"NodeList contains more than one node.");

			DualChannelProtocolConverter transformator = policy.getDualChannelProtocolConverter(policy.getDualChannelPolicyAttributes());

			if (transformator == null)
				throw new IllegalStateException(
						"Could not get a transformator "
						+ policy.getDualChannelPolicyAttributes()
						.getTransform());

			transformator.transformForSerialization(nodes, channelInfos);
		}
	}

	/**
	 * @param dualChannelNS
	 * @param value
	 * @param xpath
	 * @param declaredNameSpaces
	 * @param attributeNamePrefixed
	 */
	private void addDualChannelXPathAttributes(String dualChannelNS,
			Element value, XPathNamespaceContext ctxt,String expr, HashMap declaredNameSpaces,
			String attributeNamePrefixed) 
	{
		expr = transformXPathExpression(value, expr,
				declaredNameSpaces, ctxt);

		value.setAttributeNS(
				dualChannelNS,
				attributeNamePrefixed,expr);
	}

	/**
	 * @param value
	 * @param xPathInfo
	 * @param declaredNameSpaces
	 * @param ctxt
	 * @return
	 */
	protected String transformXPathExpression(Element value,
			String expr, HashMap declaredNameSpaces,
			XPathNamespaceContext ctxt) {
		if (expr!=null && ctxt!=null)
		{
			java.util.Iterator<java.util.Map.Entry<String, String>> entries= ctxt.getAllEntries();
			while(entries.hasNext())
			{
				java.util.Map.Entry<String, String> entry=entries.next();
				String prefixOrig=entry.getKey();
				if (expr.contains(prefixOrig+":"))
				{
					String prefix=getNameSpacePrefix(value,declaredNameSpaces,entry.getValue());

					if (Log.isDebug())
						Log.debug("Replace "+prefixOrig+" with "+prefix+" "+entry.getValue()+" in "+expr);

					expr=expr.replace(prefixOrig+":", prefix+":");
				}
			}
		}
		return expr;
	}

	/**
	 * @param dualChannelNS
	 * @param attrValueNSPrefix
	 * @param valueElement
	 * @param declaredNameSpaces 
	 * @param attributeNamePrefixed 
	 */
	protected void addDualChannelQNameAttribute(String dualChannelNS,
			Element valueElement, QName attrValue, HashMap declaredNameSpaces,
			String attributeNamePrefixed) {
		String nameSpace = attrValue.getNamespace();
		String nameSpacePrefix = getNameSpacePrefix(valueElement, declaredNameSpaces,
				nameSpace);

		valueElement.setAttributeNS(
				dualChannelNS,
				attributeNamePrefixed,
				(nameSpace == null ? "" : (nameSpacePrefix + ":"))
				+ attrValue.getLocalPart());
	}

	/**
	 * @param valueElement
	 * @param declaredNameSpaces
	 * @param nameSpace
	 * @return
	 */
	protected String getNameSpacePrefix(Element valueElement,
			HashMap declaredNameSpaces, String nameSpace) {
		String nameSpacePrefix = (String) declaredNameSpaces.get(nameSpace);
		if (nameSpacePrefix == null && nameSpace != null && valueElement!=null) {
			nameSpacePrefix = "i" + System.nanoTime();
			declaredNameSpaces.put(nameSpace, nameSpacePrefix);

			//for the attribute qname value we have to explicitly add the namespace
			valueElement
			.setAttributeNS(XMLConstants.XMLNS_NAMESPACE_NAME,
					XMLConstants.XMLNS_NAMESPACE_PREFIX + ":"
					+ nameSpacePrefix, nameSpace);
		}
		return nameSpacePrefix;
	}

	/**
	 * @param dualChannelDoc
	 * @return
	 * @throws XPathExpressionException 
	 */
	private Node getSOAPHeaderNode(Document dualChannelDoc)
	throws XPathExpressionException {
		return (Node) soapHeaderXPathInfo.evaluate(dualChannelDoc,
				XPathConstants.NODE);
	}

	private NodeList getDualChannelValueNodes(Document dualChannelDoc)
	throws XPathExpressionException {
		return (NodeList) dualChannelValuesHeaderXPathInfo.evaluate(dualChannelDoc,
				XPathConstants.NODESET);
	}

	/**
	 * @param inputDocument
	 * @param qosMsgCtxt
	 * @return
	 */
	private Document prepareDualChannelDocument(Document inputDocument,
			QoSMessageContext qosMsgCtxt) {
		return inputDocument;
	}

	private synchronized DocumentBuilder getDocumentBuilder()
	throws ParserConfigurationException {
		if (this.builder == null)
			this.builder = this.docBuilderFactory.newDocumentBuilder();

		return this.builder;
	}

	private synchronized Transformer getTransformer()
	throws TransformerConfigurationException {
		if (this.transformer == null)
			this.transformer = tFactory.newTransformer();
		return this.transformer;
	}

	@Override
	public QoSPolicyOrdinalNumber getOrdinalNumberForSubject(Class<?> subjectClass) {
		if (ServicePolicySubject.class.isAssignableFrom(subjectClass))
			return new QoSPolicyOrdinalNumber(0);
		else
			return QoSPolicyOrdinalNumber.NOT_APPLICABLE;
	}

	@Override
	public Iterator getQoSPolicyClasses() {
		return new ReadOnlyIterator(associatedPolicies.iterator());
	}

	@Override
	public Iterator getInterceptorSubjectClasses() {
		return new ReadOnlyIterator(associatedSubjectClasses.iterator());
	}

	@Override
	public QoSPolicyInterceptionDirection getInterceptionDirection() {
		return QoSPolicyInterceptionDirection.OUTBOUND;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.interception.InboundSOAPXMLInputStreamInterceptor#interceptInbound(java.io.InputStream, org.ws4d.java.communication.DPWSProtocolData, org.ws4d.java.communication.protocol.soap.generator.MessageReceiver, com.draeger.medical.mdpws.qos.QoSMessageContext, com.draeger.medical.mdpws.qos.QoSPolicy)
	 */
	@Override
	public Document interceptInbound(Document inboundSOAPDocument,
			DPWSProtocolData protocolData, MessageReceiver to,
			QoSMessageContext qosMessageCtxt, QoSPolicy policy, String action)
	throws InterceptionException {

		Document xmlOutputDocument = inboundSOAPDocument;
		if (policy instanceof DualChannelPolicy)
		{
			DualChannelPolicy dcPolicy=(DualChannelPolicy)policy;
			try {
				if (inboundSOAPDocument != null) {


					org.ws4d.java.schema.Element element=InterceptorUtil.getOutboundElement(to,action);
					List<DualChannel<?,?,?>> dualChannels = extractDualChannelInformation(inboundSOAPDocument, dcPolicy, qosMessageCtxt, protocolData, element);

					if (dualChannels!=null && !dualChannels.isEmpty())
					{
						for (DualChannel<?, ?, ?> dualChannel : dualChannels) {
							DualChannelPolicyToken<DualChannel<?,?,?>, DualChannelPolicy> token=new DualChannelPolicyToken<DualChannel<?,?,?>, DualChannelPolicy>(dcPolicy, dualChannel, QoSPolicyInterceptionDirection.INBOUND);
							qosMessageCtxt.addQoSPolicyToken(token);						
						}
					}

					xmlOutputDocument = inboundSOAPDocument;
				}
			} catch (Exception ex) {
				callQoSExceptionHandler(policy,inboundSOAPDocument,protocolData,qosMessageCtxt,null,ex.getMessage(),ex);
				throw new InterceptionException(ex);
			}
		}

		return xmlOutputDocument;
	}



	/**
	 * @param inboundSOAPDocument
	 * @param policy
	 * @param qosMessageCtxt 
	 * @param protocolData 
	 * @return
	 * @throws Exception 
	 */
	private List<DualChannel<?,?,?>> extractDualChannelInformation(
			Document inboundSOAPDocument, DualChannelPolicy policy, QoSMessageContext qosMessageCtxt, DPWSProtocolData protocolData, org.ws4d.java.schema.Element element) throws Exception {
		java.util.ArrayList<DualChannel<?,?,?>> extractedDualChannels = new java.util.ArrayList<DualChannel<?, ?, ?>>();

		DualChannelPolicyAttributes policyAttributes=policy.getDualChannelPolicyAttributes();
		if (policyAttributes==null) {
			Log.warn("No Policy attributes available");
			return extractedDualChannels;
		}

		NodeList valueNodes = getDualChannelValueNodes(inboundSOAPDocument);

		if (valueNodes!=null && valueNodes.getLength()>0)
		{
			String dualChannelNS = dualChannelNameSpace.toStringPlain();
			HashMap declaredNameSpaces = XPathInfo.getDeclaredNamesSpaces(inboundSOAPDocument);

			XPathNamespaceContext ctxt= createXPathSpaceContext(declaredNameSpaces);

			int nodeCnt=valueNodes.getLength();
			for(int valueNodeCnt=0;valueNodeCnt<nodeCnt;valueNodeCnt++)
			{

				Node dualChannelValueNode=valueNodes.item(valueNodeCnt);
				NamedNodeMap attributes=dualChannelValueNode.getAttributes();
				QName algorithm=null;
				QName transform=null;
				XPathInfo xPathInfo=null;

				String secondChannelValue=dualChannelValueNode.getTextContent();

				if (secondChannelValue==null)
				{
					callQoSExceptionHandler(policy,inboundSOAPDocument,protocolData,qosMessageCtxt,null,"Second Channel has no value !",null);
				}

				if (attributes!=null && attributes.getLength()>0)
				{
					int attrCnt=attributes.getLength();
					for (int a=0;a<attrCnt && (algorithm==null || transform==null || xPathInfo==null);a++)
					{
						Attr attrNode=(Attr) attributes.item(a);
						if (dualChannelNS.equals(attrNode.getNamespaceURI()))
						{
							String tempValue=attrNode.getValue();
							if ("algorithm".equals(attrNode.getLocalName()))
							{
								algorithm=parsePrefixValue(declaredNameSpaces,
										tempValue);
							}else if ("transform".equals(attrNode.getLocalName())){
								transform=parsePrefixValue(declaredNameSpaces,
										tempValue);
							}else if ("xpath".equals(attrNode.getLocalName())){

								tempValue=transformXPathExpression(null, tempValue,declaredNameSpaces,ctxt);
								xPathInfo=new XPathInfo(tempValue, ctxt);
							}
						}
					}
				}
				if (Log.isDebug())
					Log.debug("Dual Channel Information "+algorithm+" "+transform+" "+xPathInfo+" "+dualChannelValueNode.getTextContent());


				//set default values if necessary
				if (algorithm==null)
					algorithm=SPAConstants.SPA_ATTRIB_ALGORITHM_DUALCHANNEL_VALUE_SHA1D;
				if (transform==null)
					transform=SPAConstants.SPA_ATTRIB_TRANSFORM_DUALCHANNEL_VALUE_EXCC14NC;
				if (xPathInfo==null)
					xPathInfo=new XPathInfo(null, new SOAPNameSpaceContext());

				String policyXPath=transformXPathExpression(null, policyAttributes.getXPath().getExpression(),declaredNameSpaces,policyAttributes.getXPath().getContext());

				String xPathInfoModified=createModifiedXPathString(xPathInfo.getExpression());

				//compare attributes with policy attributes, validate content and recreate second channel
				if (!algorithm.equals(policyAttributes.getAlgorithm()) || !transform.equals(policyAttributes.getTransform())
						|| (!xPathInfo.getExpression().equals(policyXPath) && !xPathInfoModified.equals(policyXPath)))
				{
					if (Log.isDebug())
						Log.debug(algorithm+" "+transform+" "+xPathInfo+" does mot match "+policyAttributes+" "+policyXPath);

					continue;
				}

				//get the referenced content nodes
				Object xPathresult=xPathInfo.evaluate(inboundSOAPDocument);
				//				NodeList contentNodesList=(NodeList) xPathresult;
				Node[] contentNodes=getNodes(xPathresult, inboundSOAPDocument); 
				if (contentNodes==null || contentNodes.length==0)
				{
					callQoSExceptionHandler(policy,inboundSOAPDocument,protocolData,qosMessageCtxt,null,"Referenced content not found in document: "+xPathInfo,null);
				}else{

					//copy dom nodes, transform the nodes and run the algorithm on the copied dom nodes 
					Node[] contentNodesCopy=new Node[contentNodes.length];
					for(int j=0;j<contentNodes.length;j++)
					{
						contentNodesCopy[j]=contentNodes[j].cloneNode(true); //deep-clone the content node
					}

					TransformedChannelInfos channelInfos = new TransformedChannelInfos();
					transformFirstChannelNodes(policy, channelInfos, contentNodesCopy);

					String secChannelRepresentation = computeSecondChannelRepresentation((byte[])channelInfos.get(contentNodesCopy[0]), policy);

					//Validate if the copied dom nodes are not corrupted
					if (!secondChannelValue.equals(secChannelRepresentation))
					{
						callQoSExceptionHandler(policy,inboundSOAPDocument,protocolData,qosMessageCtxt,null,"Checksum comparison failed. In document ["+secondChannelValue+"] vs. calculated from copy ["+secChannelRepresentation+"]",null);
					}else{

						//ok, lets convert the data to a native struct
						DualChannelLocal2MDPWSConverter converter2Local = policy.getDualChannelLocalConverter();

						byte[] serializedData=null;
						Node copiedDOMNode=contentNodesCopy[0]; //should be only one node in the list otherwise the xpath expression is not clear enough
						try{
							Transformer t = TransformerFactory.newInstance().newTransformer();
							t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
							StringWriter sw = new StringWriter();
							t.transform(new DOMSource(copiedDOMNode), new StreamResult(sw));
							serializedData= sw.toString().getBytes(XMLConstants.ENCODING);
						}catch(Exception ex)
						{
							callQoSExceptionHandler(policy,inboundSOAPDocument,protocolData,qosMessageCtxt,null,"Could not create serializedData !",null);
						}

						IParameterValue pv=createPVFromBytes(serializedData,policy.getDualChannelPolicyAttributes(), element);
						Object computedFirstChannel =converter2Local.createObjectFromParameterValue(pv,policy.getDualChannelPolicyAttributes());
						IParameterValue pv2Channel=createPVFromBytes(serializedData,policy.getDualChannelPolicyAttributes(), element);
						Object computedSecondChannel =converter2Local.createObjectFromParameterValue(pv,policy.getDualChannelPolicyAttributes());

						//create a dom from the copy and compare it with the copy of the dom nodes that have been validated beforehand
						try {
							//					ParameterValue pv2Channel=converter2Local.getParameterValue(computedSecondChannel, policyAttributes, domnode);
							Node temptRootElement = createDOMNode(pv2Channel, inboundSOAPDocument);
							if (temptRootElement==null || !copiedDOMNode.isEqualNode(temptRootElement))
							{
								Log.warn("DOM node and copy are not equal: "+copiedDOMNode+" != "+temptRootElement);
							}

						} catch (Exception e) 
						{
							callQoSExceptionHandler(policy,inboundSOAPDocument,protocolData,qosMessageCtxt,null,e.getMessage(),e);
						}

						//transform the struct copy to the machine-dependent second channel format
						Object computedSecondChannelInLocalRep=converter2Local.convertSecondChannel2LocalRepresentation(computedSecondChannel, policyAttributes);
						String identifyingExpression=getIdentifyingPredicate(xPathInfo, valueNodeCnt+1);
						DualChannel<Object,Object,XPathInfo> dualChannel=new DualChannel<Object,Object,XPathInfo>(computedFirstChannel, computedSecondChannelInLocalRep,new XPathInfo(identifyingExpression,xPathInfo.getContext()));
						extractedDualChannels.add(dualChannel);
						Log.info("Dual Channel was OK. Added to extracted Dual Channels.");
					}
				}
			}
		}else{
			callQoSExceptionHandler(policy,inboundSOAPDocument,protocolData,qosMessageCtxt,null,"Could not determine 'dc:Value'-nodes in the message for "+policyAttributes.getXPath(),null);
		}

		return extractedDualChannels;
	}

	/**
	 * @param xPathInfo 
	 * @param valueNodeCnt 
	 * @return
	 */
	private String getIdentifyingPredicate(XPathInfo xPathInfo, int valueNodeCnt) 
	{
		return "["+Integer.toString(valueNodeCnt)+"]";
	}

	/**
	 * @param expression
	 * @return
	 */
	private String createModifiedXPathString(String expression) {
		return (expression.length()>1 && expression.lastIndexOf(")")>-1)?(expression.substring(1).substring(0,expression.lastIndexOf(")")-1)):expression;
	}

	/**
	 * @param pv
	 * @param inboundSOAPDocument 
	 * @return
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	protected Node createDOMNode(IParameterValue pv, Document inboundSOAPDocument) throws IOException,
	UnsupportedEncodingException, SAXException,
	ParserConfigurationException {

		Node temptRootElement=null;
		if (pv instanceof StringValue)
		{
			StringValue sValue=(StringValue)pv;
			temptRootElement=inboundSOAPDocument.createTextNode(sValue.get());
		}else if (pv !=null){
			Message2SOAPGenerator msgGenerator= SOAPMessageGeneratorFactory.getInstance().getMessage2SOAPGeneratorForCurrentThread();
			Message2SOAPConverter msgConverter= msgGenerator.getMessageSerializer(); 

			ByteArrayOutputStream bout = new ByteArrayOutputStream(); 

			msgConverter.getParameterValueSerializer().serializeParameterValue(pv, bout, null);
			SOAPMessageGeneratorFactory.getInstance().releaseMessage2SOAPGenerator(msgGenerator);

			if (Log.isDebug()){
				String pvString=bout.toString(XMLConstants.ENCODING);
				Log.debug("PVString: "+pvString);
			}
			//			InputStream in=InputStreamUtil.convertStringToInputStream(pvString);
			bout.flush();
			InputStream in=new ByteArrayInputStream(bout.toByteArray());
			bout.close();

			Document tempDocument= docBuilderFactory.newDocumentBuilder().parse(in);
			temptRootElement=tempDocument.getDocumentElement();
		}

		return temptRootElement;
	}

	/**
	 * @param declaredNameSpaces
	 * @param prefixedString
	 */
	protected QName parsePrefixValue(HashMap declaredNameSpaces, String prefixedString) {
		QName qNameValue;
		StringTokenizer tokenizer=new StringTokenizer(prefixedString,":");
		if (tokenizer.countTokens()==2)
		{
			String prefix=tokenizer.nextToken();
			String namespace=findNameSpaceForPrefix(declaredNameSpaces,prefix);
			if (namespace!=null)
			{
				qNameValue=QNameFactory.getInstance().getQName(tokenizer.nextToken(), namespace);
			}else{
				qNameValue=QNameFactory.getInstance().getQName(prefixedString);	
			}

		}else{
			qNameValue=QNameFactory.getInstance().getQName(prefixedString);
		}

		return qNameValue;
	}

	/**
	 * @param declaredNameSpaces
	 * @param prefix
	 * @return
	 */
	private String findNameSpaceForPrefix(HashMap declaredNameSpaces,String prefix) {
		if (declaredNameSpaces!=null && !declaredNameSpaces.isEmpty())
		{
			Set entries= declaredNameSpaces.entrySet();
			for (Iterator entIt=entries.iterator();entIt.hasNext();)
			{
				Entry entry= (Entry) entIt.next();
				if (entry.getValue().equals(prefix))
				{
					return entry.getKey().toString();
				}
			}
		}
		return null;
	}

	/**
	 * @param declaredNameSpaces
	 * @return
	 */
	private XPathNamespaceContext createXPathSpaceContext(
			HashMap declaredNameSpaces) 
	{
		XPathNamespaceContext nsContext=new SOAPNameSpaceContext();
		if (declaredNameSpaces!=null && !declaredNameSpaces.isEmpty())
		{
			Set entries= declaredNameSpaces.entrySet();
			for (Iterator entIt=entries.iterator();entIt.hasNext();)
			{
				Entry entry= (Entry) entIt.next();
				nsContext.addNamespace((String)entry.getValue(), (String)entry.getKey());
			}
		}
		return nsContext;
	}

	/**
	 * @param policy
	 * @param inboundSOAPDocument
	 * @param protocolData 
	 * @param object 
	 * @param qosMsgCtxt 
	 * @param e 
	 * @throws InterceptionException 
	 */
	private void callQoSExceptionHandler(QoSPolicy policy,
			Document soapDocument, ProtocolData protocolData, QoSMessageContext qosMsgCtxt, QoSPolicyToken<?,?> token, String additionalInfo, Throwable t) throws InterceptionException {
		//		Log.warn(additionalInfo);
		//		if (t!=null)
		//			Log.printStackTrace(t);

		if (!(t instanceof InterceptionException))
		{
			boolean throwException=QoSPolicyExceptionHandlerManager.getInstance().callExceptionHandler(policy, soapDocument, token, qosMsgCtxt, protocolData, additionalInfo);
			if (throwException)
				throw new InterceptionException(additionalInfo);
		}
	}

	public static class TransformedChannelInfos {
		private final HashMap map = new HashMap();

		public Object get(Object key) {
			return map.get(key);
		}

		public boolean isEmpty() {
			return map.isEmpty();
		}

		public Object put(Object key, Object value) {
			return map.put(key, value);
		}

		public Object remove(Object key) {
			return map.remove(key);
		}

		public int size() {
			return map.size();
		}

		public DataStructure values() {
			return map.values();
		}

		public Set entrySet() {
			return map.entrySet();
		}

		@Override
		public String toString() {
			StringBuilder builder=new StringBuilder();
			try {
				Iterator it= map.entrySet().iterator();
				while(it.hasNext())
				{
					byte[] val=(byte[])((Entry) it.next()).getValue();

					builder.append(new String(val,XMLConstants.ENCODING));

				}
			} catch (UnsupportedEncodingException e) {
				Log.warn(e);
			}
			return builder.toString();
		}

	}



}
