package com.draeger.medical.mdpws.qos.safetyinformation.transmission;

import java.util.Arrays;
import java.util.List;

@SuppressWarnings("rawtypes")
public class SafetyInformation {
	protected List<DualChannel> dualChannels;
	protected List<SafetyContext> safetyContexts;
	
	public List<DualChannel> getDualChannels() {
		return dualChannels;
	}
	public void setDualChannels(List<DualChannel> dualChannels) {
		this.dualChannels = dualChannels;
	}
	public List<SafetyContext> getSafetyContexts() {
		return safetyContexts;
	}
	public void setSafetyContexts(List<SafetyContext> safetyContexts) {
		this.safetyContexts = safetyContexts;
	}
	
	@Override
	public String toString() {
		return "SafetyInformation [dualChannels=" + (dualChannels!=null?Arrays.toString(dualChannels.toArray()):null)
				+ ", safetyContexts=" + (safetyContexts!=null?Arrays.toString(safetyContexts.toArray()):null) + "]";
	}
	
	
	
	
}
