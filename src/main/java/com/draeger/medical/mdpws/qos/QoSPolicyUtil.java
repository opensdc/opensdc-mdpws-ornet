/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package com.draeger.medical.mdpws.qos;

import org.ws4d.java.message.Message;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.OperationDescription;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.util.Log;

import com.draeger.medical.mdpws.domainmodel.MDPWSDefaultDevice;
import com.draeger.medical.mdpws.domainmodel.impl.device.MDPWSService;
import com.draeger.medical.mdpws.qos.management.QoSPolicyManager;
import com.draeger.medical.mdpws.qos.subjects.MessagePolicySubject;
import com.draeger.medical.mdpws.qos.subjects.OperationPolicySubject;
import com.draeger.medical.mdpws.qos.subjects.QoSPolicySubject;
import com.draeger.medical.mdpws.qos.subjects.ServicePolicySubject;

/**
 *
 *
 */
public class QoSPolicyUtil {

	/**
	 * @param localLoggingPolicy
	 * @param string
	 * @param b
	 */
	public static void addPolicyToMessage(MDPWSDefaultDevice device, QoSPolicy policy,
			String operation, boolean inbound, boolean outbound) {
		Iterator serviceIterator= device.getServices();
		QName opQName=QNameFactory.getInstance().getQName(operation);
		while (serviceIterator.hasNext())
		{
			Object o=serviceIterator.next();
			if (o instanceof MDPWSService)
			{
				MDPWSService service=(MDPWSService)o;
				Operation op= service.getAnyOperation(QNameFactory.getInstance().getQName(opQName.getNamespace()), opQName.getLocalPart());
				if (op!=null)
				{
					MessagePolicySubject msgPolicySubject=new MessagePolicySubject(inbound, outbound, false, op, policy.isAbstractPolicy());
					policy.addSubject(msgPolicySubject);
				}
			}
		}
	}
	
	public static void addPolicyToOperation(MDPWSDefaultDevice device, QoSPolicy policy,
			String operation) {
		Iterator serviceIterator= device.getServices();
		QName opQName=QNameFactory.getInstance().getQName(operation);
		while (serviceIterator.hasNext())
		{
			Object o=serviceIterator.next();
			if (o instanceof MDPWSService)
			{
				MDPWSService service=(MDPWSService)o;
				Operation op= service.getAnyOperation(QNameFactory.getInstance().getQName(opQName.getNamespace()), opQName.getLocalPart());
				if (op!=null)
				{
					OperationPolicySubject msgPolicySubject=new OperationPolicySubject(op, policy.isAbstractPolicy());
					policy.addSubject(msgPolicySubject);
				}
			}
		}
	}

	/**
	 * @param policies
	 */
	public static void addPoliciesToAllServices(MDPWSDefaultDevice device,ArrayList policies) {
		Iterator serviceIterator= device.getServices();
		while (serviceIterator.hasNext())
		{
			Object o=serviceIterator.next();
			if (o instanceof MDPWSService)
			{//we can add policies only MDPWS services
				//Create the service subject

				addPoliciesToService(policies, (MDPWSService)o);
			}
		}
	}
	
	public static void addPolicyToServices(String opIdentifier, QoSPolicy policy, Iterator it) {
		while(it.hasNext())
		{
			Object o=it.next();
			if (o instanceof MDPWSService)
			{
				MDPWSService mo=(MDPWSService)o;
				OperationDescription op=null;
				op= mo.getStreamSource(opIdentifier);
				if (op!=null)
				{
					ArrayList list=new ArrayList();
					list.add(policy);
					QoSPolicyUtil.addPoliciesToService(list, mo);
					continue;
				}
				op= mo.getOperation(opIdentifier);
				if (op!=null)
				{
					ArrayList list=new ArrayList();
					list.add(policy);
					QoSPolicyUtil.addPoliciesToService(list, mo);
					continue;
				}
			}
		}
	}

	/**
	 * @param policies
	 * @param o
	 */
	public static void addPoliciesToService(ArrayList policies, MDPWSService o) {
		ServicePolicySubject serviceSubject=new ServicePolicySubject(o, false);
		ServicePolicySubject aServiceSubject=new ServicePolicySubject(o, true);
		for(Iterator policiesIt=policies.iterator();policiesIt.hasNext();)
		{
			//add either abstract or binding specific service subject to policy
			QoSPolicy policy=(QoSPolicy)policiesIt.next();
			if (policy.isAbstractPolicy())
			{
				policy.addSubject(aServiceSubject);
			}else{
				policy.addSubject(serviceSubject);
			}
		}
	}
	/**
	 * @return
	 */
	public static boolean isPolicyApplicableForThisMessage(Message msg, QoSPolicy policy) {

		String action=msg.getHeader().getAction().toString();
	
		return isPolicyApplicableForThisMessage(action, policy);
	}
	
	/**
	 * @param header
	 * @param policy
	 * @return
	 */
	private static QoSPolicySubject getAssociatedOperationPolicySubject(
			String action, QoSPolicy policy) 
	{
		Iterator msgSubjects=policy.getSubjects(OperationPolicySubject.class);
		OperationPolicySubject associatedSubject=null;

		boolean applyPolicy=false;
		while(msgSubjects.hasNext())
		{
			OperationPolicySubject operationSubject=(OperationPolicySubject)msgSubjects.next();
			String outputAction=operationSubject.getOperation().getOutputAction();
			String inputAction=operationSubject.getOperation().getInputAction();
			if(!applyPolicy)
			{
				applyPolicy=action.equals(outputAction) || action.equals(inputAction);
			}
			if (applyPolicy)
			{
				associatedSubject=operationSubject;
				break;
			}else{
				if (Log.isDebug())
					Log.debug("Policy not applicable for "+outputAction+" or "+inputAction+" on message "+action);
			}
		}
		return associatedSubject;
	}

	//filter if this policy should be applied regarding the sending service
	private static ServicePolicySubject getAssociatedServicePolicySubject(String action, QoSPolicy policy) {
		Iterator serviceSubjects=policy.getSubjects(ServicePolicySubject.class);
		ServicePolicySubject associatedSubject=null;

		boolean applyPolicy=false;
		while(serviceSubjects.hasNext())
		{
			ServicePolicySubject serviceSubject=(ServicePolicySubject)serviceSubjects.next();
			Iterator portTypes=serviceSubject.getService().getPortTypes();
			while (!applyPolicy && portTypes.hasNext())
			{
				QName portTypeQName=(QName)portTypes.next();
				applyPolicy=action.startsWith(portTypeQName.toStringPlain());
			}
			if (applyPolicy){
				associatedSubject=serviceSubject;
				break;
			}
		}
		return associatedSubject;
	}
	
	/**
	 * @param header
	 * @param policy
	 * @return
	 */
	private static MessagePolicySubject getAssociatedMessagePolicySubject(
			String action, QoSPolicy policy) {
		Iterator msgSubjects=policy.getSubjects(MessagePolicySubject.class);
		MessagePolicySubject associatedSubject=null;

		boolean applyPolicy=false;
		while(msgSubjects.hasNext())
		{
			MessagePolicySubject messageSubject=(MessagePolicySubject)msgSubjects.next();


			if(!applyPolicy && messageSubject.isOutputMessageSubject())
			{
				String outputAction=messageSubject.getOperation().getOutputAction();
				applyPolicy=action.equals(outputAction);
			}
			if (!applyPolicy && messageSubject.isInputMessageSubject())
			{
				String inAction=messageSubject.getOperation().getInputAction();
				applyPolicy=action.equals(inAction);
			}
			
			//TODO SSch FaultMessageSubject???
			if (applyPolicy){
				associatedSubject=messageSubject;
				break;
			}
		}
		return associatedSubject;
	}

	/**
	 * @param action
	 * @param policy
	 */
	public static boolean isPolicyApplicableForThisMessage(String action,
			QoSPolicy policy) {
		//TODO SSch add other endpoint policy subjects
		boolean isApplicable=false;

		QoSPolicySubject subject= getAssociatedServicePolicySubject(action,policy);
		if (subject!=null) return true;

		subject=getAssociatedOperationPolicySubject(action, policy);
		if (subject!=null) return true;

		subject=getAssociatedMessagePolicySubject(action, policy);
		if (subject!=null) return true;
		
		if (Log.isDebug())
			Log.debug("Policy ["+policy+"] is not applicable for message ["+action+"]");
		
		
		return isApplicable;
	}
	
	public static ArrayList getApplicablePoliciesForAction(String action){
		ArrayList retVal=new ArrayList();
		Iterator allPolicies=QoSPolicyManager.getInstance().getQoSPolicies();
		while(allPolicies.hasNext())
		{
			QoSPolicy policy=(QoSPolicy) allPolicies.next();
			if (isPolicyApplicableForThisMessage(action, policy))
			{
				retVal.add(policy);
			}
		}
		return retVal;
	}
	
	public static boolean containsSubjectForDevice(EndpointReference devRef, Iterator subjects) {
		boolean subjectMatched=false;
		if (subjects!=null && devRef!=null)
		{
			while (!subjectMatched && subjects.hasNext()){
				//TODO SSch Was ist mit den anderen Subjects???
				Object subject = subjects.next();
				if (subject instanceof MessagePolicySubject)
				{
					MessagePolicySubject mps=(MessagePolicySubject) subject;
					OperationDescription proxyOP = mps.getOperation();
					try{
					if(proxyOP.getService()!=null && proxyOP.getService().getServiceReference()!=null && proxyOP.getService().getServiceReference().getParentDeviceRef()!=null && devRef.equals(proxyOP.getService().getServiceReference().getParentDeviceRef().getEndpointReference()))
					{
						subjectMatched=true;
					}
					}catch(NullPointerException npe)
					{
						//TODO SSch maybe we need a lock here instead of catching the exception
						//void
					}
				}
			}
		}
		return subjectMatched;
	}
	
}
