/**
 * 
 */
package com.draeger.medical.mdpws.qos.wsdl;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

/**
 *
 *
 *SPA = SOMDA Policy Assertions
 *
 */
public interface SPAConstants 
{
	public static final String SPA_NAMESPACE="http://www.somda.org/2011/04/01/policy-assertions";
	public static final String SPA_ELEM_COMPRESSION="Compression";
	public static final String SPA_ELEM_NON_REPUDIATION="NonRepudiation";
	public static final String SPA_ELEM_AUTHENTICATION="Authentication";

	public static final String SPA_ELEM_LOGGING="Logging";
	public static final String SPA_ATTRIB_METHOD="method";
	public static final String SPA_ATTRIB_COMPRESSION_METHOD_EXI_SCHEMALESS="EXI-sl";
	public static final String SPA_ATTRIB_LOGGING_LOCAL="local";
	public static final String SPA_ATTRIB_COMPRESSION_METHOD_EXI_SCHEMAINFORMED="EXI-si";
	public static final String SPA_ATTRIB_METHOD_SIGNATURE_XML="XMLSignature";
	

	public static final String SPA_ELEM_DUALCHANNEL="DualChannelAssertion";
	public static final String SPA_ATTRIB_ALGORITHM_DUALCHANNEL="algorithm";
	public static final String SPA_ATTRIB_TRANSFORM_DUALCHANNEL="transform";
	public static final String SPA_ATTRIB_XPATH_DUALCHANNEL="xpath";
	public static final QName SPA_ATTRIB_ALGORITHM_DUALCHANNEL_VALUE_HEX=QNameFactory.getInstance().getQName("HexEncoding",SPA_NAMESPACE);
	public static final QName SPA_ATTRIB_ALGORITHM_DUALCHANNEL_VALUE_SHA1D=QNameFactory.getInstance().getQName("B64SHA1Digest",SPA_NAMESPACE); //TODO SSch replace with constant from signature
	public static final QName SPA_ATTRIB_TRANSFORM_DUALCHANNEL_VALUE_NOTRANS=QNameFactory.getInstance().getQName("noTransformation",SPA_NAMESPACE);
	public static final QName SPA_ATTRIB_TRANSFORM_DUALCHANNEL_VALUE_EXCC14NC=QNameFactory.getInstance().getQName("xml-exc-c14n#WithComments",SPA_NAMESPACE);
}
