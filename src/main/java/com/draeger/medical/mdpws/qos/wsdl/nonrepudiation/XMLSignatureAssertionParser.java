/**
 * 
 */
package com.draeger.medical.mdpws.qos.wsdl.nonrepudiation;

import java.io.IOException;

import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.util.Log;
import org.xmlpull.v1.XmlPullParserException;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionParser;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyDirection;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder;
import com.draeger.medical.mdpws.qos.management.QoSPolicyBuilderInstanceRegistry;
import com.draeger.medical.mdpws.qos.nonrepudiation.AuthenticationPolicyBuilder;
import com.draeger.medical.mdpws.qos.wsdl.SPAConstants;

/**
 *
 *
 */
public class XMLSignatureAssertionParser implements WSDLAssertionParser {


	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionParser#parse(org.ws4d.java.io.xml.ElementParser, com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint)
	 */
	@Override
	public QoSPolicy parse(ElementParser eParser,
			WSDLPolicyAttachmentPoint attachmentPoint) throws IOException {
		QoSPolicy policy=null;
		try {

			String signatureMethod=eParser.getAttributeValue(SPAConstants.SPA_NAMESPACE, SPAConstants.SPA_ATTRIB_METHOD);
			if (SPAConstants.SPA_ATTRIB_METHOD_SIGNATURE_XML.equals(signatureMethod))
			{
				policy=createPolicy(attachmentPoint, signatureMethod);
			}else{
				Log.info("Unknown signature method:"+signatureMethod);
			}
			eParser.consume();
		} catch (XmlPullParserException e) {
			Log.error(e);
			throw new IOException(e.getMessage());
		}
		return policy;
	}

	public QoSPolicy createPolicy(WSDLPolicyAttachmentPoint attachmentPoint,String signatureMethod ){
		QoSPolicy policy=null;
		QoSPolicyBuilder builder=  QoSPolicyBuilderInstanceRegistry.getInstance().getPolicyBuilder(AuthenticationPolicyBuilder.class);
		if (builder instanceof AuthenticationPolicyBuilder)
		{
			AuthenticationPolicyBuilder apBuilder=(AuthenticationPolicyBuilder)builder;
			if (attachmentPoint.getPolicyDirection().equals(WSDLPolicyDirection.INBOUND)){
				policy=apBuilder.createInboundPolicy(signatureMethod);
			}else if (attachmentPoint.getPolicyDirection().equals(WSDLPolicyDirection.OUTBOUND)){
				policy=apBuilder.createOutboundPolicy(signatureMethod);
			}else if (attachmentPoint.getPolicyDirection().equals(WSDLPolicyDirection.INOUTBOUND)){
				policy=apBuilder.createInOutboundPolicy(signatureMethod);
			}
		}
		return policy;
	}

}
