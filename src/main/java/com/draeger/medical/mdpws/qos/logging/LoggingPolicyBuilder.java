package com.draeger.medical.mdpws.qos.logging;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionParser;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionSerializer;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder;
import com.draeger.medical.mdpws.qos.wsdl.SPAConstants;
import com.draeger.medical.mdpws.qos.wsdl.logging.LocalSOAPXMLLoggingAssertionParser;
import com.draeger.medical.mdpws.qos.wsdl.logging.LocalSOAPXMLLoggingAssertionSerializer;

public class LoggingPolicyBuilder implements QoSPolicyBuilder{
//	private static final LoggingPolicyBuilder instance=new LoggingPolicyBuilder();
//
//	public static LoggingPolicyBuilder getInstance() {
//		return instance;
//	}
	
	public LoggingPolicyBuilder()
	{
		//void
	}
	
	private final LocalLoggingInterceptor interceptor= new LocalSOAPXMLLoggingInterceptor();
	private final LoggingQoSPolicy outboundPolicy= new OutboundSOAPXMLLoggingQoSPolicy();
	
	@Override
	public LocalLoggingInterceptor createInboundInterceptor()
	{
		return getLocalLoggingInterceptor();
	}
	
	@Override
	public LocalLoggingInterceptor createOutboundInterceptor()
	{
		return getLocalLoggingInterceptor();
	}
	
	@Override
	public LocalLoggingInterceptor createInOutboundInterceptor()
	{
		return getLocalLoggingInterceptor();
	}
	
	public LocalLoggingInterceptor getLocalLoggingInterceptor()
	{
		return interceptor;
	}

	@Override
	public Class<?> getInboundPolicyClass()
	{
		return null;
	}
	
	@Override
	public Class<?> getOutboundPolicyClass()
	{
		return OutboundSOAPXMLLoggingQoSPolicy.class;
	}
	
	@Override
	public Class<?> getInOutboundPolicyClass()
	{
		return null;
	}

	/**
	 * @return
	 */
	@Override
	public WSDLAssertionSerializer createSerializer() {
		return new LocalSOAPXMLLoggingAssertionSerializer();
	}

	/**
	 * @return
	 */
	@Override
	public QName getInboundAssertionElementName() {
		return getAssertionElementName();
	}
	
	/**
	 * @return
	 */
	@Override
	public QName getOutboundAssertionElementName() {
		return getAssertionElementName();
	}
	
	/**
	 * @return
	 */
	@Override
	public QName getInOutboundAssertionElementName() {
		return getAssertionElementName();
	}
	
	/**
	 * @return
	 */
	private QName getAssertionElementName() {
		return QNameFactory.getInstance().getQName(SPAConstants.SPA_ELEM_LOGGING, SPAConstants.SPA_NAMESPACE);
	}

	/**
	 * @return
	 */
	@Override
	public WSDLAssertionParser createParser() {
		return new LocalSOAPXMLLoggingAssertionParser();
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder#createInboundPolicy()
	 */
	@Override
	public QoSPolicy createInboundPolicy() {
		return createInboundPolicy(null);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder#createInboundPolicy(java.lang.String)
	 */
	@Override
	public QoSPolicy createInboundPolicy(Object addInfo) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder#createInOutboundPolicy()
	 */
	@Override
	public QoSPolicy createInOutboundPolicy() {
		return  createInOutboundPolicy(null);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder#createInOutboundPolicy(java.lang.String)
	 */
	@Override
	public QoSPolicy createInOutboundPolicy(Object addInfo) {
		return null;
	}
	
	@Override
	public LoggingQoSPolicy createOutboundPolicy()
	{
		return createOutboundPolicy(null);
	}
	
	@Override
	public LoggingQoSPolicy createOutboundPolicy(Object addInfo)
	{
		return outboundPolicy;
	}
	

	
}
