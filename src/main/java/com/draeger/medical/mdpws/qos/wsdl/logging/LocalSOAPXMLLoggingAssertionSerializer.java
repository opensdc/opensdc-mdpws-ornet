/**
 * 
 */
package com.draeger.medical.mdpws.qos.wsdl.logging;



import java.io.IOException;

import org.xmlpull.v1.XmlSerializer;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionSerializer;
import com.draeger.medical.mdpws.qos.logging.OutboundSOAPXMLLoggingQoSPolicy;
import com.draeger.medical.mdpws.qos.wsdl.SPAConstants;

/**
 *
 *
 */
public class LocalSOAPXMLLoggingAssertionSerializer implements
WSDLAssertionSerializer {

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionSerializer#serialize(java.lang.Object, org.xmlpull.v1.XmlSerializer)
	 */
	@Override
	public void serialize(Object assertion, XmlSerializer serializer) throws IOException 
	{
		if (assertion instanceof OutboundSOAPXMLLoggingQoSPolicy)
		{
			serializer.startTag(SPAConstants.SPA_NAMESPACE,SPAConstants.SPA_ELEM_LOGGING );
			serializer.attribute(SPAConstants.SPA_NAMESPACE, SPAConstants.SPA_ATTRIB_METHOD, SPAConstants.SPA_ATTRIB_LOGGING_LOCAL);
			serializer.endTag(SPAConstants.SPA_NAMESPACE,SPAConstants.SPA_ELEM_LOGGING );
		}
	}

}
