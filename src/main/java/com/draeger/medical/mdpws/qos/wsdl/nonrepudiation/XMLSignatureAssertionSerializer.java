/**
 * 
 */
package com.draeger.medical.mdpws.qos.wsdl.nonrepudiation;



import java.io.IOException;

import org.xmlpull.v1.XmlSerializer;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionSerializer;
import com.draeger.medical.mdpws.qos.signature.XMLSignatureQoSPolicy;
import com.draeger.medical.mdpws.qos.wsdl.SPAConstants;

/**
 *
 *
 */
public class XMLSignatureAssertionSerializer implements
WSDLAssertionSerializer {

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionSerializer#serialize(java.lang.Object, org.xmlpull.v1.XmlSerializer)
	 */
	@Override
	public void serialize(Object assertion, XmlSerializer serializer) throws IOException 
	{
		if (assertion instanceof XMLSignatureQoSPolicy)
		{
			serializer.startTag(SPAConstants.SPA_NAMESPACE,SPAConstants.SPA_ELEM_AUTHENTICATION );
			serializer.attribute(SPAConstants.SPA_NAMESPACE, SPAConstants.SPA_ATTRIB_METHOD, SPAConstants.SPA_ATTRIB_METHOD_SIGNATURE_XML);
			serializer.endTag(SPAConstants.SPA_NAMESPACE,SPAConstants.SPA_ELEM_AUTHENTICATION );
		}
	}

}
