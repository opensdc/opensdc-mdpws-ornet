/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package com.draeger.medical.mdpws.qos.interception;

import java.io.InputStream;
import java.io.OutputStream;

import org.ws4d.java.communication.ProtocolData;

import com.draeger.medical.mdpws.qos.QoSMessageContext;
import com.draeger.medical.mdpws.qos.QoSPolicy;

/**
 *
 *
 */
public interface OutboundSOAPUTF8TransformationInterceptor 
{
	public abstract boolean interceptOutbound(InputStream inputStream, ProtocolData pd, QoSMessageContext qosMsgCtxt,
			QoSPolicy policy, OutputStream outputStream, org.ws4d.java.schema.Element element) throws Exception;
}
