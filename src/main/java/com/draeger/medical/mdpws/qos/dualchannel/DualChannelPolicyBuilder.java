package com.draeger.medical.mdpws.qos.dualchannel;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionParser;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionSerializer;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.dualchannel.impl.DualChannelInterceptorImpl;
import com.draeger.medical.mdpws.qos.dualchannel.impl.InOutboundDualChannelQoSPolicy;
import com.draeger.medical.mdpws.qos.dualchannel.impl.InboundDualChannelQoSPolicy;
import com.draeger.medical.mdpws.qos.dualchannel.impl.OutboundDualChannelQoSPolicy;
import com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder;
import com.draeger.medical.mdpws.qos.wsdl.SPAConstants;
import com.draeger.medical.mdpws.qos.wsdl.dualchannel.DualChannelAssertionParser;
import com.draeger.medical.mdpws.qos.wsdl.dualchannel.DualChannelAssertionSerializer;

public class DualChannelPolicyBuilder implements QoSPolicyBuilder{
//	private static final DualChannelPolicyBuilder instance=new DualChannelPolicyBuilder();

//	public static DualChannelPolicyBuilder getInstance() {
//		return instance;
//	}
	
	
	private final DualChannelPolicyInterceptor interceptor=new DualChannelInterceptorImpl();
	
	
	
	public DualChannelPolicyBuilder()
	{
		//void
	}
	
	@Override
	public DualChannelPolicyInterceptor createInboundInterceptor()
	{
		return getDualChannelPolicyInterceptor();
	}
	
	@Override
	public DualChannelPolicyInterceptor createOutboundInterceptor()
	{
		return getDualChannelPolicyInterceptor();
	}
	
	@Override
	public DualChannelPolicyInterceptor createInOutboundInterceptor()
	{
		return getDualChannelPolicyInterceptor();
	}
	
	private DualChannelPolicyInterceptor getDualChannelPolicyInterceptor()
	{
		return interceptor;
	}
	
	@Override
	public Class<?> getInboundPolicyClass()
	{
		return InboundDualChannelQoSPolicy.class;
	}
	
	@Override
	public Class<?> getInOutboundPolicyClass()
	{
		return InOutboundDualChannelQoSPolicy.class;
	}
	
	@Override
	public Class<?> getOutboundPolicyClass()
	{
		return OutboundDualChannelQoSPolicy.class;
	}

	/**
	 * @return
	 */
	@Override
	public WSDLAssertionSerializer createSerializer() {
		return new DualChannelAssertionSerializer();
	}

	/**
	 * @return
	 */
	@Override
	public QName getInboundAssertionElementName() {
		return QNameFactory.getInstance().getQName(SPAConstants.SPA_ELEM_DUALCHANNEL, SPAConstants.SPA_NAMESPACE);
	}
	
	@Override
	public QName getOutboundAssertionElementName() {
		return QNameFactory.getInstance().getQName(SPAConstants.SPA_ELEM_DUALCHANNEL, SPAConstants.SPA_NAMESPACE);
	}
	
	@Override
	public QName getInOutboundAssertionElementName() {
		return QNameFactory.getInstance().getQName(SPAConstants.SPA_ELEM_DUALCHANNEL, SPAConstants.SPA_NAMESPACE);
	}

	/**
	 * @return
	 */
	@Override
	public WSDLAssertionParser createParser() {
		return new DualChannelAssertionParser();
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder#createInboundPolicy()
	 */
	@Override
	public QoSPolicy createInboundPolicy() {
		return createInboundPolicy(null);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder#createInboundPolicy(java.lang.String)
	 */
	@Override
	public QoSPolicy createInboundPolicy(Object addInfo) {
		InboundDualChannelQoSPolicy inbound=new InboundDualChannelQoSPolicy();
		enrichPolicy(addInfo, inbound);
		return inbound;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder#createInOutboundPolicy()
	 */
	@Override
	public QoSPolicy createInOutboundPolicy() {
		return createInOutboundPolicy(null);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder#createInOutboundPolicy(java.lang.String)
	 */
	@Override
	public QoSPolicy createInOutboundPolicy(Object addInfo) {
		InOutboundDualChannelQoSPolicy inOutPolicy=new InOutboundDualChannelQoSPolicy();
		enrichPolicy(addInfo, inOutPolicy);
		return inOutPolicy;
	}

	/**
	 * @param addInfo
	 * @param inOutPolicy
	 */
	protected void enrichPolicy(Object addInfo,
			DualChannelPolicy policy) {
		if (addInfo instanceof DualChannelPolicyAttributes)
		{
			DualChannelPolicyAttributes attributes=(DualChannelPolicyAttributes)addInfo;
			policy.setDualChannelPolicyAttributes(attributes);
		}
		DualChannelManager.getInstance().updateDualChannelProtocolConverter(policy);
		DualChannelManager.getInstance().updateDualChanneLocal2MDPWSConverter(policy);
		DualChannelManager.getInstance().updateDualChannelComparator(policy);
	}
	
	@Override
	public DualChannelPolicy createOutboundPolicy()
	{
		return createOutboundPolicy(null);
	}
	
	@Override
	public DualChannelPolicy createOutboundPolicy(Object addInfo)
	{
		OutboundDualChannelQoSPolicy outPolicy=new OutboundDualChannelQoSPolicy();
		enrichPolicy(addInfo, outPolicy);
		return outPolicy;
	}
	
}
