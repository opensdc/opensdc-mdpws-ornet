/**
 * 
 */
package com.draeger.medical.mdpws.qos.dualchannel.impl;

import org.ws4d.java.structures.ArrayList;

import com.draeger.medical.mdpws.qos.dualchannel.DualChannelLocal2MDPWSConverter;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicy;
import com.draeger.medical.mdpws.qos.dualchannel.Local2MDPWSConverterProvider;

/**
 *
 *
 */
public class DefaultDualChannelLocal2MDPWSConverterProvider implements
		Local2MDPWSConverterProvider {

	
	private final ArrayList converters=new ArrayList();
	
	public DefaultDualChannelLocal2MDPWSConverterProvider()
	{
		addConverter(new SimpleLocal2MDPWSConverter());
	}
//	
	/**
	 * 
	 */
	private boolean addConverter(DualChannelLocal2MDPWSConverter converter) 
	{
		boolean retVal=false;
		if (converter!=null && !converters.contains(converter))
		{
			converters.add(converter);
			retVal=true;
		}
		return retVal;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelConverterProvider#getDualChannelConverter(com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicy)
	 */
	@Override
	public DualChannelLocal2MDPWSConverter getConverter(DualChannelPolicy policy) {
		DualChannelLocal2MDPWSConverter converter=null;
		if (policy!=null && policy.getDualChannelPolicyAttributes()!=null && policy.getDualChannelPolicyAttributes().getAlgorithm()!=null)
		{
			converter=(DualChannelLocal2MDPWSConverter) converters.get(0);
		}
		return converter;
	}

}
