/**
 * 
 */
package com.draeger.medical.mdpws.qos.logging;

import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptionDirection;
import com.draeger.medical.mdpws.qos.nonrepudiation.AuthenticationPolicy;


/**
 *
 *
 */
public class OutboundSOAPXMLLoggingQoSPolicy extends LoggingQoSPolicy implements AuthenticationPolicy{

	
	
	
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.QoSPolicy#isTokenRequired()
	 */
	@Override
	public boolean isTokenRequired() 
	{
		return true;
	}
	
	@Override
	public QoSPolicyInterceptionDirection getInterceptionDirection() {
		return QoSPolicyInterceptionDirection.OUTBOUND;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.QoSPolicy#isAbstractPolicy()
	 */
	@Override
	public boolean isAbstractPolicy() {
		return true;
	}

}
