package com.draeger.medical.mdpws.qos.interception;

public enum QoSPolicyInterceptionDirection {
	INBOUND, OUTBOUND, INOUTBOUND
}
 
