package com.draeger.medical.mdpws.qos.interception;

public enum QoSPolicyTokenState 
{
	UNKNOWN, VALID,  ERROR_DURING_PROCESSING, INVALID
}
