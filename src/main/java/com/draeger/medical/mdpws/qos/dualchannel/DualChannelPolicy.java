/**
 * 
 */
package com.draeger.medical.mdpws.qos.dualchannel;

import com.draeger.medical.mdpws.qos.QoSPolicy;

/**
 *
 *
 */
public interface DualChannelPolicy extends QoSPolicy
{
	public abstract void setDualChannelPolicyAttributes(DualChannelPolicyAttributes attributes);
	public abstract DualChannelPolicyAttributes getDualChannelPolicyAttributes();
	
	public abstract DualChannelLocal2MDPWSConverter getDualChannelLocalConverter();
	public abstract void addDualChannelConverter(DualChannelLocal2MDPWSConverter converter);
	
	public abstract DualChannelComparator getDualChannelComparator();
	public abstract void setDualChannelComparator(DualChannelComparator comparator);

	public abstract DualChannelProtocolConverter getDualChannelProtocolConverter(DualChannelPolicyAttributes attributes);
	public abstract void addDualChannelProtocolConverter(DualChannelProtocolConverter converter);
}
