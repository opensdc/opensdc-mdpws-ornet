/**
 * 
 */
package com.draeger.medical.mdpws.qos.safetyinformation;

import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptionDirection;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyToken;
import com.draeger.medical.mdpws.qos.safetyinformation.transmission.SafetyInformation;

/**
 *
 * @param <P>
 * @param <V>
 */
public class SafetyInformationPolicyToken<V extends SafetyInformation,P extends SafetyInformationPolicy> extends QoSPolicyToken<V, P> {

	/**
	 * @param qoSPolicy
	 * @param value
	 * @param qoSPolicyInterceptionPoint
	 */
	public SafetyInformationPolicyToken(P qoSPolicy, V value,
			QoSPolicyInterceptionDirection qoSPolicyInterceptionPoint) {
		super(qoSPolicy, value, qoSPolicyInterceptionPoint);
	}
}
