/**
 * 
 */
package com.draeger.medical.mdpws.qos.logging;

import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptor;

/**
 * A marker interface for an interceptor that handles local logging policies.
 *
 */
public interface LocalLoggingInterceptor extends QoSPolicyInterceptor{
	//void - marker interfaces
}
