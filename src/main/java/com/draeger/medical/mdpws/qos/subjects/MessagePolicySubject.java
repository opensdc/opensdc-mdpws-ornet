/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.qos.subjects;

import org.ws4d.java.service.OperationDescription;


public class MessagePolicySubject extends AbstractQoSPolicySubject 
{

	private final boolean inputMessageSubject;
	private final boolean outputMessageSubject;
	private final boolean faultMessageSubject;
	private final OperationDescription operationDescription;
	
	public MessagePolicySubject(boolean inputMessageSubject,
			boolean outputMessageSubject, boolean faultMessageSubject,
			OperationDescription operationDescription, boolean abstractSubject) {
		super(null, abstractSubject);
		this.inputMessageSubject = inputMessageSubject;
		this.outputMessageSubject = outputMessageSubject;
		this.faultMessageSubject = faultMessageSubject;
		this.operationDescription = operationDescription;
	}
	
	

//	public MessagePolicySubject(boolean inputMessageSubject,
//			boolean outputMessageSubject, boolean faultMessageSubject,
//			Class associatedClass, boolean abstractSubject) {
//		super(associatedClass, abstractSubject);
//		this.inputMessageSubject = inputMessageSubject;
//		this.outputMessageSubject = outputMessageSubject;
//		this.faultMessageSubject = faultMessageSubject;
//		this.operationDescription =null;
//	}



	public boolean isInputMessageSubject() {
		return inputMessageSubject;
	}
	 
	public boolean isOutputMessageSubject() {
		return outputMessageSubject;
	}
	 
	public boolean isFaultMessageSubject() {
		return faultMessageSubject;
	}
	 
	public OperationDescription getOperation() {
		return operationDescription;
	}
	
	@Override
	public boolean isAssociatedWith(Object o) {
		return (getOperation()!=null && (o!=null && o.equals(getOperation()))) || (getAssociatedClass()!=null && super.isAssociatedWith(o));
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.subjects.QoSPolicySubject#getAssociation()
	 */
	@Override
	public Object getAssociation() {
		return getOperation();
	}
}
 
