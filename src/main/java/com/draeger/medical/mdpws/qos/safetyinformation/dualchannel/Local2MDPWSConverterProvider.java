/**
 * 
 */
package com.draeger.medical.mdpws.qos.safetyinformation.dualchannel;

import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicy;

/**
 *
 *
 */
public interface Local2MDPWSConverterProvider {

	/**
	 * @param policy
	 * @return
	 */
	DualChannelLocal2MDPWSConverter getConverter(SafetyInformationPolicy policy);

}
