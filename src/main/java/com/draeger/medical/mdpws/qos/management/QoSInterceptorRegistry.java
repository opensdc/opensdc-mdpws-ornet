package com.draeger.medical.mdpws.qos.management;

import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.util.Log;

import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptor;
import com.draeger.medical.mdpws.qos.util.Util;

public class QoSInterceptorRegistry {
 
	private ArrayList qoSPolicyInterceptors=new ArrayList();
	
	QoSInterceptorRegistry()
	{
		//void
	}
	 
	public boolean registerInterceptor(QoSPolicyInterceptor interceptor) {
		if (interceptor!=null && !qoSPolicyInterceptors.contains(interceptor))
		{
			Log.info("Registering interceptor:"+interceptor);
			return qoSPolicyInterceptors.add(interceptor);
		}
		return false;
	}
	
	protected Iterator getInterceptors()
	{
		return new ReadOnlyIterator(qoSPolicyInterceptors.iterator());
	}
	 
	public Iterator getInterceptors(Class<?> interceptionPointClz) 
	{
		return Util.findItems(interceptionPointClz, getInterceptors());
	}
	
	public boolean unregisterInterceptor(QoSPolicyInterceptor interceptor)
	{
		if (interceptor!=null)
		{
			return qoSPolicyInterceptors.remove(interceptor);
		}
		return false;
	}
	 
}
 
