package com.draeger.medical.mdpws.qos.nonrepudiation;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionParser;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionSerializer;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder;
import com.draeger.medical.mdpws.qos.signature.InOutboundXMLSignatureQoSPolicy;
import com.draeger.medical.mdpws.qos.signature.InboundXMLSignatureQoSPolicy;
import com.draeger.medical.mdpws.qos.signature.OutboundXMLSignatureQoSPolicy;
import com.draeger.medical.mdpws.qos.signature.XMLSignatureInterceptor;
import com.draeger.medical.mdpws.qos.wsdl.SPAConstants;
import com.draeger.medical.mdpws.qos.wsdl.nonrepudiation.XMLSignatureAssertionParser;
import com.draeger.medical.mdpws.qos.wsdl.nonrepudiation.XMLSignatureAssertionSerializer;

public class AuthenticationPolicyBuilder implements QoSPolicyBuilder{
//	private static final AuthenticationPolicyBuilder instance=new AuthenticationPolicyBuilder();
//
//	public static AuthenticationPolicyBuilder getInstance() {
//		return instance;
//	}
	
	
	private final AuthenticationPolicyInterceptor interceptor=new XMLSignatureInterceptor();
	private final InboundXMLSignatureQoSPolicy inPolicy= new InboundXMLSignatureQoSPolicy();
	private final InOutboundXMLSignatureQoSPolicy inOutPolicy=new InOutboundXMLSignatureQoSPolicy();
	private final OutboundXMLSignatureQoSPolicy outPolicy=new OutboundXMLSignatureQoSPolicy();
	
	public AuthenticationPolicyBuilder()
	{
		//void
	}
	
	@Override
	public AuthenticationPolicyInterceptor createInboundInterceptor()
	{
		return getAuthenticationPolicyInterceptor();
	}
	
	@Override
	public AuthenticationPolicyInterceptor createOutboundInterceptor()
	{
		return getAuthenticationPolicyInterceptor();
	}
	
	@Override
	public AuthenticationPolicyInterceptor createInOutboundInterceptor()
	{
		return getAuthenticationPolicyInterceptor();
	}
	
	private AuthenticationPolicyInterceptor getAuthenticationPolicyInterceptor()
	{
		return interceptor;
	}
	
	@Override
	public Class<?> getInboundPolicyClass()
	{
		return InboundXMLSignatureQoSPolicy.class;
	}
	
	@Override
	public Class<?> getInOutboundPolicyClass()
	{
		return InOutboundXMLSignatureQoSPolicy.class;
	}
	
	@Override
	public Class<?> getOutboundPolicyClass()
	{
		return OutboundXMLSignatureQoSPolicy.class;
	}

	/**
	 * @return
	 */
	@Override
	public WSDLAssertionSerializer createSerializer() {
		return new XMLSignatureAssertionSerializer();
	}

	/**
	 * @return
	 */
	@Override
	public QName getInboundAssertionElementName() {
		return QNameFactory.getInstance().getQName(SPAConstants.SPA_ELEM_AUTHENTICATION, SPAConstants.SPA_NAMESPACE);
	}
	
	@Override
	public QName getOutboundAssertionElementName() {
		return QNameFactory.getInstance().getQName(SPAConstants.SPA_ELEM_AUTHENTICATION, SPAConstants.SPA_NAMESPACE);
	}
	
	@Override
	public QName getInOutboundAssertionElementName() {
		return QNameFactory.getInstance().getQName(SPAConstants.SPA_ELEM_AUTHENTICATION, SPAConstants.SPA_NAMESPACE);
	}

	/**
	 * @return
	 */
	@Override
	public WSDLAssertionParser createParser() {
		return new XMLSignatureAssertionParser();
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder#createInboundPolicy()
	 */
	@Override
	public QoSPolicy createInboundPolicy() {
		return createInboundPolicy(null);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder#createInboundPolicy(java.lang.String)
	 */
	@Override
	public QoSPolicy createInboundPolicy(Object addInfo) {
		return inPolicy;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder#createInOutboundPolicy()
	 */
	@Override
	public QoSPolicy createInOutboundPolicy() {
		return createInOutboundPolicy(null);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder#createInOutboundPolicy(java.lang.String)
	 */
	@Override
	public QoSPolicy createInOutboundPolicy(Object addInfo) {
		return inOutPolicy;
	}
	
	@Override
	public AuthenticationPolicy createOutboundPolicy()
	{
		return createOutboundPolicy(null);
	}
	
	@Override
	public AuthenticationPolicy createOutboundPolicy(Object addInfo)
	{
		return outPolicy;
	}
	
}
