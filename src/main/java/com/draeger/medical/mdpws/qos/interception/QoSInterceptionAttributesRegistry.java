/**
 * 
 */
package com.draeger.medical.mdpws.qos.interception;

import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Set;

/**
 *
 *
 */
public class QoSInterceptionAttributesRegistry {
	private static final QoSInterceptionAttributesRegistry instance=new QoSInterceptionAttributesRegistry();
	
	private QoSInterceptionAttributesRegistry()
	{
		//void
	}

	/**
	 * @return the instance
	 */
	public static QoSInterceptionAttributesRegistry getInstance() {
		return instance;
	}
	
	//inbound message id (string)->QoSInterceptionAttributeMap
	private final HashMap protocolDataAttributesMap=new HashMap();

	public void clear() {
		protocolDataAttributesMap.clear();
	}

	public Object get(Object key) {
		return protocolDataAttributesMap.get(key);
	}

	public boolean isEmpty() {
		return protocolDataAttributesMap.isEmpty();
	}

	public Object put(Object key, Object value) {
		return protocolDataAttributesMap.put(key, value);
	}

	public Object remove(Object key) {
		return protocolDataAttributesMap.remove(key);
	}

	public int size() {
		return protocolDataAttributesMap.size();
	}

	public Set entrySet() {
		return protocolDataAttributesMap.entrySet();
	}

	public Set keySet() {
		return protocolDataAttributesMap.keySet();
	}

	public DataStructure values() {
		return protocolDataAttributesMap.values();
	}
	
	
}
