/**
 * 
 */
package com.draeger.medical.mdpws.qos.dualchannel;

/**
 *
 *
 */
public interface DualChannelProtocolConverterProvider {

	/**
	 * @param policy
	 * @return
	 */
	DualChannelProtocolConverter getDualChannelTransformator(
			DualChannelPolicy policy);

}
