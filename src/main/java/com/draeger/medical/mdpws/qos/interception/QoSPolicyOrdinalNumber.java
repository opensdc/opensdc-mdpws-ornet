package com.draeger.medical.mdpws.qos.interception;

public class QoSPolicyOrdinalNumber implements Comparable<QoSPolicyOrdinalNumber>
{
	public static final QoSPolicyOrdinalNumber NOT_APPLICABLE=new QoSPolicyOrdinalNumber(-1);
	public final Integer ordinalNumber;
	
	public QoSPolicyOrdinalNumber(Number ordinalNumber)
	{
		this.ordinalNumber=ordinalNumber.intValue();
	}

	@Override
	public int compareTo(QoSPolicyOrdinalNumber arg0) {
		QoSPolicyOrdinalNumber ordNo=arg0;
		
		if (ordNo==NOT_APPLICABLE && ordNo.ordinalNumber!=ordinalNumber)
			throw new IllegalArgumentException("Can not compare with a NOT_APPLICABLE!");
		
		return ordinalNumber.compareTo(ordNo.ordinalNumber);
	}
}
