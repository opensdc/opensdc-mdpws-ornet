/**
 * 
 */
package com.draeger.medical.mdpws.qos.dualchannel.impl;

import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.HashMap;

import com.draeger.medical.mdpws.qos.AbstractQoSPolicy;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelComparator;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelLocal2MDPWSConverter;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicy;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicyAttributes;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelProtocolConverter;

/**
 *
 *
 */
public abstract class AbstractDualChannelQoSPolicy extends AbstractQoSPolicy
		implements DualChannelPolicy {

	private DualChannelPolicyAttributes attributes=null;
	private final ArrayList dualChannelLocalConverter=new ArrayList();
	private final HashMap dualChannelTransformators=new HashMap();
	private DualChannelComparator comparator=null;

	
	
	public AbstractDualChannelQoSPolicy() 
	{
		super();
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicy#setDualChannelPolicyAttributes(com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicyAttributes)
	 */
	@Override
	public void setDualChannelPolicyAttributes(
			DualChannelPolicyAttributes attributes) {
		this.attributes=attributes;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicy#getDualChannelPolicyAttributes()
	 */
	@Override
	public DualChannelPolicyAttributes getDualChannelPolicyAttributes() 
	{
		return this.attributes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((attributes == null) ? 0 : attributes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractDualChannelQoSPolicy other = (AbstractDualChannelQoSPolicy) obj;
		if (attributes == null) {
			if (other.attributes != null)
				return false;
		} else if (!attributes.equals(other.attributes))
			return false;
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicy#getDualChannelConverter()
	 */
	@Override
	public DualChannelLocal2MDPWSConverter getDualChannelLocalConverter() {
		return (DualChannelLocal2MDPWSConverter) dualChannelLocalConverter.get(0); //TODO SSch 0 ????
	}
	
	@Override
	public void addDualChannelConverter(DualChannelLocal2MDPWSConverter converter) 
	{
		if (!dualChannelLocalConverter.contains(converter))
		{
			dualChannelLocalConverter.add(converter);
		}
	}

	@Override
	public DualChannelComparator getDualChannelComparator() {
		return comparator;
	}

	@Override
	public void setDualChannelComparator(DualChannelComparator comparator) {
		this.comparator=comparator;
	}
	

	
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicy#getDualChannelTransformator(org.ws4d.java.types.QName)
	 */
	@Override
	public DualChannelProtocolConverter getDualChannelProtocolConverter(DualChannelPolicyAttributes attributes) {
		DualChannelProtocolConverter transformator=(DualChannelProtocolConverter) dualChannelTransformators.get(attributes.getTransform().toStringPlain()+attributes.getAlgorithm().toStringPlain());
		return transformator;
	}

	
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicy#addDualChannelProtocolConverter(com.draeger.medical.mdpws.qos.dualchannel.DualChannelProtocolConverter)
	 */
	@Override
	public void addDualChannelProtocolConverter(
			DualChannelProtocolConverter converter) {
		dualChannelTransformators.put(converter.getImplementedTransformation().toStringPlain()+converter.getImplementedAlgorithm().toStringPlain(),converter);
	}

	@Override
	public String toString() {
		return "AbstractDualChannelQoSPolicy [attributes=" + attributes + "]";
	}
}
