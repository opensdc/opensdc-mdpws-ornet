package com.draeger.medical.mdpws.qos;

import org.w3c.dom.Document;
import org.ws4d.java.communication.ProtocolData;

import com.draeger.medical.mdpws.qos.interception.QoSPolicyToken;


public interface QoSPolicyExceptionHandler 
{ 	 
	public abstract boolean handle(QoSPolicy policy,Document soapDocument, QoSPolicyToken<?,?> token,QoSMessageContext qosMessageCtxt, ProtocolData protocolData, String additionalInfo);
	
	public abstract boolean isResponsibleFor(QoSPolicy policy);
}
 
