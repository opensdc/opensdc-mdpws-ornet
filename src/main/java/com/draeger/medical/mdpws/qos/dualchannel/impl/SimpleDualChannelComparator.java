/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.qos.dualchannel.impl;

import com.draeger.medical.mdpws.qos.dualchannel.DualChannel;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelComparator;


public class SimpleDualChannelComparator implements DualChannelComparator {
	private static final double simpleDualChannelPrecision	= Double.parseDouble(System.getProperty("SimpleDualChannelComparator.simpleDualChannelPrecision", Double.toString(1e-6)));
	
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelComparator#compare(java.lang.Object, com.draeger.medical.mdpws.qos.dualchannel.DualChannel)
	 */
	@Override
	public boolean compare(Object computedFirstChannel,
			DualChannel<?, ?, ?> dualChannel) 
	{
		if (computedFirstChannel==null) return false;
						
		boolean retVal=false;
		if (dualChannel.getFirstChannel() instanceof Number)
		{
			try{
				double diff = Double.parseDouble(computedFirstChannel.toString())-((Number)dualChannel.getFirstChannel()).doubleValue();
				if (diff<simpleDualChannelPrecision && dualChannel.getFirstChannel().equals(dualChannel.getSecondChannel())){
					retVal=true;
				}
			}catch (NumberFormatException nfe){
				//void
			}
		}else{
			retVal= computedFirstChannel.equals(dualChannel.getFirstChannel()) && dualChannel.getFirstChannel().equals(dualChannel.getSecondChannel());
		}
		return retVal;
	}

}
