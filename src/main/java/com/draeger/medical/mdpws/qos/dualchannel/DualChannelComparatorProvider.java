/**
 * 
 */
package com.draeger.medical.mdpws.qos.dualchannel;

/**
 *
 *
 */
public interface DualChannelComparatorProvider {

	/**
	 * @param policy
	 * @return
	 */
	DualChannelComparator getDualChannelComparator(DualChannelPolicy policy);

}
