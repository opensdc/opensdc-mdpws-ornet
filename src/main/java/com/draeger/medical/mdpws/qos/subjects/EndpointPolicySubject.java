/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.qos.subjects;

import org.ws4d.java.types.EndpointReference;


public class EndpointPolicySubject extends AbstractQoSPolicySubject {
	//TODO SSch Add binding, port, portType as subclasses
	private final EndpointReference endpointReference;
	
	public EndpointPolicySubject(Class<?> associatedClass, boolean abstractSubject) {
		super(associatedClass, abstractSubject);
		this.endpointReference=null;
	}
	public EndpointReference getEndpointReference()
	{
		return endpointReference;
	}

	@Override
	public boolean isAssociatedWith(Object o) {
		return (o!=null && o.equals(endpointReference));
	}
	@Override
	public Object getAssociation() {
		return getEndpointReference();
	}
	
	
}

