package com.draeger.medical.mdpws.qos.subjects;

public abstract class AbstractQoSPolicySubject implements QoSPolicySubject{
	
	private final Class<?> associatedClass;
	private boolean isAbstract=false; 
	
	public AbstractQoSPolicySubject(Class<?> associatedClass, boolean isAbstract) {
		super();
		this.isAbstract=isAbstract;
		this.associatedClass = associatedClass;
	}

	@Override
	public String getSubjectName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public boolean isAssociatedWith(Object o) {
		return (o!=null && o.equals(associatedClass));
	}

	public Class<?> getAssociatedClass() {
		return associatedClass;
	}

	@Override
	public String toString() {
		return "AbstractQoSPolicySubject [associatedClass=" + associatedClass
				+ "]";
	}

	@Override
	public boolean isAbstract() {
		return isAbstract;
	}
}
