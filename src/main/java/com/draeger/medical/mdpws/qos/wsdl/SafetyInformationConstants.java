package com.draeger.medical.mdpws.qos.wsdl;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

public class SafetyInformationConstants {

	public static final String SI_NAMESPACE="http://www.draeger.com/projects/DSC/SI/201301";
	public static final String SI_DEFAULT_NAMESPACE_PREFIX = "si";

	
	//Assertion
	public static final String SI_ELEM_ASSERTION_SAFETY_REQ="SafetyReqAssertion";
	public static final String SI_ATTRIB_ASSERTION_TRANSMIT_DC="transmitDualChannel";
	public static final String SI_ATTRIB_ASSERTION_TRANSMIT_SCTXT="transmitSafetyContext";
	public static final boolean SI_ATTRIB_ASSERTION_TRANSMIT_DC_DEFAULT_VALUE = true;
	public static final boolean SI_ATTRIB_ASSERTION_TRANSMIT_SCTXT_DEFAULT_VALUE = true;
	
	//Requirement Definition
	public static final String SI_ELEM_DEFINITION_SAFETY_REQ="SafetyReq";
	public static final String SI_ELEM_DEFINITION_DUALCHANNEL_DEF="DualChannelDef";
	public static final String SI_ELEM_DEFINITION_SELECTOR="Selector";
	public static final String SI_ELEM_DEFINITION_CONTEXT_DEF="ContextDef";
	
	public static final String SI_ATTRIB_TARGET="target";
	public static final String SI_ATTRIB_DUALCHANNEL_ALGORITHM="algorithm";
	public static final String SI_ATTRIB_DUALCHANNEL_TRANSFORM="transform";
	//public static final String SI_ATTRIB_DUALCHANNEL_XPATH_DUALCHANNEL="xpath";
	
	//Transmission
	public static final String SI_ELEM_TRANSM_SAFETY_INFO="SafetyInfo";
	public static final String SI_ELEM_TRANSM_DUALCHANNEL="DualChannel";
	public static final String SI_ELEM_TRANSM_DUALCHANNEL_VALUE="DCValue";
	public static final String SI_ELEM_TRANSM_SAFETYCTXT="SafetyContext";
	public static final String SI_ELEM_TRANSM_SCTXT_VALUE="CtxtValue";
	public static final String SI_ATTRIB_TRANSM_ID="ID";
	public static final String SI_ATTRIB_TRANSM_SELECTOR_REF="selectorRef";
	public static final String SI_ATTRIB_TRANSM_URI="URI";
	
	//Qnames
	public static final QName SI_ATTRIB_ALGORITHM_DUALCHANNEL_VALUE_SHA1D=QNameFactory.getInstance().getQName("Base64SHA1",SI_NAMESPACE);
	public static final QName SI_ATTRIB_ALGORITHM_DUALCHANNEL_VALUE_HEX=QNameFactory.getInstance().getQName("HexEncoding",SI_NAMESPACE);
	public static final QName SI_ATTRIB_TRANSFORM_DUALCHANNEL_VALUE_NOTRANS=QNameFactory.getInstance().getQName("noTransformation",SI_NAMESPACE);
	public static final QName SI_ATTRIB_TRANSFORM_DUALCHANNEL_VALUE_EXCC14NC=QNameFactory.getInstance().getQName("xml-exc-c14n#WithComments",SI_NAMESPACE);
	
	public static final QName SI_ATTRIB_ALGORITHM_DUALCHANNEL_DEFAULT_VALUE=SI_ATTRIB_ALGORITHM_DUALCHANNEL_VALUE_SHA1D;
	public static final QName SI_ATTRIB_TRANSFORM_DUALCHANNEL_DEFAULT_VALUE=SI_ATTRIB_TRANSFORM_DUALCHANNEL_VALUE_EXCC14NC;

	
}
