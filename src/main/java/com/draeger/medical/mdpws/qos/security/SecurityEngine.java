/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.qos.security;

import java.io.IOException;
import java.util.List;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.ws.security.WSConstants;
import org.apache.ws.security.WSSecurityEngine;
import org.apache.ws.security.WSSecurityEngineResult;
import org.apache.ws.security.WSSecurityException;
import org.apache.ws.security.components.crypto.Crypto;
import org.apache.ws.security.components.crypto.CryptoFactory;
import org.apache.ws.security.message.WSSecHeader;
import org.apache.ws.security.message.WSSecSignature;
import org.w3c.dom.Document;
import org.ws4d.java.concurrency.LockSupport;
import org.ws4d.java.constants.XMLConstants;
import org.ws4d.java.util.Log;

import com.draeger.medical.mdpws.utils.InputStreamUtil;

public class SecurityEngine 
{
	private final static SecurityEngine instance=new SecurityEngine();
	public static synchronized SecurityEngine getInstance()
	{
		return instance;
	}

	//final ReentrantLock lock = new ReentrantLock();
	private final LockSupport lockSupport=new LockSupport();
	private final WSSecurityEngine secEngine =new WSSecurityEngine();

	private final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	private final CallbackHandler authCallback=new WSS4JCallback();
	private final boolean initialized;
	private final Crypto crypto;
	private final WSSecSignature  signer;
	private String alias="alias";
	private String passwordForKeyEntry="alias"; 
	private final boolean useCertificateChain=Boolean.getBoolean(System.getProperty("SecurityEngine.useCertificateChain", "true"));

	private SecurityEngine()
	{
		boolean init=false;
		WSSecSignature  signer=null;
		Crypto crypto=null;
		try{
			String cryptoPropFile=System.getProperty("MDPWS.SecurityEngine.PropFile", "defaultCrypto.properties");
			if (Log.isInfo())
				Log.info("Using "+cryptoPropFile+" in SecurityEngine.");
			
			crypto = CryptoFactory.getInstance(cryptoPropFile);
			docBuilderFactory.setNamespaceAware(true);

//			signer.setKeyIdentifierType(WSConstants.THUMBPRINT_IDENTIFIER);
			init=true;
		}catch(Exception ex)
		{
			Log.info(ex.getMessage());
		}
		this.initialized=init;
		this.crypto=crypto;
		this.signer=signer;
	}
	
	public void tick()
	{
		//void
	}

	public boolean isInitialized() {
		return initialized;
	}

	private String getAlias() {
		
		return alias;
	}

	private String getPassword() {
		//TODO SSch Read this from config
		return passwordForKeyEntry;
	}
	
	
	
	public void setUserInfo(String alias, String passwordForKeyEntry) {
		this.alias = alias;
		this.passwordForKeyEntry = passwordForKeyEntry;
		if (this.signer!=null)
			this.signer.setUserInfo(this.alias, this.passwordForKeyEntry);
	}


	public List<WSSecurityEngineResult> processSignature(Document securedDoc) throws WSSecurityException
	{
		List<WSSecurityEngineResult> securityResults=null;
		
		
		lockSupport.exclusiveLock();
		try{
			securityResults= secEngine.processSecurityHeader(securedDoc, null, authCallback, crypto, crypto);
		}finally
		{
			lockSupport.releaseExclusiveLock();
		}
		return securityResults;
	}

	public Document addSignature(Document unsignedDoc) throws WSSecurityException
	{
		long t0=System.nanoTime();
		WSSecHeader header=new WSSecHeader();
		header.insertSecurityHeader(unsignedDoc);

		Document signedDoc=null;
		lockSupport.exclusiveLock();
		try{
			signedDoc= getSigner().build(unsignedDoc,crypto, header);
		}finally
		{
			lockSupport.releaseExclusiveLock();
		}
		long t1=System.nanoTime();
		if (Log.isInfo())Log.info("Signature took "+(t1-t0)+" ns.");
		return signedDoc;
	}

	/**
	 * @return
	 */
	private WSSecSignature getSigner() {
		WSSecSignature signer= new WSSecSignature ();
		signer.setUserInfo(getAlias(), getPassword());
		signer.setUseSingleCertificate(!useCertificateChain);
		signer.setKeyIdentifierType(WSConstants.BST_DIRECT_REFERENCE);
		return signer;
	}

	public Document addSignature(String s) throws Exception 
	{
		return addSignature(createXMLDocumentFromString(s));
	}

	//TODO Extract into other Util class
	public Document createXMLDocumentFromString(String s) throws Exception
	{
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		return docBuilder.parse(InputStreamUtil.convertStringToInputStream(s, XMLConstants.ENCODING));
	}

	class WSS4JCallback implements CallbackHandler
	{

		@Override
		public void handle(Callback[] arg0) throws IOException,
		UnsupportedCallbackException {
			//Void
			if (Log.isDebug())
				Log.debug(this.getClass().getName()+" has been called!");
		}

	}


}
