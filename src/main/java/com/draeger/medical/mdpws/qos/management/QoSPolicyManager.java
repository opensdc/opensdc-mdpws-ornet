package com.draeger.medical.mdpws.qos.management;


import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;

import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptionDirection;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptor;
import com.draeger.medical.mdpws.qos.subjects.QoSPolicySubject;
import com.draeger.medical.mdpws.qos.util.Util;

public class QoSPolicyManager {
 
	private static final QoSPolicyManager instance=new QoSPolicyManager();
	private final QoSInterceptorRegistry qoSInterceptorRegistry=new QoSInterceptorRegistry();
	private final QoSPolicyRegistry qoSPolicyRegistry=new QoSPolicyRegistry();
	
	public static QoSPolicyManager getInstance() {
		return instance;
	}

	private QoSPolicyManager(){
		//void
	}
	
	public void registerInterceptor(QoSPolicyInterceptor interceptor) 
	{
		
		qoSInterceptorRegistry.registerInterceptor(interceptor);
	}
	 
	public Iterator getInterceptors(Class<?> interceptionPointClz) {
		return qoSInterceptorRegistry.getInterceptors(interceptionPointClz);
	}
	
	public Iterator getInterceptors()
	{
		return qoSInterceptorRegistry.getInterceptors();
	}
	 
	public void registerQoSPolicy(QoSPolicy policy) 
	{
		if (Log.isDebug())
			Log.debug("Registering policy:"+policy);
		
		qoSPolicyRegistry.addQoSPolicy(policy);
	}
	 
	public Iterator getQoSPoliciesForSubjectType(Class<?> subjectClz) {
		return qoSPolicyRegistry.getQoSPoliciesForSubjectType(subjectClz);
	}
	
	public Iterator getQoSPolicies()
	{
		return qoSPolicyRegistry.getPolicies();
	}
	
	public void ignoreQoSPolicyOfType(Class<?> policyClass)
	{
		qoSPolicyRegistry.ignoreQoSPolicyOfType(policyClass);
	}
	
	public void wireQoSPolicies(Class<?> subjectClz, Object association)
	{
		Iterator subjectClzPolicies=QoSPolicyManager.getInstance().getQoSPoliciesForSubjectType(subjectClz);
		while (subjectClzPolicies.hasNext())
		{
			Object policiesO=subjectClzPolicies.next();
			if (!(policiesO instanceof QoSPolicy)) continue;
			
			QoSPolicy policy=(QoSPolicy)policiesO;
			ArrayList  interceptorsForPolicy=new ArrayList();
			Iterator subjectsIt=policy.getSubjects(subjectClz);
			while (subjectsIt.hasNext())
			{
				Object sbjO=subjectsIt.next();
				if (!(sbjO instanceof QoSPolicySubject)) continue;
				QoSPolicySubject subject=(QoSPolicySubject)sbjO;
				if (subject.isAssociatedWith(association))
				{
					Iterator interceptors= QoSPolicyManager.getInstance().getInterceptors();
					while (interceptors.hasNext())
					{
						Object o=interceptors.next();
						if (!(o instanceof QoSPolicyInterceptor)) continue;
						QoSPolicyInterceptor interceptor=(QoSPolicyInterceptor) o;
						
						Iterator interceptorSubjectClasses=Util.findClasses(subjectClz, interceptor.getInterceptorSubjectClasses());
						if (interceptorSubjectClasses.hasNext()){
//							if (Log.isDebug())
//								Log.debug(policy.getInterceptionDirection()+" "+interceptor.getInterceptionDirection());
							
							Iterator interceptorPolicyClasses=Util.findClasses(policy.getClass(), interceptor.getQoSPolicyClasses());
							if ((interceptorPolicyClasses.hasNext())
									&& (policy.getInterceptionDirection()==null 
										|| QoSPolicyInterceptionDirection.INOUTBOUND.equals(policy.getInterceptionDirection())
										|| QoSPolicyInterceptionDirection.INOUTBOUND.equals(QoSPolicyInterceptionDirection.INOUTBOUND)
										|| policy.getInterceptionDirection().equals(interceptor.getInterceptionDirection())
									)
								)
							{
								//only interceptors should be added that are applicable for the same direction as the policy 
								interceptorsForPolicy.add(interceptor);
							}
						}
					}
				}
			}
			for(Iterator it=getSortedInterceptors(subjectClz, interceptorsForPolicy, policy, association);it.hasNext();)
			{
				policy.addInterceptor((QoSPolicyInterceptor)it.next());
			}
		}	
	}
		
	
	private Iterator getSortedInterceptors(Class<?> subjectClz, ArrayList interceptors, QoSPolicy policy, Object association)
	{
	    boolean changed = false;
	    do {
	        changed = false;
	        for (int a = 0; a < interceptors.size() - 1; a++) {
	        	QoSPolicyInterceptor ai=(QoSPolicyInterceptor)interceptors.get(a);
	        	QoSPolicyInterceptor ai1=(QoSPolicyInterceptor)interceptors.get(a+1);
	            if (ai.getOrdinalNumberForSubject(subjectClz).compareTo(ai1.getOrdinalNumberForSubject(subjectClz)) > 0) {
	            	interceptors.set(a, ai1);
	            	interceptors.set(a+1, ai1);
	                changed = true;
	            }
	        }
	    } while (changed);
	    
		return interceptors.iterator();
	}

}
 
