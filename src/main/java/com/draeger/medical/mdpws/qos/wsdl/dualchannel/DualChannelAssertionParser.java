/**
 * 
 */
package com.draeger.medical.mdpws.qos.wsdl.dualchannel;

import java.io.IOException;

import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.util.Log;
import org.xmlpull.v1.XmlPullParserException;

import com.draeger.medical.mdpws.common.util.XPathInfo;
import com.draeger.medical.mdpws.common.util.XPathNamespaceContext;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionParser;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyDirection;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicyAttributes;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicyBuilder;
import com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder;
import com.draeger.medical.mdpws.qos.management.QoSPolicyBuilderInstanceRegistry;
import com.draeger.medical.mdpws.qos.wsdl.SPAConstants;

/**
 *
 *
 */
public class DualChannelAssertionParser implements WSDLAssertionParser {


	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionParser#parse(org.ws4d.java.io.xml.ElementParser, com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint)
	 */
	@Override
	public QoSPolicy parse(ElementParser assertionEParser,
			WSDLPolicyAttachmentPoint attachmentPoint) throws IOException {
		QoSPolicy policy=null;
		try {

			String algorithmStr=assertionEParser.getAttributeValue(SPAConstants.SPA_NAMESPACE, SPAConstants.SPA_ATTRIB_ALGORITHM_DUALCHANNEL);
			String transformStr=assertionEParser.getAttributeValue(SPAConstants.SPA_NAMESPACE, SPAConstants.SPA_ATTRIB_TRANSFORM_DUALCHANNEL);
			String xPath=assertionEParser.getAttributeValue(SPAConstants.SPA_NAMESPACE, SPAConstants.SPA_ATTRIB_XPATH_DUALCHANNEL);

			QName algorithmQN=null;
			if (algorithmStr!=null)
			{
				algorithmQN=getQName(algorithmStr, assertionEParser);
			}else{
				algorithmQN=SPAConstants.SPA_ATTRIB_ALGORITHM_DUALCHANNEL_VALUE_SHA1D;
			}
			QName transformQN=null;
			if (transformStr!=null)
			{
				transformQN=getQName(transformStr, assertionEParser);
			}else{
				transformQN=SPAConstants.SPA_ATTRIB_TRANSFORM_DUALCHANNEL_VALUE_EXCC14NC;
			}

			if (SPAConstants.SPA_ATTRIB_ALGORITHM_DUALCHANNEL_VALUE_HEX.equals(algorithmQN) 
					|| SPAConstants.SPA_ATTRIB_ALGORITHM_DUALCHANNEL_VALUE_SHA1D.equals(algorithmQN))
			{
				policy=createPolicy(attachmentPoint, algorithmQN, transformQN, xPath, assertionEParser);
			}else{
				Log.info("Unknown algorithm:"+algorithmStr);
			}
			assertionEParser.consume();
		} catch (XmlPullParserException e) {
			Log.error(e);
			throw new IOException(e.getMessage());
		}
		return policy;
	}

	protected QoSPolicy createPolicy(WSDLPolicyAttachmentPoint attachmentPoint,QName algorithm, QName transform, String xPath, ElementParser eParser ) throws XmlPullParserException{
		DualChannelPolicyAttributes attributes=new DualChannelPolicyAttributes();
		attributes.setAlgorithm(algorithm);
		attributes.setTransform(transform);
		XPathInfo xpInfo=new XPathInfo(xPath, getXPathContext(eParser));
		attributes.setXPath(xpInfo);		

		if (Log.isDebug())
		{
			Log.debug("Parser DC attributes:"+attributes);
		}
		QoSPolicy policy=null;

		QoSPolicyBuilder builder=  QoSPolicyBuilderInstanceRegistry.getInstance().getPolicyBuilder(DualChannelPolicyBuilder.class);
		if (builder instanceof DualChannelPolicyBuilder)
		{
			DualChannelPolicyBuilder dcpBuilder=(DualChannelPolicyBuilder) builder;
			if (attachmentPoint.getPolicyDirection().equals(WSDLPolicyDirection.INBOUND)){
				policy=dcpBuilder.createInboundPolicy(attributes);
			}else if (attachmentPoint.getPolicyDirection().equals(WSDLPolicyDirection.OUTBOUND)){
				policy=dcpBuilder.createOutboundPolicy(attributes);
			}else if (attachmentPoint.getPolicyDirection().equals(WSDLPolicyDirection.INOUTBOUND)){
				policy=dcpBuilder.createInOutboundPolicy(attributes);
			}
		}
		return policy;
	}

	/**
	 * @param eParser
	 * @return
	 * @throws XmlPullParserException 
	 */
	private XPathNamespaceContext getXPathContext(ElementParser eParser) throws XmlPullParserException {
		Log.info("Creating namespace context");
		XPathNamespaceContext ctxt=new XPathNamespaceContext();

		//int nsStart = eParser.getNamespaceCount(eParser.getDepth()-1);
		int nsEnd = eParser.getNamespaceCount(eParser.getDepth());
		for (int i = 0 ; i < nsEnd; i++) {
			String prefix = eParser.getNamespacePrefix(i);
			String ns = eParser.getNamespaceUri(i);
			Log.info("Adding namespace to context: "+prefix+" "+ns);
			ctxt.addNamespace(prefix, ns);
		}

		return ctxt;
	}

	private QName getQName(String attributeContent, ElementParser parser)
	{

		String local=attributeContent;
		String nsp=null;
		int index= local.indexOf(":");
		if (index>=0)
		{
			if (index>0){
				nsp=local.substring(0,index);
			}
			local=local.substring(index+1);
		}
		QName qn=QNameFactory.getInstance().getQName(local,parser.getNamespace(nsp));
		return qn;
	}

}
