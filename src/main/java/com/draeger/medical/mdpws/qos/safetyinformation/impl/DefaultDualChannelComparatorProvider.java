/**
 * 
 */
package com.draeger.medical.mdpws.qos.safetyinformation.impl;

import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicy;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelComparator;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelComparatorProvider;


/**
 *
 *
 */
public class DefaultDualChannelComparatorProvider implements
		DualChannelComparatorProvider {

	SimpleDualChannelComparator simpleComparator=new SimpleDualChannelComparator();
	

	@Override
	public DualChannelComparator getDualChannelComparator(
			SafetyInformationPolicy policy) {
		// TODO Auto-generated method stub
		return simpleComparator;
	}

}
