/**
 * 
 */
package com.draeger.medical.mdpws.qos.safetyinformation.dualchannel;

import org.w3c.dom.Node;
import org.ws4d.java.types.QName;

import com.draeger.medical.mdpws.qos.safetyinformation.impl.SafetyInformationInterceptorImpl.TransformedChannelInfos;


/**
 *
 *
 */
public interface DualChannelProtocolConverter 
{
	//transformation
	public abstract QName getImplementedTransformation();
	public abstract void transformForSerialization(Node[] identifiedNodes, TransformedChannelInfos channelInfos);
	
	//algorithm
	public abstract QName getImplementedAlgorithm();
	public abstract String getDualChannelRepresentation(byte[] serializedData);
		
}
