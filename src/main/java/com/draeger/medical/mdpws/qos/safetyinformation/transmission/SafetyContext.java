package com.draeger.medical.mdpws.qos.safetyinformation.transmission;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import org.ws4d.java.types.QName;

public class SafetyContext {
	private final String targetIdentifier;
	private final HashMap<QName, SafetyContextValue> safetyContextValues=new HashMap<QName, SafetyContextValue>();
	
	public SafetyContext(String targetIdentifier) {
		super();
		this.targetIdentifier = targetIdentifier;
	}

	public String getTargetIdentifier() {
		return targetIdentifier;
	}

	public SafetyContextValue getValue(QName selectorReference) {
		return safetyContextValues.get(selectorReference);
	}

	public Set<QName> getSelectorReferences() {
		return safetyContextValues.keySet();
	}

	public Collection<SafetyContextValue> getValues() {
		return safetyContextValues.values();
	}

	public SafetyContextValue addContextValue(QName selectorReference, SafetyContextValue value) {
		return safetyContextValues.put(selectorReference, value);
	}
	
	public boolean containsReferencedElement(QName selectorReference) {
		return safetyContextValues.containsKey(selectorReference);
	}

	public boolean isEmpty() {
		return safetyContextValues.isEmpty();
	}

	@Override
	public String toString() {
		return "SafetyContext [targetIdentifier=" + targetIdentifier
				+ ", safetyContextValues=" + safetyContextValues + "]";
	}
}
