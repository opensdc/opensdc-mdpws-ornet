/**
 * 
 */
package com.draeger.medical.mdpws.qos.saftyinformation.impl;

import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicy;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelComparator;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelComparatorProvider;


/**
 * @author Stefan Schlichting
 *
 */
public class DefaultDualChannelComparatorProvider implements
		DualChannelComparatorProvider {

	SimpleDualChannelComparator simpleComparator=new SimpleDualChannelComparator();
	

	@Override
	public DualChannelComparator getDualChannelComparator(
			SafetyInformationPolicy policy) {
		// TODO Auto-generated method stub
		return simpleComparator;
	}

}
