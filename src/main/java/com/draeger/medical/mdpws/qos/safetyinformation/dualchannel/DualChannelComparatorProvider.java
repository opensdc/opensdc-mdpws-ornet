/**
 * 
 */
package com.draeger.medical.mdpws.qos.safetyinformation.dualchannel;

import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicy;

/**
 *
 *
 */
public interface DualChannelComparatorProvider {

	/**
	 * @param policy
	 * @return
	 */
	DualChannelComparator getDualChannelComparator(SafetyInformationPolicy policy);

}
