/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.qos.dualchannel.impl;

import com.draeger.medical.mdpws.qos.dualchannel.DualChannelComparator;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelComparatorProvider;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicy;


public class DefaultDualChannelComparatorProvider implements
		DualChannelComparatorProvider {

	SimpleDualChannelComparator simpleComparator=new SimpleDualChannelComparator();
	
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelComparatorProvider#getDualChannelComparator(com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicy)
	 */
	@Override
	public DualChannelComparator getDualChannelComparator(
			DualChannelPolicy policy) {
		return simpleComparator;
	}

}
