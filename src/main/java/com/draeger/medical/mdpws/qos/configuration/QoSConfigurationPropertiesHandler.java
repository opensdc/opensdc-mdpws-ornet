package com.draeger.medical.mdpws.qos.configuration;

import org.ws4d.java.configuration.PropertiesHandler;
import org.ws4d.java.configuration.Property;
import org.ws4d.java.configuration.PropertyHeader;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;




public class QoSConfigurationPropertiesHandler implements
		PropertiesHandler {
	public static final PropertyHeader HEADER_SECTION_QoS = new PropertyHeader(new String[]{ "QoS" });
	public static final PropertyHeader HEADER_SUBSECTION_POLICIES = new PropertyHeader("PolicyBuilder", HEADER_SECTION_QoS);

	private static QoSConfigurationPropertiesHandler handler=null;
	
	private QoSConfigurationWrapper config=null;
	private final HashMap qosConfigurations=new HashMap();
	
	public QoSConfigurationPropertiesHandler()
	{
		super();
		if (handler != null) {
			throw new RuntimeException("QoSProperties: class already instantiated!");
		}
		handler = this;
	}
	
	public static synchronized QoSConfigurationPropertiesHandler getInstance() {
		if (handler == null) {
			handler = new QoSConfigurationPropertiesHandler();
		}
		return handler;
	}

	@Override
	public void setProperties(PropertyHeader header, Property property) {
		if (QoSConfigurationPropertiesHandler.HEADER_SECTION_QoS.equals(header)) {
			if (Log.isDebug())
				Log.debug("Handling QoS Property");
			
		}else if (QoSConfigurationPropertiesHandler.HEADER_SUBSECTION_POLICIES.equals(header))
		{
			if (config==null)
				config=new QoSConfigurationWrapper();
			
			if ("BuilderClass".equalsIgnoreCase(property.key))
			{
				config.builderClass=property.value;
			}else if (property.key!=null && property.value!=null)
			{
				config.map.put(property.key,property.value);
			}
		}

	}

	@Override
	public void finishedSection(int depth) 
	{
		if (depth==2 && this.config!=null && config.builderClass!=null)
		{
			try {
				qosConfigurations.put(config.builderClass, config);
			} catch (Exception e) {
				Log.error(e);
			}finally{
				this.config=null;
			}
		}
	}
		
	public Iterator getQoSConfigurations()
	{
		return qosConfigurations.values().iterator();
	}
}
