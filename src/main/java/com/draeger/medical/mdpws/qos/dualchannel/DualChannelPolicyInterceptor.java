/**
 * 
 */
package com.draeger.medical.mdpws.qos.dualchannel;

import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptor;

/**
 *  A marker interface for an interceptor that handles dual channel policies.
 *
 */
public interface DualChannelPolicyInterceptor  extends QoSPolicyInterceptor{
	//void - marker interfaces
}
