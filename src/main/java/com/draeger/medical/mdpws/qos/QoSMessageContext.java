package com.draeger.medical.mdpws.qos;

import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;

import com.draeger.medical.mdpws.message.MDPWSMessageContext;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyToken;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyTokenState;

public class QoSMessageContext implements MDPWSMessageContext {
 
	private static final int LIST_SIZE=5;
	private ArrayList qoSPolicyTokens=new ArrayList(LIST_SIZE);
	 
	 
	/**
	 *@see DSC Stack.Messages.MDPWSMessageContext#getQoSPolicyToken()
	 */
	public Iterator getQoSPolicyToken() 
	{
		return new ReadOnlyIterator(qoSPolicyTokens.iterator());
	}
	 
	/**
	 *@see DSC Stack.Messages.MDPWSMessageContext#getValidQoSPolicyToken()
	 */
	public Iterator getValidQoSPolicyToken() {
		ArrayList validTokens=findTokens(QoSPolicyTokenState.VALID);
		return validTokens.iterator();
	}
	 
	/**
	 *@see DSC Stack.Messages.MDPWSMessageContext#getInvalidQoSPolicyToken()
	 */
	public Iterator getInvalidQoSPolicyToken() {
		ArrayList validTokens=findTokens(QoSPolicyTokenState.INVALID);
		return validTokens.iterator();
	}
	 
	/**
	 *@see DSC Stack.Messages.MDPWSMessageContext#getUnknownQoSPolicyToken()
	 */
	public Iterator getUnknownQoSPolicyToken() {
		ArrayList validTokens=findTokens(QoSPolicyTokenState.UNKNOWN);
		return validTokens.iterator();
	}
	 

	public void addQoSPolicyToken(QoSPolicyToken<?,?> token) 
	{
		if (token!=null && !qoSPolicyTokens.contains(token))
		{
			qoSPolicyTokens.add(token);
		}
	}
	 

	@Override
	public void merge(MDPWSMessageContext msgContext ) 
	{
	 //void
	}
	
	protected ArrayList findTokens(QoSPolicyTokenState state){
		ArrayList tokens=new ArrayList(LIST_SIZE);
		Iterator it=getQoSPolicyToken();
		while(it.hasNext())
		{
			Object o= it.hasNext();
			if (o instanceof QoSPolicyToken)
			{
				QoSPolicyToken<?,?> token=(QoSPolicyToken<?,?>)o;
				if (token.getTokenState().equals(state))
				{
					tokens.add(token);
				}
			}
		}
		return tokens;
	}
	 
}
 
