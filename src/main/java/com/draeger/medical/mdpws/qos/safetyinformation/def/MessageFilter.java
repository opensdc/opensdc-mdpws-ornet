package com.draeger.medical.mdpws.qos.safetyinformation.def;

import java.util.ArrayList;

import com.draeger.medical.mdpws.common.util.XPathInfo;

public class MessageFilter {
	private final ArrayList<String> applicationSpecificIDs=new ArrayList<String>();
	private final XPathInfo xPath;
	
	public MessageFilter(ArrayList<String> applicationSpecificIDs,XPathInfo messageFilter)
	{
		if (applicationSpecificIDs!=null)
		{
			this.applicationSpecificIDs.addAll(applicationSpecificIDs);
		}
		this.xPath=messageFilter;
	}
	
	public XPathInfo getXPathMessageFilter() 
	{
		return xPath;
	}

	public ArrayList<String> getApplicationSpecificIDs() {
		return applicationSpecificIDs;
	}

	@Override
	public String toString() {
		return "MessageFilter [applicationSpecificIDs="
				+ applicationSpecificIDs + ", xPath=" + xPath + "]";
	}
	
	
	
}
