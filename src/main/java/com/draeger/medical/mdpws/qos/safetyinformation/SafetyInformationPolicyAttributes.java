package com.draeger.medical.mdpws.qos.safetyinformation;

import java.util.ArrayList;

import org.ws4d.java.types.QName;

import com.draeger.medical.mdpws.qos.safetyinformation.def.ContextDefinition;
import com.draeger.medical.mdpws.qos.safetyinformation.def.DualChannelSelector;
import com.draeger.medical.mdpws.qos.safetyinformation.def.MessageFilter;
import com.draeger.medical.mdpws.qos.wsdl.SafetyInformationConstants;

public class SafetyInformationPolicyAttributes {

	private final ArrayList<DualChannelSelector> dualChannelSelectors=new ArrayList<DualChannelSelector>();
	private final ArrayList<ContextDefinition> contextDefinitions=new ArrayList<ContextDefinition>();
	private boolean transmitDualChannel=SafetyInformationConstants.SI_ATTRIB_ASSERTION_TRANSMIT_DC_DEFAULT_VALUE;
	private boolean transmitSafetyContext=SafetyInformationConstants.SI_ATTRIB_ASSERTION_TRANSMIT_SCTXT_DEFAULT_VALUE;
	private QName transform=SafetyInformationConstants.SI_ATTRIB_TRANSFORM_DUALCHANNEL_DEFAULT_VALUE;
	private QName algorithm=SafetyInformationConstants.SI_ATTRIB_ALGORITHM_DUALCHANNEL_DEFAULT_VALUE;
	private MessageFilter messageFilter=null;
	
	public boolean isTransmitDualChannel() {
		return transmitDualChannel;
	}
	public void setTransmitDualChannel(boolean transmitDualChannel) {
		this.transmitDualChannel = transmitDualChannel;
	}
	public boolean isTransmitSafetyContext() {
		return transmitSafetyContext;
	}
	public void setTransmitSafetyContext(boolean transmitSafetyContext) {
		this.transmitSafetyContext = transmitSafetyContext;
	}
	
	public void setAlgorithm(QName algorithm) {
		this.algorithm=algorithm;
	}
	public void setTransform(QName transform) {
		this.transform=transform;
	}
	
	public QName getTransform() {
		return transform;
	}
	public QName getAlgorithm() {
		return algorithm;
	}
	

	public MessageFilter getMessageFilter() {
		return messageFilter;
	}
	public void setMessageFilter(MessageFilter messageFilter) {
		this.messageFilter = messageFilter;
	}
	public ArrayList<DualChannelSelector> getDualChannelSelectors() {
		return dualChannelSelectors;
	}
	
	public ArrayList<ContextDefinition> getContextDefinitions() {
		return contextDefinitions;
	}
	@Override
	public String toString() {
		return "SafetyInformationPolicyAttributes [dualChannelSelectors="
				+ dualChannelSelectors + ", contextDefinitions="
				+ contextDefinitions + ", transmitDualChannel="
				+ transmitDualChannel + ", transmitSafetyContext="
				+ transmitSafetyContext + ", transform=" + transform
				+ ", algorithm=" + algorithm + ", messageFilter="
				+ messageFilter + "]";
	}

	
}
	
