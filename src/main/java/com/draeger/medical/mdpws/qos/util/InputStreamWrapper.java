package com.draeger.medical.mdpws.qos.util;

import java.io.IOException;
import java.io.InputStream;

import org.ws4d.java.util.Log;

public class InputStreamWrapper extends InputStream
{

	private InputStream is;
	protected boolean closingAllowed=false;


	public InputStreamWrapper(InputStream is)
	{
		this.is=is;
	}

	
//	private int i=0, a=0;
	@Override
	public int read() throws IOException {
//		System.out.println("InputStreamWrapper: "+(++i));
		return this.is.read();
	}
	
	@Override
	public int available() throws IOException {
		return super.available();
	}

	@Override
	public synchronized void mark(int readlimit) {
		this.is.mark(readlimit);
	}

	@Override
	public boolean markSupported() {
		return this.is.markSupported();
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
//		System.out.println("InputStreamWrapper A: "+(++a));
		return this.is.read(b, off, len);
	}

	@Override
	public int read(byte[] b) throws IOException {
//		System.out.println("InputStreamWrapper A: "+(++a));
		return this.is.read(b);
	}

	@Override
	public synchronized void reset() throws IOException {
		this.is.reset();
	}

	@Override
	public long skip(long n) throws IOException {
		return this.is.skip(n);
	}

	@Override
	public void close() throws IOException 
	{

		if (closingAllowed){
			if (Log.isDebug())Log.debug("Closing stream");
			this.is.close();
		}else
		{
			if (Log.isDebug())
			{
				Log.debug("Closing denied");
			}
		}
	}

	public boolean isClosingAllowed() {
		return closingAllowed;
	}

	public void setClosingAllowed(boolean closingAllowed) {
		this.closingAllowed = closingAllowed;
	}
}
