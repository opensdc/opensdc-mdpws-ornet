package com.draeger.medical.mdpws.qos.safetyinformation.transmission;

import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.types.QName;

public class SafetyContextValue {
	private final QName referencedElementQName;
	private final IParameterValue ctxtParameterValue;
	public SafetyContextValue(QName referencedElementQName,
			IParameterValue ctxtParameterValue) {
		super();
		this.referencedElementQName = referencedElementQName;
		this.ctxtParameterValue = ctxtParameterValue;
	}
	
	public QName getReferencedElementQName() {
		return referencedElementQName;
	}
	public IParameterValue getCtxtParameterValue() {
		return ctxtParameterValue;
	}

	@Override
	public String toString() {
		return "SafetyContextValue [referencedElementQName="
				+ referencedElementQName + ", ctxtParameterValue="
				+ ctxtParameterValue + "]";
	}
	
}
