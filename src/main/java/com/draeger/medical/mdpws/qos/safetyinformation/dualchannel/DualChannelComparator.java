/**
 * 
 */
package com.draeger.medical.mdpws.qos.safetyinformation.dualchannel;

import com.draeger.medical.mdpws.qos.safetyinformation.transmission.DualChannel;

/**
 *
 *
 */
public interface DualChannelComparator {

	/**
	 * @param computedFirstChannel
	 * @param dualChannel
	 */
	public abstract boolean compare(Object computedFirstChannel, DualChannel<?, ?, ?> dualChannel);

}
