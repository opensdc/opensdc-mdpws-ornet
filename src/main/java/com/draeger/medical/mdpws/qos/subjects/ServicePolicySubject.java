/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.qos.subjects;

import org.ws4d.java.service.Service;



public class ServicePolicySubject extends AbstractQoSPolicySubject {
 
	private final Service service;

	public ServicePolicySubject(Service service, boolean abstractSubject) {
		super(null, abstractSubject);
		this.service = service;
	}
	
	public ServicePolicySubject(Class<?> associatedClass, boolean abstractSubject) {
		super(associatedClass, abstractSubject);
		this.service=null;
	}

	public Service getService() 
	{
		return service;
	}

	@Override
	public boolean isAssociatedWith(Object o) {
		return (getService()!=null && (o!=null && o.equals(getService()))) || (getAssociatedClass()!=null && super.isAssociatedWith(o));
	}

	@Override
	public String toString() {
		return "ServicePolicySubject [service=" + service + ", associatedClass="
				+ super.toString() + "]";
	}

	@Override
	public Object getAssociation() {
		return getService();
	}
	
	
}
 
