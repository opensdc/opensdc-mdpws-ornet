package com.draeger.medical.mdpws.qos.management;

import org.ws4d.java.structures.HashMap;

public class QoSPolicyBuilderInstanceRegistry 
{
	private final static QoSPolicyBuilderInstanceRegistry instance=new QoSPolicyBuilderInstanceRegistry();
	private final HashMap builderInstances=new HashMap();
	
	public static QoSPolicyBuilderInstanceRegistry getInstance() {
		return instance;
	}
	
	private QoSPolicyBuilderInstanceRegistry()
	{
		//void
	}
	
	public QoSPolicyBuilder getPolicyBuilder(Class<?> builderClass)
	{
		return (QoSPolicyBuilder) builderInstances.get(builderClass);
	}
	
	public void registerBuilderForClass(Class<?> builderClass, QoSPolicyBuilder builder)
	{
		if (builderClass!=null && builder!=null)
			builderInstances.put(builderClass, builder);
	}
	
	public void unregisterBuilderForClass(Class<?> builderClass, QoSPolicyBuilder builder)
	{
		if (builderClass!=null && builder!=null)
			builderInstances.remove(builderClass);
	}
	
	public void registerBuilder(QoSPolicyBuilder builder)
	{
		if (builder!=null)
			registerBuilderForClass(builder.getClass(),builder);
	}
	
	public void unregisterBuilder(QoSPolicyBuilder builder)
	{
		if (builder!=null)
			unregisterBuilderForClass(builder.getClass(),builder);
	}
}
