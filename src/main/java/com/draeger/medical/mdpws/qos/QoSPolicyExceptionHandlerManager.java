/**
 * 
 */
package com.draeger.medical.mdpws.qos;

import org.w3c.dom.Document;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;

import com.draeger.medical.mdpws.qos.interception.QoSPolicyToken;

/**
 *
 *
 */
public final class QoSPolicyExceptionHandlerManager 
{
	private static final QoSPolicyExceptionHandlerManager instance=new QoSPolicyExceptionHandlerManager();
	
	private QoSPolicyExceptionHandlerManager()
	{
		//void
	}

	public static QoSPolicyExceptionHandlerManager getInstance() {
		return instance;
	}
	
	//contains ExceptionHandler
	private final ArrayList registry=new ArrayList();

	public boolean add(Object obj) 
	{	
		return registry.add(obj);
	}

	public Iterator getEntries() {
		return registry.iterator();
	}

	public boolean remove(Object o) 
	{
		return registry.remove(o);
	}
	
	public boolean callExceptionHandler(QoSPolicy policy,Document soapDocument, QoSPolicyToken<?,?> token, QoSMessageContext qosMessageCtxt, ProtocolData protocolData, String additionalInfo)
	{
		boolean throwException=true;
		boolean tempThrowException=false;
		
		for (Iterator it=this.registry.iterator();it.hasNext();)
		{
			QoSPolicyExceptionHandler handler=(QoSPolicyExceptionHandler)it.next();
			 if (handler.isResponsibleFor(policy))
			 {
				 tempThrowException|=handler.handle(policy,soapDocument, token, qosMessageCtxt, protocolData, additionalInfo);
			 }
			 
			 throwException= tempThrowException;
		}
		return throwException;
	}
	
}
