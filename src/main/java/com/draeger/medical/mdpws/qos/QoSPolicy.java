/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.qos;


import org.ws4d.java.structures.Iterator;

import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptionDirection;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptor;
import com.draeger.medical.mdpws.qos.subjects.QoSPolicySubject;

/**
 * A Quality of service policy.
 */
public interface QoSPolicy {
	
	/**
	 * Gets the subjects.
	 *
	 * @return the subjects
	 */
	public abstract Iterator getSubjects();
	
	/**
	 * Checks if is subject for.
	 *
	 * @param subjectClz the subject clz
	 * @return true, if is subject for
	 */
	public abstract boolean isSubjectFor(Class<?> subjectClz);
	
	/**
	 * Gets the interceptors.
	 *
	 * @return the interceptors
	 */
	public abstract Iterator getInterceptors();
	
	/**
	 * Gets the subjects.
	 *
	 * @param subjectClz the subject clz
	 * @return the subjects
	 */
	public abstract Iterator getSubjects(Class<?> subjectClz);
	
	/**
	 * Checks if is token required.
	 *
	 * @return true, if is token required
	 */
	public abstract boolean isTokenRequired();
	
	/**
	 * Checks if is abstract policy.
	 *
	 * @return true, if is abstract policy
	 */
	public abstract boolean isAbstractPolicy();
	
//	TODO SSch public abstract void addExceptionHandler(QoSPolicyExceptionHandler handler);
//	TODO SSch public abstract Iterator getExceptionHandlers();
	
	/**
 * Gets the interception direction.
 *
 * @return the interception direction
 */
public abstract QoSPolicyInterceptionDirection getInterceptionDirection();
	
	/**
	 * Adds the subject.
	 *
	 * @param subject the subject
	 * @return true, if successful
	 */
	public abstract boolean addSubject(QoSPolicySubject subject);
	
	/**
	 * Adds the interceptor.
	 *
	 * @param interceptor the interceptor
	 * @return true, if successful
	 */
	public abstract boolean addInterceptor(QoSPolicyInterceptor interceptor);
}
 
