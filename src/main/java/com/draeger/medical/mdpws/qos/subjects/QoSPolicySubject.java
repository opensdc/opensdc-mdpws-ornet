package com.draeger.medical.mdpws.qos.subjects;

public interface QoSPolicySubject {
 	 
	public abstract String getSubjectName();
	public abstract boolean isAbstract();
	public abstract boolean isAssociatedWith(Object o);

	public abstract Object getAssociation();
}
 
