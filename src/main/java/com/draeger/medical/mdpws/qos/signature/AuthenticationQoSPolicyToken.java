/**
 * 
 */
package com.draeger.medical.mdpws.qos.signature;

import java.security.cert.X509Certificate;
import java.util.List;

import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptionDirection;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyToken;

/**
 *
 * @param <P>
 * @param <V>
 *
 */
public class AuthenticationQoSPolicyToken<V extends List<X509Certificate>,P extends QoSPolicy> extends QoSPolicyToken<V, P> {

	/**
	 * @param qoSPolicy
	 * @param value
	 * @param qoSPolicyInterceptionPoint
	 */
	public AuthenticationQoSPolicyToken(P qoSPolicy, V value,
			QoSPolicyInterceptionDirection qoSPolicyInterceptionPoint) {
		super(qoSPolicy, value, qoSPolicyInterceptionPoint);
	}

	@Override
	public String toString() {
		return "AuthenticationQoSPolicyToken [toString()=" + super.toString()
				+ "]";
	}	
}
