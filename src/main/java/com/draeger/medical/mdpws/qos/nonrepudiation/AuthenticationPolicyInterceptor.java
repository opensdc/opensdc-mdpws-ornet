/**
 * 
 */
package com.draeger.medical.mdpws.qos.nonrepudiation;

import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptor;

/**
 *
 *
 */
public interface AuthenticationPolicyInterceptor  extends QoSPolicyInterceptor{
	//void - marker interfaces
}
