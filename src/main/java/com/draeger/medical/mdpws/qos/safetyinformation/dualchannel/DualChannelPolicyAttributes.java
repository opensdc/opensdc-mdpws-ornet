/**
 * 
 */
package com.draeger.medical.mdpws.qos.safetyinformation.dualchannel;

import org.ws4d.java.types.QName;

import com.draeger.medical.mdpws.common.util.XPathInfo;
import com.draeger.medical.mdpws.qos.wsdl.SafetyInformationConstants;

/**
 *
 *
 */
public class DualChannelPolicyAttributes 
{
	
	private QName algorithm=SafetyInformationConstants.SI_ATTRIB_ALGORITHM_DUALCHANNEL_VALUE_SHA1D;
	private QName transform=SafetyInformationConstants.SI_ATTRIB_TRANSFORM_DUALCHANNEL_VALUE_NOTRANS;
	private XPathInfo xPath=null;
	

	public QName getAlgorithm() {
		return algorithm;
	}
	public void setAlgorithm(QName algorithm) {
		this.algorithm = algorithm;
	}
	public QName getTransform() {
		return transform;
	}
	public void setTransform(QName transform) {
		this.transform = transform;
	}
	
	public XPathInfo getXPath() 
	{
		return xPath;
	}
	public void setXPath(XPathInfo xPath) {
		this.xPath = xPath;
	}
	
	

	@Override
	public String toString() {
		return "DualChannelPolicyAttributes [algorithm=" + algorithm
				+ ", transform=" + transform + ", xPath=" + xPath + "]";
	}


}
