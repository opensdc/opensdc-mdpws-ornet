/**
 * 
 */
package com.draeger.medical.mdpws.qos.saftyinformation.impl;

import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptionDirection;


/**
 * @author Stefan Schlichting
 *
 */
public class InboundSafetyInformationQoSPolicy  extends AbstractSafetyInformationQoSPolicy{

	private final boolean isAbstract=false;
	private final boolean isTokenRequired=true;
	private final QoSPolicyInterceptionDirection direction=QoSPolicyInterceptionDirection.INBOUND;
	
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.QoSPolicy#isTokenRequired()
	 */
	@Override
	public boolean isTokenRequired() 
	{
		return isTokenRequired;
	}
	
	@Override
	public QoSPolicyInterceptionDirection getInterceptionDirection() {
		return direction;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.QoSPolicy#isAbstractPolicy()
	 */
	@Override
	public boolean isAbstractPolicy() {
		return isAbstract;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((direction == null) ? 0 : direction.hashCode());
		result = prime * result + (isAbstract ? 1231 : 1237);
		result = prime * result + (isTokenRequired ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		InboundSafetyInformationQoSPolicy other = (InboundSafetyInformationQoSPolicy) obj;
		if (direction != other.direction)
			return false;
		if (isAbstract != other.isAbstract)
			return false;
		if (isTokenRequired != other.isTokenRequired)
			return false;
		return true;
	}
	
}
