package com.draeger.medical.mdpws.qos.management;

import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;

import com.draeger.medical.mdpws.qos.QoSPolicy;

//TODO SSch Make thread safe
public class QoSPolicyRegistry {
 	 
	private final ArrayList qoSPolicies=new ArrayList();
	private final ArrayList ignoredPolicies=new ArrayList();
	
	QoSPolicyRegistry()
	{
		//void
	}
	 
	public boolean addQoSPolicy(QoSPolicy policy) 
	{
		if (policy!=null && !ignoredPolicies.contains(policy.getClass()) && !qoSPolicies.contains(policy))
		{
			return qoSPolicies.add(policy);
		}
		
		return false;
	}
	
	public boolean removeQoSPolicy(QoSPolicy policy) 
	{
		if (policy!=null)
		{
			return qoSPolicies.remove(policy);
		}
		
		return false;
	}
	 
	public Iterator getQoSPoliciesForSubjectType(Class<?> subjectClz) {
		ArrayList qosPolciesWithSubjectType=new ArrayList();
		Iterator policies=getPolicies();
		while (policies.hasNext())
		{
			Object policyO=policies.next();
			if (!(policyO instanceof QoSPolicy)) continue;
			QoSPolicy policy=(QoSPolicy)policyO;
			if (policy.isSubjectFor(subjectClz))
			{
				qosPolciesWithSubjectType.add(policy);
			}
		}
		return new ReadOnlyIterator(qosPolciesWithSubjectType.iterator());
	}
	
	public Iterator getPolicies(){
		return new ReadOnlyIterator(qoSPolicies.iterator());
	}

	/**
	 * @param policyClass
	 */
	public void ignoreQoSPolicyOfType(Class<?> policyClass) 
	{
		if (!ignoredPolicies.contains(policyClass))
		{
			ignoredPolicies.add(policyClass);
		}
	}
	 
}
 
