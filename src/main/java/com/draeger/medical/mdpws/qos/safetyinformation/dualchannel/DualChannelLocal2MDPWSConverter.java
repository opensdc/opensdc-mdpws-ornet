/**
 * 
 */
package com.draeger.medical.mdpws.qos.safetyinformation.dualchannel;

import org.w3c.dom.Node;
import org.ws4d.java.service.parameter.IParameterValue;

import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicyAttributes;



/**
 *
 *
 */
public interface DualChannelLocal2MDPWSConverter 
{
	//object is in local representation
	//public abstract Object createChannelValueFromBytes(byte[] serializedData, DualChannelPolicyAttributes attributes);
	
	//the object is in local representation
	public abstract Object createObjectFromParameterValue(IParameterValue pv,SafetyInformationPolicyAttributes dualChannelPolicyAttributes);

	public abstract IParameterValue getParameterValue(Object channelValue, SafetyInformationPolicyAttributes policyAttributes, Node exemplaryChannelDomNode);


	/**
	 * Channel value in the same representation as the first channel, e.g. fc=1 & sc=1 ==> convertSecondChannel2LocalRepresentation(sc,something) ==> sc=42.
	 * 
	 * @param channel 
	 * @param policyAttributes
	 * @return
	 */
	public abstract Object convertSecondChannel2LocalRepresentation(
			Object channel,
			SafetyInformationPolicyAttributes policyAttributes);



}
