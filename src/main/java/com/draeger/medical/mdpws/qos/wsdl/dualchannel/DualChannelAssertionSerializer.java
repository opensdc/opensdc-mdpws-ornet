/**
 * 
 */
package com.draeger.medical.mdpws.qos.wsdl.dualchannel;



import java.io.IOException;
import java.util.Map.Entry;

import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;
import org.xmlpull.v1.XmlSerializer;

import com.draeger.medical.mdpws.common.util.XPathInfo;
import com.draeger.medical.mdpws.common.util.XPathNamespaceContext;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionSerializer;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicy;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicyAttributes;
import com.draeger.medical.mdpws.qos.wsdl.SPAConstants;

/**
 *
 *
 */
public class DualChannelAssertionSerializer implements
WSDLAssertionSerializer {

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionSerializer#serialize(java.lang.Object, org.xmlpull.v1.XmlSerializer)
	 */
	@Override
	public void serialize(Object assertion, XmlSerializer serializer) throws IOException 
	{
		if (assertion instanceof DualChannelPolicy)
		{
			DualChannelPolicy dualChannelPolicy=(DualChannelPolicy)assertion;
			DualChannelPolicyAttributes attributes= dualChannelPolicy.getDualChannelPolicyAttributes();

			
			serializer.startTag(SPAConstants.SPA_NAMESPACE,SPAConstants.SPA_ELEM_DUALCHANNEL );
			if (attributes!=null)
			{ 
				if (Log.isDebug())
					Log.debug("Attributes found for Dual Channel Policy.");
				
				if (attributes.getAlgorithm()!=null)
					serializer.attribute(SPAConstants.SPA_NAMESPACE, SPAConstants.SPA_ATTRIB_ALGORITHM_DUALCHANNEL, getPrefixedString(attributes.getAlgorithm(), serializer));

				if (attributes.getTransform()!=null)
					serializer.attribute(SPAConstants.SPA_NAMESPACE, SPAConstants.SPA_ATTRIB_TRANSFORM_DUALCHANNEL, getPrefixedString(attributes.getTransform(), serializer));

				if (attributes.getXPath()!=null)
				{
					
					//use the namespace resolver
					XPathInfo xPathInfo= attributes.getXPath();
					XPathNamespaceContext ctxt= xPathInfo.getContext();
					String expr=xPathInfo.getExpression();
					
					if (ctxt==null)
					{
						if (Log.isInfo())
							Log.info("No namespace context provided for xpath expression."+expr);
						
						serializer.attribute(SPAConstants.SPA_NAMESPACE, SPAConstants.SPA_ATTRIB_XPATH_DUALCHANNEL,expr);
					}else{
						java.util.Iterator<Entry<String, String>> entries= ctxt.getAllEntries();
						while(entries.hasNext())
						{
							Entry<String, String> entry=entries.next();
							String prefixOrig=entry.getKey();
							String prefix=serializer.getPrefix(entry.getValue(), true);
							if (Log.isDebug())
								Log.debug("Replace "+prefixOrig+" with "+prefix+" "+entry.getValue());
							
							expr=expr.replace(prefixOrig+":", prefix+":");
						}
						serializer.attribute(SPAConstants.SPA_NAMESPACE, SPAConstants.SPA_ATTRIB_XPATH_DUALCHANNEL,expr);
					}
				}
			}else
			{
				if (Log.isDebug())
					Log.debug("No Attributes for Dual Channel Policy. Use default values.");
			}
			serializer.endTag(SPAConstants.SPA_NAMESPACE,SPAConstants.SPA_ELEM_DUALCHANNEL );		}
	}

	private String getPrefixedString(QName qname, XmlSerializer serializer)
	{
		String prefixedStr="";
		if (qname==null) return prefixedStr;

		String nSpace=qname.getNamespace();
		if (nSpace!=null)
		{
			nSpace=nSpace.trim();
			if (nSpace.length()>0)
			{
				String prefix=serializer.getPrefix(nSpace, true);
				if (prefix!=null)
					prefixedStr=prefix+":"+qname.getLocalPart();
			}else{
				prefixedStr=qname.toStringPlain();
			}
		}
		return prefixedStr;
	}
}


