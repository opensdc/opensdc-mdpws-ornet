/**
 * 
 */
package com.draeger.medical.mdpws.qos.safetyinformation.impl;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Node;
import org.ws4d.java.constants.XMLConstants;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelProtocolConverter;
import com.draeger.medical.mdpws.qos.safetyinformation.impl.SafetyInformationInterceptorImpl.TransformedChannelInfos;
import com.draeger.medical.mdpws.qos.wsdl.SafetyInformationConstants;


/**
 *
 *
 */
public class DualChannelNoTransHexConverter implements DualChannelProtocolConverter
{

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelTransformator#transformForSerialization(org.w3c.dom.NodeList, com.draeger.medical.mdpws.qos.dualchannel.impl.DualChannelInterceptorImpl.TransformedChannelInfos)
	 */
	@Override
	public void transformForSerialization(Node[] nodes,
			TransformedChannelInfos channelInfos) 
	{

		int nodeCnt=nodes.length;
		for(int i=0;i<nodeCnt;i++)
		{
			try{
				Node n=nodes[i];
				byte[] bytes=getBytes(n);
				channelInfos.put(n, bytes);
			}catch (Exception e)
			{
				Log.error(e);
				throw new IllegalStateException(e.getMessage());
			}
		}
	}

	private byte[] getBytes(Node node) throws TransformerFactoryConfigurationError, TransformerException, UnsupportedEncodingException
	{
		Transformer t = TransformerFactory.newInstance().newTransformer();
		t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		StringWriter sw = new StringWriter();
		t.transform(new DOMSource(node), new StreamResult(sw));

		return sw.toString().getBytes(XMLConstants.ENCODING);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelTransformator#getImplementedTransformation()
	 */
	@Override
	public QName getImplementedTransformation() {
		return SafetyInformationConstants.SI_ATTRIB_TRANSFORM_DUALCHANNEL_VALUE_NOTRANS;
	}


	private String toHex(String arg) {
		return arg==null || arg.trim().length()==0?null:String.format("%x", new BigInteger(arg.getBytes()));
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelConverter#getDualChannelRepresentation(com.draeger.medical.mdpws.qos.dualchannel.impl.DualChannelInterceptorImpl.TransformedChannelInfos)
	 */
	@Override
	public String getDualChannelRepresentation(
			byte[] serializedData) {
		if (serializedData==null) return null;

		StringBuilder builder=new StringBuilder();

		try{
			builder.append(toHex(new String(serializedData,XMLConstants.ENCODING)));

		}catch(Exception e)
		{
			Log.error(e);
			throw new RuntimeException(e);
		}
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelConverter#getImplementedAlgorithm()
	 */
	@Override
	public QName getImplementedAlgorithm() {
		return SafetyInformationConstants.SI_ATTRIB_ALGORITHM_DUALCHANNEL_VALUE_HEX;
	}


}
