/**
 * 
 */
package com.draeger.medical.mdpws.qos.dualchannel;

import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptionDirection;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyToken;

/**
 *
 * @param <P>
 * @param <V>
 */
public class DualChannelPolicyToken<V extends DualChannel<?, ?, ?>,P extends DualChannelPolicy> extends QoSPolicyToken<V, P> {

	/**
	 * @param qoSPolicy
	 * @param value
	 * @param qoSPolicyInterceptionPoint
	 */
	public DualChannelPolicyToken(P qoSPolicy, V value,
			QoSPolicyInterceptionDirection qoSPolicyInterceptionPoint) {
		super(qoSPolicy, value, qoSPolicyInterceptionPoint);
	}
}
