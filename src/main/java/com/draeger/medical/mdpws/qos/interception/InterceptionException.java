package com.draeger.medical.mdpws.qos.interception;

public class InterceptionException extends Exception {
	
	private static final long serialVersionUID = 1214709338012991994L;

	public InterceptionException() {
		super();
	}

	public InterceptionException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public InterceptionException(String arg0) {
		super(arg0);
	}

	public InterceptionException(Throwable arg0) {
		super(arg0);
	}

}
