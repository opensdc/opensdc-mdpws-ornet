/**
 * 
 */
package com.draeger.medical.mdpws.qos.safetyinformation.impl;

import java.security.NoSuchAlgorithmException;

import org.ws4d.java.structures.HashMap;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicy;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelProtocolConverter;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelProtocolConverterProvider;



/**
 *
 *
 */
public class DefaultDualChannelProtocolConverterProvider implements
		DualChannelProtocolConverterProvider {

	
	private final HashMap transformations=new HashMap();
	
	
	public DefaultDualChannelProtocolConverterProvider() {
		addProtocolConverter(new DualChannelNoTransHexConverter());
		try {
			addProtocolConverter(new DualChannelEXCC14NCSHA1Converter());
		} catch (NoSuchAlgorithmException e) {
			Log.warn(e);
		}
	}

	/**
	 * @param protocolConverter
	 */
	private boolean addProtocolConverter(
			DualChannelProtocolConverter protocolConverter) {
		boolean retVal=false;
		if (protocolConverter!=null && protocolConverter.getImplementedTransformation()!=null && protocolConverter.getImplementedAlgorithm()!=null)
		{
			transformations.put(
					createKey(protocolConverter.getImplementedTransformation(), protocolConverter.getImplementedAlgorithm()), protocolConverter);
			retVal=true;
		}
		return retVal;
	}

	/**
	 * @param transformator
	 * @return
	 */
	private String createKey(QName transformation, QName algorithm) {
		return transformation.toStringPlain()+algorithm.toStringPlain();
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelTransformatorProvider#getDualChannelTransformator(com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicy)
	 */
	@Override
	public DualChannelProtocolConverter getDualChannelTransformator(
			SafetyInformationPolicy policy) 
	{
		DualChannelProtocolConverter protocolConverter=null;
		if (policy!=null && policy.getSafetyInformationPolicyAttributes()!=null && policy.getSafetyInformationPolicyAttributes().getAlgorithm()!=null && policy.getSafetyInformationPolicyAttributes().getTransform()!=null)
		{
			protocolConverter=(DualChannelProtocolConverter) transformations.get(createKey(policy.getSafetyInformationPolicyAttributes().getTransform(), policy.getSafetyInformationPolicyAttributes().getAlgorithm()));
		}
		return protocolConverter;
	}



}
