/**
 * 
 */
package com.draeger.medical.mdpws.qos.wsdl.safetyinformation;



import java.io.IOException;

import org.ws4d.java.util.Log;
import org.xmlpull.v1.XmlSerializer;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionSerializer;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicy;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicyAttributes;
import com.draeger.medical.mdpws.qos.wsdl.SafetyInformationConstants;
/**
 *
 *
 */
public class SafetyInformationAssertionSerializer implements
WSDLAssertionSerializer {

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionSerializer#serialize(java.lang.Object, org.xmlpull.v1.XmlSerializer)
	 */
	@Override
	public void serialize(Object assertion, XmlSerializer serializer) throws IOException 
	{
		if (assertion instanceof SafetyInformationPolicy)
		{
			SafetyInformationPolicy dualChannelPolicy=(SafetyInformationPolicy)assertion;
			SafetyInformationPolicyAttributes attributes= dualChannelPolicy.getSafetyInformationPolicyAttributes();


			serializer.startTag(SafetyInformationConstants.SI_NAMESPACE,SafetyInformationConstants.SI_ELEM_ASSERTION_SAFETY_REQ );
			if (attributes!=null)
			{ 
				if (Log.isDebug())
					Log.debug("Attributes found for SafetyInformation Assertion.");

				if (attributes.isTransmitDualChannel())
					serializer.attribute(SafetyInformationConstants.SI_NAMESPACE, SafetyInformationConstants.SI_ATTRIB_ASSERTION_TRANSMIT_DC, Boolean.toString(attributes.isTransmitDualChannel()));

				if (attributes.isTransmitSafetyContext())
					serializer.attribute(SafetyInformationConstants.SI_NAMESPACE, SafetyInformationConstants.SI_ATTRIB_ASSERTION_TRANSMIT_SCTXT, Boolean.toString(attributes.isTransmitSafetyContext()));

			}else
			{
				if (Log.isDebug())
					Log.debug("No Attributes for SafetyInformation Assertion. Use default values.");
			}
			serializer.endTag(SafetyInformationConstants.SI_NAMESPACE,SafetyInformationConstants.SI_ELEM_ASSERTION_SAFETY_REQ );		
		}
	}
}


