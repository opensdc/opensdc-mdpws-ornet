/**
 * 
 */
package com.draeger.medical.mdpws.qos.safetyinformation.dualchannel;

import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicy;

/**
 *
 *
 */
public interface DualChannelProtocolConverterProvider {

	/**
	 * @param policy
	 * @return
	 */
	DualChannelProtocolConverter getDualChannelTransformator(
			SafetyInformationPolicy policy);

}
