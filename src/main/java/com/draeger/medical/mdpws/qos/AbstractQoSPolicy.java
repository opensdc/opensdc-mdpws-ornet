/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.qos;

import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;

import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptor;
import com.draeger.medical.mdpws.qos.subjects.QoSPolicySubject;
import com.draeger.medical.mdpws.qos.util.Util;

/**
 * Provides an abstract implementation of a {@link QoSPolicy}.
 */
public abstract class AbstractQoSPolicy implements QoSPolicy {
	private ArrayList subjects=new ArrayList(); 
	
	/** The interceptors. */
	private ArrayList interceptors=new ArrayList();
	
	@Override
	public Iterator getSubjects() {
		return new ReadOnlyIterator(subjects.iterator());
	}

	@Override
	public boolean isSubjectFor(Class<?> subjectClz) {
		if (subjectClz==null) return false;
		Iterator compatibleSubjectsIterator=getSubjects(subjectClz);
		return compatibleSubjectsIterator.hasNext();
	}

	@Override
	public Iterator getInterceptors() {
		return new ReadOnlyIterator(interceptors.iterator());
	}

	@Override
	public Iterator getSubjects(Class<?> subjectClz) {
		return Util.findItems(subjectClz, getSubjects());
	}

	@Override
	public boolean addSubject(QoSPolicySubject subject) 
	{
		if (subject!=null && !subjects.contains(subject))
		{
			return subjects.add(subject);
		}
		return false;
	}
	
	protected boolean removeSubject(QoSPolicySubject subject) 
	{
		if (subject!=null)
		{
			return subjects.remove(subject);
		}
		return false;
	}

	@Override
	public boolean addInterceptor(QoSPolicyInterceptor interceptor) 
	{
		if (interceptor!=null && !interceptors.contains(interceptor))
		{
			return interceptors.add(interceptor);
		}
		return false;
	}
	
	protected boolean removeInterceptor(QoSPolicyInterceptor interceptor) 
	{
		if (interceptor!=null)
		{
			return interceptors.remove(interceptor);
		}
		return false;
	}

}
