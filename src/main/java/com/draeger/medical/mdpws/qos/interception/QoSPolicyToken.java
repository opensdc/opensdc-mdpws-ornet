package com.draeger.medical.mdpws.qos.interception;

import com.draeger.medical.mdpws.qos.QoSPolicy;


public class QoSPolicyToken<V, P extends QoSPolicy> {
 	 
	private QoSPolicyInterceptionDirection qoSPolicyInterceptionPoint;
	 
	private P qoSPolicy;
	 
	private QoSPolicyTokenState tokenState=QoSPolicyTokenState.UNKNOWN;
	
	private V value;
		 
	public QoSPolicyToken(
			P qoSPolicy,
			V value, QoSPolicyInterceptionDirection qoSPolicyInterceptionPoint) {
		super();
		this.qoSPolicyInterceptionPoint = qoSPolicyInterceptionPoint;
		this.qoSPolicy = qoSPolicy;
		this.value = value;
	}

	public P getQoSPolicy() {
		return qoSPolicy;
	}
	 
	public QoSPolicyTokenState getTokenState() {
		return tokenState;
	}


	public void updateValue(V value, QoSPolicyTokenState tokenState, QoSPolicyInterceptionDirection interceptionPoint) {
		this.value = value;
		this.tokenState = tokenState;
		this.qoSPolicyInterceptionPoint=interceptionPoint;
	}

	public QoSPolicyInterceptionDirection checkedAt() {
		return qoSPolicyInterceptionPoint;
	}
	 
	public V getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "QoSPolicyToken [qoSPolicyInterceptionPoint="
				+ qoSPolicyInterceptionPoint + ", qoSPolicy=" + qoSPolicy
				+ ", tokenState=" + tokenState + ", value=" + value + "]";
	}
	
}
 
