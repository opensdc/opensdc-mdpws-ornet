/**
 * 
 */
package com.draeger.medical.mdpws.qos.signature;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.ws.security.WSSecurityEngineResult;
import org.w3c.dom.Document;
import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.protocol.soap.generator.MessageReceiver;
import org.ws4d.java.schema.Element;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.util.Log;
import org.xml.sax.InputSource;

import com.draeger.medical.mdpws.qos.QoSMessageContext;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.interception.InboundSOAPXMLDocumentInterceptor;
import com.draeger.medical.mdpws.qos.interception.InterceptionException;
import com.draeger.medical.mdpws.qos.interception.OutboundSOAPUTF8TransformationInterceptor;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptionDirection;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyOrdinalNumber;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyToken;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyTokenState;
import com.draeger.medical.mdpws.qos.nonrepudiation.AuthenticationPolicyInterceptor;
import com.draeger.medical.mdpws.qos.security.SecurityEngine;
import com.draeger.medical.mdpws.qos.subjects.MessagePolicySubject;
import com.draeger.medical.mdpws.qos.subjects.OperationPolicySubject;
import com.draeger.medical.mdpws.qos.subjects.ServicePolicySubject;

/**
 *
 *
 */
public class XMLSignatureInterceptor 
implements 	AuthenticationPolicyInterceptor, 
SignaturePolicyInterceptor, 
OutboundSOAPUTF8TransformationInterceptor, InboundSOAPXMLDocumentInterceptor
{
	private DocumentBuilderFactory docBuilderFactory;
	private DocumentBuilder builder;
	private TransformerFactory tFactory;
	private Transformer transformer;


	private final ArrayList associatedPolicies=new ArrayList();
	private final ArrayList associatedSubjectClasses=new ArrayList();

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.interception.OutboundSOAPUTF8TransformationInterceptor#interceptOutbound(java.io.InputStream, org.ws4d.java.communication.ProtocolData, com.draeger.medical.mdpws.qos.QoSMessageContext, com.draeger.medical.mdpws.qos.QoSPolicy, java.io.OutputStream)
	 */
	public XMLSignatureInterceptor() {
		super();
		this.docBuilderFactory = DocumentBuilderFactory.newInstance();
		this.docBuilderFactory.setNamespaceAware(true);
		// Use a Transformer for output
		this.tFactory =TransformerFactory.newInstance();

		//tick the security engine
		SecurityEngine.getInstance().tick();

		associatedPolicies.add(InOutboundXMLSignatureQoSPolicy.class);
		associatedPolicies.add(InboundXMLSignatureQoSPolicy.class);
		associatedPolicies.add(OutboundXMLSignatureQoSPolicy.class);
		associatedSubjectClasses.add(ServicePolicySubject.class);
		associatedSubjectClasses.add(OperationPolicySubject.class);
		associatedSubjectClasses.add(MessagePolicySubject.class);
	}

	@Override
	public boolean interceptOutbound(
			InputStream interceptableSOAPUTF8InputStream, ProtocolData pd,
			QoSMessageContext qosMsgCtxt, QoSPolicy policy,
			OutputStream soapUTF8OutputStream, Element element) throws InterceptionException, IOException {

		try {
			//TODO SSch Handle that the SecurityEngine is not available...
			Document inputDocument=getDocumentBuilder().parse(new InputSource(interceptableSOAPUTF8InputStream));
			Document signedDoc = SecurityEngine.getInstance().addSignature(inputDocument);
			if (Log.isDebug())
			{
				String outputString= org.apache.ws.security.util.XMLUtils.PrettyDocumentToString(signedDoc);
				Log.debug(outputString);
			}
			
			Log.info("Added Signature to message");

			DOMSource resultingDOMSource = new DOMSource(signedDoc);
			StreamResult result = new StreamResult(soapUTF8OutputStream);
			getTransformer().transform(resultingDOMSource, result); 


		} catch (Exception e) {
			Log.error("Could not sign message! "+e.getMessage()+ " SecurityEngine init: "+SecurityEngine.getInstance().isInitialized());
			if (!Boolean.parseBoolean(System.getProperty("MDPWS.SecurityFailFallbackEnabled", "false")))
			{
				throw new IOException(e.getMessage());
			}
		}

		return false;
	}

	private synchronized DocumentBuilder getDocumentBuilder() throws ParserConfigurationException
	{
		if (this.builder==null)
			this.builder = this.docBuilderFactory.newDocumentBuilder();

		return this.builder;
	}

	private synchronized Transformer getTransformer() throws TransformerConfigurationException
	{
		if (this.transformer==null)
			this.transformer = tFactory.newTransformer();
		return this.transformer;
	}

	@Override
	public QoSPolicyOrdinalNumber getOrdinalNumberForSubject(Class<?> subjectClass) 
	{
		if (ServicePolicySubject.class.isAssignableFrom(subjectClass))
			return new QoSPolicyOrdinalNumber(0);
		else
			return QoSPolicyOrdinalNumber.NOT_APPLICABLE;
	}

	@Override
	public Iterator getQoSPolicyClasses() {
		return new ReadOnlyIterator(associatedPolicies.iterator());
	}

	@Override
	public Iterator getInterceptorSubjectClasses() {
		return new ReadOnlyIterator(associatedSubjectClasses.iterator());
	}

	@Override
	public QoSPolicyInterceptionDirection getInterceptionDirection() {
		return QoSPolicyInterceptionDirection.OUTBOUND;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.interception.InboundSOAPXMLInputStreamInterceptor#interceptInbound(java.io.InputStream, org.ws4d.java.communication.DPWSProtocolData, org.ws4d.java.communication.protocol.soap.generator.MessageReceiver, com.draeger.medical.mdpws.qos.QoSMessageContext, com.draeger.medical.mdpws.qos.QoSPolicy)
	 */
	@Override
	public Document interceptInbound(Document inboundSOAPDocument,
			DPWSProtocolData protocolData, MessageReceiver to,
			QoSMessageContext qosMessageCtxt, QoSPolicy policy, String action)
	throws InterceptionException 
	{
		Document outputDocument=null;
		//System.out.println("\t\t\t\t\t\t Checking security Engine [id="+protocolData.getInstanceId()+"] "+this);
		if (SecurityEngine.getInstance().isInitialized()) 		//TODO Check if  security measures could be applied
		{
			try {
				if (inboundSOAPDocument!=null)
				{
					
					QoSPolicyToken<List<X509Certificate>, QoSPolicy> token=new AuthenticationQoSPolicyToken<List<X509Certificate>, QoSPolicy>(policy, null, QoSPolicyInterceptionDirection.INBOUND);
					try{
						//System.out.println("\t\t\t\t\t\t Processing signature [id="+protocolData.getInstanceId()+"] "+this);
						 List<WSSecurityEngineResult> securityResults=SecurityEngine.getInstance().processSignature(inboundSOAPDocument);
						 //System.out.println("\t\t\t\t\t\t Processing signature done. [id="+protocolData.getInstanceId()+"] "+this);
						if (securityResults!=null)
						{
							java.util.ArrayList<X509Certificate> certs=new java.util.ArrayList<X509Certificate>();
							Log.info("SecurityHeader Information available: "+securityResults.size());
							
							for (Object object : securityResults) 
							{
								if (object instanceof WSSecurityEngineResult)
								{
									WSSecurityEngineResult engResult=(WSSecurityEngineResult)object;
									
									X509Certificate cert= (X509Certificate) engResult.get(WSSecurityEngineResult.TAG_X509_CERTIFICATE);
									if (cert!=null)
									{
										certs.add(cert);
									}
									
//									X509Certificate[] certs2= (X509Certificate[] ) engResult.get(WSSecurityEngineResult.TAG_X509_CERTIFICATES);
									if (Log.isInfo()) Log.info("CertificateChain Available "+certs.size());
								}
							}
							token.updateValue(certs, QoSPolicyTokenState.VALID, QoSPolicyInterceptionDirection.INBOUND);
						}else {
							Log.info("No SecurityHeader Information included");
							token.updateValue(null, QoSPolicyTokenState.INVALID, QoSPolicyInterceptionDirection.INBOUND);
						}
					}catch(org.apache.ws.security.WSSecurityException ex)
					{
						logError(to,ex, protocolData, inboundSOAPDocument);
						//throw new InterceptionException(ex);
						token.updateValue(null, QoSPolicyTokenState.ERROR_DURING_PROCESSING, QoSPolicyInterceptionDirection.INBOUND);
					}

				
					qosMessageCtxt.addQoSPolicyToken(token);
					outputDocument=inboundSOAPDocument;
				}
			} catch (Exception ex) {
				logError(to,ex, protocolData, inboundSOAPDocument);
				throw new InterceptionException(ex);
			}
		}


		//System.out.println("\t\t\t\t\t\t Interception done output:"+outputDocument+" [id="+protocolData.getInstanceId()+"] "+this);
		return outputDocument;
	}


	/**
	 * @param to
	 * @param ex
	 * @param protocolData
	 * @param inboundSOAPDocument 
	 */
	private void logError(MessageReceiver to, Exception ex,
			DPWSProtocolData protocolData, Document inboundSOAPDocument) {
		//ex.printStackTrace();
		Log.error(ex.getMessage()+" "+protocolData.toString());
		//to.receiveFailed(ex,protocolData);
	}


}
