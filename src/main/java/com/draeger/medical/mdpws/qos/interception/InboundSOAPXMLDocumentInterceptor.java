/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.qos.interception;

import org.w3c.dom.Document;
import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.protocol.soap.generator.MessageReceiver;

import com.draeger.medical.mdpws.qos.QoSMessageContext;
import com.draeger.medical.mdpws.qos.QoSPolicy;

public interface InboundSOAPXMLDocumentInterceptor extends QoSPolicyInterceptor 
{
	public abstract Document interceptInbound(Document xmlInputDocument, DPWSProtocolData protocolData, MessageReceiver to, QoSMessageContext qosMessageCtxt, QoSPolicy policy, String action) throws InterceptionException;
}
