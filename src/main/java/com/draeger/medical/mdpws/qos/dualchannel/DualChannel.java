/**
 * 
 */
package com.draeger.medical.mdpws.qos.dualchannel;

/**
 *
 *
 */
public class DualChannel<F,S, I> 
{
	private final I identifyingInformation;
	private final F firstChannel;
	private final S secondChannel; //always local representation
	
	
	public DualChannel(F firstChannel, S secondChannel, I idInfo) {
		super();
		this.firstChannel = firstChannel;
		this.secondChannel = secondChannel;
		this.identifyingInformation = idInfo;
	}
	
	public F getFirstChannel()
	{
		return firstChannel;
	}


	/**
	 * @return the secondChannel
	 */
	public S getSecondChannel() {
		return secondChannel;
	}

	public I getIdentifyingInformation() {
		return identifyingInformation;
	}

	@Override
	public String toString() {
		return "DualChannel [identifyingInformation=" + identifyingInformation
				+ ", firstChannel=" + firstChannel + ", secondChannel="
				+ secondChannel + "]";
	}
}
