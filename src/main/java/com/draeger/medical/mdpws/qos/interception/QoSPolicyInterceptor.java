package com.draeger.medical.mdpws.qos.interception;

import org.ws4d.java.structures.Iterator;


public interface QoSPolicyInterceptor 
{
	public abstract Iterator getInterceptorSubjectClasses();
	public abstract QoSPolicyOrdinalNumber getOrdinalNumberForSubject(Class<?> subjectClass);
	public abstract QoSPolicyInterceptionDirection getInterceptionDirection();
	public abstract Iterator getQoSPolicyClasses();
}
 
