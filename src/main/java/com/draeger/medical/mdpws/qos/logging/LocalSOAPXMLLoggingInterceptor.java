/**
 * 
 */
package com.draeger.medical.mdpws.qos.logging;

import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.schema.Element;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.util.Log;
import org.xml.sax.InputSource;

import com.draeger.medical.mdpws.qos.QoSMessageContext;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.interception.InterceptionException;
import com.draeger.medical.mdpws.qos.interception.OutboundSOAPUTF8TransformationInterceptor;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptionDirection;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyOrdinalNumber;
import com.draeger.medical.mdpws.qos.subjects.MessagePolicySubject;
import com.draeger.medical.mdpws.qos.subjects.OperationPolicySubject;
import com.draeger.medical.mdpws.qos.subjects.ServicePolicySubject;

/**
 *
 *
 */
public class LocalSOAPXMLLoggingInterceptor 
implements 	 
LocalLoggingInterceptor, 
OutboundSOAPUTF8TransformationInterceptor 
{
	private DocumentBuilderFactory factory;
	private DocumentBuilder builder;
	private TransformerFactory tFactory;
	private Transformer transformer;


	private final ArrayList associatedPolicies=new ArrayList();
	private final ArrayList associatedSubjectClasses=new ArrayList();

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.interception.OutboundSOAPUTF8TransformationInterceptor#interceptOutbound(java.io.InputStream, org.ws4d.java.communication.ProtocolData, com.draeger.medical.mdpws.qos.QoSMessageContext, com.draeger.medical.mdpws.qos.QoSPolicy, java.io.OutputStream)
	 */
	public LocalSOAPXMLLoggingInterceptor() {
		super();
		this.factory = DocumentBuilderFactory.newInstance();
		this.factory.setNamespaceAware(true);
		// Use a Transformer for output
		this.tFactory =TransformerFactory.newInstance();


		associatedPolicies.add(OutboundSOAPXMLLoggingQoSPolicy.class);
		associatedSubjectClasses.add(ServicePolicySubject.class);
		associatedSubjectClasses.add(OperationPolicySubject.class);
		associatedSubjectClasses.add(MessagePolicySubject.class);
	}

	@Override
	public boolean interceptOutbound(
			InputStream interceptableSOAPUTF8InputStream, ProtocolData pd,
			QoSMessageContext qosMsgCtxt, QoSPolicy policy,
			OutputStream soapUTF8OutputStream, Element element) throws InterceptionException {


		try {
			Document inputDocument=getDocumentBuilder().parse(new InputSource(interceptableSOAPUTF8InputStream));

			String outputString= org.apache.ws.security.util.XMLUtils.PrettyDocumentToString(inputDocument);
			if (Log.isInfo())
			{
				Log.info(outputString);
			}

			DOMSource source = new DOMSource(inputDocument);
			StreamResult result = new StreamResult(soapUTF8OutputStream);
			getTransformer().transform(source, result); 


		} catch (Exception e) {
			Log.error("Could not log message! "+e.getMessage());
		}
		return false;
	}

	private synchronized DocumentBuilder getDocumentBuilder() throws ParserConfigurationException
	{
		if (this.builder==null)
			this.builder = this.factory.newDocumentBuilder();

		return this.builder;
	}

	private synchronized Transformer getTransformer() throws TransformerConfigurationException
	{
		if (this.transformer==null)
			this.transformer = tFactory.newTransformer();
		return this.transformer;
	}

	@Override
	public QoSPolicyOrdinalNumber getOrdinalNumberForSubject(Class<?> subjectClass) 
	{
		if (ServicePolicySubject.class.isAssignableFrom(subjectClass))
			return new QoSPolicyOrdinalNumber(0);
		else
			return QoSPolicyOrdinalNumber.NOT_APPLICABLE;
	}

	@Override
	public Iterator getQoSPolicyClasses() {
		return new ReadOnlyIterator(associatedPolicies.iterator());
	}

	@Override
	public Iterator getInterceptorSubjectClasses() {
		return new ReadOnlyIterator(associatedSubjectClasses.iterator());
	}

	@Override
	public QoSPolicyInterceptionDirection getInterceptionDirection() {
		return QoSPolicyInterceptionDirection.OUTBOUND;
	}
}
