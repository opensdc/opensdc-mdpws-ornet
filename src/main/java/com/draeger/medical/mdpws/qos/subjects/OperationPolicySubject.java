/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.qos.subjects;

import org.ws4d.java.service.OperationDescription;


public class OperationPolicySubject extends AbstractQoSPolicySubject
{
//	public OperationPolicySubject(Class associatedClass, boolean abstractSubject) {
//		super(associatedClass, abstractSubject);
//	}

	/**
	 * @param op
	 * @param abstractPolicy
	 */
	public OperationPolicySubject(OperationDescription operationDescription, boolean abstractSubject) {
		 super(null, abstractSubject);
		 this.operationDescription=operationDescription;
	}

	private OperationDescription operationDescription;
	
	public OperationDescription getOperation() 
	{
		return operationDescription;
	}

	@Override
	public boolean isAssociatedWith(Object o) {
		return (o!=null && o.equals(operationDescription));
	}

	@Override
	public Object getAssociation() {
		return getOperation();
	}	
	
	
}
 
