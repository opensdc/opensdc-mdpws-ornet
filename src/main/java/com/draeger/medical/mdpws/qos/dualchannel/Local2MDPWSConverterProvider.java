/**
 * 
 */
package com.draeger.medical.mdpws.qos.dualchannel;

/**
 *
 *
 */
public interface Local2MDPWSConverterProvider {

	/**
	 * @param policy
	 * @return
	 */
	DualChannelLocal2MDPWSConverter getConverter(DualChannelPolicy policy);

}
