/**
 * 
 */
package com.draeger.medical.mdpws.qos.saftyinformation.impl;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.QNameValue;
import org.ws4d.java.service.parameter.StringValue;
import org.ws4d.java.service.parameter.StringValueFactory;

import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicyAttributes;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelLocal2MDPWSConverter;

/**
 * @author Stefan Schlichting
 *
 */
public class SimpleLocal2MDPWSConverter implements
DualChannelLocal2MDPWSConverter {

	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelLocal2MDPWSConverter#createChannelValueFromBytes(byte[], com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicyAttributes)
	//	 */
	//	public Object createChannelValueFromBytes(byte[] serializedData,
	//			DualChannelPolicyAttributes attributes) {
	//		try{
	//			return new String(serializedData,"UTF-8");
	//		}catch(Exception e){
	//			Log.error(e.getMessage());
	//			Log.printStackTrace(e);
	//		}
	//		return null;
	//	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelLocal2MDPWSConverter#getParameterValue(java.lang.Object)
	 */
	@Override
	public IParameterValue getParameterValue(Object channelValue, SafetyInformationPolicyAttributes policyAttributes, Node exemplaryChannelDomNode) {

		ParameterValue val=null;
		if (channelValue!=null)
		{
			if (exemplaryChannelDomNode instanceof Text)
			{
				//val=new StringValue(channelValue.toString());
				val=StringValueFactory.getInstance().getStringValue();
				((StringValue)val).set(channelValue.toString());
			}else if (exemplaryChannelDomNode instanceof Element){
				val=new ParameterValue();
				//TODO SSch build complex PV???
				val.overrideSerialization(channelValue.toString());
			}
		}

		return val;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelLocal2MDPWSConverter#convertSecondChannel2LocalRepresentation(java.lang.Object, com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicyAttributes)
	 */
	@Override
	public Object convertSecondChannel2LocalRepresentation(Object channel,
			SafetyInformationPolicyAttributes policyAttributes) {
		Object retVal=null;
		if (channel!=null)
		{
//			StringBuffer buffer = new StringBuffer(channel.toString());  
//			buffer 				= buffer.reverse();
//			retVal 				= buffer.toString();
			retVal=channel;
		}
		return retVal;  
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.qos.dualchannel.DualChannelLocal2MDPWSConverter#createObjectFromParameterValue(org.ws4d.java.service.parameter.ParameterValue, com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicyAttributes)
	 */
	@Override
	public Object createObjectFromParameterValue(IParameterValue pv,
			SafetyInformationPolicyAttributes dualChannelPolicyAttributes) {
		if (pv instanceof StringValue)
		{
			return ((StringValue)pv).get();
		}else if (pv instanceof QNameValue)
		{
			return ((QNameValue)pv).get();
		}
		return pv;
	}


}
