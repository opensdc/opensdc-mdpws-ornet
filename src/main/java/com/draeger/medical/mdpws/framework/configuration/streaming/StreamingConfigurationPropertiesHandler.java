/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.framework.configuration.streaming;

import org.ws4d.java.configuration.PropertiesHandler;
import org.ws4d.java.configuration.Property;
import org.ws4d.java.configuration.PropertyHeader;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;


public class StreamingConfigurationPropertiesHandler implements
		PropertiesHandler {
	public static final PropertyHeader HEADER_SECTION_STREAMING = new PropertyHeader(new String[]{ "Streaming" });
	public static final PropertyHeader HEADER_SUBSECTION_STREAM = new PropertyHeader("Stream", HEADER_SECTION_STREAMING);

	private static StreamingConfigurationPropertiesHandler handler=null;
	
	private StreamConfigurationWrapper config=null;
	private final HashMap streamConfigs=new HashMap();
	
	public StreamingConfigurationPropertiesHandler()
	{
		super();
		if (handler != null) {
			throw new RuntimeException("StreamingProperties: class already instantiated!");
		}
		handler = this;
	}
	
	public static synchronized StreamingConfigurationPropertiesHandler getInstance() {
		return handler;
	}

	@Override
	public void setProperties(PropertyHeader header, Property property) {
		if (StreamingConfigurationPropertiesHandler.HEADER_SECTION_STREAMING.equals(header)) {
			Log.info("Handling Streaming Property");
		}else if (StreamingConfigurationPropertiesHandler.HEADER_SUBSECTION_STREAM.equals(header))
		{
			if (config==null)
				config=new StreamConfigurationWrapper();
			
			if ("ConfigurationId".equalsIgnoreCase(property.key))
			{
				config.configId=property.value;
			}else if ("StreamTypeCreatorClz".equalsIgnoreCase(property.key)){
				config.clz=property.value;
			}else{
				config.map.put(property.key,property.value);
			}
		}

	}

	@Override
	public void finishedSection(int depth) 
	{
		if (depth==2 & this.config!=null)
		{
			try {
				Class<?> builderClass = Class.forName(config.clz);
				StreamConfigurationBuilder  loadedBuilder = (StreamConfigurationBuilder) builderClass.newInstance();
				StreamConfiguration sConfig= loadedBuilder.createStreamConfiguration(this.config);
				streamConfigs.put(sConfig.getConfigId(), sConfig);
			} catch (Exception e) {
				Log.error(e);
			}finally{
				this.config=null;
			}
		}

	}
	
	public StreamConfiguration getStreamConfiguration(String configId)
	{
		return (StreamConfiguration) streamConfigs.get(configId);
	}
	
	public Iterator getStreamConfigurations()
	{
		return streamConfigs.values().iterator();
	}
}
