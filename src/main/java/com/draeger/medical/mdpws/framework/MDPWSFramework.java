/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.framework;

import org.ws4d.java.DPWSFramework;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.configuration.Properties;
import org.ws4d.java.constants.SOAPConstants;
import org.ws4d.java.dispatch.FrameworkModuleRegistry;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;

import com.draeger.medical.mdpws.communication.MDPWSCommunicationManagerID;
import com.draeger.medical.mdpws.domainmodel.wsdl.SOAPoverUDPStreamConfigurationHandler;
import com.draeger.medical.mdpws.domainmodel.wsdl.StreamConfigurationSupportRegistry;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAssertionSupportRegistry;
import com.draeger.medical.mdpws.qos.configuration.QoSConfigurationPropertiesHandler;
import com.draeger.medical.mdpws.qos.configuration.QoSConfigurationWrapper;
import com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder;
import com.draeger.medical.mdpws.qos.management.QoSPolicyBuilderInstanceRegistry;
import com.draeger.medical.mdpws.qos.management.QoSPolicyManager;

/**
 * The Class MDPWSFramework.
 */
public class MDPWSFramework {

	private static MDPWSFramework instance=new MDPWSFramework();

	/**
	 * Gets the single instance of MDPWSFramework.
	 *
	 * @return single instance of MDPWSFramework
	 */
	public static MDPWSFramework getInstance() {
		return instance;
	}

	private MDPWSFramework() {
		super();
	}


	/**
	 * Checks if is running.
	 *
	 * @return true, if is running
	 */
	public synchronized boolean isRunning()
	{
		return FrameworkModuleRegistry.getInstance().isRunning();
	}


	/**
	 * Starts the framework.
	 *
	 * @param args the args
	 */
	public synchronized void start(String[] args) {

		if (!isRunning())
		{
			setupStreamingModule();
			DPWSFramework.start(args);

			CommunicationManagerRegistry.load(MDPWSCommunicationManagerID.INSTANCE);
		}

	}


	/**
	 * Initialize qos framework.
	 */
	public void initializeQoSFramework() 
	{	

		Iterator policyBuilderClassesIt=getPolicyBuilderClasses(); 

		if (policyBuilderClassesIt!=null)
		{
			while(policyBuilderClassesIt.hasNext()) {
				Class<?> policyBuilderClass=(Class<?>) policyBuilderClassesIt.next();
				try{

					QoSPolicyBuilder builder=getBuilderInstanceFromClass(policyBuilderClass);
					QoSPolicyBuilderInstanceRegistry.getInstance().registerBuilderForClass(policyBuilderClass, builder);
					wireQoSFrameworkUsingBuilder(builder);
				}catch(Exception e){
					Log.warn(e);
				}
			}
		}		
	}


	private QoSPolicyBuilder getBuilderInstanceFromClass(Class<?> policyBuilderClass) throws InstantiationException, IllegalAccessException 
	{
		QoSPolicyBuilder builder=null;
		if (policyBuilderClass!=null)
			builder=(QoSPolicyBuilder) policyBuilderClass.newInstance();

		return builder;
	}

	private Iterator getPolicyBuilderClasses() {

		//"com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicyBuilder"
		//com.draeger.medical.mdpws.qos.compression.CompressionPolicyBuilder
		//com.draeger.medical.mdpws.qos.nonrepudiation.AuthenticationPolicyBuilder
		//com.draeger.medical.mdpws.qos.logging.LoggingPolicyBuilder

		ArrayList builderClasses=new ArrayList();
		Iterator it= QoSConfigurationPropertiesHandler.getInstance().getQoSConfigurations();
		while(it.hasNext())
		{
			Object o= it.next();
			if (o instanceof QoSConfigurationWrapper)
			{
				QoSConfigurationWrapper config=(QoSConfigurationWrapper) o;
				if (config.builderClass!=null)
				{
					try {
						Class<?> builderClass = Class.forName(config.builderClass);
						builderClasses.add(builderClass);
					} catch (ClassNotFoundException e) {
						Log.warn(e);
					}
				}
			}
		}

		return builderClasses.iterator();
	}

	private void wireQoSFrameworkUsingBuilder(QoSPolicyBuilder builder){

		try{
			//interceptor
			QoSPolicyManager.getInstance().registerInterceptor(builder.createInboundInterceptor());
			QoSPolicyManager.getInstance().registerInterceptor(builder.createOutboundInterceptor());
			QoSPolicyManager.getInstance().registerInterceptor(builder.createInOutboundInterceptor());


			//serializer
			WSDLPolicyAssertionSupportRegistry.getInstance().addAssertionSerializer(builder.getInboundPolicyClass(), 
					builder.createSerializer());
			WSDLPolicyAssertionSupportRegistry.getInstance().addAssertionSerializer(builder.getOutboundPolicyClass(), 
					builder.createSerializer());
			WSDLPolicyAssertionSupportRegistry.getInstance().addAssertionSerializer(builder.getInOutboundPolicyClass(), 
					builder.createSerializer());

			//parser
			WSDLPolicyAssertionSupportRegistry.getInstance().addAssertionParser(builder.getInboundAssertionElementName(), 
					builder.createParser());
		}catch(Exception e){
			Log.warn("Could not wire Qos component using Builder "+builder);
			Log.warn(e);

		}
	}

	/**
	 * Forces the framework to stop.
	 */
	public synchronized void kill() {
		DPWSFramework.kill();
	}

	/**
	 * Stops the framework.
	 */
	public synchronized void stop() {
		DPWSFramework.stop();
	}

	protected void setupStreamingModule() {
		//TODO SSch Add Streaming Module
		//boolean result = false;
		//15try {
		//Class.forName("com.draeger.medical.mdpws.domainmodel.StreamSource");
		//result = true;
		//} catch (ClassNotFoundException e) {
		//// void
		//}
		//HAVE_MDPWS_STREAMING_MODULE = result;
		//TODO SSch Remove constant string
		StreamConfigurationSupportRegistry.getInstance().addSupportHandler(SOAPConstants.SOAP_OVER_UDP_SCHEMA_URI, new SOAPoverUDPStreamConfigurationHandler());
		Properties.getInstance().register(QoSConfigurationPropertiesHandler.HEADER_SECTION_QoS,  QoSConfigurationPropertiesHandler.getInstance());		
	}
}
