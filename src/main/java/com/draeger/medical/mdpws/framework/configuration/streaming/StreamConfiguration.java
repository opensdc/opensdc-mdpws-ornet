/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.framework.configuration.streaming;

import com.draeger.medical.mdpws.communication.streaming.StreamBinding;

public abstract class StreamConfiguration 
{
	private String configId;
	private final StreamBinding binding;
	private final StreamTransmissionConfiguration transmisson;
	
	public StreamConfiguration(StreamBinding binding,
			StreamTransmissionConfiguration transmisson) {
		super();
		this.binding = binding;
		this.transmisson = transmisson;
	}

	public StreamBinding getBinding() {
		return binding;
	}

	public StreamTransmissionConfiguration getTransmissonConfiguration() {
		return transmisson;
	}

	public String getConfigId() {
		return configId;
	}

	public void setConfigId(String configId) {
		this.configId = configId;
	}

	@Override
	public String toString() {
		return "StreamConfiguration [configId=" + configId + ", binding="
				+ binding + ", transmisson=" + transmisson + "]";
	}
	
	
}
