/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.client.streaming;

import java.io.IOException;

import org.ws4d.java.communication.CommunicationManager;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.communication.IncomingMessageListener;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.concurrency.LockSupport;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.discovery.ByeMessage;
import org.ws4d.java.message.discovery.HelloMessage;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ProbeMessage;
import org.ws4d.java.message.discovery.ResolveMatchesMessage;
import org.ws4d.java.message.discovery.ResolveMessage;
import org.ws4d.java.message.eventing.GetStatusMessage;
import org.ws4d.java.message.eventing.GetStatusResponseMessage;
import org.ws4d.java.message.eventing.RenewMessage;
import org.ws4d.java.message.eventing.RenewResponseMessage;
import org.ws4d.java.message.eventing.SubscribeMessage;
import org.ws4d.java.message.eventing.SubscribeResponseMessage;
import org.ws4d.java.message.eventing.SubscriptionEndMessage;
import org.ws4d.java.message.eventing.UnsubscribeMessage;
import org.ws4d.java.message.eventing.UnsubscribeResponseMessage;
import org.ws4d.java.message.metadata.GetMessage;
import org.ws4d.java.message.metadata.GetMetadataMessage;
import org.ws4d.java.message.metadata.GetMetadataResponseMessage;
import org.ws4d.java.message.metadata.GetResponseMessage;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.service.OperationDescription;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.HashMap.Entry;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;

import com.draeger.medical.mdpws.communication.streaming.StreamBinding;
import com.draeger.medical.mdpws.dispatcher.streaming.client.IStreamConsumer;
import com.draeger.medical.mdpws.dispatcher.streaming.client.IStreamFrameHandler;
import com.draeger.medical.mdpws.domainmodel.impl.client.ProxyStreamSource;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamConfiguration;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamTransmissionConfiguration;

public class DefaultStreamConsumer  implements IStreamConsumer, IncomingMessageListener
{

	private final LockSupport lockSupport=new LockSupport();
	private final HashMap frameHandlerMap=new HashMap();
	private final ProxyStreamSource streamSource;
	//	protected SOAPoverUDPServer server;
	private final boolean runInThread=Boolean.parseBoolean(System.getProperty("MDPWS.DefaultStreamConsumer.RunInThread", "true"));

	public DefaultStreamConsumer(ProxyStreamSource sourceInfo)
	{
		this.streamSource=sourceInfo;
	}

	@Override
	public void addStreamFrameHandler(IStreamFrameHandler handler) {
		if (handler==null){
			RuntimeException e=new IllegalArgumentException("Could not add null StreamFrameHandler");
			Log.error(e.getMessage());
			throw e;
		}
		if (getStreamSource()==null || getStreamSource().getStreamConfiguration()==null || getStreamSource().getStreamConfiguration().getTransmissonConfiguration()==null)
		{
			RuntimeException e=new IllegalArgumentException("Could not add StreamFrameHandler "+handler+" due to lack of transmissionconfig.");
			Log.error(e.getMessage());
			throw e;
		}
		StreamTransmissionConfiguration transmissionConfig=streamSource.getStreamConfiguration().getTransmissonConfiguration();
		exclusiveLock();
		try{
			if (Log.isDebug())
				Log.debug("Adding "+getStreamSource().getOutputAction()+"@"+transmissionConfig.getDestinationAddress());

			getFrameHandlerMap().put(getStreamSource().getOutputAction()+"@"+transmissionConfig.getDestinationAddress(), handler);
		}finally{
			releaseExclusiveLock();
		}
	}

	protected HashMap getFrameHandlerMap() {
		return frameHandlerMap;
	}

	public ProxyStreamSource getStreamSource() {
		return streamSource;
	}

	@Override
	public synchronized void open() throws IOException
	{
		StreamConfiguration config= streamSource.getStreamConfiguration();
		if (config!=null && config.getBinding()!=null)
		{
			StreamBinding binding=config.getBinding();
			CommunicationManager manager = CommunicationManagerRegistry.getManager(binding.getCommunicationManagerId());
			if (manager!=null)
				manager.registerService(null, binding, this);
		}
		//		if (this.server==null)
		//			this.server=SOAPoverUDPServer.get(sourceInfo.getStreamAddress().getHost(), sourceInfo.getStreamAddress().getPort(), this, true);

	}

	@Override
	public synchronized void close() throws IOException
	{
		StreamConfiguration config= streamSource.getStreamConfiguration();
		if (config!=null && config.getBinding()!=null)
		{
			StreamBinding binding=config.getBinding();
			CommunicationManager manager = CommunicationManagerRegistry.getManager(binding.getCommunicationManagerId());
			if (manager!=null)
				manager.unregisterService(null, binding, this);
		}
	}

	public void exclusiveLock() {
		lockSupport.exclusiveLock();
	}

	public boolean releaseExclusiveLock() {
		return lockSupport.releaseExclusiveLock();
	}

	public void releaseSharedLock() {
		lockSupport.releaseSharedLock();
	}

	public void sharedLock() {
		lockSupport.sharedLock();
	}

	public boolean tryExclusiveLock() {
		return lockSupport.tryExclusiveLock();
	}

	public boolean trySharedLock() {
		return lockSupport.trySharedLock();
	}

	@Override
	public void handle(HelloMessage hello, ProtocolData protocolData) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handle(ByeMessage bye, ProtocolData protocolData) {
		// TODO Auto-generated method stub
	}

	@Override
	public ProbeMatchesMessage handle(ProbeMessage probe,
			ProtocolData protocolData) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResolveMatchesMessage handle(ResolveMessage resolve,
			ProtocolData protocolData) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetResponseMessage handle(GetMessage get, ProtocolData protocolData) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetMetadataResponseMessage handle(GetMetadataMessage getMetadata,
			ProtocolData protocolData) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SubscribeResponseMessage handle(SubscribeMessage subscribe,
			ProtocolData protocolData) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetStatusResponseMessage handle(GetStatusMessage getStatus,
			ProtocolData protocolData) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RenewResponseMessage handle(RenewMessage renew,
			ProtocolData protocolData) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UnsubscribeResponseMessage handle(UnsubscribeMessage unsubscribe,
			ProtocolData protocolData) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void handle(SubscriptionEndMessage subscriptionEnd,
			ProtocolData protocolData) {
		// TODO Auto-generated method stub

	}


	@Override
	public OperationDescription getOperation(String action) {
		return getStreamSource();
	}


	@Override
	public InvokeMessage handle(final InvokeMessage invokeRequest,
			final ProtocolData protocolData) {
		if (runInThread)
		{
			boolean executedThread=PlatformSupport.getInstance().getToolkit().getThreadPool().executeOrAbort(new Runnable(){

				@Override
				public void run() {
					handlingInvokeInternal(invokeRequest,protocolData);
				}
			});
			if (!executedThread)
				Log.warn("Stream could not be executed");
		}else{
			handlingInvokeInternal(invokeRequest,protocolData);
		}
		return null;
	}


	public void handlingInvokeInternal(final InvokeMessage invokeRequest,
			final ProtocolData protocolData)
	{
		sharedLock();
		try{	
			//TODO SSch Check if we should apply a filter 
			Iterator eIt= getFrameHandlerMap().entrySet().iterator();
			while(eIt.hasNext())
			{
				Entry e=(Entry) eIt.next();
				IStreamFrameHandler handler= (IStreamFrameHandler) e.getValue();
				if (Log.isDebug() && invokeRequest!=null && invokeRequest.getAction()!=null ){
					
					Log.debug("handlingInvokeInternal: "+invokeRequest.getAction().toString()+" Source:"+(invokeRequest.getHeader()!=null?invokeRequest.getHeader().getFrom():""));
				}
				handler.frameReceived(invokeRequest.getContent(),invokeRequest, protocolData, streamSource);
			}
		}finally{
			releaseSharedLock();
		}
	}

}
