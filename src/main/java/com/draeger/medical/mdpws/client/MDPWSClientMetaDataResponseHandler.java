/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.client;

import org.ws4d.java.communication.DefaultResponseCallback;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.metadata.GetMetadataResponseMessage;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;
import org.ws4d.java.wsdl.WSDLPortType;

import com.draeger.medical.mdpws.domainmodel.impl.client.MDPWSProxyService;
import com.draeger.medical.mdpws.message.metadata.MDPWSGetMetadataResponseMessage;

public class MDPWSClientMetaDataResponseHandler extends DefaultResponseCallback {


	private final MDPWSProxyService proxyService;
	private final Object notificationObject;

	public MDPWSClientMetaDataResponseHandler(MDPWSProxyService pServ, Object notificationObject) 
	{
		this.proxyService=pServ;
		this.notificationObject=notificationObject;
	}

	@Override
	public void handle(Message request,
			GetMetadataResponseMessage getMetadataResponse,
			ProtocolData protocolData) 
	{
		if (getMetadataResponse instanceof MDPWSGetMetadataResponseMessage){

			MDPWSGetMetadataResponseMessage respMsg=(MDPWSGetMetadataResponseMessage)getMetadataResponse;
			
			if (Log.isInfo())
				Log.info("Received MDPWSGetMetadataResponseMessage "+respMsg);
		
			if (!respMsg.getPortTypes().isEmpty())
			{
				DataStructure portTypes=respMsg.getPortTypes();
				for(Iterator pIt=portTypes.iterator();pIt.hasNext();)
				{
					WSDLPortType portType=(WSDLPortType)pIt.next();
					
					if (Log.isInfo())
						Log.info("Calling resetPortTypes from "+this.getClass());
					
					this.proxyService.resetPortType(portType);	
				}
			}
			this.proxyService.setChanged();
			this.proxyService.notifyObservers(notificationObject);
		}
	}




	//	public void receive(GetMetadataResponseMessage getMetadataResponse,
	//			DPWSProtocolData protocolData) {
	//		if (getMetadataResponse instanceof MDPWSGetMetadataResponseMessage){
	//			
	//			MDPWSGetMetadataResponseMessage respMsg=(MDPWSGetMetadataResponseMessage)getMetadataResponse;
	//			Log.info("Received MDPWSGetMetadataResponseMessage "+respMsg);
	//			if (!respMsg.getPortTypes().isEmpty())
	//			{
	//				DataStructure portTypes=respMsg.getPortTypes();
	//				for(Iterator pIt=portTypes.iterator();pIt.hasNext();)
	//				{
	//					WSDLPortType portType=(WSDLPortType)pIt.next();
	//					Log.info("Calling resetPortTypes from "+this.getClass());
	//					this.proxyService.resetPortType(portType);	
	//				}
	//			}
	//			this.proxyService.setChanged();
	//			this.proxyService.notifyObservers(notificationObject);
	//		}
	//	}
}
