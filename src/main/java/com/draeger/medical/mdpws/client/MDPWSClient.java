/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.client;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.communication.DefaultResponseCallback;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.dispatch.OutDispatcher;
import org.ws4d.java.dispatch.ProtocolVersionInfoRegistry;
import org.ws4d.java.eventing.DefaultEventSink;
import org.ws4d.java.eventing.EventSink;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.UnsupportedOperationException;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

import com.draeger.medical.mdpws.client.streaming.DefaultStreamConsumer;
import com.draeger.medical.mdpws.communication.MDPWSCommunicationManagerID;
import com.draeger.medical.mdpws.dispatcher.streaming.client.IStreamConsumer;
import com.draeger.medical.mdpws.dispatcher.streaming.client.IStreamFrameHandler;
import com.draeger.medical.mdpws.domainmodel.impl.client.MDPWSProxyService;
import com.draeger.medical.mdpws.domainmodel.impl.client.ProxyStreamSource;
import com.draeger.medical.mdpws.message.metadata.MDPWSGetStreamingMetadataMessage;
import com.draeger.medical.mdpws.utils.InternalObservable;

public class MDPWSClient extends DefaultClient implements Observer 
{

	protected ArrayList streamConsumers=new ArrayList();
	protected IStreamFrameHandler streamFrameHandler=new DefaultStreamHandler();

	public IStreamConsumer getStreamConsumer(ProxyStreamSource sourceInfo)
	{
		return new DefaultStreamConsumer(sourceInfo);
	}

	public IStreamFrameHandler getStreamFrameHandler() {
		return streamFrameHandler;
	}
	
	protected void setStreamFrameHandler(IStreamFrameHandler streamFrameHandler) {
		this.streamFrameHandler = streamFrameHandler;
	}

	//this should be the api for an application developer 
	public void subscribeToStream(MDPWSProxyService pServ,QName action) throws IOException {
		subscribeToStreamInternal(pServ,action, true);
	}

	protected void subscribeToStreamInternal(MDPWSProxyService pServ,QName action, boolean requestMetaData) throws IOException {
		Iterator it2=pServ.getStreamSources();
		if (action!=null && !getStreamFrameHandler().containsActionFilter(action))
			getStreamFrameHandler().addActionFilter(action);

		while(it2.hasNext())
		{
			ProxyStreamSource sSource=(ProxyStreamSource)it2.next();
			String streamAct=sSource.getOutputAction();

			if (Log.isDebug())
				Log.debug("Action: "+streamAct+" @ "+ sSource.getStreamConfiguration()+" "+sSource);

			if (action==null || (streamAct!=null && streamAct.startsWith(action.toStringPlain())))
			{
				if ((sSource.getStreamConfiguration()==null ||sSource.getStreamConfiguration().getTransmissonConfiguration()==null ||(sSource.getStreamConfiguration().getTransmissonConfiguration()!=null && !sSource.getStreamConfiguration().getTransmissonConfiguration().isValid())) && requestMetaData)
				{
					String streamIdentifier=sSource.getPortType().toStringPlain();
					MDPWSClientMetaDataResponseHandler respHandler=new MDPWSClientMetaDataResponseHandler(pServ, action);
					//sendGetMetaDataForStreamInfo(pServ, sSource.getPortType().toStringPlain(), respHandler);
					sendGetMetaDataForStreamInfo(pServ, streamIdentifier, respHandler);
					continue;
				}else if (sSource.getStreamConfiguration()!=null && sSource.getStreamConfiguration().getTransmissonConfiguration()!=null && sSource.getStreamConfiguration().getTransmissonConfiguration().getDestinationAddress()!=null){
					IStreamConsumer streamEater= this.getStreamConsumer(sSource);
					streamConsumers.add(streamEater);
					streamEater.addStreamFrameHandler(getStreamFrameHandler());
					streamEater.open();
					if (Log.isInfo())
						Log.info("Opened StreamEater for "+sSource+" "+sSource.getStreamConfiguration());
					continue;
				}else{
					if (Log.isWarn())
						Log.warn("Could not subscribe due to information lack! "+sSource.getStreamConfiguration()+" Req-MetaData:"+requestMetaData);
					continue;
				}
			}
		}

	}


	protected void sendGetMetaDataForStreamInfo(MDPWSProxyService pServ, String metaDataIdentifier, DefaultResponseCallback respHandler) throws IOException {
		//Try GetMetaDataRequest
		for(Iterator eIt=pServ.getEndpointReferences();eIt.hasNext();)
		{
			EndpointReference epr = (EndpointReference) eIt.next();
			if (epr.isXAddress())
			{
				pServ.addObserver(this);

				if (Log.isDebug())
					Log.debug("Sending GetMetadataMessage "+epr);

				MDPWSGetStreamingMetadataMessage metaDataMessage=new MDPWSGetStreamingMetadataMessage();
				metaDataMessage.setVersion(ProtocolVersionInfoRegistry.getInstance().get(epr));
				metaDataMessage.getHeader().setEndpointReference(epr);
				metaDataMessage.setIdentifier(new URI(metaDataIdentifier));
				metaDataMessage.setTargetAddress(epr.getAddress());
				
				if (Log.isDebug())
					Log.debug("Send MDPWSGetStreamingMetadataMessage: "+metaDataMessage);
				

				OutDispatcher.getInstance().send(metaDataMessage, MDPWSCommunicationManagerID.INSTANCE, respHandler); //FIXME SSch Callback==null??? 
				synchronized(this)
				{
					try {
						this.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	@Override
	public void update(Observable observable, Object param) 
	{
		if (observable instanceof InternalObservable && param instanceof QName)
		{
			Log.info("Updating ProxyService "+param);
			try {
				subscribeToStreamInternal((MDPWSProxyService) ((InternalObservable)observable).getSource(), (QName)param, false);
				synchronized(this)
				{
					this.notifyAll();
				}
			} catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}

	public static class DefaultStreamHandler implements IStreamFrameHandler
	{
		protected final QNameSet set=new QNameSet();

		@Override
		public void frameReceived(IParameterValue content, InvokeMessage msg,
				ProtocolData protocolData, ProxyStreamSource streamSource) {
			//System.out.println(msg);
			if (Log.isDebug())
			{
				Log.debug("****************");
				Log.debug("Received Frame: ");
				Log.debug(msg.toString());
				Log.debug(protocolData.toString());
				Log.debug(streamSource.toString());
				Log.debug("++++++++++++++++");
			}

		}

		@Override
		public Iterator getActionsFilter() 
		{
			return set.iterator();
		}

		@Override
		public boolean containsActionFilter(QName qn) 
		{
			return set.contains(qn);
		}

		@Override
		public boolean filtersSpecificActions() 
		{
			return set.isEmpty();
		}

		@Override
		public void addActionFilter(QName qn) throws UnsupportedOperationException 
		{
			set.add(qn);
		}

		@Override
		public boolean removeActionFilter(QName qn) throws UnsupportedOperationException 
		{
			return set.remove(qn);
		}


	}



	HashMap eventSinks=new HashMap();
	@Override
	public EventSink getEventSink(DataStructure bindings) 
	{
		synchronized (eventSinks) {
			EventSink retVal=null;
			if (eventSinks.containsKey(bindings))
			{
				retVal=(EventSink) eventSinks.get(bindings);
			}
			if (retVal==null)
			{
				retVal=new DefaultEventSink(this, bindings);
				eventSinks.put(bindings, retVal);
			}
			return retVal;
		}
	}

	public void stop()
	{
		if (!streamConsumers.isEmpty()){
			for (Iterator it=streamConsumers.iterator();it.hasNext();){
				try {
					((IStreamConsumer)it.next()).close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
