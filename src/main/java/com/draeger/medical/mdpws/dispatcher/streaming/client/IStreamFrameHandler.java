/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.dispatcher.streaming.client;

import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.UnsupportedOperationException;
import org.ws4d.java.types.QName;

import com.draeger.medical.mdpws.domainmodel.impl.client.ProxyStreamSource;

public interface IStreamFrameHandler {

	public Iterator getActionsFilter(); 
	
	public boolean filtersSpecificActions();
	
	public void addActionFilter(QName qn) throws UnsupportedOperationException ;
	
	public boolean removeActionFilter(QName qn) throws UnsupportedOperationException;
	
	public boolean containsActionFilter(QName qn);
	
	public void frameReceived(IParameterValue content, InvokeMessage msg, ProtocolData protocolData, ProxyStreamSource streamSource);

}
