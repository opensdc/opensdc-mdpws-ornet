/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package com.draeger.medical.mdpws.domainmodel.wsdl.policy;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.impl.DefaultWSDLPolicySerializer;

/**
 *
 *
 */
public class WSDLPolicySerializerFactory {

	private static final WSDLPolicySerializerFactory instance=new WSDLPolicySerializerFactory();

	public static WSDLPolicySerializerFactory getInstance() {
		return instance;
	}
	
	private WSDLPolicySerializerFactory()
	{
		//void
	}
	
	public WSDLPolicySerializer getSerializer()
	{
		return new DefaultWSDLPolicySerializer();
	}
}
