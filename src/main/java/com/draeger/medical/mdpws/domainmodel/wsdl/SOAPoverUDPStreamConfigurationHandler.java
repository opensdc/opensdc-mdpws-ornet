/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel.wsdl;

import java.io.IOException;

import org.ws4d.java.constants.SOAPConstants;
import org.ws4d.java.io.xml.AbstractElementParser;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import com.draeger.medical.mdpws.common.util.XMLParserUtil;
import com.draeger.medical.mdpws.communication.configuration.streaming.soapoverudp.SOAPoverUDPStreamConfiguration;
import com.draeger.medical.mdpws.communication.configuration.streaming.soapoverudp.SOAPoverUDPStreamTransmissionConfiguration;
import com.draeger.medical.mdpws.communication.protocol.constants.WSSTMConstants;
import com.draeger.medical.mdpws.communication.streaming.soapoverudp.SOAPoverUDPStreamBinding;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLPortType;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamConfiguration;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamTransmissionConfiguration;

public class SOAPoverUDPStreamConfigurationHandler implements StreamConfigurationSupport {

	@Override
	public StreamConfiguration createStreamConfiguration(String streamID, AbstractElementParser parser,
			MDPWSWSDLPortType portType) throws XmlPullParserException, IOException {

		//TODO SSch handle that there is no Transmission tag, but just Text, maybe use nextTag
		parser.next();
		
		//skip over to StreamTransmission tag
		parser.next();

		//Check if we have a streamAddress or Streamperiod
		URI streamAddr=null;
		String streamPeriod=null;
		String historyEPR=null;
		// we found an element so check if it is an element we know /expect
		//TODO Vielleicht unabhängig von der Reihenfolge machen
		if (XMLParserUtil.checkElementStart(parser,WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMADDRESS))
		{
			parser.next();
			String streamAddrStr=parser.getText();
			if (streamAddrStr!=null)
				streamAddr=new URI(streamAddrStr);
			XMLParserUtil.skipRemainingOnLevel(parser, parser.getDepth());
		}
		if (XMLParserUtil.checkElementStart(parser, WSSTMConstants.WSSTM_NAMESPACE_NAME,WSSTMConstants.WSSTM_ELEM_STREAMPERIOD))
		{
			parser.next();
			streamPeriod=parser.getText();

			XMLParserUtil.skipRemainingOnLevel(parser, parser.getDepth());
		}
		if (XMLParserUtil.checkElementStart(parser, WSSTMConstants.WSSTM_NAMESPACE_NAME,WSSTMConstants.WSSTM_ELEM_HISTORYEPR))
		{
			parser.next();
			historyEPR=parser.getText();
			XMLParserUtil.skipRemainingOnLevel(parser, parser.getDepth());
		}

		if (Log.isDebug())
			Log.debug(" addr:"+streamAddr+" period:"+streamPeriod+" hepr"+historyEPR);

		SOAPoverUDPStreamTransmissionConfiguration transmission=null;
		SOAPoverUDPStreamBinding binding=null;
		if (streamAddr!=null && streamAddr.getSchema().equals(SOAPConstants.SOAP_OVER_UDP_SCHEMA)) //TODO SSch Remove constant string
		{
			transmission=new SOAPoverUDPStreamTransmissionConfiguration(streamAddr.toString(), streamPeriod, historyEPR);
			binding=new SOAPoverUDPStreamBinding();
			binding.setTransportAddress(streamAddr);

		}
		StreamConfiguration streamConfig=new SOAPoverUDPStreamConfiguration(binding, transmission);
		streamConfig.setConfigId(streamID);

		return streamConfig;
	}

	@Override
	public void serializeStreamTransmission(XmlSerializer serializer,
			StreamTransmissionConfiguration streamTransmissionConfig) {
		try{
			if (serializer!=null && streamTransmissionConfig instanceof SOAPoverUDPStreamTransmissionConfiguration)
			{
				SOAPoverUDPStreamTransmissionConfiguration soapOverUDPStreamTransmissionConfiguration = (SOAPoverUDPStreamTransmissionConfiguration)streamTransmissionConfig;
				serializer.startTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMTRANSMISSION);
				serializeStreamAddress(serializer,soapOverUDPStreamTransmissionConfiguration );
				serializeStreamPeriod(serializer,soapOverUDPStreamTransmissionConfiguration);
				serializeHistoryEPR(serializer, soapOverUDPStreamTransmissionConfiguration);
				serializer.endTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMTRANSMISSION);

			}
		}catch(Exception e)
		{
			Log.error(e);
		}
	}

	protected void serializeHistoryEPR(XmlSerializer serializer, SOAPoverUDPStreamTransmissionConfiguration streamTransmissionConfig) throws IllegalArgumentException, IllegalStateException, IOException {
		if (streamTransmissionConfig!=null && streamTransmissionConfig.getHistoryEPR()!=null)
		{
			serializer.startTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_HISTORYEPR);
			serializer.text(streamTransmissionConfig.getHistoryEPR().toString());
			serializer.endTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_HISTORYEPR);
		}
	}



	protected void serializeStreamAddress(XmlSerializer serializer, SOAPoverUDPStreamTransmissionConfiguration streamTransmissionConfig) throws IllegalArgumentException, IllegalStateException, IOException {
		if (streamTransmissionConfig!=null && streamTransmissionConfig.getDestinationAddress()!=null)
		{
			serializer.startTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMADDRESS);
			serializer.text(streamTransmissionConfig.getDestinationAddress().toString());
			serializer.endTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMADDRESS);
		}
	}



	protected void serializeStreamPeriod(XmlSerializer serializer, SOAPoverUDPStreamTransmissionConfiguration streamTransmissionConfig) throws IllegalArgumentException, IllegalStateException, IOException {
		if (streamTransmissionConfig!=null && streamTransmissionConfig.getStreamPeriod()!=null)
		{
			serializer.startTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMPERIOD);
			serializer.text(streamTransmissionConfig.getStreamPeriod().toString());
			serializer.endTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMPERIOD);
		}

	}

}
