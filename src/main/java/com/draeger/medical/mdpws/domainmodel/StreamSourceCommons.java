/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel;

import org.ws4d.java.service.OperationCommons;
import org.ws4d.java.types.QName;
import org.ws4d.java.wsdl.WSDLOperation;

import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLStreamOperation;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamConfiguration;

public abstract class StreamSourceCommons extends OperationCommons {

	private StreamConfiguration streamConfiguration;
	
	protected StreamSourceCommons(String name, QName portType, StreamConfiguration streamConfig) {
		super(name, portType);
		setStreamConfiguration(streamConfig);
	}

	protected StreamSourceCommons(MDPWSWSDLStreamOperation operation) {
		super(operation);
		setStreamConfiguration(operation.getStreamConfiguration());
	}

	@Override
	public int getType() {
		if (type == WSDLOperation.TYPE_UNKNOWN) {
			if (getOutput() != null && getInput()==null) {
				type = WSDLOperation.TYPE_NOTIFICATION;
			} 
		}
		return type;
	}
	
	public final boolean isNotification() {
		return getType() == WSDLOperation.TYPE_NOTIFICATION;
	}
	

	public StreamConfiguration getStreamConfiguration() {
		return streamConfiguration;
	}
	

	public void setStreamConfiguration(StreamConfiguration streamConfiguration) {
		//Log.debug(this.getName()+" "+streamConfiguration);
		this.streamConfiguration = streamConfiguration;
	}
}
