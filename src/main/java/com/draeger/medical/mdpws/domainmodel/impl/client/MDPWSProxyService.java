/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel.impl.client;

import java.util.Observer;

import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.Schema;
import org.ws4d.java.service.DefaultEventSource;
import org.ws4d.java.service.Fault;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.OperationCommons;
import org.ws4d.java.service.OperationDescription;
import org.ws4d.java.service.ProxyService;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.service.reference.ServiceReferenceInternal;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.HashSet;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.List;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.structures.Set;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;
import org.ws4d.java.wsdl.WSDL;
import org.ws4d.java.wsdl.WSDLBinding;
import org.ws4d.java.wsdl.WSDLOperation;
import org.ws4d.java.wsdl.WSDLPortType;

import com.draeger.medical.mdpws.communication.protocol.soap.wsdl.MDPWSSOAP12DocumentLiteralHTTPBinding;
import com.draeger.medical.mdpws.domainmodel.MDPWSOperation;
import com.draeger.medical.mdpws.domainmodel.MDPWSPortType;
import com.draeger.medical.mdpws.domainmodel.StreamSourceCommons;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.SimpleWSDLPolicy;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicy;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachment;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyDirection;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyEmbeddedAttachment;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSIOType;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLOperation;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLStreamOperation;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.STMIOType;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;
import com.draeger.medical.mdpws.message.invoke.MDPWSInvokeMessage;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.interception.InterceptionException;
import com.draeger.medical.mdpws.qos.management.QoSPolicyManager;
import com.draeger.medical.mdpws.qos.subjects.MessagePolicySubject;
import com.draeger.medical.mdpws.qos.subjects.OperationPolicySubject;
import com.draeger.medical.mdpws.qos.subjects.QoSPolicySubject;
import com.draeger.medical.mdpws.qos.subjects.ServicePolicySubject;
import com.draeger.medical.mdpws.utils.InternalObservable;

public class MDPWSProxyService extends ProxyService {

	protected final InternalObservable internalObservable=new InternalObservable(this);

	protected final HashMap	streams		= new HashMap();

	private boolean isConstructed = false;

	public MDPWSProxyService(ServiceReference serviceReference,
			DeviceReference parentDeviceReference) {
		super(serviceReference, parentDeviceReference);
		isConstructed=true;
	}

	public void resetPortType(WSDLPortType portType)
	{
		QName portTypeName = portType.getName();


		if (getPortTypesInternal().containsKey(portTypeName)) 
		{
			if (Log.isDebug())
				Log.debug("Removing Porttype "+portTypeName);

			getPortTypesInternal().remove(portTypeName);
		}
		processWSDLPortType(portType);
	}




	@Override
	public void initialize(ServiceReference serviceReference,
			DeviceReference parentDeviceReference)
					throws InstantiationException {
		if (isConstructed){
			super.initialize(serviceReference, parentDeviceReference);
		}
		else
			throw new InstantiationException("Not constructed!");

	}



	@Override
	protected void processWSDLPortType(WSDLPortType portType) {
		if (!isConstructed) return;
		QName portTypeName = portType.getName();
		if (getPortTypesInternal().containsKey(portTypeName)) {
			/*
			 * we have already imported this port type probably through a
			 * different WSDL file
			 */
			return;
		}
		MDPWSPortType port = new MDPWSPortType();
		DataStructure operations = portType.getOperations();
		for (Iterator it = operations.iterator(); it.hasNext();) {
			WSDLOperation operation = (WSDLOperation) it.next();
			OperationCommons op;
			if (operation instanceof MDPWSWSDLStreamOperation)
			{
				MDPWSWSDLStreamOperation streamOperation = (MDPWSWSDLStreamOperation)operation;

				if  (!(streamOperation.getOutput() instanceof STMIOType))
				{
					if (Log.isWarn())
					{
						Log.warn("No output defined for "+streamOperation+". Skipping operation.");
					}				
					continue; //for loop
				}else{
					STMIOType streamOutputType = (STMIOType) streamOperation.getOutput();
					QName outputMessageName = streamOutputType.getMessageName();
					if (streamOutputType.getElement()==null)
					{
						if (outputMessageName==null)
						{
							if (Log.isWarn())
							{
								Log.warn("No output defined for "+streamOperation+". Skipping operation.");
							}				
							continue; //for loop
						}
						Iterator wsdlIt = this.getServiceReference().getWSDLs();
						if (wsdlIt==null)
						{
							if (Log.isWarn())
							{
								Log.warn("No wsdl defined for "+streamOperation+". Skipping operation.");
							}				
							continue; //for loop
						}


						while (wsdlIt.hasNext())
						{
							WSDL wsdl=(WSDL) wsdlIt.next();
							Schema schemaForNamespace= wsdl.getTypes(outputMessageName.getNamespace());
							if (schemaForNamespace!=null)
							{
								Element elementForMessage= schemaForNamespace.getElement(outputMessageName);
								streamOutputType.setElement(elementForMessage);
								if (Log.isDebug())
									Log.debug("Set  elementForMessage "+elementForMessage+" for "+streamOperation);
								break; //while
							}
						}
					}
				}
				//TODO Resolve missing
				// MDPWSWSDLStreamOperation
				// stmOp.setOutput(outputIO); ==> GetOutput
				//ioOutQName = (STMIOType)output.getMessageName  
				// types.getElement(ioOutQName);
				// Element elem=types.getElement(ioOutQName);
				// outputIO.setElement(elem);
				// this.getServiceReference().getWSDLs(); => WSDL portType.getWsdl().getTypes(ioOutQName.getNamespace());

				ProxyStreamSource realOP=new ProxyStreamSource(streamOperation);
				op=realOP;
				port.addStreamSource(new OperationSignature(op), realOP);
				if (this.getServiceReference()!=null)
					realOP.setProxySourceAddress(this.getServiceReference().getPreferredXAddress());

				this.streams.put(realOP.getOutputAction(), realOP);
				if (Log.isDebug()) {
					Log.debug("[NEW STREAM SOURCE]: " + realOP.toString());
				}				
			}else{
				if (operation.isRequest()) {
					Operation realOp = createOperation(operation);
					op = realOp;
					port.addOperation(new OperationSignature(op), realOp);
					getOperationsInternal().put(realOp.getInputAction(), realOp);
					if (Log.isDebug()) {
						Log.debug("[NEW OPERATION]: " + realOp.toString());
					}
				} else if (operation.isEvented()) {
					DefaultEventSource realEvent = new DefaultEventSource(operation);
					op = realEvent;
					port.addEventSource(new OperationSignature(op), realEvent);
					getEventsInternal().put(realEvent.getOutputAction(), realEvent);
					if (Log.isDebug()) {
						Log.debug("[NEW EVENT SOURCE]: " + realEvent.toString());
					}
				} else {
					throw new IllegalArgumentException("Unknown type of WSDL operation: " + operation);
				}
			}
			/*
			 * no need to check names, as they should be correctly sent by
			 * existing service or within given WSDL; if not, than it is not our
			 * fault :D
			 */
			op.setService(this);
		}
		getPortTypesInternal().put(portTypeName, port);
	}



	@Override
	protected Operation createOperation(WSDLOperation wsdlOperation) {
		Operation operation= super.createOperation(wsdlOperation);
		MDPWSProxyOperation proxyOp=new MDPWSProxyOperation(operation);
		operation.setService(this);

		return proxyOp;
	}

	public StreamSourceCommons getStreamSource(String outputAction) {
		return (StreamSourceCommons) streams.get(outputAction);
	}

	public Iterator getStreamSources()
	{
		Set operations = new HashSet();
		for (Iterator it = getPortTypesInternal().values().iterator(); it.hasNext();) {
			PortType type = (PortType) it.next();
			if (type instanceof MDPWSPortType)
			{
				for (Iterator it2 = ((MDPWSPortType)type).getStreamSources(); it2.hasNext();) {
					operations.add(it2.next());
				}
			}
		}
		return new ReadOnlyIterator(operations.iterator());

	}




	@Override
	protected void prepareWSDLOnServiceLevel(WSDL wsdl) {
		super.prepareWSDLOnServiceLevel(wsdl);

		if (Log.isDebug()) Log.debug("Extracting policies on Service level from WSDL");

		wirePoliciesFromWSDL(wsdl.getBindings(), false, ServicePolicySubject.class );
		wirePoliciesFromWSDL(wsdl.getPortTypes(),  true, ServicePolicySubject.class );

		//TODO SSch extracting policies on operation level
		Iterator bindings=wsdl.getBindings();
		while(bindings.hasNext())
		{
			WSDLBinding binding=(WSDLBinding) bindings.next();
			if (binding instanceof MDPWSSOAP12DocumentLiteralHTTPBinding)
			{
				MDPWSSOAP12DocumentLiteralHTTPBinding mdpwsWSDLBinding=(MDPWSSOAP12DocumentLiteralHTTPBinding)binding;
				List childAttachmentPoints= mdpwsWSDLBinding.getChildOperationAttachmentPoints();
				if (childAttachmentPoints!=null)
				{
					wirePoliciesFromWSDL(childAttachmentPoints.iterator(),false, OperationPolicySubject.class);
				}

				childAttachmentPoints= mdpwsWSDLBinding.getChildIOTypeAttachmentPoints();
				if (childAttachmentPoints!=null)
				{
					wirePoliciesFromWSDL(childAttachmentPoints.iterator(),false, MessagePolicySubject.class);
				}
			}
		}

		if (this.getServiceReference() instanceof ServiceReferenceInternal)
		{
			((ServiceReferenceInternal)this.getServiceReference()).addWSDLs(wsdl);
		}
	}

	//new ServicePolicySubject(this, false)

	/**
	 * @param bindings
	 */
	private void wirePoliciesFromWSDL(Iterator possibleAttachmentPoints,  boolean isAbstract, Class<?> subjectClass) {
		while (possibleAttachmentPoints.hasNext())
		{
			Object pAttachmentPt=possibleAttachmentPoints.next();
			if (pAttachmentPt instanceof WSDLPolicyAttachmentPoint)
			{
				WSDLPolicyAttachmentPoint attachmentPoint=(WSDLPolicyAttachmentPoint)pAttachmentPt;
				QoSPolicySubject subject=createSubject(attachmentPoint, isAbstract, subjectClass);
				wirePoliciesFromWSDL(attachmentPoint, subject , wireWith(subject));
			}
		}
	}

	/**
	 * @param subject
	 * @return
	 */
	private Object wireWith(QoSPolicySubject subject) {
		Object retVal=null;
		if (subject instanceof ServicePolicySubject)
		{
			retVal=this;
		}
		return retVal;
	}

	/**
	 * @param pAttachmentPt
	 * @param isAbstract
	 * @param subjectClass
	 * @return
	 */
	private QoSPolicySubject createSubject(WSDLPolicyAttachmentPoint pAttachmentPt,
			boolean isAbstract, Class<?> subjectClass) {

		QoSPolicySubject subject=null;
		if (ServicePolicySubject.class.equals(subjectClass))
		{
			subject= new ServicePolicySubject(this, isAbstract);
		}else if (OperationPolicySubject.class.equals(subjectClass))
		{
			MDPWSWSDLOperation wsdlOP=(MDPWSWSDLOperation)pAttachmentPt;
			subject=new OperationPolicySubject(this.getOperation(wsdlOP.getInputAction()), isAbstract);
		}else if (MessagePolicySubject.class.equals(subjectClass)){
			MDPWSIOType wsdlIOType=(MDPWSIOType)pAttachmentPt;


			OperationDescription operation=this.getOperation(wsdlIOType.getOperation().getInputAction());

			//SSch determine flags (input/inoutput => outgoing message interception)
			boolean isInputSubject=!pAttachmentPt.getPolicyDirection().equals(WSDLPolicyDirection.OUTBOUND);
			boolean isOutputSubject=!pAttachmentPt.getPolicyDirection().equals(WSDLPolicyDirection.INBOUND);
			Log.debug("Operation for MessagePolicySubject "+operation+" "+isInputSubject+" "+isOutputSubject);

			subject=new MessagePolicySubject(isInputSubject, isOutputSubject, false, operation, false);
		}else{
			Log.warn("Subject Class not supported: "+subjectClass);
		}
		Log.debug("Created Subject: "+subject);
		return subject;
	}

	/**
	 */
	private void wirePoliciesFromWSDL(WSDLPolicyAttachmentPoint attachmentPoint,QoSPolicySubject subject, Object wireWith) {
		Iterator policyAttachments= attachmentPoint.getWSDLPolicyAttachments();
		while(policyAttachments.hasNext())
		{
			WSDLPolicyAttachment attachment=(WSDLPolicyAttachment)policyAttachments.next();
			if (attachment instanceof WSDLPolicyEmbeddedAttachment)
			{
				WSDLPolicyEmbeddedAttachment eAttachment= (WSDLPolicyEmbeddedAttachment)attachment;
				WSDLPolicy wsdlPolicy=eAttachment.getPolicy();
				if (wsdlPolicy instanceof SimpleWSDLPolicy)
				{
					Object assertion=((SimpleWSDLPolicy)wsdlPolicy).getAssertion();
					if (assertion instanceof QoSPolicy)
					{
						Log.debug("Wiring Policy from WSDL "+assertion+" "+subject+" "+wireWith+" "+attachmentPoint);
						QoSPolicy policy=(QoSPolicy)assertion;
						QoSPolicyManager.getInstance().registerQoSPolicy(policy);
						policy.addSubject(subject);
						QoSPolicyManager.getInstance().wireQoSPolicies(subject.getClass(), subject.getAssociation());
					}
				}else{
					//TODO SSch handle complex policies
				}
			}else{
				//TODO Ssch handle reference attachments
			}
		}
	}

	protected IParameterValue dispatchInvokeInternal(Operation op,MDPWSMessageContextMap msgContextMap, IParameterValue parameterValue, final MDPWSMessageContextMap replyMessageCtxtMap) throws InvocationException, TimeoutException {
		MDPWSInvokeMessage msg = new MDPWSInvokeMessage(op.getInputAction());
		if (msgContextMap!=null)
		{
			msg.addMessageContextMapContent(msgContextMap);
		}

		msg.getHeader().setMessageEndpoint(op);



		CallbackHandler handler=new InvokeCallbackHandler(op, replyMessageCtxtMap);

		return dispatchInvoke(msg, op, parameterValue, handler);

	}




	@Override
	protected CallbackHandler createCallbackHandler(Operation object) {
		// TODO Auto-generated method stub
		return super.createCallbackHandler(object);
	}

	private class InvokeCallbackHandler extends CallbackHandler{
		private MDPWSMessageContextMap replyMessageCtxtMap;

		/**
		 * @param op
		 * @param replyMessageCtxtMap
		 */
		public InvokeCallbackHandler(Operation op,
				MDPWSMessageContextMap replyMessageCtxtMap) {
			super(op);
			this.replyMessageCtxtMap=replyMessageCtxtMap;
		}

		@Override
		public void handle(Message request, InvokeMessage msg,
				ProtocolData protocolData) {
			if (msg instanceof MDPWSInvokeMessage)
			{
				MDPWSInvokeMessage mdpwsMsg=(MDPWSInvokeMessage)msg;
				MDPWSMessageContextMap tempMap=mdpwsMsg.getMessageContextMap();
				if (tempMap!=null && replyMessageCtxtMap!=null)
					replyMessageCtxtMap.putAll(tempMap);
			}
			super.handle(request, msg, protocolData);
		}
	}

	protected class MDPWSCallbackHandler extends CallbackHandler{

		public MDPWSCallbackHandler() {
			super();
		}

		public MDPWSCallbackHandler(Operation op) {
			super(op);
		}

		@Override
		public void handleMalformedResponseException(Message request,
				Exception exception) {
			if (exception instanceof InterceptionException)
			{
				//TODO SSch
				Log.warn(exception.getMessage());
			}
			synchronized (this) 
			{
				this.notifyAll();
			}
		}



	}

	protected class MDPWSProxyOperation extends MDPWSOperation{

		private Operation op;

		public MDPWSProxyOperation(String name, QName portType) {
			super(name, portType);
		}

		public MDPWSProxyOperation(WSDLOperation operation) {
			super(operation);
		}

		public MDPWSProxyOperation(Operation op)
		{
			super(op.getName(),op.getPortType());
			this.op=op;
		}

		@Override
		public IParameterValue invoke(IParameterValue parameterValue)
				throws InvocationException, TimeoutException {
			return dispatchInvokeInternal(op,null, parameterValue, null);
		}

		@Override
		public IParameterValue invoke(IParameterValue parameterValue,
				MDPWSMessageContextMap msgContextMap, MDPWSMessageContextMap replyMessageCtxtMap)
						throws InvocationException, TimeoutException {
			return dispatchInvokeInternal(op,msgContextMap, parameterValue, replyMessageCtxtMap);
		}

		@Override
		public String toString() {
			return op.toString();
		}

		@Override
		public String getName() {
			return op.getName();
		}

		@Override
		public QName getPortType() {
			return op.getPortType();
		}

		@Override
		public String getInputName() {
			return op.getInputName();
		}

		@Override
		public void setInputName(String inputName) {
			op.setInputName(inputName);
		}

		@Override
		public String getInputAction() {
			return op.getInputAction();
		}

		@Override
		public void setInputAction(String inputAction) {
			op.setInputAction(inputAction);
		}

		@Override
		public String getOutputName() {
			return op.getOutputName();
		}

		@Override
		public void setOutputName(String outputName) {
			op.setOutputName(outputName);
		}

		@Override
		public String getOutputAction() {
			return op.getOutputAction();
		}

		@Override
		public void setOutputAction(String outputAction) {
			op.setOutputAction(outputAction);
		}

		@Override
		public void setInput(Element element) {
			op.setInput(element);
		}

		@Override
		public void setOutput(Element element) {
			op.setOutput(element);
		}

		@Override
		public Element getInput() {
			return op.getInput();
		}

		@Override
		public Element getOutput() {
			return op.getOutput();
		}

		@Override
		public Iterator getFaults() {
			return op.getFaults();
		}

		@Override
		public Fault getFault(String faultName) {
			return op.getFault(faultName);
		}

		@Override
		public void addFault(Fault fault) {
			op.addFault(fault);
		}

		@Override
		public void removeFault(String faultName) {
			op.removeFault(faultName);
		}

		@Override
		public IParameterValue createOutputValue() {
			return op.createOutputValue();
		}

		@Override
		public IParameterValue createInputValue() {
			return op.createInputValue();
		}

		@Override
		public IParameterValue createFaultValue(String faultName) {
			return op.createFaultValue(faultName);
		}

		@Override
		public Service getService() {
			return op.getService();
		}

	}


	public void addObserver(Observer arg0) {
		internalObservable.addObserver(arg0);
	}
	public int countObservers() {
		return internalObservable.countObservers();
	}

	public void deleteObserver(Observer arg0) {
		internalObservable.deleteObserver(arg0);
	}

	public void deleteObservers() {
		internalObservable.deleteObservers();
	}

	public boolean hasChanged() {
		return internalObservable.hasChanged();
	}

	public void notifyObservers() {
		internalObservable.notifyObservers();
	}

	public void notifyObservers(Object arg0) {
		internalObservable.notifyObservers(arg0);
	}

	public void setChanged() {
		internalObservable.setChangedExternal();
	}



}
