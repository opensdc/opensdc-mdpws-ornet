/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package com.draeger.medical.mdpws.domainmodel.wsdl.policy;

import org.ws4d.java.structures.HashMap;
import org.ws4d.java.types.QName;

/**
 *
 *
 */
public class WSDLPolicyAssertionSupportRegistry {
	private static final WSDLPolicyAssertionSupportRegistry instance=new WSDLPolicyAssertionSupportRegistry();

	public static WSDLPolicyAssertionSupportRegistry getInstance() {
		return instance;
	}

	private final HashMap serializerMap=new HashMap();
	private final HashMap parserMap=new HashMap();

	private WSDLPolicyAssertionSupportRegistry()
	{
		//void
	}

	public void addAssertionSerializer(Class<?> assertionClz, WSDLAssertionSerializer serializer)
	{
		synchronized(serializerMap)
		{
			if (assertionClz!=null && serializer!=null)
			{
				serializerMap.put(assertionClz,serializer);
			}
		}
	}

	public WSDLAssertionSerializer getSerializerForAssertion(Object assertion)
	{
		if (assertion==null) throw new IllegalArgumentException("Assertion must nor be null");
		
		WSDLAssertionSerializer serializer=null;
		Class<?> assertionClass=assertion.getClass();
		synchronized (serializerMap) {
			
			serializer=(WSDLAssertionSerializer) serializerMap.get(assertionClass);
			
			if (serializer==null)
			{
				//maybe an interface is registered
				 Class<?>[] interfaces=assertion.getClass().getInterfaces();
				 if (interfaces!=null)
				 {
					 for (Class<?> interfaceClass : interfaces) {
						 serializer=(WSDLAssertionSerializer) serializerMap.get(interfaceClass);
						 if (serializer!=null) break;
					}
				 }
				 
			}
			if (serializer==null)
			{
				//maybe a super class is registered
				 Class<?> superClass=assertion.getClass().getSuperclass();
				 while (superClass!=null)
				 {
						 serializer=(WSDLAssertionSerializer) serializerMap.get(superClass);
						 if (serializer!=null) break;
						 //TODO SSch check initQoSFramework beforehand
						 Class<?> newSuperClass= assertion.getClass().getSuperclass();
						 if (superClass.equals(newSuperClass)) break;	
						 superClass=newSuperClass;
				 }
			}
		}
		return serializer;
	}
	
	public void addAssertionParser(QName elementName, WSDLAssertionParser parser)
	{
		synchronized(parserMap)
		{
			if (elementName!=null && parser!=null)
			{
				parserMap.put(elementName.toStringPlain(),parser);
			}
		}
	}
	
	public WSDLAssertionParser getParserForAssertionElement(QName elementName)
	{
		if (elementName==null) throw new IllegalArgumentException("Assertion element name must nor be null");
		synchronized (parserMap) {
			WSDLAssertionParser parser=(WSDLAssertionParser) parserMap.get(elementName.toStringPlain());
			return parser;
		}
	}
}
