/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel.wsdl.types;

import java.io.IOException;

import org.ws4d.java.schema.Element;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.EmptyStructures;
import org.ws4d.java.types.QName;
import org.ws4d.java.wsdl.WSDLMessagePart;
import org.xmlpull.v1.XmlSerializer;

import com.draeger.medical.mdpws.communication.protocol.constants.WSSTMConstants;

public class STMIOType extends MDPWSIOType {


	private Element element;

	public STMIOType() {
		super();
	}

	public STMIOType(QName message) {
		super(message);
	}

	public STMIOType(String name, QName message) {
		super(name, message);
	}


	public void serialize(XmlSerializer serializer) throws IOException {
		if (getName() != null && isNameSet()) {
			serializer.attribute(null, WSSTMConstants.WSSTM_ATTRIB_NAME, getName());
		}
		String namespace = getMessageName().getNamespace();
		String prefix = serializer.getPrefix(namespace, true);
		String msgQName = prefix == null ? getMessageName().getLocalPart() : prefix + ":" + getMessageName().getLocalPart();
		serializer.attribute(null, WSSTMConstants.WSSTM_ATTRIB_ELEMENT, msgQName);
		String action = getAction();
		serializer.attribute(null, WSSTMConstants.WSSTM_ATTRIB_ACTIONURI, action);
	}

	@Override
	public DataStructure getParts() {
		DataStructure parts=EmptyStructures.EMPTY_STRUCTURE;
		if (this.element!=null)
		{
			parts = new ArrayList();
			WSDLMessagePart part=new WSDLMessagePart(getMessageName().getLocalPart()){

				@Override
				public Element getElement() {
					return element;
				}

				@Override
				public boolean isElement() {
					return true;
				}

			};
			parts.add(part);
		}
		return parts;
	}

	public Element getElement()
	{
		return element;
	}

	public void setElement(Element elem) 
	{
		this.element=elem;
	}



}
