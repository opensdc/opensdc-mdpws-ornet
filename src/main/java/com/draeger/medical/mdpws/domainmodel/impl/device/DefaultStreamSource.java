/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel.impl.device;

import org.ws4d.java.dispatch.OutDispatcher;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.types.AppSequence;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

import com.draeger.medical.mdpws.communication.protocol.constants.WSSTMConstants;
import com.draeger.medical.mdpws.communication.streaming.StreamBinding;
import com.draeger.medical.mdpws.domainmodel.LocalStreamSource;
import com.draeger.medical.mdpws.domainmodel.StreamSourceCommons;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamConfiguration;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamTransmissionConfiguration;
import com.draeger.medical.mdpws.message.streaming.StreamFrameMessage;
import com.draeger.medical.mdpws.message.streaming.msgCtxt.StreamConfigurationMessageContext;
import com.draeger.medical.mdpws.message.streaming.msgCtxt.StreamingMessageContextBuilder;

public class DefaultStreamSource extends StreamSourceCommons implements LocalStreamSource{

	//private final URI actionID;
	private final AppSequenceStreamingHelper appSequenceHelper;
	private final boolean runInThread=Boolean.parseBoolean(System.getProperty("MDPWS.DefaultStreamSource.RunInThread", "false"));
	private final static long INSTANCE_ID=PlatformSupport.getInstance().getToolkit().getColdBootCount(); //new java.util.Date().getTime(); 


	public DefaultStreamSource(String name, QName portType, StreamConfiguration streamConfig) {
		super(name, portType, streamConfig);

		this.appSequenceHelper=new AppSequenceStreamingHelper(this);
		//		this.actionID=new URI(QNameFactory.getInstance().getQName(getName(),getPortType().toString()).toStringPlain());
	}
 
	//	public DefaultStreamSource(MDPWSWSDLStreamOperation operation) {
	//		super(operation);
	//	}

	@Override
	public void fire(final IParameterValue paramValue, final int eventNumber) 
	{
		fire(paramValue,eventNumber, false);
	}


	@Override
	public synchronized void fire(final IParameterValue paramValue, final int eventNumber, final boolean compress) {
		if (runInThread)
		{
			boolean executed=PlatformSupport.getInstance().getToolkit().getThreadPool().executeOrAbort(new FireInternalTask(paramValue, eventNumber));
			if (!executed)
				Log.warn("Stream could not be executed");
		}else{
			fireInternal(paramValue, eventNumber);
		}
	}

	/**
	 * @param paramValue
	 * @param eventNumber
	 */
	protected void fireInternal(final IParameterValue paramValue,
			final int eventNumber) {
		if (getStreamConfiguration()!=null)
		{
			StreamTransmissionConfiguration tConfig=getStreamConfiguration().getTransmissonConfiguration();
			StreamBinding sBinding=getStreamConfiguration().getBinding();

			StreamFrameMessage streamFrameMessage = new StreamFrameMessage(getOutputAction());
			streamFrameMessage.setContent(paramValue);
			StreamConfigurationMessageContext scMsgCtxt=StreamingMessageContextBuilder.getInstance().newStreamConfigurationMessageContext();
			scMsgCtxt.setStreamConfiguration(getStreamConfiguration());
			streamFrameMessage.addMessageContext(StreamConfigurationMessageContext.class, scMsgCtxt);

			//Create the header  
			MessageHeader header = streamFrameMessage.getHeader();
			AppSequence appSeq=this.appSequenceHelper.augmentAppSequenceWithStreamingInfo(new AppSequence(INSTANCE_ID,this.appSequenceHelper.getSequenceID(), eventNumber));
			header.setAppSequence(appSeq);

			if (getService()!=null && getService().getServiceReference()!=null && getService().getServiceReference().getPreferredXAddress()!=null){
				header.setFrom(new AttributedURI(getService().getServiceReference().getPreferredXAddress()));
			}
			
			EndpointReference epr = new EndpointReference(new URI(tConfig.getDestinationAddress()));
			header.setEndpointReference(epr);

			//					header.setReferenceParameters(getReferenceParams());

			// set to preferred xAddress of client / event sink
			streamFrameMessage.setTargetAddress(epr.getAddress());
			streamFrameMessage.setRoutingScheme(sBinding.getRoutingScheme());
			OutDispatcher.getInstance().sendGenericMessage(streamFrameMessage, sBinding.getCommunicationManagerId(), null);
		}else{
			Log.warn("No stream configuration provided for "+getName());
		}
	}

	@Override
	public int getSubscriptionCount() {
		return 1;
	}


	private final class FireInternalTask implements Runnable {
		private final IParameterValue paramValue;
		private final int eventNumber;

		private FireInternalTask(IParameterValue paramValue, int eventNumber) {
			this.paramValue = paramValue;
			this.eventNumber = eventNumber;
		}

		@Override
		public void run() 
		{
			fireInternal(paramValue, eventNumber);
		}
	}

	private final class AppSequenceStreamingHelper {
		private final QName	WSSTM_ATTRIB_DEVICE_EPR_ADDRESS_QNAME=QNameFactory.getInstance().getQName(WSSTMConstants.WSSTM_ATTRIB_DEVICE_EPR_ADDRESS, WSSTMConstants.WSSTM_NAMESPACE_NAME);
		private final QName	WSSTM_ATTRIB_STREAM_ID_QNAME=QNameFactory.getInstance().getQName(WSSTMConstants.WSSTM_ATTRIB_SERVICE_ID, WSSTMConstants.WSSTM_NAMESPACE_NAME);
		private final QName	WSSTM_ATTRIB_STREAM_DESC_TNS_QNAME=QNameFactory.getInstance().getQName(WSSTMConstants.WSSTM_ATTRIB_STREAM_DESC_TNS, WSSTMConstants.WSSTM_NAMESPACE_NAME);
		private final QName	WSSTM_ATTRIB_STREAM_TYPE_ID_QNAME=QNameFactory.getInstance().getQName(WSSTMConstants.WSSTM_ATTRIB_STREAM_TYPE_ID, WSSTMConstants.WSSTM_NAMESPACE_NAME);


		private final DefaultStreamSource streamSource;
		
		private volatile URI streamDescriptionTns;
		private volatile String streamTypeId;
		private volatile URI actionID;
		private volatile Service service;
		private volatile URI deviceEprAddress;
		private volatile URI serviceId;
		private volatile URI sequenceID;




		//		private AppSequenceStreamingHelper(URI deviceEprAddress, URI serviceId, URI streamDescriptionTns, String streamTypeId,URI actionID)
		//		{
		//			this.deviceEprAddress=deviceEprAddress;
		//			this.serviceId=serviceId;
		//			this.streamDescriptionTns=streamDescriptionTns;
		//			this.streamTypeId=streamTypeId;
		//			this.actionID=actionID;
		//		}

		public AppSequenceStreamingHelper(DefaultStreamSource streamSource) 
		{
			URI tempActionID=null;

			this.streamSource=streamSource;
			if (this.streamSource!=null)
			{
				updateReferences(this.streamSource);
				tempActionID=new URI(QNameFactory.getInstance().getQName(this.streamSource.getName(),this.streamSource.getPortType().toString()).toStringPlain());
			}

			this.actionID=tempActionID;
		}



		private synchronized void updateReferences(DefaultStreamSource streamSource) {
			if (streamSource!=null && this.service==null){
				this.service=streamSource.getService();
				
				if (streamDescriptionTns==null && this.streamSource.getPortType()!=null)
					this.streamDescriptionTns=new URI(this.streamSource.getPortType().toStringPlain());
				
				if (streamTypeId==null && this.streamSource.getStreamConfiguration()!=null)
					this.streamTypeId=this.streamSource.getStreamConfiguration().getConfigId();
			}

			if (service!=null)
			{
				if (deviceEprAddress==null)
				{
					DeviceReference devRef= this.service.getParentDeviceReference();
					if (devRef!=null)
						this.deviceEprAddress= devRef.getEndpointReference().getAddress();	
				}

				if (serviceId==null)
				{

					this.serviceId=this.service.getServiceId();	
				}
				
			}

		//System.out.println("updateReferences "+deviceEprAddress+" "+serviceId+" "+this.service );
	}



	protected AppSequence augmentAppSequenceWithStreamingInfo(AppSequence appSeq) {
		if (appSeq!=null)
		{
//			System.out.println("appSeq:"+appSeq);
//			System.out.println(this.streamSource.getStreamConfiguration());
			if (this.deviceEprAddress!=null)
				appSeq.addUnknownAttribute(WSSTM_ATTRIB_DEVICE_EPR_ADDRESS_QNAME, this.deviceEprAddress.toString());
			
			if (this.serviceId!=null)
				appSeq.addUnknownAttribute(WSSTM_ATTRIB_STREAM_ID_QNAME, this.serviceId.toString());
				
			if (this.streamDescriptionTns!=null)
				appSeq.addUnknownAttribute(WSSTM_ATTRIB_STREAM_DESC_TNS_QNAME, this.streamDescriptionTns.toString());
			
			if (this.streamTypeId!=null)
				appSeq.addUnknownAttribute(WSSTM_ATTRIB_STREAM_TYPE_ID_QNAME, this.streamTypeId.toString());
			
			//System.out.println(appSeq.toString());
		}
		return appSeq;
	}

	protected String getSequenceID() 
	{	

		if (sequenceID==null){
			synchronized(this)
			{
				if (sequenceID==null){
					if (this.actionID==null) return null;

					updateReferences(this.streamSource);

					if (deviceEprAddress!=null && serviceId!=null && this.actionID!=null)
					{
						String postfix=Integer.toString((streamDescriptionTns+"/"+streamTypeId).hashCode());
						sequenceID=new URI(actionID.toString().replace("http://", "mdpws://")+"?"+deviceEprAddress.toString()+"#"+serviceId.toString()+"_"+postfix);
					}else if (Log.isError()){
						//							sequenceID=this.actionID;

						Log.error("Invalid MDPWS sequence ID. Use default sequence id:"+sequenceID+" deviceEprAddress:"+deviceEprAddress+" serviceId:"+serviceId+" actionID:"+actionID);
						return actionID.toString();
					}
				}
			}
		}

		return 	sequenceID.toString();
	}
}

}
