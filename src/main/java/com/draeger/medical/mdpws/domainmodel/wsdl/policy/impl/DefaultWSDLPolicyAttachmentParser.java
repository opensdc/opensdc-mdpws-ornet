/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package com.draeger.medical.mdpws.domainmodel.wsdl.policy.impl;

import java.io.IOException;

import org.ws4d.java.constants.WSPConstants;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.util.Log;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionParser;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAssertionSupportRegistry;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachment;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentBuilder;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentParser;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint;
import com.draeger.medical.mdpws.qos.QoSPolicy;

/**
 *
 *
 */
public class DefaultWSDLPolicyAttachmentParser implements
WSDLPolicyAttachmentParser {

	@Override
	public void parsePolicyTag(WSDLPolicyAttachmentPoint attachmentPoint, ElementParser parser) throws IOException {
		WSDLPolicyAttachment attachment=null;
		try{
			int eventType=parser.getEventType();
			if (eventType!=XmlPullParser.START_TAG)
				throw new IOException("Parsing of a policy tag has to start at the start element.");

			int policyDepth=parser.getDepth();

			String name=parser.getName();
			if (WSPConstants.WSP_ELEM_POLICY.equals(name))
			{


				parser.nextTag();
				if (parser.getDepth()<=policyDepth)
				{
					if (Log.isDebug()) Log.debug("Empty policy tag -> no policy attached to attachment point: "+attachmentPoint);
					return;
				}


				attachment = createPolicyAttachmentFromElement(attachmentPoint, new ElementParser(parser));

			}else if (WSPConstants.WSP_ELEM_POLICYREFERENCE.equals(name)){
				attachment = createPolicyAttachmentFromReference(attachmentPoint, parser);
			}

			addPolicyAttachment(attachmentPoint, attachment);

		} catch (XmlPullParserException e) {
			Log.error(e);
			throw new IOException(e.getMessage());
		}
	}

	/**
	 * @param attachmentPoint
	 * @param parser
	 * @return
	 */
	@Override
	public WSDLPolicyAttachment createPolicyAttachmentFromReference(
			WSDLPolicyAttachmentPoint attachmentPoint, ElementParser parser) {
		//TODO SSch parse reference
		return null;
	}

	/**
	 * @param attachmentPoint
	 * @param attachment
	 */
	private void addPolicyAttachment(WSDLPolicyAttachmentPoint attachmentPoint,
			WSDLPolicyAttachment attachment) {
		if (attachment!=null)
		{
			attachmentPoint.addWSDLPolicyAttachment(attachment);
			return;
		}
		if (Log.isWarn()) Log.warn("Could not attach policy to "+attachmentPoint);
	}

	/**
	 * @param attachmentPoint
	 * @param policyContentParser
	 * @param attachment
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	@Override
	public WSDLPolicyAttachment createPolicyAttachmentFromElement(
			WSDLPolicyAttachmentPoint attachmentPoint, ElementParser policyContentParser) throws 
			IOException
			{
		WSDLPolicyAttachment attachment=null;
		try{

			String namespace;
			String name;
			QoSPolicy policy=null;
			//TODO SSch parse attributes of the policy element
			//assertion or structuring element like all
			namespace=policyContentParser.getNamespace();
			name=policyContentParser.getName();

			QName assertionName=QNameFactory.getInstance().getQName(name,namespace);
			WSDLAssertionParser aParser=WSDLPolicyAssertionSupportRegistry.getInstance().getParserForAssertionElement(assertionName);
			if (aParser==null)
			{
				if (Log.isInfo()) Log.info("No parser for assertion:"+assertionName.toStringPlain());
				
				policyContentParser.consume();
				return null;
			}

			policy=aParser.parse(policyContentParser, attachmentPoint);

			if (policy!=null)
			{
				attachment=WSDLPolicyAttachmentBuilder.getInstance().buildEmbeddedAttachment(policy);
			}else{
				if (Log.isInfo()) Log.info("Could not determine policy assertion by parser "+aParser);
			}
			policyContentParser.consume();
		}catch(Exception e){

			IOException ioe= new IOException(e.getMessage());
			ioe.setStackTrace(e.getStackTrace());
			throw ioe;
		}

		return attachment;
			}



}
