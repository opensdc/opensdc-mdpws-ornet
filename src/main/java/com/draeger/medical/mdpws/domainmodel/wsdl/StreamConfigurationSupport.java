/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel.wsdl;

import java.io.IOException;

import org.ws4d.java.io.xml.AbstractElementParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLPortType;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamConfiguration;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamTransmissionConfiguration;

public interface StreamConfigurationSupport 
{
	public abstract StreamConfiguration createStreamConfiguration(String stmTypeID, AbstractElementParser parser, MDPWSWSDLPortType portType) throws XmlPullParserException, IOException;

	public abstract void serializeStreamTransmission(XmlSerializer serializer, StreamTransmissionConfiguration streamTransmissionConfig);
}
