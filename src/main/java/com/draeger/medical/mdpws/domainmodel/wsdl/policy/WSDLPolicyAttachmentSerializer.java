/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package com.draeger.medical.mdpws.domainmodel.wsdl.policy;

import java.io.IOException;

import org.ws4d.java.structures.Iterator;
import org.xmlpull.v1.XmlSerializer;

/**
 *
 *
 */
public interface WSDLPolicyAttachmentSerializer {
	public abstract void serializePolicyAttachments(Iterator policyAttachments, XmlSerializer serializer, String idPrefix) throws IOException;
}
