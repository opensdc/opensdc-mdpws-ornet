/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package com.draeger.medical.mdpws.domainmodel.wsdl.policy.impl;

import java.io.IOException;

import org.ws4d.java.structures.Iterator;
import org.xmlpull.v1.XmlSerializer;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicy;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentSerializer;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyEmbeddedAttachment;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyReferenceAttachment;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicySerializerFactory;

/**
 *
 *
 */
public class DefaultWSDLPolicyAttachmentSerializer implements
		WSDLPolicyAttachmentSerializer {

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentSerializer#serializePolicyAttachments(org.ws4d.java.structures.Iterator, org.xmlpull.v1.XmlSerializer)
	 */
	@Override
	public void serializePolicyAttachments(Iterator policyAttachments,
			XmlSerializer serializer, String idPrefix) throws IOException {
		int policyCnt=0;
		while(policyAttachments.hasNext())
		{
			Object policyAttachmentO=policyAttachments.next();
			if (policyAttachmentO instanceof WSDLPolicyReferenceAttachment)
			{
				//TODO SSch implement WSDLPolicyReferenceAttachment serialization
			}else if (policyAttachmentO instanceof WSDLPolicyEmbeddedAttachment){
				WSDLPolicyEmbeddedAttachment embeddedPolicy=(WSDLPolicyEmbeddedAttachment)policyAttachmentO;
				WSDLPolicy wspPolicy=embeddedPolicy.getPolicy();
				wspPolicy.setId(idPrefix+policyCnt);
				policyCnt++;
				WSDLPolicySerializerFactory.getInstance().getSerializer().serialize(wspPolicy, serializer);
			}
		}

	}

}
