/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package com.draeger.medical.mdpws.domainmodel.wsdl.policy.impl;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicy;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentAbstractionState;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyEmbeddedAttachment;

/**
 *
 *
 */
public class DefaultWSDLPolicyEmbeddedAttachment implements
		WSDLPolicyEmbeddedAttachment {
	private WSDLPolicy policy=null;
	private WSDLPolicyAttachmentAbstractionState abstractionState=WSDLPolicyAttachmentAbstractionState.UNKNOWN;
	
	
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyEmbeddedAttachment#getPolicy()
	 */
	@Override
	public WSDLPolicy getPolicy() {
		return this.policy;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyEmbeddedAttachment#setPolicy(com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicy)
	 */
	@Override
	public void setPolicy(WSDLPolicy policy) {
		this.policy=policy;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyEmbeddedAttachment#setAbstract(boolean)
	 */
	@Override
	public void setAbstractState(WSDLPolicyAttachmentAbstractionState abstractionState) {
		this.abstractionState=abstractionState;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyEmbeddedAttachment#isAbstract()
	 */
	@Override
	public WSDLPolicyAttachmentAbstractionState getAbstractionState() {
		return abstractionState;
	}


}
