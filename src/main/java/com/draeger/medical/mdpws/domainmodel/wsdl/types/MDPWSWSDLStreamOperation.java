/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel.wsdl.types;

import java.io.IOException;

import org.ws4d.java.schema.Schema;
import org.ws4d.java.util.Log;
import org.ws4d.java.wsdl.IOType;
import org.xmlpull.v1.XmlSerializer;

import com.draeger.medical.mdpws.communication.protocol.constants.WSSTMConstants;
import com.draeger.medical.mdpws.domainmodel.wsdl.StreamConfigurationSupport;
import com.draeger.medical.mdpws.domainmodel.wsdl.StreamConfigurationSupportRegistry;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamConfiguration;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamTransmissionConfiguration;

public class MDPWSWSDLStreamOperation extends MDPWSWSDLOperation {

	private final StreamConfiguration streamConfig;
	private final StreamTransmissionConfiguration streamTransmissionConfig;

	public MDPWSWSDLStreamOperation(String name, StreamConfiguration streamConfig) {
		super(name);
		this.streamConfig=streamConfig;
		if (streamConfig!=null)
			this.streamTransmissionConfig=streamConfig.getTransmissonConfiguration();
		else
			this.streamTransmissionConfig=null;
	}




	public void serialize(XmlSerializer serializer) throws IOException {
		serialize(serializer, false);
	}

	public void serialize(XmlSerializer serializer, boolean serializeAddInfo) throws IOException{

		//Start the WSSTM OperationDescription Extension
		serializer.startTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMTYPE);

		//String namespace = getNamespace();
		//String prefix = serializer.getPrefix(namespace, true);
		//String id= (prefix == null) ? getName() : prefix + ":" + getName();
		serializer.attribute(null, WSSTMConstants.WSSTM_ATTRIB_ID, getName());

		//serializer.attribute(null, WSDLConstants.WSDL_ATTRIB_NAME, getName());
		if (streamTransmissionConfig!=null)
			serializer.attribute(null, WSSTMConstants.WSSTM_ATTRIB_STREAMTYPE, streamTransmissionConfig.getStreamTransmissionType());

		// In-Output adden
		switch (getType()) {
		case (TYPE_ONE_WAY): 
		case (TYPE_REQUEST_RESPONSE):
		case (TYPE_UNKNOWN): 
		case (TYPE_SOLICIT_RESPONSE):
			Log.error(getName()+"Illegal Type of Stream Operation. "+getTypeAsString());
		break;
		case (TYPE_NOTIFICATION): {
			serializeOutput(serializer);
			break;
		}
		default:
			break;
		}



		//Serialize Adding the additional stream info
		if (serializeAddInfo && streamTransmissionConfig !=null)
		{
			StreamConfigurationSupport support= StreamConfigurationSupportRegistry.getInstance().getSupportHandler(streamTransmissionConfig.getStreamTransmissionType());
			if (support!=null)
			{
				support.serializeStreamTransmission(serializer,streamTransmissionConfig); 
			}
			//			serializeStreamAddress(serializer, (SOAPoverUDPStreamTransmissionConfiguration)streamTransmissionConfig);
			//			serializeStreamPeriod(serializer,(SOAPoverUDPStreamTransmissionConfiguration)streamTransmissionConfig);
			//			serializeHistoryEPR(serializer, (SOAPoverUDPStreamTransmissionConfiguration)streamTransmissionConfig);
		}
		//Closing the WS-Streaming extension tag 
		serializer.endTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMTYPE);
	}



	protected void serializeInput(XmlSerializer serializer) throws IOException {
		//void
		return;
	}


	protected void serializeOutput(XmlSerializer serializer) throws IOException {
		IOType output=getOutput();
		if (output instanceof STMIOType)
		{
			((STMIOType)output).serialize(serializer);
		}
	}



	public StreamConfiguration getStreamConfiguration() {
		return this.streamConfig;
	}



}
