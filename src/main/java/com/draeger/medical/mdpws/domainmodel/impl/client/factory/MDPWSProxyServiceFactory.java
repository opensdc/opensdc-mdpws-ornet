/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel.impl.client.factory;

import org.ws4d.java.service.ProxyService;
import org.ws4d.java.service.ProxyServiceFactory;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.util.Log;

import com.draeger.medical.mdpws.domainmodel.impl.client.MDPWSProxyService;

public class MDPWSProxyServiceFactory extends ProxyServiceFactory {

	@Override
	public ProxyService newProxyService(ServiceReference serviceReference,
			DeviceReference parentDeviceReference) {
		MDPWSProxyService ps= new MDPWSProxyService(serviceReference, parentDeviceReference);
		try {
			ps.initialize(serviceReference, parentDeviceReference);
		} catch (InstantiationException e) {
			Log.error(e);
		}
		return ps;
	}

}
