/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel.impl.client;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.URI;

import com.draeger.medical.mdpws.domainmodel.StreamSourceCommons;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLStreamOperation;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamConfiguration;

public class ProxyStreamSource extends StreamSourceCommons {

	private URI proxySourceAddress;

	public ProxyStreamSource(String name, QName portType, StreamConfiguration streamConfig) {
		super(name, portType, streamConfig);
	}

	public ProxyStreamSource(MDPWSWSDLStreamOperation operation) {
		super(operation);
	}

	/**
	 * @param preferredXAddress
	 */
	public void setProxySourceAddress(final URI proxySourceAddress) 
	{
		this.proxySourceAddress=proxySourceAddress;
	}

	public URI getProxySourceAddress() {
		return proxySourceAddress;
	}
	
}
