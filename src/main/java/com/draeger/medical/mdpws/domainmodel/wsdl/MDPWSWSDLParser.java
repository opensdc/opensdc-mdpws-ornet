/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel.wsdl;

import java.io.IOException;
import java.io.InputStream;

import org.ws4d.java.constants.SchemaConstants;
import org.ws4d.java.constants.WSDLConstants;
import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.constants.WSPConstants;
import org.ws4d.java.io.xml.AbstractElementParser;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.Schema;
import org.ws4d.java.schema.SchemaException;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.StringUtil;
import org.ws4d.java.wsdl.DefaultWSDLParser;
import org.ws4d.java.wsdl.WSDL;
import org.ws4d.java.wsdl.WSDLBinding;
import org.ws4d.java.wsdl.WSDLOperation;
import org.ws4d.java.wsdl.WSDLPortType;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.draeger.medical.mdpws.common.util.XMLParserUtil;
import com.draeger.medical.mdpws.communication.protocol.constants.WSSTMConstants;
import com.draeger.medical.mdpws.communication.protocol.soap.wsdl.MDPWSSOAP12DocumentLiteralHTTPBinding;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachment;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentSupportFactory;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLOperation;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLPortType;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLStreamOperation;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.STMIOType;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamConfiguration;

public class MDPWSWSDLParser extends DefaultWSDLParser
{

	@Override
	public WSDL parse(InputStream in, URI fromUri, String targetNamespace)
	throws XmlPullParserException, IOException {
		WSDL wsdl= super.parse(in, fromUri, targetNamespace);
		return wsdl;
	}

	@Override
	public WSDL parse(XmlPullParser parser, URI fromUri, String targetNamespace)
	throws XmlPullParserException, IOException {
		WSDL wsdl= super.parse(parser, fromUri, targetNamespace);
		return wsdl;
	}



	@Override
	protected void handleDefinitions(WSDL wsdl, ElementParser parser,
			URI fromUri, String targetNamespace) throws XmlPullParserException,
			IOException {
		super.handleDefinitions(wsdl, parser, fromUri, targetNamespace);
		//SSch Everything should be parsed now, try to resolve schema reference from stream sources
		if (wsdl!=null && wsdl.getTypes().hasNext())
		{
			Iterator ptIter=wsdl.getPortTypes();
			while (ptIter.hasNext())
			{
				Object o=ptIter.next();
				if (!(o instanceof MDPWSWSDLPortType)){continue;}

				MDPWSWSDLPortType pt=(MDPWSWSDLPortType)o;
				DataStructure operations = pt.getOperations();
				for (Iterator opIt = operations.iterator(); opIt.hasNext();) 
				{
					Object op = opIt.next();
					if (op instanceof MDPWSWSDLStreamOperation)
					{
						MDPWSWSDLStreamOperation streamOp=(MDPWSWSDLStreamOperation)op;
						if (streamOp.getOutput() instanceof STMIOType)
						{
							STMIOType outputIO=(STMIOType)streamOp.getOutput();
							if (outputIO!=null && outputIO.getMessageName()!=null)
							{
								QName ioOutQName=outputIO.getMessageName();
								Schema types=wsdl.getTypes(ioOutQName.getNamespace());
								if (types!=null)
								{
									Element elem=types.getElement(ioOutQName);
									if (elem!=null)
										outputIO.setElement(elem);
									else
										Log.warn("Could not find element "+ioOutQName+" in schema.");
								}else{
									Log.warn("Could not find schema for "+ioOutQName);
								}
							}
						}
					}
				}
			}
		}
	}

	@Override
	public WSDLPortType parsePortType(ElementParser parser,
			String targetNamespace) throws XmlPullParserException, IOException {
		MDPWSWSDLPortType portType = new MDPWSWSDLPortType();
		int attributeCount = parser.getAttributeCount();
		for (int i = 0; i < attributeCount; i++) {
			String attributeNamespace = parser.getAttributeNamespace(i);
			String attributeName = parser.getAttributeName(i);
			if ("".equals(attributeNamespace)) {
				attributeNamespace = parser.getNamespace();
			}
			String attributeValue = parser.getAttributeValue(i);
			if (WSDLConstants.WSDL_NAMESPACE_NAME.equals(attributeNamespace)) {
				if (WSDLConstants.WSDL_ATTRIB_NAME.equals(attributeName)) {
					portType.setName(QNameFactory.getInstance().getQName(attributeValue, targetNamespace));
				} else {
					portType.setAttribute(QNameFactory.getInstance().getQName(attributeName, attributeNamespace), attributeValue);
				}
			} else if (WSEConstants.WSE_NAMESPACE_NAME.equals(attributeNamespace)) {
				if (WSEConstants.WSE_ATTR_EVENTSOURCE.equals(attributeName)) {
					portType.setEventSource(StringUtil.equalsIgnoreCase("true", attributeValue));
				} else {
					portType.setAttribute(QNameFactory.getInstance().getQName(attributeName, attributeNamespace), attributeValue);
				}
			} else {
				portType.setAttribute(QNameFactory.getInstance().getQName(attributeName, attributeNamespace), attributeValue);
			}
		}
		while (parser.nextTag() != XmlPullParser.END_TAG) {
			String namespace = parser.getNamespace();
			String name = parser.getName();
			if (WSDLConstants.WSDL_NAMESPACE_NAME.equals(namespace)) {
				if (WSDLConstants.WSDL_ELEM_OPERATION.equals(name)) {
					portType.addOperation(parsePorttypeOperation(parser));
				} else if (WSDLConstants.WSDL_ELEM_DOCUMENTATION.equals(name)) {
					// eat everything below
					new ElementParser(parser).consume();
				}
			}else if (WSPConstants.WSP_NAMESPACE_NAME.equals(namespace) || WSPConstants.WSP_NAMESPACE_NAME_DPWS11.equals(namespace))
			{
				if (WSPConstants.WSP_ELEM_POLICY.equals(name))
				{
					handlePolicyTags(parser, portType, parser.getFromUri());
				}//TODO SSch Add Handling for PolicyREF
			}
		}
		return portType;
	}



	@Override
	public WSDLOperation parsePorttypeOperation(ElementParser parser)
	throws XmlPullParserException, IOException {
		// TODO SSch implement Assertion parsing
		return super.parsePorttypeOperation(parser);
	}


	@Override
	protected void handleUnknownOperationExtension(WSDLBinding binding,
			ElementParser parser, String operationName)
	throws XmlPullParserException, IOException {

		ElementParser unknownExtensionParser=new ElementParser(parser);
		if (binding instanceof MDPWSSOAP12DocumentLiteralHTTPBinding)
		{
			String namespace=unknownExtensionParser.getNamespace();
			String name=unknownExtensionParser.getName();
			MDPWSSOAP12DocumentLiteralHTTPBinding mdpwsBinding=(MDPWSSOAP12DocumentLiteralHTTPBinding)binding;
			if (WSPConstants.WSP_NAMESPACE_NAME.equals(namespace) || WSPConstants.WSP_NAMESPACE_NAME_DPWS11.equals(namespace))
			{
				if (WSPConstants.WSP_ELEM_POLICY.equals(name))
				{
					Log.info("Found per-binding extensibility element from policy ns: "+name+" "+namespace);
					MDPWSWSDLOperation attachmentPoint=new MDPWSWSDLOperation(operationName);
					attachmentPoint.setPortType(binding.getPortType());
					DataStructure operations= binding.getPortType().getOperations();
					for (Iterator opIt=operations.iterator();opIt.hasNext();)
					{
						WSDLOperation tWSDLOp=(WSDLOperation)opIt.next();
						if (tWSDLOp.getName().equals(attachmentPoint.getName()))
						{
							attachmentPoint.setInput(tWSDLOp.getInput());
							attachmentPoint.setOutput(tWSDLOp.getOutput());
						}
					}
					WSDLPolicyAttachmentSupportFactory.getInstance().getParser().parsePolicyTag(attachmentPoint, unknownExtensionParser);
					mdpwsBinding.addChildOperationAttachmentPoint(attachmentPoint);
				}
			}
			unknownExtensionParser.consume();
		}else{
			super.handleUnknownOperationExtension(binding, unknownExtensionParser, operationName);
		}
	}

	@Override
	protected void handlePolicyTags(ElementParser parser, Object parent,URI fromURI)
	throws XmlPullParserException, IOException {

		parser.nextTag(); //Jump to the assertion
		int pElemetDepth= parser.getDepth();

		if (parent instanceof MDPWSWSDLPortType)
		{
			MDPWSWSDLPortType portType=(MDPWSWSDLPortType)parent;
			while(parser.getEventType()==XmlPullParser.START_TAG){
				if (XMLParserUtil.checkElementStart(parser, WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMSOURCE))
				{
					if (Log.isDebug())
						Log.debug("Handling Stream Source Assertion from WSDL");

					handleStreamSourceAssertion(parser, portType, fromURI);
				}else{
					handleUnknownPolicyElement(parser, portType);
				}
			}		
			XMLParserUtil.skipRemainingOnLevel(parser, pElemetDepth);
			return;
		}
		super.handlePolicyTags(parser, parent,fromURI);
	}

	protected void handleUnknownPolicyElement(ElementParser parser,
			Object parent) throws XmlPullParserException, IOException {
		//Is it another policy assertion that we do not know???
		//		XMLParserUtil.ignoreEmbeddedTags(parser);
		//		parser.next();
		ElementParser policyParser=new ElementParser(parser);

		if (parent instanceof WSDLPolicyAttachmentPoint)
		{
			WSDLPolicyAttachment attachment= WSDLPolicyAttachmentSupportFactory.getInstance().getParser().createPolicyAttachmentFromElement((WSDLPolicyAttachmentPoint)parent, policyParser);
			((WSDLPolicyAttachmentPoint)parent).addWSDLPolicyAttachment(attachment);
		}

		policyParser.consume();		
	}



	/*
	 * Start Stream Source Assertion Handling
	 */
	public void handleStreamSourceAssertion(AbstractElementParser parser, MDPWSWSDLPortType portType, URI fromURI) throws XmlPullParserException, IOException {
		int ssourceDepth=parser.getDepth();
		parser.next();
		if (XMLParserUtil.checkElementStart(parser, WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMDESCRIPTIONS))
		{
			handleStreamDescriptions(parser, portType, fromURI);
		}else{
			Log.warn("Could not parse StreamDescriptions");
		}

		XMLParserUtil.skipRemainingOnLevel(parser, ssourceDepth);			
	}

	public void handleStreamDescriptions(AbstractElementParser parser,
			MDPWSWSDLPortType portType, URI fromURI) throws XmlPullParserException, IOException {
		int sdDepth=parser.getDepth();

		String stmDescTNS=XMLParserUtil.getAttributeValue(parser, WSSTMConstants.WSSTM_ATTRIB_TARGETNAMESPACE, WSSTMConstants.WSSTM_NAMESPACE_NAME);
		QName portTypeName=QNameFactory.getInstance().getQName(stmDescTNS);
		if (portType.getName()==null)
			portType.setName(portTypeName);
		else if (!portType.getName().equals(portTypeName))
		{
			Log.error("Porttype QName ["+portType.getName()+"] != StreamDescriptions TNS ["+portTypeName+"]");
		}

		parser.next(); //next should be a <Types> or <STREAMType>

		Schema types=null;
		if (XMLParserUtil.checkElementStart(parser,WSSTMConstants.WSSTM_NAMESPACE_NAME,WSSTMConstants.WSSTM_ELEM_TYPES))
		{
			types=handleWSSTMTypesElement(parser, portType, stmDescTNS, fromURI);
		}
		//Check if we find the streamtype elements
		int cnt=-1;
		while (XMLParserUtil.checkElementStart(parser, WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMTYPE))
		{
			cnt++;
			int streamTypeDepth=parser.getDepth();
			String stmTypeID=XMLParserUtil.getAttributeValue(parser,WSSTMConstants.WSSTM_ATTRIB_ID, WSSTMConstants.WSSTM_NAMESPACE_NAME );
			String stmTypeMessageType=XMLParserUtil.getAttributeValue(parser,WSSTMConstants.WSSTM_ATTRIB_STREAMTYPE, WSSTMConstants.WSSTM_NAMESPACE_NAME );
			String stmTypeName=XMLParserUtil.getAttributeValue(parser,WSSTMConstants.WSSTM_ATTRIB_NAME, WSSTMConstants.WSSTM_NAMESPACE_NAME );
			String stmTypeElement=XMLParserUtil.getAttributeValue(parser,WSSTMConstants.WSSTM_ATTRIB_ELEMENT, WSSTMConstants.WSSTM_NAMESPACE_NAME );
			String stmTypeActionURI=XMLParserUtil.getAttributeValue(parser,WSSTMConstants.WSSTM_ATTRIB_ACTIONURI, WSSTMConstants.WSSTM_NAMESPACE_NAME );

			if (Log.isDebug())
			{
				Log.debug(cnt+": Stream: id:"+stmTypeID+" mtype:"+stmTypeMessageType+" name:"+stmTypeName+" Element:"+stmTypeElement+" uri:"+stmTypeActionURI);
			}

			StreamConfiguration streamConfig=null;
			StreamConfigurationSupport support= StreamConfigurationSupportRegistry.getInstance().getSupportHandler(stmTypeMessageType); //TODO SSch replace string with something that has been parsed beforehand
			if (support!=null){
				streamConfig=support.createStreamConfiguration(stmTypeID,parser, portType);
			}

			if (stmTypeActionURI==null || stmTypeActionURI.trim().length()==0)
			{
				//build the default
				stmTypeActionURI=stmDescTNS+"/"+stmTypeID;
			}

			//Create DPWS WSDL Object
			QName ioOutQName=null;
			if (stmTypeElement!=null && stmTypeElement.contains(":"))
			{
				String p = SchemaUtil.getPrefix(stmTypeElement);
				String n = SchemaUtil.getName(stmTypeElement);
				String ns = parser.getNamespace(p);
				ioOutQName=QNameFactory.getInstance().getQName(n, ns);
			}else if (stmTypeElement!=null){
				ioOutQName=QNameFactory.getInstance().getQName(stmTypeElement,stmDescTNS);
			}else if (stmTypeID!=null){
				ioOutQName=QNameFactory.getInstance().getQName(stmTypeID,stmDescTNS);
			}

			if (ioOutQName!=null)
			{
				STMIOType outputIO = new STMIOType(ioOutQName);
				outputIO.setAction(stmTypeActionURI);

				MDPWSWSDLStreamOperation stmOp=new MDPWSWSDLStreamOperation(ioOutQName.getLocalPart(),streamConfig);
				stmOp.setOutput(outputIO);
				if (types!=null)
				{
					assignSchemaElement(types, stmTypeActionURI, ioOutQName, outputIO);
				}else if (portType.getWsdl()!=null)
				{

					if (Log.isDebug())
						Log.debug("Types was null for Stream "+stmTypeActionURI+" in metadata. Try to recover from wsdl using ns="+ioOutQName.getNamespace()+".");
					
					
					types= portType.getWsdl().getTypes(ioOutQName.getNamespace());
					
					if (types!=null){
						//we found a schema
						assignSchemaElement(types, stmTypeActionURI, ioOutQName, outputIO);
					}else{
						if (Log.isWarn())
							Log.warn("Types was null for Stream "+stmTypeActionURI+" in metadata. Could not recover from wsdl using ns="+ioOutQName.getNamespace()+".");
						
					}
					
				}
				portType.addOperation(stmOp);
			}else{
				if (Log.isWarn())
					Log.warn("Could not create ioOutQName!"+cnt+": Stream: id:"+stmTypeID+" mtype:"+stmTypeMessageType+" name:"+stmTypeName+" Element:"+stmTypeElement+" uri:"+stmTypeActionURI);
			}
			//Skip the rest
			XMLParserUtil.skipRemainingOnLevel(parser, streamTypeDepth);
		}
		XMLParserUtil.skipRemainingOnLevel(parser, sdDepth);

	}

	private void assignSchemaElement(Schema types, String stmTypeActionURI,
			QName ioOutQName, STMIOType outputIO) {
		Element elem=types.getElement(ioOutQName);
		
		if (elem!=null)
		{
			outputIO.setElement(elem);
			if (Log.isDebug())
				Log.debug("Setting element "+elem+" for Stream "+stmTypeActionURI);

		}else{
			if (Log.isWarn())
				Log.warn("Could not set element ("+ioOutQName+") for Stream, because it could not be recovered from schema."+stmTypeActionURI);
			
		}
	}


	protected Schema handleWSSTMTypesElement(AbstractElementParser parser,
			MDPWSWSDLPortType portType, String stmDescTNS, URI fromURI) throws XmlPullParserException, IOException {
		int tDepth=parser.getDepth();

		parser.next();
		//Ok, we find a types elements
		Schema schema=null;
		if (XMLParserUtil.checkElementStart(parser,Schema.XMLSCHEMA_NAMESPACE,Schema.SCHEMA_SCHEMA)) 
		{
			schema=handleSchema(parser, portType, stmDescTNS, fromURI);
		}
		XMLParserUtil.skipRemainingOnLevel(parser, tDepth);
		
		return schema;
	}

	protected  Schema handleSchema(AbstractElementParser parser, MDPWSWSDLPortType portType, String stmDescTNS, URI fromURI) throws XmlPullParserException, IOException {
		int schemaDepth=parser.getDepth();

		try {
			String schemaTNS = parser.getAttributeValue(null, SchemaConstants.SCHEMA_TARGETNAMESPACE);
			if (schemaTNS == null) {
				/*
				 * no explicit namespace set? use the targetNamespace from the
				 * streamDescriptions element?
				 */
				schemaTNS = stmDescTNS;
			}
			Schema schema = Schema.parse(parser,fromURI , schemaTNS);
			//TODO Do something with the schema
			XMLParserUtil.skipRemainingOnLevel(parser, schemaDepth);
			return schema;
		} catch (SchemaException e) {
			Log.error(e);
			throw new XmlPullParserException("Unable to parse schema import", parser, e);
		}
	}
	/*
	 * End Stream Source Assertion Handling
	 */

}
