/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel;

import org.ws4d.java.service.parameter.IParameterValue;

public interface LocalStreamSource {
	public abstract void fire(final IParameterValue paramValue, final int eventNumber);
	public abstract void fire(final IParameterValue paramValue, final int eventNumber, final boolean compress);
	public abstract int getSubscriptionCount();
}
