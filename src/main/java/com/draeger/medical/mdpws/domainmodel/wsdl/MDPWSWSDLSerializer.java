/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel.wsdl;

import java.io.IOException;
import java.io.OutputStream;

import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.constants.WSDLConstants;
import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.constants.WSPConstants;
import org.ws4d.java.schema.Schema;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.EmptyStructures;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.HashMap.Entry;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.LinkedList;
import org.ws4d.java.structures.List;
import org.ws4d.java.structures.ListIterator;
import org.ws4d.java.util.Log;
import org.ws4d.java.wsdl.DefaultWSDLSerializer;
import org.ws4d.java.wsdl.IOType;
import org.ws4d.java.wsdl.WSDL;
import org.ws4d.java.wsdl.WSDLOperation;
import org.ws4d.java.wsdl.WSDLPortType;
import org.xmlpull.v1.XmlSerializer;

import com.draeger.medical.mdpws.communication.protocol.constants.WSSTMConstants;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachment;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentAbstractionState;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentSupportFactory;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLPortType;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLPortType.OperationLists;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLStreamOperation;

public class MDPWSWSDLSerializer extends DefaultWSDLSerializer 
{

	@Override
	public void serialize(WSDL wsdl, OutputStream out) throws IOException {

		//START Adding MDPWS default namespaces
		if (wsdl!=null && wsdl.getPortTypes()!= EmptyStructures.EMPTY_STRUCTURE)
		{
			wsdl.getDefaultNamespaces(); //initialize Default namespaces
			for (Iterator it = wsdl.getPortTypes(); it.hasNext();) {
				WSDLPortType pt= (WSDLPortType) it.next();
				if (pt instanceof MDPWSWSDLPortType)
				{
					wsdl.storeDefaultNamespaces(WSPConstants.WSP_NAMESPACE_PREFIX, WSPConstants.WSP_NAMESPACE_NAME);
					wsdl.storeDefaultNamespaces(WSSTMConstants.WSSTM_NAMESPACE_PREFIX, WSSTMConstants.WSSTM_NAMESPACE_NAME);						
				}
			}
		}
		//END Adding MDPWS default namespaces
		super.serialize(wsdl, out);
	}



	@Override
	public void serialize(WSDLPortType portType, XmlSerializer serializer, LinkedList serializedOperations)
			throws IOException {
		if (!(portType instanceof MDPWSWSDLPortType))
		{
			super.serialize(portType, serializer, serializedOperations);	
			return;
		}

		MDPWSWSDLPortType pt=(MDPWSWSDLPortType)portType;

		// Start-Tag wsdl:portTypes
		serializer.startTag(WSDLConstants.WSDL_NAMESPACE_NAME, WSDLConstants.WSDL_ELEM_PORTTYPE);
		serializer.attribute(null, WSDLConstants.WSDL_ATTRIB_NAME, pt.getLocalName());
		if (pt.isEventSource()) {
			serializer.attribute(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ATTR_EVENTSOURCE, "true");
		}
		pt.serializeAttributes(serializer);

		OperationLists opLists=pt.getOperationLists();

		serializePortTypeExtensionInternal(pt, serializer, opLists);	//Added by SSch: Allow extension

		if (pt.getOperations() != EmptyStructures.EMPTY_STRUCTURE) 
		{
			addWSDLOperations(pt, serializer, opLists, serializedOperations);		
		}
		// End-Tag wsdl:portTypes
		serializer.endTag(WSDLConstants.WSDL_NAMESPACE_NAME, WSDLConstants.WSDL_ELEM_PORTTYPE);

	}



	@Override
	protected void serializePortTypeExtension(WSDLPortType portType,
			XmlSerializer serializer) throws IOException {
		serializePortTypeExtensionInternal(portType, serializer, null);
	}



	protected void serializePortTypeExtensionInternal(WSDLPortType portType,
			XmlSerializer serializer, OperationLists opLists) throws IOException
			{
		//1. serialize all extensions from the super class
		super.serializePortTypeExtension(portType, serializer);


		//2. serialize generic policies
		Iterator policyAttachments=getWSDLPolicyAttachmentsForAnyAbstractAttachmentPoint(portType);
		if (policyAttachments!=null && policyAttachments.hasNext())
		{
			WSDLPolicyAttachmentSupportFactory.getInstance().getSerializer().serializePolicyAttachments(policyAttachments, serializer, "PortTypePolicy");
		}


		//3. Serialize Streaming Extension Policy
		if (portType instanceof MDPWSWSDLPortType && (!opLists.stmsList.isEmpty()))
		{
			MDPWSWSDLPortType pt=(MDPWSWSDLPortType) portType;
			if (Log.isDebug())
				Log.debug("StreamList Cnt:"+opLists.stmsList.size());

			serializer.startTag(WSPConstants.WSP_NAMESPACE_NAME, WSPConstants.WSP_ELEM_POLICY);

			serializeStreamSources(pt, opLists.stmsList, serializer, true, null,null);
			serializer.endTag(WSPConstants.WSP_NAMESPACE_NAME, WSPConstants.WSP_ELEM_POLICY);
		}

			}




	@Override
	protected void serializePortTypeOperationExtension(WSDLOperation operation,
			XmlSerializer serializer) throws IOException{
		//1. serialize all extensions from the super class
		super.serializePortTypeOperationExtension(operation, serializer);

		//2. serialize generic policies
		Iterator policyAttachments=getWSDLPolicyAttachmentsForAnyAbstractAttachmentPoint(operation);
		if (policyAttachments!=null && policyAttachments.hasNext())
		{
			WSDLPolicyAttachmentSupportFactory.getInstance().getSerializer().serializePolicyAttachments(policyAttachments, serializer, "PortTypeOperationPolicy");
		}
	}



	@Override
	protected void serializePortTypeIOExtension(IOType io, XmlSerializer serializer,
			String tagName) throws IOException{
		//1. serialize all extensions from the super class
		super.serializePortTypeIOExtension(io, serializer, tagName);

		//2. serialize generic policies
		Iterator policyAttachments=getWSDLPolicyAttachmentsForAnyAbstractAttachmentPoint(io);
		if (policyAttachments!=null && policyAttachments.hasNext())
		{
			WSDLPolicyAttachmentSupportFactory.getInstance().getSerializer().serializePolicyAttachments(policyAttachments, serializer, "PortTypeIOPolicy");
		}
	}



	/**
	 * @param portType
	 * @return
	 */
	private Iterator getWSDLPolicyAttachmentsForAnyAbstractAttachmentPoint(Object possibleAttachmentPoint) {
		Iterator retVal=null;
		Iterator unfilteredRetVal=null;
		if (possibleAttachmentPoint instanceof WSDLPolicyAttachmentPoint)
		{
			WSDLPolicyAttachmentPoint attachmentPoint= (WSDLPolicyAttachmentPoint)possibleAttachmentPoint;
			unfilteredRetVal= attachmentPoint.getWSDLPolicyAttachments();
		}

		if (unfilteredRetVal!=null)
		{
			ArrayList filteredPolicyAttachments=new ArrayList();
			while(unfilteredRetVal.hasNext())
			{
				WSDLPolicyAttachment attachment=(WSDLPolicyAttachment)unfilteredRetVal.next();
				if (attachment.getAbstractionState().equals(WSDLPolicyAttachmentAbstractionState.ABSTRACT))
					filteredPolicyAttachments.add(attachment);
			}
			retVal=filteredPolicyAttachments.iterator();
		}
		return retVal;
	}



	protected void addWSDLOperations(MDPWSWSDLPortType portType,  XmlSerializer serializer, OperationLists opLists, LinkedList serializedOperations) throws IOException {

		//stms are not serialized here hence they should not be included in the list
		if (!opLists.opMDPWSList.isEmpty())
		{
			ListIterator listIt=opLists.opMDPWSList.listIterator();
			//TODO SSch Optional QoS-Policy FW impl
			serializeWSDLOperations(listIt, serializer, serializedOperations);
		}

		if (!opLists.defaultDPWSOpList.isEmpty())
		{
			ListIterator listIt=opLists.defaultDPWSOpList.listIterator();
			serializeWSDLOperations(listIt, serializer, serializedOperations);
		}
	}

	protected void serializeWSDLOperations(ListIterator listIt, XmlSerializer serializer, LinkedList serializedOperations) throws IOException
	{
		if (listIt==null) return;
		while(listIt.hasNext())
		{
			WSDLOperation operation = (WSDLOperation) listIt.next();
			//SSch 2011-01-12 Due to jmeds beta4
			serialize(operation, serializer, serializedOperations);
		}
	}




	public void serializeStreamSources(MDPWSWSDLPortType portType, List stmsList, XmlSerializer serializer, boolean insideWSDL, HashMap schemaMap, ProtocolData pd) throws IllegalArgumentException, IllegalStateException, IOException {


		if (insideWSDL)
		{
			serializer.startTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMSOURCE);
			//Bugfix SSch 2011-11-29 wsp:optional should be in out tag streamsource
			serializer.attribute(WSPConstants.WSP_NAMESPACE_NAME, WSPConstants.WSP_ATTR_OPTIONAL, "true");
		}

		serializer.startTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMDESCRIPTIONS);

		serializer.attribute(null, WSSTMConstants.WSSTM_ATTRIB_TARGETNAMESPACE, portType.getName().toStringPlain());


		if (!insideWSDL)
		{			
			if (schemaMap!=null)
			{
				serializeStreamTypes(serializer, schemaMap,pd);
			}
		}

		ListIterator streamListIt=stmsList.listIterator();
		while(streamListIt.hasNext())
		{

			MDPWSWSDLStreamOperation operation = (MDPWSWSDLStreamOperation) streamListIt.next();
			if (Log.isDebug())
				Log.debug("Serialize Stm Operation: "+operation);

			operation.serialize(serializer, !insideWSDL);
		}
		serializer.endTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMDESCRIPTIONS);

		if (insideWSDL){
			serializer.endTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMSOURCE);
		}
	}

	private void serializeStreamTypes(XmlSerializer serializer, HashMap schemaMap, ProtocolData pd) throws IllegalArgumentException, IllegalStateException, IOException 
	{


		if (schemaMap!=null && !schemaMap.isEmpty())
		{
			serializer.startTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_TYPES);

			for(Iterator it=schemaMap.entrySet().iterator();it.hasNext();)
			{
				Entry entry=(Entry) it.next();
				//Ssch 2011-01-12 Due to beta 4
				((Schema) entry.getValue()).serialize(serializer);
				//				Schema.serialize(serializer,(Schema) entry.getValue());
			}
			serializer.endTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_TYPES);
		}
	}
}
