/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package com.draeger.medical.mdpws.domainmodel.wsdl.policy.impl;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.SimpleWSDLPolicy;

/**
 *
 *
 */
public class DefaultSimpleWSDLPolicy implements SimpleWSDLPolicy {
	private Object assertion=null;
	private boolean isRequired=true;
	private String policyId="";
	
	public DefaultSimpleWSDLPolicy(Object assertion) {
		super();
		this.assertion = assertion;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicy#getId()
	 */
	@Override
	public String getId() {
		return this.policyId;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicy#setId(java.lang.String)
	 */
	@Override
	public void setId(String id) {
		this.policyId=id;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicy#isRequired()
	 */
	@Override
	public boolean isRequired() {
		return this.isRequired;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicy#setRequired(boolean)
	 */
	@Override
	public void setRequired(boolean required) {
		this.isRequired=required;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.SimpleWSDLPolicy#setWrappedObject(java.lang.Object)
	 */
	@Override
	public void setWrappedObject(Object o) {
		this.assertion=o;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.SimpleWSDLPolicy#getAssertion()
	 */
	@Override
	public Object getAssertion() {
		return this.assertion;
	}

}
