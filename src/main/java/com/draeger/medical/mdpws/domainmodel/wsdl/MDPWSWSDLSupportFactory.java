/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel.wsdl;

import org.ws4d.java.util.Log;
import org.ws4d.java.wsdl.WSDLParser;
import org.ws4d.java.wsdl.WSDLSerializer;
import org.ws4d.java.wsdl.WSDLSupportFactory;


public class MDPWSWSDLSupportFactory implements WSDLSupportFactory {

	public MDPWSWSDLSupportFactory() {
		super();
		if (Log.isDebug())
			Log.debug("Using "+this.getClass().getName());
	}

	@Override
	public WSDLParser newParser() {
		return new MDPWSWSDLParser();
	}

	@Override
	public WSDLSerializer newSerializer() {
		return new MDPWSWSDLSerializer();
	}

}
