/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package com.draeger.medical.mdpws.domainmodel.wsdl.policy;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.impl.DefaultWSDLPolicyAttachmentParser;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.impl.DefaultWSDLPolicyAttachmentSerializer;

/**
 *
 *
 */
public class WSDLPolicyAttachmentSupportFactory {

	private static final WSDLPolicyAttachmentSupportFactory instance=new WSDLPolicyAttachmentSupportFactory();

	public static WSDLPolicyAttachmentSupportFactory getInstance() {
		return instance;
	}
	
	private WSDLPolicyAttachmentSupportFactory()
	{
		//void
	}
	
	public WSDLPolicyAttachmentSerializer getSerializer()
	{
		return new DefaultWSDLPolicyAttachmentSerializer();
	}
	
	public WSDLPolicyAttachmentParser getParser()
	{
		return new DefaultWSDLPolicyAttachmentParser();
	}
}
