/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package com.draeger.medical.mdpws.domainmodel.wsdl.policy;

import java.io.IOException;

import org.ws4d.java.io.xml.ElementParser;

/**
 *
 *
 */
public interface WSDLPolicyAttachmentParser {

	
	public abstract void parsePolicyTag(WSDLPolicyAttachmentPoint attachmentPoint, ElementParser parser) throws IOException;

	public abstract WSDLPolicyAttachment createPolicyAttachmentFromElement(WSDLPolicyAttachmentPoint attachmentPoint,
			ElementParser parser) throws IOException;

	public abstract WSDLPolicyAttachment createPolicyAttachmentFromReference(WSDLPolicyAttachmentPoint attachmentPoint,
			ElementParser parser) throws IOException;

}
