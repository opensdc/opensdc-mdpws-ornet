/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel.wsdl;

import org.ws4d.java.structures.HashMap;

public class StreamConfigurationSupportRegistry 
{
	private static final StreamConfigurationSupportRegistry instance=new StreamConfigurationSupportRegistry();
	
	private StreamConfigurationSupportRegistry()
	{
		//void
	}

	public static StreamConfigurationSupportRegistry getInstance() {
		return instance;
	}
	
	private final HashMap handlers=new HashMap();
	
	public StreamConfigurationSupport getSupportHandler(String streamBindingId){
		return (StreamConfigurationSupport) handlers.get(streamBindingId);
	}
	
	public void addSupportHandler(String streamBindingId, StreamConfigurationSupport handler){
		handlers.put(streamBindingId, handler);
	}
	
	public void removeSupportHandler(String streamBindingId){
		handlers.remove(streamBindingId);
	}

}
