/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel;

import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.List;
import org.ws4d.java.types.QName;
import org.ws4d.java.wsdl.WSDLOperation;

import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLOperation;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;
import com.draeger.medical.mdpws.qos.QoSMessageContext;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyToken;

public abstract class MDPWSOperation extends Operation implements MDPWSOperationDescription{

	
	private final ArrayList qosPolicies=new ArrayList();

	
	public MDPWSOperation(String name, QName portType) {
		super(name, portType);
	}

	protected MDPWSOperation(WSDLOperation operation) {
		super(operation);
	}

	public IParameterValue invoke(IParameterValue parameterValue, MDPWSMessageContextMap msgContextMap, MDPWSMessageContextMap replyMessageCtxtMap) throws InvocationException, TimeoutException
	{		
		
		return invoke(parameterValue);
	}

	public MDPWSWSDLOperation createWSDLOperation() 
	{
		MDPWSWSDLOperation wsdlOP=new MDPWSWSDLOperation(this.getName());
		wsdlOP.setAssociatedOperationDescription(this);
		return wsdlOP;
	}

	@Override
	public boolean addQoSPolicy(QoSPolicy policy) {
		if (policy!=null)
		{
			return qosPolicies.add(policy);
		}
		return false;
	}
	
	/**
	 * @param invokeRequest
	 * @param operation
	 * @param protocolData
	 * @return
	 */
	protected List getQoSTokenList(MDPWSMessageContextMap msgContextMap) {
		ArrayList relevantTokenList=new ArrayList();

		if (msgContextMap!=null)
		{
			QoSMessageContext qosMsgContext= (QoSMessageContext) msgContextMap.get(QoSMessageContext.class);
			Iterator policyToken=qosMsgContext.getQoSPolicyToken();
			while(policyToken.hasNext())
			{
				QoSPolicyToken<?, ?> token=(QoSPolicyToken<?, ?>)policyToken.next();
				relevantTokenList.add(token);
			}
		}

		return relevantTokenList;
	}
}
