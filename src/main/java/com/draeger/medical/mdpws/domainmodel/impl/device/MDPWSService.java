/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel.impl.device;

import java.io.IOException;

import org.ws4d.java.communication.CommunicationBinding;
import org.ws4d.java.communication.CommunicationManager;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.constants.MEXConstants;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.MessageException;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.message.MessageHeaderBuilder;
import org.ws4d.java.message.metadata.GetMetadataMessage;
import org.ws4d.java.message.metadata.GetMetadataResponseMessage;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.Schema;
import org.ws4d.java.schema.SchemaBuilder;
import org.ws4d.java.service.DefaultEventSource;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.service.Fault;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.parameter.ITypedParameterValue;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.HashMap.Entry;
import org.ws4d.java.structures.HashSet;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.List;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.structures.Set;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.WS4DIllegalStateException;
import org.ws4d.java.wsdl.IOType;
import org.ws4d.java.wsdl.WSDL;
import org.ws4d.java.wsdl.WSDLBinding;
import org.ws4d.java.wsdl.WSDLMessage;
import org.ws4d.java.wsdl.WSDLMessagePart;
import org.ws4d.java.wsdl.WSDLOperation;
import org.ws4d.java.wsdl.WSDLPortType;
import org.ws4d.java.wsdl.soap12.SOAP12DocumentLiteralHTTPBindingBuilder;

import com.draeger.medical.mdpws.communication.protocol.soap.wsdl.MDPWSSOAP12DocumentLiteralHTTPBindingBuilder;
import com.draeger.medical.mdpws.domainmodel.MDPWSOperation;
import com.draeger.medical.mdpws.domainmodel.MDPWSPortType;
import com.draeger.medical.mdpws.domainmodel.StreamSourceCommons;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentAbstractionState;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentBuilder;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyDirection;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyEmbeddedAttachment;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSIOType;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLOperation;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLPortType;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.MDPWSWSDLStreamOperation;
import com.draeger.medical.mdpws.domainmodel.wsdl.types.STMIOType;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamConfiguration;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;
import com.draeger.medical.mdpws.message.invoke.MDPWSInvokeMessage;
import com.draeger.medical.mdpws.message.metadata.MDPWSGetMetadataResponseMessage;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptionDirection;
import com.draeger.medical.mdpws.qos.management.QoSPolicyManager;
import com.draeger.medical.mdpws.qos.subjects.EndpointPolicySubject;
import com.draeger.medical.mdpws.qos.subjects.MessagePolicySubject;
import com.draeger.medical.mdpws.qos.subjects.OperationPolicySubject;
import com.draeger.medical.mdpws.qos.subjects.QoSPolicySubject;
import com.draeger.medical.mdpws.qos.subjects.ServicePolicySubject;

public class MDPWSService extends DefaultService {

	protected final HashMap	streams		= new HashMap();

	public MDPWSService(int i) {
		super(i);
		this.incomingListener=new MDPWSServiceMessageListener();
	}

	public StreamSourceCommons getStreamSource(String outputAction) {
		return (StreamSourceCommons) streams.get(outputAction);
	}

	public Iterator getStreamSources()
	{
		sharedLock();
		try {
			Set operations = new HashSet();
			for (Iterator it = getPortTypesInternal().values().iterator(); it.hasNext();) {
				PortType type = (PortType) it.next();
				if (type instanceof MDPWSPortType)
				{
					for (Iterator it2 = ((MDPWSPortType)type).getStreamSources(); it2.hasNext();) {
						operations.add(it2.next());
					}
				}
			}
			return new ReadOnlyIterator(operations.iterator());
		} finally {
			releaseSharedLock();
		}
	}



	@Override
	public void addOperation(Operation operation) {
		if (operation instanceof MDPWSOperation)
		{
			addMDPWSOperation((MDPWSOperation)operation);
		}else{
			super.addOperation(operation);
		}
	}


	/**
	 * Create a MDPWS Port type. Used in {@link #addMDPWSOperation(MDPWSOperation)}.
	 * @param portType
	 */
	protected void createMDPWSPortType(QName portType) {
		exclusiveLock();
		try{
			PortType ptype = (PortType) getPortTypesInternal().get(portType);
			if (!(ptype instanceof MDPWSPortType)){
				if (ptype == null) {
					ptype = new MDPWSPortType();
				} else{
					MDPWSPortType mptype=new MDPWSPortType();
					mptype.convert(ptype);
					ptype=mptype;
					//				throw new WS4DIllegalStateException("Porttype not of Type MDPWSPortType "+ptype);
				}
				getPortTypesInternal().put(portType, ptype);
			}}finally{
				releaseExclusiveLock();
			}
	}


	protected void addMDPWSOperation(MDPWSOperation operation) 
	{
		// Check for necessary stuff.
		if (operation == null) {
			throw new NullPointerException("operation is null");
		}
		exclusiveLock();
		try {
			QName portType = operation.getPortType();
			OperationSignature signature = new OperationSignature(operation);

			//MDPWS Special to MDPWS
			createMDPWSPortType(portType);

			// Add operation to port type table.
			PortType ptype = (PortType) getPortTypesInternal().get(portType);
			MDPWSPortType type=(MDPWSPortType) ptype;

			if (type.isPlombed()) {
				throw new WS4DIllegalStateException("Operations can not be added to an existing port type after a service has been started once");
			}

			String inputName = operation.getInputName();
			String outputName = operation.getOutputName();
			int inputCounter = 1;
			int outputCounter = 1;
			while (type.contains(signature)) {
				if (operation.isInputNameSet()) {
					if (operation.isOneWay() || operation.isOutputNameSet()) {
						throw new IllegalArgumentException("duplicate operation or event: " + operation);
					} else {
						operation.setOutputNameInternal(outputName + outputCounter++);
					}
				} else {
					operation.setInputNameInternal(inputName + inputCounter++);
				}

				signature = new OperationSignature(operation);
			}

			// check for duplicate input action
			String inputAction = operation.getInputAction();
			if (getOperationsInternal().containsKey(inputAction)) {
				if (operation.isInputActionSet() || operation.isOneWay()) {
					throw new IllegalArgumentException("duplicate input action: " + inputAction);
				}
				inputAction = operation.setExtendedDefaultInputAction();
				if (getOperationsInternal().containsKey(inputAction)) {
					throw new IllegalArgumentException("duplicate input action: " + inputAction);
				}
			}
			type.addOperation(signature, operation);
			// add operation with wsa:Action of input for faster access
			getOperationsInternal().put(operation.getInputAction(), operation);
			operation.setService(this);

			if (Log.isDebug()) {
				Log.debug("[NEW OPERATION]: " + operation.toString());
			}
		} finally {
			releaseExclusiveLock();
		}
	}

	public void addStreamSource(DefaultStreamSource streamSource) {
		if (streamSource == null) {
			throw new NullPointerException("StreamSource is null");
		}
		exclusiveLock();
		try {
			QName portType = streamSource.getPortType();
			OperationSignature signature = new OperationSignature(streamSource);

			// add stream to port type table
			PortType ptype = (PortType) getPortTypesInternal().get(portType);

			if (!(ptype instanceof MDPWSPortType)){
				if (ptype == null) {
					ptype = new MDPWSPortType();
					getPortTypesInternal().put(portType, ptype);
				} else{
					throw new WS4DIllegalStateException("Porttype not of Type MDPWSPortType");
				}
			}
			MDPWSPortType type=(MDPWSPortType) ptype;

			if (type.isPlombed()) {
				throw new WS4DIllegalStateException("Streamsources can not be added to an existing port type after a service has been started once");
			}
			String outputName = streamSource.getOutputName();
			String inputName = streamSource.getInputName();
			int outputCounter = 1;
			int inputCounter = 1;
			while (type.contains(signature)) {
				if (streamSource.isOutputNameSet()) {
					if (streamSource.isNotification() || streamSource.isInputNameSet()) {
						throw new IllegalArgumentException("duplicate streamsource: " + streamSource);
					} else {
						streamSource.setInputNameInternal(inputName + inputCounter++);
					}
				} else {
					streamSource.setOutputNameInternal(outputName + outputCounter++);
				}

				signature = new OperationSignature(streamSource);
			}
			// check for duplicate output action
			String outputAction = streamSource.getOutputAction();
			if (streams.containsKey(outputAction)) {
				if (streamSource.isOutputActionSet() || streamSource.isNotification()) {
					throw new IllegalArgumentException("duplicate output action: " + outputAction);
				}
				outputAction = streamSource.setExtendedDefaultOutputAction();
				if (streams.containsKey(outputAction)) {
					throw new IllegalArgumentException("duplicate output action: " + outputAction);
				}
			}
			type.addStreamSource(signature, streamSource);
			// add event with wsa:Action of output for faster access
			streams.put(streamSource.getOutputAction(), streamSource);


			streamSource.setService(this);

			if (Log.isDebug()) {
				Log.debug("[NEW STREAM SOURCE]: " + streamSource.toString());
			}

		} finally {
			releaseExclusiveLock();
		}
	}

	@Override
	public WSDL getDescription(String targetNamespace) {

		WSDL wsdl = (WSDL) wsdls.get(targetNamespace);
		if (wsdl != null) {
			return wsdl;
		}
		/*
		 * we have a WSDL instance for each distinct namespace within our
		 * service types
		 */
		wsdl = new WSDL(targetNamespace);
		//CHANGED 2010-08-11 SSch There may be a set of schemas not only one
		HashMap schemaList=SchemaBuilder.createSchema(this, targetNamespace);

		//SSch For MPDWS we need add the schema elements for streams
		SchemaBuilder.addToSchemaMap(this.getStreamSources(),schemaList);
		Iterator schemasIt=schemaList.entrySet().iterator();
		while(schemasIt.hasNext())
		{
			Entry entry=(Entry) schemasIt.next();
			wsdl.addTypes((Schema) entry.getValue());
		}

		/*
		 * Time to create the WSDL document for this service. No change allowed
		 * if the service is running.
		 */
		List firstLevelPolicyAttachmentPoints=new ArrayList();  

		Set ptypes = getPortTypesInternal().entrySet();
		Iterator ptit = ptypes.iterator();
		while (ptit.hasNext()) {
			Entry entry = (Entry) ptit.next();
			QName portTypeName = (QName) entry.getKey();
			String namespace = portTypeName.getNamespace();
			if (!targetNamespace.equals(namespace)) {
				// skip port types from other target namespaces
				Log.info("Skipping porttype "+namespace);
				continue;
			}

			PortType type = (PortType) entry.getValue();
			WSDLPortType portType=createWSDLPortType(type, portTypeName);
			addWSDLPortType(firstLevelPolicyAttachmentPoints,wsdl,namespace, portType, type);
			addBindingForPortType(firstLevelPolicyAttachmentPoints, wsdl,namespace, portType);
		}

		if (!firstLevelPolicyAttachmentPoints.isEmpty())
		{

			attachPoliciesToWSDLAttachmentPoints(firstLevelPolicyAttachmentPoints);
		}

		wsdls.put(targetNamespace, wsdl);


		return wsdl;
	}

	/**
	 * @param policyAttachmentPoints 
	 * 
	 */
	private void attachPoliciesToWSDLAttachmentPoints(List policyAttachmentPoints) {
		for (Iterator attachmentPoints=policyAttachmentPoints.iterator(); attachmentPoints.hasNext();)
		{
			Object wsdlObject= attachmentPoints.next();

			if (wsdlObject instanceof WSDLBinding)
			{
				boolean isAbstract=false;
				WSDLBinding binding=(WSDLBinding) wsdlObject;
				attachPoliciesToWSDLObject(binding, isAbstract,new BindingMatcher(binding));

				for (Iterator it = binding.getOperations().iterator(); it.hasNext();) {
					WSDLOperation operation = (WSDLOperation) it.next();
					attachPoliciesToWSDLObject(operation, isAbstract,new OperationMatcher(operation));

					IOType input= operation.getInput();
					attachPoliciesToWSDLObject(input, isAbstract,new MessageMatcher(input, true, false, false));

					IOType output= operation.getOutput();
					attachPoliciesToWSDLObject(output, isAbstract, new MessageMatcher(output, false,true,false));

					DataStructure faults= operation.getFaults();
					if (faults!=null)
					{
						for(Iterator faultsIt= faults.iterator();faultsIt.hasNext();)
						{
							IOType faultIOType=(IOType) faultsIt.next();
							attachPoliciesToWSDLObject(faultIOType, isAbstract, new MessageMatcher(faultIOType, false,false,true));
						}
					}

				}

			}else if (wsdlObject instanceof WSDLPortType)
			{
				boolean isAbstract=true;
				WSDLPortType portType=(WSDLPortType) wsdlObject;
				attachPoliciesToWSDLObject(portType, isAbstract, new PortTypeMatcher(portType));

				for (Iterator it = portType.getOperations().iterator(); it.hasNext();) {
					WSDLOperation operation = (WSDLOperation) it.next();
					attachPoliciesToWSDLObject(operation, isAbstract, new OperationMatcher(operation));

					IOType input= operation.getInput();
					attachPoliciesToWSDLObject(input, isAbstract, new MessageMatcher(input, true, false, false));

					IOType output= operation.getOutput();
					attachPoliciesToWSDLObject(output, isAbstract, new MessageMatcher(output, false, true, false));

					DataStructure faults= operation.getFaults();
					if (faults!=null)
					{
						for(Iterator faultsIt= faults.iterator();faultsIt.hasNext();)
						{
							IOType faultIOType=(IOType) faultsIt.next();
							attachPoliciesToWSDLObject(faultIOType, isAbstract, new MessageMatcher(faultIOType, false, false, true));
						}
					}
				}

			}else {
				//TODO SSch Check if there is anything else
				Log.info("This type of policy attachment point is not handled: "+wsdlObject);
			}
		}
		//		for (Iterator attachmentPoints=policyAttachmentPoints.iterator(); attachmentPoints.hasNext();)
		//		{
		//			WSDLPolicyAttachmentPoint attachmentPoint=(WSDLPolicyAttachmentPoint) attachmentPoints.next();
		//
		//			if (attachmentPoint instanceof WSDLBinding)
		//			{
		//				ArrayList qosPolicies=new ArrayList();
		//				collectPoliciesForSubjectClass(qosPolicies, ServicePolicySubject.class);
		//				collectPoliciesForSubjectClass(qosPolicies, EndpointPolicySubject.class);
		//
		//				for (Iterator policies=qosPolicies.iterator();policies.hasNext();)
		//				{
		//					QoSPolicy policy=(QoSPolicy) policies.next();
		//
		//					//No abstract policies allowed at this place
		//					if (policy.isAbstractPolicy()) continue;
		//
		//					//TODO SSch implement Policy references
		//					WSDLPolicyEmbeddedAttachment policyAttachment=WSDLPolicyAttachmentBuilder.getInstance().buildEmbeddedAttachment(policy);
		//					attachmentPoint.addWSDLPolicyAttachment(policyAttachment);
		//				}
		//			}else if (attachmentPoint instanceof WSDLPortType)
		//			{
		//				ArrayList qosPolicies=new ArrayList();
		//				collectPoliciesForSubjectClass(qosPolicies, ServicePolicySubject.class);
		//				for (Iterator policies=qosPolicies.iterator();policies.hasNext();)
		//				{
		//					QoSPolicy policy=(QoSPolicy) policies.next();
		//
		//					//No abstract policies allowed at this place
		//					if (!policy.isAbstractPolicy()) continue;
		//
		//					//TODO SSch implement Policy references
		//					WSDLPolicyEmbeddedAttachment policyAttachment=WSDLPolicyAttachmentBuilder.getInstance().buildEmbeddedAttachment(policy);
		//					attachmentPoint.addWSDLPolicyAttachment(policyAttachment);
		//				}
		//			}else {
		//				//TODO SSch implement the rest
		//				Log.info("Not handled this type of policy attachment point");
		//			}
		//		}

	}

	private void attachPoliciesToWSDLObject(Object wsdlObject, boolean isAbstract, SubjectMatcher matcher){
		if (wsdlObject instanceof WSDLPolicyAttachmentPoint)
		{
			WSDLPolicyAttachmentPoint attachmentPoint=(WSDLPolicyAttachmentPoint)wsdlObject;

			ArrayList qosPolicies=new ArrayList();

			matcher.getMatchingPoliciesForSubject(qosPolicies);

			for (Iterator policies=qosPolicies.iterator();policies.hasNext();)
			{
				QoSPolicy policy=(QoSPolicy) policies.next();

				//No abstract policies allowed at this place
				if (isAbstract!=policy.isAbstractPolicy()) continue;

				if (!hasMatchingDirection(policy, attachmentPoint)) continue;

				//TODO SSch implement Policy references
				WSDLPolicyEmbeddedAttachment policyAttachment=WSDLPolicyAttachmentBuilder.getInstance().buildEmbeddedAttachment(policy, isAbstract?WSDLPolicyAttachmentAbstractionState.ABSTRACT:WSDLPolicyAttachmentAbstractionState.NOTABSTRACT);
				attachmentPoint.addWSDLPolicyAttachment(policyAttachment);
			}
		}
	}

	/**
	 * @param policy
	 * @param attachmentPoint
	 * @return
	 */
	private boolean hasMatchingDirection(QoSPolicy policy,
			WSDLPolicyAttachmentPoint attachmentPoint) 
	{
		boolean retVal=false;
		String interceptionDirection= policy.getInterceptionDirection().toString();
		String attachmentPointDirection=attachmentPoint.getPolicyDirection().toString();

		retVal= ((policy.getInterceptionDirection().equals(QoSPolicyInterceptionDirection.INOUTBOUND))
				|| (attachmentPoint.getPolicyDirection().equals(WSDLPolicyDirection.INOUTBOUND)) 
				|| (interceptionDirection.equals(attachmentPointDirection)));

		return retVal;
	}




	private WSDLPortType createWSDLPortType(PortType type, QName portTypeName)
	{
		WSDLPortType portType =null;
		//TODO Check if type instanceof MDPWSPortType
		if (type instanceof MDPWSPortType)
		{
			portType = new MDPWSWSDLPortType(portTypeName);
		}else{
			portType = new WSDLPortType(portTypeName);
		}
		return portType;
	}

	protected void addWSDLPortType(List policyAttachmentPoints, WSDL wsdl, String namespace, WSDLPortType portType, PortType type) {



		if (type instanceof MDPWSPortType && portType instanceof MDPWSWSDLPortType)
		{
			MDPWSPortType mdpwsType=(MDPWSPortType)type;
			Iterator stmIt= mdpwsType.getStreamSources();
			while (stmIt.hasNext())
			{
				DefaultStreamSource stmSource=(DefaultStreamSource)stmIt.next();
				addWSDLStreamSource(stmSource,wsdl, (MDPWSWSDLPortType)portType, namespace);
			}
		}

		addWSDLOperations(wsdl, namespace, portType, type);
		addWSDLEventSources(wsdl,namespace, portType, type);

		if (portType instanceof WSDLPolicyAttachmentPoint)
		{
			policyAttachmentPoints.add(portType);
		}

		wsdl.addPortType(portType);

	}

	protected void addWSDLStreamSource(DefaultStreamSource stmSource, WSDL wsdl,
			MDPWSWSDLPortType portType, String namespace) {
		/*
		 * Create a WSDL operation and add it to the actual port type.
		 */
		String stmSourceName = stmSource.getName();
		portType.setEventSource(false);


		MDPWSWSDLStreamOperation wsdlOperation = new MDPWSWSDLStreamOperation(stmSourceName, stmSource.getStreamConfiguration());
		wsdlOperation.setAssociatedOperationDescription(stmSource);
		//WSDLStreamOperation(stmSourceName,stmSource.getStreamPeriod(), stmSource.getStreamAddress(), stmSource.getHistoryEPR());

		/*
		 * Create the input/output message names.
		 */
		String outputName = stmSource.getOutputName();
		QName outMsgName = stmSource.getOutput().getName();
		IOType outputIO = new STMIOType(outMsgName);

		// check whether auto-generated or set
		if (stmSource.isOutputNameSet()) {
			outputIO.setName(outputName);
		}
		if (stmSource.isOutputActionSet() || stmSource.isOutputActionExtended()) {
			outputIO.setAction(stmSource.getOutputAction());
		}

		wsdlOperation.setOutput(outputIO);

		if (!stmSource.isNotification()) {
			Log.warn("Ignoring non-notification stream "+stmSourceName);
			return;
		}

		// add fault IOTypes and action URIs
		for (Iterator it = stmSource.getFaults(); it.hasNext();) {
			Fault fault = (Fault) it.next();

			String faultName = fault.getName();
			QName faultMsgName = QNameFactory.getInstance().getQName(stmSourceName + faultName + FAULT_MSG_POSTFIX, namespace);
			IOType faultIO = new MDPWSIOType(faultMsgName);
			// check whether auto-generated or set
			faultIO.setName(fault.getName());
			String action = fault.getAction();
			if (action != null) {
				faultIO.setAction(action);
			}
			WSDLMessage wsdlMessageFault = new WSDLMessage(faultMsgName);
			Element faultElement = fault.getElement();
			if (faultElement != null) {
				WSDLMessagePart part = new WSDLMessagePart();
				part.setElementName(faultElement.getName());
				wsdlMessageFault.addPart(part);
			}
			wsdl.addMessage(wsdlMessageFault);
			wsdlOperation.addFault(faultIO);
		}

		portType.addOperation(wsdlOperation);
	}

	protected void addWSDLEventSources(WSDL wsdl, String namespace,
			WSDLPortType portType, PortType type) {

		Iterator evit = type.getEventSources();
		while (evit.hasNext()) {
			/*
			 * Get the next event.
			 */
			DefaultEventSource event = (DefaultEventSource) evit.next();
			addWSDLEventSource(event, wsdl, portType, namespace);
		}


	}

	protected void addWSDLOperations(WSDL wsdl, String namespace, WSDLPortType portType, PortType type)
	{
		Iterator opit = type.getOperations();
		while (opit.hasNext()) {
			/*
			 * Get the next operation.
			 */
			Operation operation = (Operation) opit.next();
			addWSDLOperation(operation, wsdl, portType, namespace);
		}

	}

	protected WSDLBinding addBindingForPortType(List policyAttachmentPoints, WSDL wsdl, String namespace, WSDLPortType portType) {
		SOAP12DocumentLiteralHTTPBindingBuilder bindingBuilder =new SOAP12DocumentLiteralHTTPBindingBuilder();
		WSDLBinding binding=null;
		if (portType instanceof MDPWSWSDLPortType)
		{

			MDPWSWSDLPortType wsdlPt=(MDPWSWSDLPortType)portType;

			if (wsdlPt.hasDefaultDPWSOperations() || wsdlPt.hasMDPWSOperations() || wsdlPt.hasStreamOperations())
			{
				if (!wsdlPt.hasMDPWSOperations() && !wsdlPt.hasStreamOperations())
				{
					binding= addDocLitBinding(bindingBuilder,wsdl, namespace, portType);
				}else
				{
					bindingBuilder =new MDPWSSOAP12DocumentLiteralHTTPBindingBuilder();
					//TODO SSch Binding sollte im communicationsManager erzeugt werden, um Abhängigkeit zum konkreten Transport Binding zu reduzieren
					//wsdl.addBinding(new MDPWSSOAP12DocumentLiteralHTTPBinding(QNameFactory.getInstance().getQName(portType.getName().getLocalPart() + BINDING_POSTFIX, namespace), portType.getName()));
					binding= addDocLitBinding(bindingBuilder, wsdl, namespace, portType);
				}
			}else{
				//void
			}
		}else{
			binding= addDocLitBinding(bindingBuilder, wsdl, namespace, portType);	
		}


		if (binding instanceof WSDLPolicyAttachmentPoint)
		{
			policyAttachmentPoints.add(binding);
		}

		return binding;

	}

	protected WSDLBinding addDocLitBinding(SOAP12DocumentLiteralHTTPBindingBuilder builder, WSDL wsdl, String namespace, WSDLPortType portType)
	{
		//		wsdl.addBinding(new SOAP12DocumentLiteralHTTPBinding(QNameFactory.getInstance().getQName(portType.getName().getLocalPart() + BINDING_POSTFIX, namespace), portType.getName()));
		WSDLBinding binding= builder.buildBinding(QNameFactory.getInstance().getQName(portType.getName().getLocalPart() + BINDING_POSTFIX, namespace), portType.getName());
		wsdl.addBinding(binding);
		return binding;
	}

	protected void addWSDLEventSource(DefaultEventSource event, WSDL wsdl,
			WSDLPortType portType, String namespace) {
		/*
		 * Create a WSDL operation and add it to the actual port type.
		 */
		String eventName = event.getName();
		portType.setEventSource(true);
		MDPWSWSDLOperation wsdlOperation = new MDPWSWSDLOperation(eventName);
		wsdlOperation.setAssociatedOperationDescription(event);
		/*
		 * Create the input/output message names.
		 */
		String outputName = event.getOutputName();
		QName outMsgName = QNameFactory.getInstance().getQName(outputName + OUT_MSG_POSTFIX, namespace);
		IOType outputIO = new MDPWSIOType(outMsgName);
		// check whether auto-generated or set
		if (event.isOutputNameSet()) {
			outputIO.setName(outputName);
		}
		if (event.isOutputActionSet() || event.isOutputActionExtended()) {
			outputIO.setAction(event.getOutputAction());
		}
		WSDLMessage wsdlMessageOutput = new WSDLMessage(outMsgName);
		Element output = event.getOutput();
		if (output != null) {
			WSDLMessagePart part = new WSDLMessagePart();
			part.setElementName(output.getName());
			wsdlMessageOutput.addPart(part);
		}
		/*
		 * in case there are no output parameters, we add an empty
		 * message (with no parts) to WSDL operation
		 */
		wsdl.addMessage(wsdlMessageOutput);
		wsdlOperation.setOutput(outputIO);
		if (event.isSolicitResponse()) {
			String inputName = event.getInputName();
			QName inMsgName = QNameFactory.getInstance().getQName(inputName + IN_MSG_POSTFIX, namespace);
			IOType inputIO = new MDPWSIOType(inMsgName);
			// check whether auto-generated or set
			if (event.isInputNameSet()) {
				inputIO.setName(inputName);
			}
			if (event.isInputActionSet()) {
				inputIO.setAction(event.getInputAction());
			}
			WSDLMessage wsdlMessageInput = new WSDLMessage(inMsgName);
			Element input = event.getInput();
			// input is never null for solicit-response
			WSDLMessagePart part = new WSDLMessagePart();
			part.setElementName(input.getName());
			wsdlMessageInput.addPart(part);
			wsdl.addMessage(wsdlMessageInput);
			wsdlOperation.setInput(inputIO);
		}
		// add fault IOTypes and action URIs
		for (Iterator it = event.getFaults(); it.hasNext();) {
			Fault fault = (Fault) it.next();

			String faultName = fault.getName();
			QName faultMsgName = QNameFactory.getInstance().getQName(eventName + faultName + FAULT_MSG_POSTFIX, namespace);
			IOType faultIO = new MDPWSIOType(faultMsgName);
			// check whether auto-generated or set
			faultIO.setName(fault.getName());
			String action = fault.getAction();
			if (action != null) {
				faultIO.setAction(action);
			}
			WSDLMessage wsdlMessageFault = new WSDLMessage(faultMsgName);
			Element faultElement = fault.getElement();
			if (faultElement != null) {
				WSDLMessagePart part = new WSDLMessagePart();
				part.setElementName(faultElement.getName());
				wsdlMessageFault.addPart(part);
			}
			wsdl.addMessage(wsdlMessageFault);
			wsdlOperation.addFault(faultIO);
		}

		portType.addOperation(wsdlOperation);

	}

	protected void addWSDLOperation(Operation operation, WSDL wsdl, WSDLPortType portType, String namespace) {
		/*
		 * Create a WSDL operation and add it to the actual port type.
		 */
		String operationName = operation.getName();
		WSDLOperation wsdlOperation=null;
		if (operation instanceof MDPWSOperation)
		{
			MDPWSOperation op=(MDPWSOperation)operation;
			MDPWSWSDLOperation mdpwsOP=op.createWSDLOperation();
			mdpwsOP.addMessagesToWSDL(wsdl);
			wsdlOperation=mdpwsOP;
		}else{
			wsdlOperation= new WSDLOperation(operationName);
		}

		/*
		 * Create the input/output message names.
		 */
		String inputName = operation.getInputName();
		QName inMsgName = QNameFactory.getInstance().getQName(inputName + IN_MSG_POSTFIX, namespace);
		MDPWSIOType inputIO = new MDPWSIOType(inMsgName);
		inputIO.setPolicyDirection(WSDLPolicyDirection.INBOUND);
		// check whether auto-generated or set
		if (operation.isInputNameSet()) {
			inputIO.setName(inputName);
		}
		if (operation.isInputActionSet() || operation.isInputActionExtended()) {
			inputIO.setAction(operation.getInputAction());
		}
		WSDLMessage wsdlMessageInput = new WSDLMessage(inMsgName);
		Element input = operation.getInput();
		if (input != null) {
			WSDLMessagePart part = new WSDLMessagePart();
			part.setElementName(input.getName());
			wsdlMessageInput.addPart(part);
		}
		/*
		 * in case there are no input parameters, we add an empty
		 * message (with no parts) to WSDL operation
		 */
		wsdl.addMessage(wsdlMessageInput);
		wsdlOperation.setInput(inputIO);

		if (operation.isRequestResponse()) {
			String outputName = operation.getOutputName();
			QName outMsgName = QNameFactory.getInstance().getQName(outputName + OUT_MSG_POSTFIX, namespace);
			MDPWSIOType outputIO = new MDPWSIOType(outMsgName);
			outputIO.setPolicyDirection(WSDLPolicyDirection.OUTBOUND);

			// check whether auto-generated or set
			if (operation.isOutputNameSet()) {
				outputIO.setName(outputName);
			}
			if (operation.isOutputActionSet()) {
				outputIO.setAction(operation.getOutputAction());
			}
			WSDLMessage wsdlMessageOutput = new WSDLMessage(outMsgName);
			Element output = operation.getOutput();
			// output is never null for request-response
			WSDLMessagePart part = new WSDLMessagePart();
			part.setElementName(output.getName());
			wsdlMessageOutput.addPart(part);
			wsdl.addMessage(wsdlMessageOutput);
			wsdlOperation.setOutput(outputIO);
		}
		// add fault IOTypes and action URIs
		for (Iterator it = operation.getFaults(); it.hasNext();) {
			Fault fault = (Fault) it.next();

			String faultName = fault.getName();
			QName faultMsgName = QNameFactory.getInstance().getQName(operationName + faultName + FAULT_MSG_POSTFIX, namespace);
			MDPWSIOType faultIO = new MDPWSIOType(faultMsgName);
			faultIO.setPolicyDirection(WSDLPolicyDirection.OUTBOUND);
			// check whether auto-generated or set
			faultIO.setName(fault.getName());
			String action = fault.getAction();
			if (action != null) {
				faultIO.setAction(action);
			}
			WSDLMessage wsdlMessageFault = new WSDLMessage(faultMsgName);
			Element faultElement = fault.getElement();
			if (faultElement != null) {
				WSDLMessagePart part = new WSDLMessagePart();
				part.setElementName(faultElement.getName());
				wsdlMessageFault.addPart(part);
			}
			wsdl.addMessage(wsdlMessageFault);
			wsdlOperation.addFault(faultIO);
		}

		portType.addOperation(wsdlOperation);

	}

	@Override
	public synchronized void start() throws IOException {
		//initialize qos policies
		initializeQoSPolicies();

		//start the dpws service
		super.start();
		//open streams
		openStreamSources();
	}

	private void initializeQoSPolicies() 
	{
		QoSPolicyManager.getInstance().wireQoSPolicies(ServicePolicySubject.class, this);
		//TODO SSch wire Endpoint
		//TODO SSch wire Operations
		Iterator operationsToWire=getOperationsInternal().values().iterator();
		while (operationsToWire.hasNext())
		{
			Object o=operationsToWire.next();
			QoSPolicyManager.getInstance().wireQoSPolicies(OperationPolicySubject.class,o );
			QoSPolicyManager.getInstance().wireQoSPolicies(MessagePolicySubject.class, o);
		}

	}


	@Override
	public synchronized void stop() throws IOException {
		super.stop();
	}

	public void closeStreamSource()
	{
		//TODO 2011-01-12 SSch Implement ???
		//		sharedLock();
		//		try{
		//			Set ptypes = portTypes.entrySet();
		//			Iterator ptit = ptypes.iterator();
		//			while (ptit.hasNext()) {
		//				Entry entry = (Entry) ptit.next();
		//				PortType type = (PortType) entry.getValue();
		//				if (type instanceof MDPWSPortType)
		//				{
		//					MDPWSPortType mdpwsType=(MDPWSPortType)type;
		//					Iterator stmIt= mdpwsType.getStreamSources();
		//					while (stmIt.hasNext())
		//					{
		//						StreamSource stmSource=(StreamSource)stmIt.next();
		//						stmSource.close();
		//					}
		//				}
		//			}
		//		}finally{
		//			releaseSharedLock();
		//		}
	}

	public void openStreamSources() {
		//TODO 2011-01-12 SSch Implement ???
		sharedLock();
		try{
			Iterator ptit = getPortTypesInternal().values().iterator();
			while (ptit.hasNext()) 
			{
				PortType type = (PortType) ptit.next();
				if (type instanceof MDPWSPortType)
				{
					MDPWSPortType mdpwsType=(MDPWSPortType)type;
					Iterator stmIt= mdpwsType.getStreamSources();
					while (stmIt.hasNext())
					{
						DefaultStreamSource stmSource=(DefaultStreamSource)stmIt.next();
						StreamConfiguration config= stmSource.getStreamConfiguration();
						if (config!=null && config.getBinding()!=null)
						{

							CommunicationBinding binding =config.getBinding();
							CommunicationManager manager = CommunicationManagerRegistry.getManager(binding.getCommunicationManagerId());
							if (manager!=null)
								manager.registerService(null, binding, incomingListener);
							else{
								Log.warn("Could not open StreamSource because the communication Manager "+binding.getCommunicationManagerId()+" is not available.");
							}
						}
					}
				}
			}
		} catch (Exception e) {
			Log.error(e);
		}finally{
			releaseSharedLock();
		}
	}


	protected class MDPWSServiceMessageListener extends ServiceMessageListener {



		private final boolean resolveTypes=Boolean.parseBoolean(System.getProperty("MDPWSServiceMessageListener.resolveTypes", "false"));
		

		@Override
		public InvokeMessage handle(InvokeMessage invokeRequest,
				ProtocolData protocolData) throws MessageException {

			if (!isRunning()) {
				// send Fault wsa:ServiceUnavailable
				throw new MessageException(FaultMessage.createEndpointUnavailableFault(invokeRequest));
			}

			Operation operation = null;

			sharedLock();
			try {
				// Remote invocation
				String inputAction = invokeRequest.getAction().toString();

				if (Log.isDebug()) {
					Log.debug("<I> Receiving invocation input for " + inputAction);
				}

				operation = (Operation) getOperation(inputAction);

				if (operation == null) {
					throw new MessageException(FaultMessage.createActionNotSupportedFault(invokeRequest));
				}
			} finally {
				releaseSharedLock();
			}

			try {
				/*
				 * User Thread
				 */

				/*
				 * Resolve the types based on the input!
				 */
				IParameterValue reqVal = invokeRequest.getContent();
				if (resolveTypes)
				{
					if (reqVal instanceof ITypedParameterValue) {
						DataStructure wsdlCol = wsdls.values();
						if (wsdlCol != null) {
							Iterator wsdlIt = wsdlCol.iterator();
							while (wsdlIt.hasNext()) {
								WSDL wsdl = (WSDL) wsdlIt.next();
								Iterator schemaIt = wsdl.getTypes();
								while (schemaIt.hasNext()) {
									Schema schema = (Schema) schemaIt.next();

									((ITypedParameterValue)reqVal).resolveTypes(schema);
								}
							}
						}
					}
				}

				return callInvoke(invokeRequest, protocolData, operation, reqVal);
			} catch (InvocationException e) {
				// Log.printStackTrace(e);
				Log.warn("Exception during invocation: " + e.getMessage());
				// respond with fault to sender
				FaultMessage fault = new FaultMessage(e.getAction());
				fault.setResponseTo(invokeRequest);
				fault.setCode(e.getCode());
				fault.setSubcode(e.getSubcode());
				fault.setReason(e.getReason());
				fault.setDetail(e.getDetail());
				throw new MessageException(fault);
			} catch (TimeoutException e) {
				// this shouldn't ever occur locally
				Log.error(e);
				return null;
			}
		}



		/**
		 * @param invokeRequest
		 * @param protocolData
		 * @param operation
		 * @param reqVal
		 * @return
		 * @throws InvocationException
		 * @throws TimeoutException
		 */
		public InvokeMessage callInvoke(InvokeMessage invokeRequest,
				ProtocolData protocolData, Operation operation,
				IParameterValue reqVal) throws InvocationException,
				TimeoutException {
			IParameterValue retVal=null;
			InvokeMessage invokeResponse =null;

			if (operation instanceof MDPWSOperation)
			{
				MDPWSOperation mdpwsOp=(MDPWSOperation)operation;

				MDPWSMessageContextMap messageContextMap=getMessageContextMap(invokeRequest);

				MDPWSMessageContextMap outboundMessageCtxtMap=new MDPWSMessageContextMap();
				//hand over the request to the called method 
				retVal= mdpwsOp.invoke(reqVal,messageContextMap, outboundMessageCtxtMap);

				if (operation.isRequestResponse())
				{
					invokeResponse=new MDPWSInvokeMessage(operation.getOutputAction(), false);

					((MDPWSInvokeMessage)invokeResponse).addMessageContextMapContent(outboundMessageCtxtMap);
				}

			}else{ 
				//handling default jmeds operations
				retVal= operation.invoke(reqVal);

				if (operation.isRequestResponse())
				{
					invokeResponse=new InvokeMessage(operation.getOutputAction(), false);
				}
			}


			//preparing and returning the response
			if (invokeResponse!=null) {
				invokeResponse.getHeader().setMessageEndpoint(operation);
				/*
				 * Send response
				 */
				invokeResponse.setResponseTo(invokeRequest);
				invokeResponse.setContent(retVal);
				return invokeResponse;
			} else {
				// send HTTP response (202)
				return null;
			}
		}

		/**
		 * @param invokeRequest
		 * @return
		 */
		public MDPWSMessageContextMap getMessageContextMap(
				InvokeMessage invokeRequest) {
			MDPWSMessageContextMap msgContextMap=null;
			if (invokeRequest instanceof MDPWSInvokeMessage)
			{
				MDPWSInvokeMessage mdpwsInvokeRequest=(MDPWSInvokeMessage)invokeRequest;
				msgContextMap=mdpwsInvokeRequest.getMessageContextMap();
			}
			return msgContextMap;
		}


		@Override
		public GetMetadataResponseMessage handle(GetMetadataMessage getMetadata,
				ProtocolData protocolData) throws MessageException {


			GetMetadataResponseMessage responseMsg=super.handle(getMetadata, protocolData);

			MDPWSGetMetadataResponseMessage response = new MDPWSGetMetadataResponseMessage(responseMsg);

			URI metaDataIdentifierURU=getMetadata.getIdentifier();

			Iterator wsdlIT=getDescriptions();
			while(wsdlIT.hasNext())
			{
				WSDL wsdl=(WSDL) wsdlIT.next();
				HashMap schemaMap=new HashMap();
				SchemaBuilder.addToSchemaMap(getStreamSources(),schemaMap);
				if (metaDataIdentifierURU==null)
				{
					Iterator ptIt= wsdl.getPortTypes();
					while(ptIt.hasNext())
					{
						WSDLPortType portType=(WSDLPortType)ptIt.next();
						response=addPortTypeToResponse(portType, response, schemaMap, getMetadata);
					}
				}else{
					QName metaDataIdentifier=QNameFactory.getInstance().getQName(metaDataIdentifierURU.toString());
					WSDLPortType portType=wsdl.getPortType(metaDataIdentifier);
					response=addPortTypeToResponse(portType, response, schemaMap, getMetadata);
					break;
				}
			}
			if (response==null)
			{
				//TODO Choose correct FaultMessage
				throw new MessageException(FaultMessage.createEndpointUnavailableFault(getMetadata));
			}


			return response;
			//			}else{
			//				return super.handle(getMetadata, protocolData);
			//			}
		}



		private MDPWSGetMetadataResponseMessage addPortTypeToResponse(
				WSDLPortType portType, MDPWSGetMetadataResponseMessage response, HashMap schemaMap, GetMetadataMessage getMetadata) {
			if (portType instanceof MDPWSWSDLPortType)
			{
				if (response==null)
				{
					MessageHeader header=MessageHeaderBuilder.getInstance().createHeader(MEXConstants.WSX_ACTION_GETMETADATA_RESPONSE);
					if (getMetadata.getHeader()!=null)
						header=getMetadata.getHeader();
					response = new MDPWSGetMetadataResponseMessage(header);
				}

				response.setSchemaMap(schemaMap);
				response.addPortType(portType);
				if (Log.isDebug())
					Log.debug("Adding PortType to response "+response.getClass().getName());
			}
			return response;
		}

	}


	@Override
	protected void deployMetadataResources() {

		super.deployMetadataResources();
	}

	protected abstract class SubjectMatcher
	{
		protected Class<?>[] matchingClasses=null;

		SubjectMatcher(Class<?>[]  matchingClasses)
		{
			this.matchingClasses=matchingClasses;
		}

		protected void getMatchingPoliciesForSubject(List qosPolicies)
		{
			for (Class<?> subjectClz : matchingClasses) {
				collectPoliciesForSubjectClass(qosPolicies, subjectClz);
			}

		}

		private void collectPoliciesForSubjectClass(List qosPolicies, Class<?> subjectClz){
			Iterator subjectClzPolicies=QoSPolicyManager.getInstance().getQoSPoliciesForSubjectType(subjectClz);
			while (subjectClzPolicies.hasNext())
			{
				Object policiesO=subjectClzPolicies.next();
				if (!(policiesO instanceof QoSPolicy)) continue;

				if (isMatching((QoSPolicy)policiesO, subjectClz))
					qosPolicies.add(policiesO);
			}
		}

		/**
		 * @param subject
		 * @param policiesO
		 * @param subjectClz 
		 * @return
		 */
		protected boolean isMatching(QoSPolicy policy, Class<?> subjectClz){
			Iterator subjectsIt=policy.getSubjects(subjectClz);
			while (subjectsIt.hasNext())
			{
				Object sbjO=subjectsIt.next();
				if (!(sbjO instanceof QoSPolicySubject)) continue;
				QoSPolicySubject subject=(QoSPolicySubject)sbjO;

				return isValidSubject(subject);
			}
			return false;
		}

		/**
		 * @param subject
		 * @return
		 */
		protected boolean isValidSubject(QoSPolicySubject subject) {
			return (subject.isAssociatedWith(convertWSDLObject()));
		}

		/**
		 * @return
		 */
		protected abstract Object convertWSDLObject();
	}

	protected class OperationMatcher extends SubjectMatcher
	{

		private final WSDLOperation wsdlOperation;

		/**
		 * @param operation 
		 * @param matchingClasses
		 */
		OperationMatcher(WSDLOperation operation) {
			super(new Class[]{OperationPolicySubject.class});
			this.wsdlOperation=operation;
		}

		@Override
		protected Object convertWSDLObject() {

			if (wsdlOperation instanceof MDPWSWSDLOperation)
			{
				return ((MDPWSWSDLOperation)wsdlOperation).getAssociatedOperationDescription();
			}
			return null;
		}



	}

	protected class MessageMatcher extends SubjectMatcher
	{

		private final IOType ioType;
		private final boolean inbound;
		private final boolean outbound;
		private final boolean fault;

		/**
		 * @param ioType
		 * @param d 
		 * @param c 
		 * @param b 
		 */
		MessageMatcher(IOType ioType, boolean inbound, boolean outbound, boolean fault) {
			super( new Class[]{MessagePolicySubject.class});
			this.ioType=ioType;
			this.inbound=inbound;
			this.outbound=outbound;
			this.fault=fault;
		}

		@Override
		protected Object convertWSDLObject() 
		{
			WSDLOperation op= ioType.getOperation();

			if (op instanceof MDPWSWSDLOperation)
			{
				return ((MDPWSWSDLOperation)op).getAssociatedOperationDescription();
			}
			return null;
		}

		@Override
		protected boolean isValidSubject(QoSPolicySubject subject) {

			boolean isValid= false;

			if (subject instanceof MessagePolicySubject)
			{
				MessagePolicySubject mSubject=(MessagePolicySubject) subject;

				if (!((this.inbound &&mSubject.isInputMessageSubject()!=this.inbound) || (this.outbound && mSubject.isOutputMessageSubject()!=this.outbound) || (this.fault && mSubject.isFaultMessageSubject()!=this.fault)))
				{

					Object wsdlObj=null;
					WSDLOperation op= ioType.getOperation();

					if (op instanceof MDPWSWSDLOperation)
					{
						wsdlObj=((MDPWSWSDLOperation)op).getAssociatedOperationDescription();
					}

					isValid=subject.isAssociatedWith(wsdlObj);
				}
			}
			return isValid;
		}	
	}

	protected class BindingMatcher extends SubjectMatcher{

		/**
		 * @param matchingClasses
		 */
		BindingMatcher( WSDLBinding binding) {
			super(new Class[]{ServicePolicySubject.class,EndpointPolicySubject.class});
		}

		@Override
		protected Object convertWSDLObject() {
			return MDPWSService.this;
		}

	}

	protected class PortTypeMatcher extends SubjectMatcher{

		/**
		 * @param matchingClasses
		 */
		PortTypeMatcher( WSDLPortType portType) {
			super(new Class[]{ServicePolicySubject.class});
		}

		/* (non-Javadoc)
		 * @see com.draeger.medical.mdpws.domainmodel.impl.device.MDPWSService.SubjectMatcher#isMatching(com.draeger.medical.mdpws.qos.QoSPolicy)
		 */
		@Override
		protected boolean isMatching(QoSPolicy policy,Class<?> subjectClz) {
			Iterator subjectsIt=policy.getSubjects(subjectClz);
			while (subjectsIt.hasNext())
			{
				Object sbjO=subjectsIt.next();
				if (!(sbjO instanceof QoSPolicySubject)) continue;
				QoSPolicySubject subject=(QoSPolicySubject)sbjO;

				return (subject.isAssociatedWith(MDPWSService.this));
			}
			return false;
		}

		@Override
		protected Object convertWSDLObject() {
			return MDPWSService.this;
		}

	}



}
