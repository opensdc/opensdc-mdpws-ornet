/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel;

import org.ws4d.java.eventing.EventSource;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.ServiceCommons.OperationSignature;
import org.ws4d.java.service.ServiceCommons.PortType;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;

public class MDPWSPortType extends PortType{
	
	private final HashMap	streams		= new HashMap();
	

	boolean hasStreamSources() {
		return streams.size() != 0;
	}

	public Iterator getStreamSources() {
		return streams.values().iterator();
	}

	public StreamSourceCommons getStreamSource(String name, String inputName, String outputName) {
		return (StreamSourceCommons) streams.get(new OperationSignature(name, inputName, outputName));
	}

	public void addStreamSource(OperationSignature signature, StreamSourceCommons streamSource) {
		streams.put(signature, streamSource);
	}

	public synchronized void convert(PortType ptype) 
	{
		if (ptype!=null)
		{
			this.events.clear();
			this.operations.clear();
			
			Iterator iter=ptype.getOperations();
			
			while (iter.hasNext())
			{
				Operation op=(Operation) iter.next();
				addOperation(new OperationSignature(op.getName(), op.getInputName(), op.getOutputName()), op);
			}
			
			iter=ptype.getEventSources();
			while (iter.hasNext())
			{
				EventSource event=(EventSource) iter.next();
				addEventSource(new OperationSignature(event.getName(), event.getInputName(), event.getOutputName()), event);
			}

			if (ptype.isPlombed())
				this.plomb();

		}
	}


}
