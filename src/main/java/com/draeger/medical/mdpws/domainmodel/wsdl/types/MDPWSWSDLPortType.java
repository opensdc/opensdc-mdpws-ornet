/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel.wsdl.types;

import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.EmptyStructures;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.List;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.types.QName;
import org.ws4d.java.wsdl.WSDLOperation;
import org.ws4d.java.wsdl.WSDLPortType;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachment;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyDirection;

public class MDPWSWSDLPortType extends WSDLPortType implements WSDLPolicyAttachmentPoint{

	private final ArrayList policyAttachments=new ArrayList();
	
	public MDPWSWSDLPortType() {
		super();
	}

	public MDPWSWSDLPortType(QName name) {
		super(name);
	}

	//Moved SSch 2011-01-12 to MDPWSWSDLParser
//	static WSDLPortType parse(ElementParser parser, String targetNamespace) throws XmlPullParserException, IOException {
//		MDPWSWSDLPortType portType = new MDPWSWSDLPortType();
//		int portTypeDepth=parser.getDepth();
//		int attributeCount = parser.getAttributeCount();
//		for (int i = 0; i < attributeCount; i++) {
//			String attributeNamespace = parser.getAttributeNamespace(i);
//			String attributeName = parser.getAttributeName(i);
//			if ("".equals(attributeNamespace)) {
//				attributeNamespace = parser.getNamespace();
//			}
//			if (WSDLConstants.WSDL_NAMESPACE_NAME.equals(attributeNamespace)) {
//				if (WSDLConstants.WSDL_ATTRIB_NAME.equals(attributeName)) {
//					portType.setName(QNameFactory.getInstance().getQName(parser.getAttributeValue(i), targetNamespace));
//				}
//			} else if (WSEConstants.WSE_NAMESPACE_NAME.equals(attributeNamespace)) {
//				if (WSEConstants.WSE_ATTR_EVENTSOURCE.equals(attributeName)) {
//					portType.setEventSource(StringUtil.equalsIgnoreCase("true", parser.getAttributeValue(i)));
//				}
//			}
//		}
//		parser.next();
//		while(parser.getEventType()!=XmlPullParser.END_TAG)
//		{
//			//Handle WSDL:operation
//			if (XMLParserUtil.checkElementStart(parser, WSDLConstants.WSDL_NAMESPACE_NAME, WSDLConstants.WSDL_ELEM_OPERATION)) 
//			{
//				int opDepth=parser.getDepth();
//				portType.addOperation(WSDLOperation.parse(parser));
//				//We need to clean up
//				if ((parser.getEventType()==XmlPullParser.END_TAG && parser.getDepth()>=opDepth)|| parser.getDepth()>opDepth)
//				{
//						XMLParserUtil.ignoreEmbeddedTags(parser, opDepth);
//						parser.next();
//				}
//					
//			}
//			//Handle also Policy elements because a streamsource could be nested
//			else if (WSPConstants.WSP_NAMESPACE_NAME.equals(parser.getNamespace()) && WSPConstants.WSP_ELEM_POLICY.equals(parser.getName()))
//			{
//				handlePolicyElement(parser, portType);
//			}else{
//				XMLParserUtil.ignoreEmbeddedTags(parser);
//				parser.next();
//			}
//		}
//
//		return portType;
//	}
//
//
//	private static void handlePolicyElement(ElementParser parser,
//			MDPWSWSDLPortType portType) throws XmlPullParserException, IOException {
//		int pElemetDepth= parser.getDepth();
//		parser.nextTag();
//
//		while(parser.getEventType()==XmlPullParser.START_TAG){
//			if (XMLParserUtil.checkElementStart(parser, WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMSOURCE))
//			{
//				handleStreamSourceAssertion(parser, portType);
//			}else{
//				handleUnknownPolicyElement(parser, portType);
//			}
//		}		
//		XMLParserUtil.skipRemainingOnLevel(parser, pElemetDepth);
//	}
//
//	private static void handleUnknownPolicyElement(ElementParser parser,
//			MDPWSWSDLPortType portType) throws XmlPullParserException, IOException {
//		//Is it another policy assertion that we do not know???
//		XMLParserUtil.ignoreEmbeddedTags(parser);
//		parser.next();
//	}
//
//	public static void handleStreamSourceAssertion(ElementParser parser, MDPWSWSDLPortType portType) throws XmlPullParserException, IOException {
//		int ssourceDepth=parser.getDepth();
//
//		parser.next();
//		if (XMLParserUtil.checkElementStart(parser, WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMDESCRIPTIONS))
//		{
//			handleStreamDescriptions(parser, portType);
//		}
//
//		XMLParserUtil.skipRemainingOnLevel(parser, ssourceDepth);			
//	}
//
//	private static void handleStreamDescriptions(ElementParser parser,
//			MDPWSWSDLPortType portType) throws XmlPullParserException, IOException {
//		int sdDepth=parser.getDepth();
//
//		String stmDescTNS=XMLParserUtil.getAttributeValue(parser, WSSTMConstants.WSSTM_ATTRIB_TARGETNAMESPACE, WSSTMConstants.WSSTM_NAMESPACE_NAME);
//		QName portTypeName=QName.construct(stmDescTNS);
//		if (portType.getName()==null)
//			portType.setName(portTypeName);
//		else if (!portType.getName().equals(portTypeName))
//		{
//			Log.error("Porttype QName ["+portType.getName()+"] != StreamDescriptions TNS ["+portTypeName+"]");
//		}
//
//		parser.next(); //next should be a <Types> or <STREAMType>
//
//		if (XMLParserUtil.checkElementStart(parser,WSSTMConstants.WSSTM_NAMESPACE_NAME,WSSTMConstants.WSSTM_ELEM_TYPES))
//		{
//			handleWSSTMTypesElement(parser, portType, stmDescTNS);
//		}
//		//Check if we find the streamtype elements
//		while (XMLParserUtil.checkElementStart(parser, WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMTYPE))
//		{
//			int streamTypeDepth=parser.getDepth();
//
//
//			String stmTypeName=XMLParserUtil.getAttributeValue(parser,WSSTMConstants.WSSTM_ATTRIB_NAME, WSSTMConstants.WSSTM_NAMESPACE_NAME );
//			String stmTypeElement=XMLParserUtil.getAttributeValue(parser,WSSTMConstants.WSSTM_ATTRIB_ELEMENT, WSSTMConstants.WSSTM_NAMESPACE_NAME );
//			String stmTypeActionURI=XMLParserUtil.getAttributeValue(parser,WSSTMConstants.WSSTM_ATTRIB_ACTIONURI, WSSTMConstants.WSSTM_NAMESPACE_NAME );
//
//			Log.debug("Stream: "+stmTypeName+" Element:"+stmTypeElement+" uri:"+stmTypeActionURI);
//
//			//Check if we have a streamAddress or Streamperiod
//			int type=parser.next();
//
//			URI streamAddr=null;
//			String streamPeriod=null;
//			String historyEPR=null;
//			// we found an element so check if it is an element we know /expect
//			//TODO Vielleicht unabhängig von der Reihenfolge machen
//			if (XMLParserUtil.checkElementStart(parser,WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMADDRESS))
//			{
//				parser.next();
//				String streamAddrStr=parser.getText();
//				if (streamAddrStr!=null)
//					streamAddr=new URI(streamAddrStr);
//				XMLParserUtil.skipRemainingOnLevel(parser, parser.getDepth());
//			}
//			if (XMLParserUtil.checkElementStart(parser, WSSTMConstants.WSSTM_NAMESPACE_NAME,WSSTMConstants.WSSTM_ELEM_STREAMPERIOD))
//			{
//				parser.next();
//				streamPeriod=parser.getText();
//
//				XMLParserUtil.skipRemainingOnLevel(parser, parser.getDepth());
//			}
//			if (XMLParserUtil.checkElementStart(parser, WSSTMConstants.WSSTM_NAMESPACE_NAME,WSSTMConstants.WSSTM_ELEM_HISTORYEPR))
//			{
//				parser.next();
//				historyEPR=parser.getText();
//				XMLParserUtil.skipRemainingOnLevel(parser, parser.getDepth());
//			}
//			
//			Log.debug(" addr:"+streamAddr+" period:"+streamPeriod+" hepr"+historyEPR);
//
//			//Create DPWS WSDL Object
//			QName ioOutQName=null;
//			if (stmTypeElement.contains(":"))
//			{
//				String p = SchemaUtil.getPrefix(stmTypeElement);
//				String n = SchemaUtil.getName(stmTypeElement);
//				String ns = parser.getNamespace(p);
//				ioOutQName=QNameFactory.getInstance().getQName(n, ns);
//			}else{
//				ioOutQName=QNameFactory.getInstance().getQName(stmTypeElement,stmDescTNS);
//			}
//			IOType outputIO = new STMIOType(ioOutQName);
//			outputIO.setAction(stmTypeActionURI);
//
//			WSDLStreamOperation stmOp=new WSDLStreamOperation(stmTypeName,streamPeriod,streamAddr, null);
//			stmOp.setOutput(outputIO);
//			portType.addOperation(stmOp);
//
//			//Skip the rest
//			XMLParserUtil.skipRemainingOnLevel(parser, streamTypeDepth);
//		}
//		XMLParserUtil.skipRemainingOnLevel(parser, sdDepth);
//
//	}
//
//
//	private static void handleWSSTMTypesElement(ElementParser parser,
//			MDPWSWSDLPortType portType, String stmDescTNS) throws XmlPullParserException, IOException {
//		int tDepth=parser.getDepth();
//
//		parser.next();
//		//Ok, we find a types elements
//		if (XMLParserUtil.checkElementStart(parser,Schema.XMLSCHEMA_NAMESPACE,Schema.TAG_SCHEMA)) 
//		{
//
//			handleSchema(parser, portType, stmDescTNS);
//		}
//		XMLParserUtil.skipRemainingOnLevel(parser, tDepth);
//	}
//
//
//
//	private static void handleSchema(ElementParser parser, MDPWSWSDLPortType portType, String stmDescTNS) throws XmlPullParserException, IOException {
//		int schemaDepth=parser.getDepth();
//
//		try {
//			String schemaTNS = parser.getAttributeValue(null, SchemaConstants.SCHEMA_TARGETNAMESPACE);
//			if (schemaTNS == null) {
//				/*
//				 * no explicit namespace set? use the targetNamespace from the
//				 * streamDescriptions element?
//				 */
//				schemaTNS = stmDescTNS;
//			}
//			Schema schema = Schema.parse(parser,null , schemaTNS);
//			//TODO Do something with the schema
//
//			XMLParserUtil.skipRemainingOnLevel(parser, schemaDepth);
//
//		} catch (SchemaException e) {
//			Log.error(e.getMessage());
//			Log.printStackTrace(e);
//			throw new XmlPullParserException("Unable to parse schema import", parser, e);
//		}
//	}




	
	
	public boolean hasMessagesToSign()
	{
		//TODO Check
		return false;
	}
	
	public boolean hasDualChannelMessages()
	{
		//TODO Check
		return false;
	}
	
	public boolean hasMessageToEncrypt()
	{
		//TODO Check
		return false;
	}
	
	public boolean hasMDPWSOperations()
	{
		OperationLists opLists=getOperationLists();
		return !opLists.opMDPWSList.isEmpty();
	}
	
	public boolean hasDefaultDPWSOperations()
	{
		OperationLists opLists=getOperationLists();
		return !opLists.defaultDPWSOpList.isEmpty();
	}
	
	public boolean hasStreamOperations()
	{
			OperationLists opLists=getOperationLists();
			return !opLists.stmsList.isEmpty();
	}
	
	public OperationLists getOperationLists()
	{
		OperationLists retVal=new OperationLists();
		if (getOperations() != EmptyStructures.EMPTY_STRUCTURE) {
			Iterator operationsit = getOperations().iterator();
			getOperationLists(operationsit, retVal);
		}
		return retVal;
	}
	
	public void getOperationLists(Iterator operationsit, OperationLists opList)
	{
		
		while (operationsit.hasNext()) {
			WSDLOperation operation = (WSDLOperation) operationsit.next();
			if (operation instanceof MDPWSWSDLStreamOperation)
			{
				opList.stmsList.add(operation);
			}else if (operation instanceof MDPWSWSDLOperation)
			{
				opList.opMDPWSList.add(operation);
			}else{
				opList.defaultDPWSOpList.add(operation);
			}
		}
	}
	
	
	//#### Serialize stuff ####
//	public void serialize(XmlSerializer serializer) throws IOException {
//		// Start-Tag wsdl:portTypes
//		serializer.startTag(WSDLConstants.WSDL_NAMESPACE_NAME, WSDLConstants.WSDL_ELEM_PORTTYPE);
//		serializer.attribute(null, WSDLConstants.WSDL_ATTRIB_NAME, getLocalName());
//		if (isEventSource()) {
//			serializer.attribute(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ATTR_EVENTSOURCE, "true");
//		}
//
//		if (getOperations() != EmptyStructures.EMPTY_STRUCTURE) {
//			Iterator operationsit = getOperations().iterator();
//			addWSDLOperations(operationsit, serializer);		
//		}
//		// End-Tag wsdl:portTypes
//		serializer.endTag(WSDLConstants.WSDL_NAMESPACE_NAME, WSDLConstants.WSDL_ELEM_PORTTYPE);
//
//	}
//
//	protected void addWSDLOperations(Iterator operationsit, XmlSerializer serializer) throws IOException {
//		OperationLists opLists=new OperationLists();
//		getOperationLists(operationsit, opLists);
//		
//		if (!opLists.stmsList.isEmpty())
//		{
//			serializer.startTag(WSPConstants.WSP_NAMESPACE_NAME, WSPConstants.WSP_ELEM_POLICY);
//			serializeStreamSourceList(opLists.stmsList, serializer, false, null);
//			serializer.endTag(WSPConstants.WSP_NAMESPACE_NAME, WSPConstants.WSP_ELEM_POLICY);
//		}
//
//		if (!opLists.defaultDPWSOpList.isEmpty())
//		{
//			ListIterator listIt=opLists.defaultDPWSOpList.listIterator();
//			serializeWSDLOperation(listIt, serializer);
//		}
//		
//		if (!opLists.defaultDPWSOpList.isEmpty())
//		{
//			ListIterator listIt=opLists.opMDPWSList.listIterator();
//			serializeWSDLOperation(listIt, serializer);
//		}
//	}
//	
//	protected void serializeWSDLOperation(ListIterator listIt, XmlSerializer serializer) throws IOException
//	{
//		if (listIt==null) return;
//		while(listIt.hasNext())
//		{
//			WSDLOperation operation = (WSDLOperation) listIt.next();
//			//SSch 2011-01-12 Due to beta4
//			operation.serialize(serializer);
//		}
//	}

//	public void serializeStreamSourceList(List stmsList, XmlSerializer serializer, boolean isNotInsideWSDL, HashMap schemaMap) throws IllegalArgumentException, IllegalStateException, IOException {
//		serializer.startTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMSOURCE);
//		serializer.startTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMDESCRIPTIONS);
//
//		serializer.attribute(null, WSSTMConstants.WSSTM_ATTRIB_TARGETNAMESPACE, getName().toStringPlain());
//
//		if (!isNotInsideWSDL)
//		{
//			serializer.attribute(WSPConstants.WSP_NAMESPACE_NAME, WSPConstants.WSP_ATTR_OPTIONAL, "true");
//		}
//
//		if (isNotInsideWSDL)
//		{			
//			for(Iterator it=schemaMap.entrySet().iterator();it.hasNext();)
//			{
//				Entry entry=(Entry) it.next();
//				String tns=((Schema) entry.getValue()).getTargetNamespace();
//				String prefix=serializer.getPrefix(tns, true);
//			}
//
//			serializeStreamTypes(serializer, schemaMap);
//		}
//
//		ListIterator streamListIt=stmsList.listIterator();
//		while(streamListIt.hasNext())
//		{
//			WSDLStreamOperation operation = (WSDLStreamOperation) streamListIt.next();
//			operation.serialize(serializer, isNotInsideWSDL);
//		}
//		serializer.endTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMDESCRIPTIONS);
//		serializer.endTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_STREAMSOURCE);
//	}
//
//	private void serializeStreamTypes(XmlSerializer serializer, HashMap schemaMap) throws IllegalArgumentException, IllegalStateException, IOException 
//	{
//
//
//		if (schemaMap!=null && !schemaMap.isEmpty())
//		{
//			serializer.startTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_TYPES);
//
//			for(Iterator it=schemaMap.entrySet().iterator();it.hasNext();)
//			{
//				Entry entry=(Entry) it.next();
//				//TODO Ssch 2011-01-12 Due to beta 4
//				Schema.serialize(serializer,(Schema) entry.getValue());
//			}
//			serializer.endTag(WSSTMConstants.WSSTM_NAMESPACE_NAME, WSSTMConstants.WSSTM_ELEM_TYPES);
//		}
//
//
//	}
	

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint#getWSDLPolicyAttachments()
	 */
	@Override
	public Iterator getWSDLPolicyAttachments() {
		return new ReadOnlyIterator(policyAttachments.iterator());
	}
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint#addWSDLPolicyAttachment(com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachment)
	 */
	@Override
	public boolean addWSDLPolicyAttachment(WSDLPolicyAttachment attachment) {
		if (attachment !=null && !policyAttachments.contains(attachment))
		{
			return policyAttachments.add(attachment);
		}
		return false;
	}
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint#removeWSDLPolicyAttachment(com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachment)
	 */
	@Override
	public boolean removeWSDLPolicyAttachment(WSDLPolicyAttachment attachment) {
		if (attachment !=null)
		{
			return policyAttachments.remove(attachment);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint#getPolicyDirection()
	 */
	@Override
	public WSDLPolicyDirection getPolicyDirection() {
		return WSDLPolicyDirection.INOUTBOUND;
	}
	

	public class OperationLists
	{
		public List stmsList=new ArrayList();
		public List defaultDPWSOpList=new ArrayList();
		public List opMDPWSList=new ArrayList();
	}

}
