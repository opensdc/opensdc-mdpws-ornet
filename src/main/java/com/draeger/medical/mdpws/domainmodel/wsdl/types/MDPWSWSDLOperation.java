/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel.wsdl.types;

import org.ws4d.java.service.OperationDescription;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.wsdl.IOType;
import org.ws4d.java.wsdl.WSDL;
import org.ws4d.java.wsdl.WSDLOperation;
import org.ws4d.java.wsdl.WSDLPortType;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachment;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyDirection;

public class MDPWSWSDLOperation extends WSDLOperation implements WSDLPolicyAttachmentPoint{
	private final ArrayList policyAttachments=new ArrayList();
	private OperationDescription associatedOperationDescription;


	public MDPWSWSDLOperation(String name) {
		super(name);
	}
	
	@Override
	public IOType getInput() {
		return super.getInput();
	}

	@Override
	public IOType getOutput() {
		return super.getOutput();
	}
	
	
	public void setAssociatedOperationDescription(OperationDescription mdpwsOP)
	{
		this.associatedOperationDescription=mdpwsOP;
	}
	
	public OperationDescription getAssociatedOperationDescription() {
		return associatedOperationDescription;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint#getWSDLPolicyAttachments()
	 */
	@Override
	public Iterator getWSDLPolicyAttachments() {
		return new ReadOnlyIterator(policyAttachments.iterator());
	}
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint#addWSDLPolicyAttachment(com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachment)
	 */
	@Override
	public boolean addWSDLPolicyAttachment(WSDLPolicyAttachment attachment) {
		if (attachment !=null && !policyAttachments.contains(attachment))
		{
			return policyAttachments.add(attachment);
		}
		return false;
	}
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint#removeWSDLPolicyAttachment(com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachment)
	 */
	@Override
	public boolean removeWSDLPolicyAttachment(WSDLPolicyAttachment attachment) {
		if (attachment !=null)
		{
			return policyAttachments.remove(attachment);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint#getPolicyDirection()
	 */
	@Override
	public WSDLPolicyDirection getPolicyDirection() {
		return WSDLPolicyDirection.INOUTBOUND;
	}
	

	/**
	 * @param wsdl
	 */
	public void addMessagesToWSDL(WSDL wsdl) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setPortType(WSDLPortType portType) {
		super.setPortType(portType);
	}

}
