/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package com.draeger.medical.mdpws.domainmodel.wsdl.policy.impl;

import java.io.IOException;

import org.ws4d.java.constants.WSDLConstants;
import org.ws4d.java.constants.WSPConstants;
import org.ws4d.java.constants.WSSecurityConstants;
import org.ws4d.java.util.Log;
import org.xmlpull.v1.XmlSerializer;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.SimpleWSDLPolicy;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLAssertionSerializer;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicy;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAssertionSupportRegistry;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicySerializer;

/**
 *
 *
 */
public class DefaultWSDLPolicySerializer implements WSDLPolicySerializer{

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicySerializer#serialize(com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicy, org.xmlpull.v1.XmlSerializer)
	 */
	@Override
	public void serialize(WSDLPolicy policy, XmlSerializer serializer) throws IOException 
	{
		if (policy!=null && serializer!=null)
		{	
			if (policy instanceof SimpleWSDLPolicy)
			{
				SimpleWSDLPolicy sPolicy=(SimpleWSDLPolicy)policy;
				Object assertion=sPolicy.getAssertion();
				if (assertion!=null){
					serializer.startTag(WSPConstants.WSP_NAMESPACE_NAME, WSPConstants.WSP_ELEM_POLICY);
					if (sPolicy.getId()!=null && sPolicy.getId().trim().length()>0)
						serializer.attribute(WSSecurityConstants.WS_SECURITY_WSU, WSSecurityConstants.ATTR_ID, sPolicy.getId());
					
					serializer.attribute(WSDLConstants.WSDL_NAMESPACE_NAME, WSDLConstants.WSDL_ATTRIB_REQUIRED, "true");
					
					WSDLAssertionSerializer assertionSerializer= WSDLPolicyAssertionSupportRegistry.getInstance().getSerializerForAssertion(assertion);
					if (assertionSerializer!=null)
						assertionSerializer.serialize(assertion, serializer);
					else{
						Log.warn("Could not serialize assertion ["+assertion+"] due to the lack of a serializer.");
					}
					
					serializer.endTag(WSPConstants.WSP_NAMESPACE_NAME, WSPConstants.WSP_ELEM_POLICY);
				}
			}else{
				Log.warn("Policy could not be serialized. Unknown Type of Policy! Policy="+policy);
			}
		}else{
			Log.warn("Policy could not be serialized: Policy="+policy+" serializer="+serializer);
		}
	}

}
