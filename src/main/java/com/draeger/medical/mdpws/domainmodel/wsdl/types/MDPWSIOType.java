/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.domainmodel.wsdl.types;

import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.types.QName;
import org.ws4d.java.wsdl.IOType;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachment;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyDirection;

public class MDPWSIOType extends IOType implements WSDLPolicyAttachmentPoint {

	private final ArrayList policyAttachments=new ArrayList();
	private WSDLPolicyDirection policyDirection=WSDLPolicyDirection.INOUTBOUND;
	
	public MDPWSIOType() {
		super();
	}

	public MDPWSIOType(QName message) {
		super(message);
	}

	public MDPWSIOType(String name, QName message) {
		super(name, message);
	}
	
	public MDPWSIOType(IOType type)
	{
		this(type.getName(), type.getMessageName());
		setAction(type.getAction());
		setOperation(type.getOperation());
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint#getWSDLPolicyAttachments()
	 */
	@Override
	public Iterator getWSDLPolicyAttachments() {
		return new ReadOnlyIterator(policyAttachments.iterator());
	}
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint#addWSDLPolicyAttachment(com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachment)
	 */
	@Override
	public boolean addWSDLPolicyAttachment(WSDLPolicyAttachment attachment) {
		if (attachment !=null && !policyAttachments.contains(attachment))
		{
			return policyAttachments.add(attachment);
		}
		return false;
	}
	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint#removeWSDLPolicyAttachment(com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachment)
	 */
	@Override
	public boolean removeWSDLPolicyAttachment(WSDLPolicyAttachment attachment) {
		if (attachment !=null)
		{
			return policyAttachments.remove(attachment);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.mdpws.domainmodel.wsdl.policy.WSDLPolicyAttachmentPoint#getPolicyDirection()
	 */
	@Override
	public WSDLPolicyDirection getPolicyDirection() {
		return policyDirection;
	}

	public void setPolicyDirection(WSDLPolicyDirection policyDirection) {
		this.policyDirection = policyDirection;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((policyAttachments == null) ? 0 : policyAttachments
						.hashCode());
		result = prime * result
				+ ((policyDirection == null) ? 0 : policyDirection.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MDPWSIOType other = (MDPWSIOType) obj;
		if (policyAttachments == null) {
			if (other.policyAttachments != null)
				return false;
		} else if (!policyAttachments.equals(other.policyAttachments))
			return false;
		if (policyDirection != other.policyDirection)
			return false;
		return true;
	}	
}
