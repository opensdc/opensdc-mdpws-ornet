/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package com.draeger.medical.mdpws.domainmodel.wsdl.policy;

import com.draeger.medical.mdpws.domainmodel.wsdl.policy.impl.DefaultSimpleWSDLPolicy;
import com.draeger.medical.mdpws.domainmodel.wsdl.policy.impl.DefaultWSDLPolicyEmbeddedAttachment;
import com.draeger.medical.mdpws.qos.QoSPolicy;

/**
 *
 *
 */
public class WSDLPolicyAttachmentBuilder {
	private static final WSDLPolicyAttachmentBuilder instance=new WSDLPolicyAttachmentBuilder();



	public static WSDLPolicyAttachmentBuilder getInstance() {
		return instance;
	}



	private WSDLPolicyAttachmentBuilder()
	{
		//void
	}
	
	public WSDLPolicyEmbeddedAttachment buildEmbeddedAttachment(QoSPolicy policy){
		return buildEmbeddedAttachment(policy, WSDLPolicyAttachmentAbstractionState.UNKNOWN);
	}
	
	public WSDLPolicyEmbeddedAttachment buildEmbeddedAttachment(QoSPolicy policy, WSDLPolicyAttachmentAbstractionState abstractionState){
		WSDLPolicyEmbeddedAttachment embeddedAttachment= new DefaultWSDLPolicyEmbeddedAttachment();
		embeddedAttachment.setAbstractState(abstractionState);
		SimpleWSDLPolicy wsdlPolicy=new  DefaultSimpleWSDLPolicy(policy);
		wsdlPolicy.setRequired(true);
		embeddedAttachment.setPolicy(wsdlPolicy);
		return embeddedAttachment;
		
	}
	
	public WSDLPolicyReferenceAttachment buildReferenceAttachment(){
		//TODO SSch implement WSDLPolicyReferenceAttachment
		return null;
	}
}
