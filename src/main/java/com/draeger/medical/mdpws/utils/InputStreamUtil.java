/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.ws4d.java.util.Log;

public class InputStreamUtil {

	public static String convertStreamToStringExtended(InputStream is, boolean close) throws IOException {
		/*
    	28.         * To convert the InputStream to String we use the BufferedReader.readLine()
    	29.         * method. We iterate until the BufferedReader return null which means
    	30.         * there's no more data to read. Each line will appended to a StringBuilder
    	31.         * and returned as String.
    	32.         */
		if (is != null) 
		{
			StringBuilder sb = new StringBuilder();
			String line;
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				while ((line = reader.readLine()) != null) {
					sb.append(line).append("\n");
				}
			} finally {
				if (close)
					is.close();
			}
			return sb.toString().trim();
		} else {       
			return "";
		}
	}

	public static InputStream debugInputStream(InputStream is) throws IOException{

		//		System.out.println("isAvailable "+is.available());
		int b;
		int cnt=0;
		int max=4;
		ByteArrayOutputStream baos=new ByteArrayOutputStream(max);
		StringBuffer sb=new StringBuffer();
		while((b=is.read())!=-1 && cnt<=max )
		{
			cnt++;
			baos.write(b);
			sb.append(b).append('[').append((char)b).append(']');
			b=-1;
		}
		if (b!=-1)
			baos.write(b);

		if (Log.isDebug())
			Log.debug("Stream: "+sb.toString());
		return new InputStreamJoiner(baos.toByteArray(), is);
	}

	public static String convertStreamToString(InputStream is) throws IOException {
		return convertStreamToStringExtended(is,true);
	}

	public static InputStream convertStringToInputStream(String s, String encoding) throws UnsupportedEncodingException
	{
		InputStream is = new ByteArrayInputStream(s.getBytes(encoding));
		return is;
	}

	public static ByteArrayInputStream convertToByteArrayInputStream(InputStream is, boolean close) throws IOException{
		ByteArrayInputStream retVal=null;
		if (is instanceof ByteArrayInputStream)
		{
			retVal=(ByteArrayInputStream) is;
		}else{
			int len;
			int size = 1024;
			byte[] buf;
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			buf = new byte[size];
			while ((len = is.read(buf, 0, size)) != -1)
				bos.write(buf, 0, len);
			
			bos.flush();
			
			if (close)
				is.close();
			
			retVal= new ByteArrayInputStream(bos.toByteArray());
			bos.close();
		}


		return retVal;
	}
}
