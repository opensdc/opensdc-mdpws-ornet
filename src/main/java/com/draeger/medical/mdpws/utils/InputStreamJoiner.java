/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.utils;

import java.io.IOException;
import java.io.InputStream;

import org.ws4d.java.util.Log;


/**
 * Joins a byte[] and an InputStream whereas the byte[] is the head and the InputStream is the tail.
*
 *
 */
public class InputStreamJoiner extends InputStream {

	private byte[] head;
	private InputStream is;
	private int cnt=0;

	public InputStreamJoiner(byte[] head, InputStream is)
	{
		this.head=head;
		if (Log.isDebug())
		{
			Log.debug("InputStreamJoiner: "+is);
		}
		this.is=is;
	}
	
	@Override
	public int read() throws IOException {
		if (head!=null && cnt<head.length)
			return head[cnt++];
		else
			return is.read();
	}

	@Override
	public int available() throws IOException 
	{
		return is.available();
	}

	@Override
	public void close() throws IOException 
	{
		is.close();
		this.head=null;
		this.is=null;
	}

	@Override
	public void mark(int arg0) {
		is.mark(arg0);
	}

	@Override
	public boolean markSupported() {
		if (head!=null && cnt<head.length)
			return false;
		return is.markSupported();
	}
	
	@Override
	public int read(byte[] b) throws IOException {
		return read(b,0,b.length);
	}

	@Override
	public synchronized int read(byte[] b, int off, int len) throws IOException {
		int no=0;
		if (head!=null && cnt<head.length)
		{
			for (int i=cnt;(i<head.length && no<len);i++)
			{
				b[no+off]=head[cnt];
				cnt++;
				no++;
			}
		}
			
		no+=is.read(b, off+no, len-no);
		
		return no;
	}



	@Override
	public void reset() throws IOException {
		is.reset();
	}

	@Override
	public long skip(long skip) throws IOException {

		long remaining=skip;
		if (head!=null && cnt<head.length)
		{
			cnt+=skip;
			remaining=cnt-(head.length-1);
		}
		return is.skip(remaining<0?0:remaining);
	}


}
