/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.utils;

import java.util.Observable;

public class InternalObservable extends Observable
{
	private final Object source;

	public InternalObservable(Object source) 
	{
		this.source=source;
	}
	
	public Object getSource() {
		return source;
	}

	public void setChangedExternal() {
		this.setChanged();
	}
}
