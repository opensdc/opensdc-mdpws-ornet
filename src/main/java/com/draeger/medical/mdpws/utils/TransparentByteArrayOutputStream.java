/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.mdpws.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public class TransparentByteArrayOutputStream extends ByteArrayOutputStream {
	final private TransparentByteArrayInputStream is=new TransparentByteArrayInputStream(new byte[1]); 

	public TransparentByteArrayOutputStream()
	{
		super(100000);
	}
	
	public TransparentByteArrayOutputStream(int size)
	{
		super(size);
	}

	public byte[] getInternalBytes(){
		return buf;
	}

	public int getCount()
	{
		return count;
	}

	public ByteArrayInputStream getByteArrayInputStream()
	{
		is.setBytes(getInternalBytes(), getCount());
		return is;
	}
	
	public static class TransparentByteArrayInputStream extends ByteArrayInputStream{
		TransparentByteArrayInputStream(byte[] buf) {
			super(buf);
		}

		public void setBytes(byte[] bytes, int count)
		{
			this.buf=bytes;
			this.count=count;
			this.mark = 0;
			this.pos = 0;
		}
	}
}
